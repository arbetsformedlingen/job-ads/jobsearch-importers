region_value_test_cases = [( {'type': 'region', 'id': 'G6DV_fKE_Viz', 'national_nuts_level_3_code_2019': '19', 'preferred_label': 'Västmanlands län'},
{'legacy_ams_taxonomy_id': '19', 'type': 'region', 'label': 'Västmanlands län', 'concept_id': 'G6DV_fKE_Viz', 'legacy_ams_taxonomy_num_id': 19}),

( {'type': 'region', 'id': '65Ms_7r1_RTG', 'national_nuts_level_3_code_2019': '23', 'preferred_label': 'Jämtlands län'},
{'legacy_ams_taxonomy_id': '23', 'type': 'region', 'label': 'Jämtlands län', 'concept_id': '65Ms_7r1_RTG', 'legacy_ams_taxonomy_num_id': 23}),

( {'type': 'region', 'id': 'CifL_Rzy_Mku', 'national_nuts_level_3_code_2019': '01', 'preferred_label': 'Stockholms län'},
{'legacy_ams_taxonomy_id': '01', 'type': 'region', 'label': 'Stockholms län', 'concept_id': 'CifL_Rzy_Mku', 'legacy_ams_taxonomy_num_id': 1}),

( {'type': 'region', 'id': 'g5Tt_CAV_zBd', 'national_nuts_level_3_code_2019': '24', 'preferred_label': 'Västerbottens län'},
{'legacy_ams_taxonomy_id': '24', 'type': 'region', 'label': 'Västerbottens län', 'concept_id': 'g5Tt_CAV_zBd', 'legacy_ams_taxonomy_num_id': 24}),

( {'type': 'region', 'id': 'wjee_qH2_yb6', 'national_nuts_level_3_code_2019': '13', 'preferred_label': 'Hallands län'},
{'legacy_ams_taxonomy_id': '13', 'type': 'region', 'label': 'Hallands län', 'concept_id': 'wjee_qH2_yb6', 'legacy_ams_taxonomy_num_id': 13}),

( {'type': 'region', 'id': 'DQZd_uYs_oKb', 'national_nuts_level_3_code_2019': '10', 'preferred_label': 'Blekinge län'},
{'legacy_ams_taxonomy_id': '10', 'type': 'region', 'label': 'Blekinge län', 'concept_id': 'DQZd_uYs_oKb', 'legacy_ams_taxonomy_num_id': 10}),

( {'type': 'region', 'id': 's93u_BEb_sx2', 'national_nuts_level_3_code_2019': '04', 'preferred_label': 'Södermanlands län'},
{'legacy_ams_taxonomy_id': '04', 'type': 'region', 'label': 'Södermanlands län', 'concept_id': 's93u_BEb_sx2', 'legacy_ams_taxonomy_num_id': 4}),

( {'type': 'region', 'id': 'zdoY_6u5_Krt', 'national_nuts_level_3_code_2019': '14', 'preferred_label': 'Västra Götalands län'},
{'legacy_ams_taxonomy_id': '14', 'type': 'region', 'label': 'Västra Götalands län', 'concept_id': 'zdoY_6u5_Krt', 'legacy_ams_taxonomy_num_id': 14}),

( {'type': 'region', 'id': 'CaRE_1nn_cSU', 'national_nuts_level_3_code_2019': '12', 'preferred_label': 'Skåne län'},
{'legacy_ams_taxonomy_id': '12', 'type': 'region', 'label': 'Skåne län', 'concept_id': 'CaRE_1nn_cSU', 'legacy_ams_taxonomy_num_id': 12}),

( {'type': 'region', 'id': 'EVVp_h6U_GSZ', 'national_nuts_level_3_code_2019': '17', 'preferred_label': 'Värmlands län'},
{'legacy_ams_taxonomy_id': '17', 'type': 'region', 'label': 'Värmlands län', 'concept_id': 'EVVp_h6U_GSZ', 'legacy_ams_taxonomy_num_id': 17}),

( {'type': 'region', 'id': 'NvUF_SP1_1zo', 'national_nuts_level_3_code_2019': '22', 'preferred_label': 'Västernorrlands län'},
{'legacy_ams_taxonomy_id': '22', 'type': 'region', 'label': 'Västernorrlands län', 'concept_id': 'NvUF_SP1_1zo', 'legacy_ams_taxonomy_num_id': 22}),

( {'type': 'region', 'id': '9QUH_2bb_6Np', 'national_nuts_level_3_code_2019': '08', 'preferred_label': 'Kalmar län'},
{'legacy_ams_taxonomy_id': '08', 'type': 'region', 'label': 'Kalmar län', 'concept_id': '9QUH_2bb_6Np', 'legacy_ams_taxonomy_num_id': 8}),

( {'type': 'region', 'id': 'zupA_8Nt_xcD', 'national_nuts_level_3_code_2019': '21', 'preferred_label': 'Gävleborgs län'},
{'legacy_ams_taxonomy_id': '21', 'type': 'region', 'label': 'Gävleborgs län', 'concept_id': 'zupA_8Nt_xcD', 'legacy_ams_taxonomy_num_id': 21}),

( {'type': 'region', 'id': 'tF3y_MF9_h5G', 'national_nuts_level_3_code_2019': '07', 'preferred_label': 'Kronobergs län'},
{'legacy_ams_taxonomy_id': '07', 'type': 'region', 'label': 'Kronobergs län', 'concept_id': 'tF3y_MF9_h5G', 'legacy_ams_taxonomy_num_id': 7}),

( {'type': 'region', 'id': '9hXe_F4g_eTG', 'national_nuts_level_3_code_2019': '25', 'preferred_label': 'Norrbottens län'},
{'legacy_ams_taxonomy_id': '25', 'type': 'region', 'label': 'Norrbottens län', 'concept_id': '9hXe_F4g_eTG', 'legacy_ams_taxonomy_num_id': 25}),

( {'type': 'region', 'id': 'oDpK_oZ2_WYt', 'national_nuts_level_3_code_2019': '20', 'preferred_label': 'Dalarnas län'},
{'legacy_ams_taxonomy_id': '20', 'type': 'region', 'label': 'Dalarnas län', 'concept_id': 'oDpK_oZ2_WYt', 'legacy_ams_taxonomy_num_id': 20}),

( {'type': 'region', 'id': 'MtbE_xWT_eMi', 'national_nuts_level_3_code_2019': '06', 'preferred_label': 'Jönköpings län'},
{'legacy_ams_taxonomy_id': '06', 'type': 'region', 'label': 'Jönköpings län', 'concept_id': 'MtbE_xWT_eMi', 'legacy_ams_taxonomy_num_id': 6}),

( {'type': 'region', 'id': 'oLT3_Q9p_3nn', 'national_nuts_level_3_code_2019': '05', 'preferred_label': 'Östergötlands län'},
{'legacy_ams_taxonomy_id': '05', 'type': 'region', 'label': 'Östergötlands län', 'concept_id': 'oLT3_Q9p_3nn', 'legacy_ams_taxonomy_num_id': 5}),

( {'type': 'region', 'id': 'K8iD_VQv_2BA', 'national_nuts_level_3_code_2019': '09', 'preferred_label': 'Gotlands län'},
{'legacy_ams_taxonomy_id': '09', 'type': 'region', 'label': 'Gotlands län', 'concept_id': 'K8iD_VQv_2BA', 'legacy_ams_taxonomy_num_id': 9}),

( {'type': 'region', 'id': 'zBon_eET_fFU', 'national_nuts_level_3_code_2019': '03', 'preferred_label': 'Uppsala län'},
{'legacy_ams_taxonomy_id': '03', 'type': 'region', 'label': 'Uppsala län', 'concept_id': 'zBon_eET_fFU', 'legacy_ams_taxonomy_num_id': 3}),

( {'type': 'region', 'id': 'xTCk_nT5_Zjm', 'national_nuts_level_3_code_2019': '18', 'preferred_label': 'Örebro län'},
{'legacy_ams_taxonomy_id': '18', 'type': 'region', 'label': 'Örebro län', 'concept_id': 'xTCk_nT5_Zjm', 'legacy_ams_taxonomy_num_id': 18}),

]