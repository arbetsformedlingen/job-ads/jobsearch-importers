def get_original_value(my_list):
    for item in my_list:
        if item['original_value']:
            return item
    return False
