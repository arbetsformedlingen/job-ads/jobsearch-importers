test_ads = [

    {
        "annonsId": 23448227,
        "annonsrubrik": "Driven fältsäljare Östergötland/Västergötland",
        "annonstext": "Köksbörsen är inne i ett expansivt skede. Vi har egen import ifrån Europa och Asien samt USA.  Vi har egna agenturprodukter. Vi levererar och projekterar kompletta restaurangkök över hela Sverige. Vi har en säljare i Göteborg, en säljare i Umeå/Skellefteå samt ett antal säljare i Stockholm. Vi expanderar nu säljteamet i Stockholm med ytterligare en fältsäljare för Stockholm/Mälardalen,  då vi har sett ett utökat behov av ytterligare en säljare,  vi ett härligt säljteam!\nVi ligger centralt i Stockholm vid Gärdet/Östermalm. VI har egen utställning av nya/begagnade restaurangmaskiner, samt även en utställning för möbler/glas/porslin/bestick. För att lyckas med denna tjänst, så krävs flera års erfarenhet inom försäljning till dagligvaruhandeln/restaurangbranschen, det krävs att du har ett brett kontaktnät inom branschen och många kontakter att börja jobba med direkt.  Det är ett stort plus om du tidigare har jobbat som säljare av restaurangmaskiner/inredning till restauranger/dagligvaruhandeln. I den här rollen kommer fokus att ligga på nykundsbearbetning, men du kommer även att utveckla befintliga kunder. Du ansvarar för din egen budget, kundstock och du bokar dina egna möten i ditt arbete.\nDet är meriterande om du har goda försäljningsresultat sedan tidigare och det är ett krav att du har körkort. För att lyckas i rollen bör du tycka om att arbeta med uppsökande försäljning och vara duktig på att skapa ett brett kontaktnät inom branschen. Du ska självständigt kunna planera ditt arbete och vara en driven person som är hungrig på nya utmaningar.\nDu är en självgående person som kan ta egna initiativ till nya affärer. Som person är du driven, stresstålig och ansvarsfull. Du har skinn på näsan och kan hantera många saker samtidigt. Det är viktigt att du är målinriktad och resultatfokuserad i ditt arbete och vill få snabba avslut i dina affärer. Vi ser att du är en social person som tycker om att ha mycket kundkontakt samt har ett sinne för att göra affärer. Vi ser det som meriterande om du har CAD vana.  En fördel är om du är utbildad kock, eller har jobbat på restaurang. Vi söker en driven fältsäljare som kommer att bearbeta nya samt befintliga kunder i Östergötland/Västergötland. Du ansvarar för din egen budget, kundstock och du bokar dina egna möten. I rollen som fältsäljare planerar du dina egna dagar och det är därför viktigt att du är ansvarsfull och målinriktad i ditt arbete.\nFör att lyckas med denna tjänst, så krävs flera års erfarenhet inom försäljning till dagligvaruhandeln/restaurangbranschen, det krävs att du har ett brett kontaktnät inom branschen och många kontakter att börja jobba med direkt. Du kommer även jobba med ringlistor för att bearbeta kedjekunder samt byggprojekt. Dom kunder som du ska bearbeta är restaurangkedjor, hotel, byggfirmor, kökskonsulter/arkitekter, livsmedelsbutiker, bensinstationer mm.",
        "annonstextFormaterad": "Köksbörsen är inne i ett expansivt skede. Vi har egen import ifrån Europa och Asien samt USA.  Vi har egna agenturprodukter. Vi levererar och projekterar kompletta restaurangkök över hela Sverige. Vi har en säljare i Göteborg, en säljare i Umeå/Skellefteå samt ett antal säljare i Stockholm. Vi expanderar nu säljteamet i Stockholm med ytterligare en fältsäljare för Stockholm/Mälardalen,  då vi har sett ett utökat behov av ytterligare en säljare,  vi ett härligt säljteam!<br>Vi ligger centralt i Stockholm vid Gärdet/Östermalm. VI har egen utställning av nya/begagnade restaurangmaskiner, samt även en utställning för möbler/glas/porslin/bestick. För att lyckas med denna tjänst, så krävs flera års erfarenhet inom försäljning till dagligvaruhandeln/restaurangbranschen, det krävs att du har ett brett kontaktnät inom branschen och många kontakter att börja jobba med direkt.  Det är ett stort plus om du tidigare har jobbat som säljare av restaurangmaskiner/inredning till restauranger/dagligvaruhandeln. I den här rollen kommer fokus att ligga på nykundsbearbetning, men du kommer även att utveckla befintliga kunder. Du ansvarar för din egen budget, kundstock och du bokar dina egna möten i ditt arbete.<br>Det är meriterande om du har goda försäljningsresultat sedan tidigare och det är ett krav att du har körkort. För att lyckas i rollen bör du tycka om att arbeta med uppsökande försäljning och vara duktig på att skapa ett brett kontaktnät inom branschen. Du ska självständigt kunna planera ditt arbete och vara en driven person som är hungrig på nya utmaningar.<br>Du är en självgående person som kan ta egna initiativ till nya affärer. Som person är du driven, stresstålig och ansvarsfull. Du har skinn på näsan och kan hantera många saker samtidigt. Det är viktigt att du är målinriktad och resultatfokuserad i ditt arbete och vill få snabba avslut i dina affärer. Vi ser att du är en social person som tycker om att ha mycket kundkontakt samt har ett sinne för att göra affärer. Vi ser det som meriterande om du har CAD vana.  En fördel är om du är utbildad kock, eller har jobbat på restaurang. Vi söker en driven fältsäljare som kommer att bearbeta nya samt befintliga kunder i Östergötland/Västergötland. Du ansvarar för din egen budget, kundstock och du bokar dina egna möten. I rollen som fältsäljare planerar du dina egna dagar och det är därför viktigt att du är ansvarsfull och målinriktad i ditt arbete.<br>För att lyckas med denna tjänst, så krävs flera års erfarenhet inom försäljning till dagligvaruhandeln/restaurangbranschen, det krävs att du har ett brett kontaktnät inom branschen och många kontakter att börja jobba med direkt. Du kommer även jobba med ringlistor för att bearbeta kedjekunder samt byggprojekt. Dom kunder som du ska bearbeta är restaurangkedjor, hotel, byggfirmor, kökskonsulter/arkitekter, livsmedelsbutiker, bensinstationer mm.",
        "ansokningssattEpost": " EMAIL ",
        "ansokningssattViaAF": False,
        "anstallningTyp": {
            "namn": "Vanlig anställning",
            "varde": "PFZr_Syz_cUq"
        },
        "antalPlatser": 1,
        "arbetsgivareId": "20513633",
        "arbetsgivareNamn": "Köksbörsen AB",
        "arbetsplatsId": "83191774",
        "arbetsplatsNamn": "KÖKSBÖRSEN AB",
        "arbetsplatsadress": {
            "gatuadress": "Linköping/Jönköping",
            "kommun": {
                "namn": "Linköping",
                "varde": "bm2x_1mr_Qhx"
            },
            "koordinatPrecision": "POSTNUMMER",
            "lan": {
                "namn": "Östergötlands län",
                "varde": "oLT3_Q9p_3nn"
            },
            "land": {
                "namn": "Sverige",
                "varde": "i46j_HmG_v64"
            },
            "latitud": "58.4044001929687",
            "longitud": "15.6003742645864",
            "postnr": "58002",
            "postort": "LINKÖPING"
        },
        "arbetstidTyp": {
            "namn": "Heltid",
            "varde": "6YE1_gAC_R2G"
        },
        "avpublicerad": False,
        "ejKravSvenskaEngelska": False,
        "informationAnsokningssatt": None,
        "ingenErfarenhetKravs": False,
        "inkluderande": False,
        "kallaTyp": "VIA_ANNONSERA",
        "kontaktpersoner": [
            {
                "befattning": None,
                "beskrivning": "blabla",
                "efternamn": "Testsson",
                "epost": "test@jobtechdev.se",
                "fackligRepresentant": False,
                "fornamn": "Testy",
                "telefonnummer": "+01011122233"
            }
        ],
        "lonTyp": {
            "namn": "Fast och rörlig lön",
            "varde": "asrX_9Df_ukn"
        },
        "organisationsnummer": "5567425631",
        "publiceringsdatum": "2021-03-19 12:41:52",
        "referens": "Utesäljare",
        "sistaAnsokningsdatum": "2021-04-18 23:59:59",
        "sistaPubliceringsdatum": "2022-06-14 07:38:46",
        "telefonnummer": None,
        "tillgangTillEgenBil": False,
        "uppdateradTid": 1616154112141,
        "utbildningsinriktning": {
            "namn": "Företagsekonomi, handel och administration",
            "varde": "CU11_PU7_Yj1",
            "vikt": 5
        },
        "utbildningsniva": {
            "namn": "Gymnasial utbildning",
            "varde": "DeBt_ahh_bkx",
            "vikt": 5
        },
        "varaktighetTyp": {
            "namn": "Tillsvidare",
            "varde": "a7uU_j21_mkL"
        },
        "version": "1.0",
        "yrkeserfarenheter": [
            {
                "erfarenhetsniva": {
                    "namn": "1-2 års erfarenhet",
                    "varde": "LLnd_5GJ_4ju"
                },
                "namn": "Fältsäljare/Utesäljare",
                "varde": "p17k_znk_osi",
                "vikt": 10
            }
        ],
        "yrkesroll": {
            "namn": "Fältsäljare/Utesäljare",
            "varde": "p17k_znk_osi"
        }
    },

    {'annonsId': 23699999, 'annonsrubrik': 'Greenkeeper -- Banpersonal 2021 Ullna Golf',
     'annonstext': 'Ullna Golf Club är en klassisk golfanläggning som funnits sedan 1981, vi har som mål att presentera en förstklassig golfbana och erbjuda en service med hög nivå.\n\nDu som söker dig till oss är positiv, flexibel, ordningsam och intresserad av att jobba i grupp. Du kommer jobba med skötsel av golfbanan. Det är tidiga morgnar, runt 06.00, i gengäld slutar vi tidigt på eftermiddagen, ca 15.00, du kommer även jobba några helger.\n\nDina arbetsuppgifter kommer till största del bestå av att klippa tees (utslagsplatser), greener (mest finklippta ytan), klippa ruffar samt kratta bunkrar.\n\nVi söker ett flertal banarbetare med varierande start av tjänsterna. De första tjänsterna börjar i FIRSTNAME/LASTNAME och löper i ca 4-6 månader beroende på tjänst. Det är ett krav att du kan ta dig hit själv, vi har mycket dåliga kollektiva kommunikationer. Du måste även kunna göra dig förstådd och förstå engelska. Det är meriterande om du spelar golf och/eller har jobbat på en golfbana, har B-kort eller traktorbehörighet. Det är bra om du har jobbat med röj såg, kört traktor.\n\nVi ser fram emot att läsa din ansökan och i slutändan jobba tillsammans.\n\nRekryteringen sker löpande.\n\nFör frågor och ansökningar mailas till \n\n Banchef FIRSTNAME LASTNAME\n\n EMAIL \n\nSkriv "Banpersonal2021" i ämnesraden\n\nDu kan besöka oss på Ullnagolf.se',
     'annonstextFormaterad': '<p>Ullna Golf Club är en klassisk golfanläggning som funnits sedan 1981, vi har som mål att presentera en förstklassig golfbana och erbjuda en service med hög nivå.</p><p>Du som söker dig till oss är positiv, flexibel, ordningsam och intresserad av att jobba i grupp. Du kommer jobba med skötsel av golfbanan. Det är tidiga morgnar, runt 06.00, i gengäld slutar vi tidigt på eftermiddagen, ca 15.00, du kommer även jobba några helger.</p><p>Dina arbetsuppgifter kommer till största del bestå av att klippa tees (utslagsplatser), greener (mest finklippta ytan), klippa ruffar samt kratta bunkrar.</p><p>Vi söker ett flertal banarbetare med varierande start av tjänsterna. De första tjänsterna börjar i FIRSTNAME/LASTNAME och löper i ca 4-6 månader beroende på tjänst. Det är ett krav att du kan ta dig hit själv, vi har mycket dåliga kollektiva kommunikationer. Du måste även kunna göra dig förstådd och förstå engelska. Det är meriterande om du spelar golf och/eller har jobbat på en golfbana, har B-kort eller traktorbehörighet. Det är bra om du har jobbat med röj såg, kört traktor.</p><p>Vi ser fram emot att läsa din ansökan och i slutändan jobba tillsammans.</p><p>Rekryteringen sker löpande.</p><p><br></p><p>För frågor och ansökningar mailas till </p><p> Banchef FIRSTNAME LASTNAME</p><p> EMAIL </p><p>Skriv "Banpersonal2021" i ämnesraden</p><p>Du kan besöka oss på Ullnagolf.se</p><p><br></p><p><br></p>',
     'ansokningssattEpost': ' EMAIL ', 'ansokningssattViaAF': False,
     'anstallningTyp': {'namn': 'Sommarjobb / feriejobb', 'varde': 'Jh8f_q9J_pbJ'}, 'antalPlatser': 4,
     'arbetsgivareId': '21057934', 'arbetsgivareNamn': 'Ullna Golf AB', 'arbetsplatsId': '85992110',
     'arbetsplatsNamn': 'Ullna Golf AB',
     'arbetsplatsadress': {'gatuadress': 'Roslagsvägen 36', 'kommun': {'namn': 'Österåker', 'varde': '8gKt_ZsV_PGj'},
                           'koordinatPrecision': 'GATUADRESS',
                           'lan': {'namn': 'Stockholms län', 'varde': 'CifL_Rzy_Mku'},
                           'land': {'namn': 'Sverige', 'varde': 'i46j_HmG_v64'}, 'latitud': '59.49548570116787',
                           'longitud': '18.152517169298417', 'postnr': '18494', 'postort': 'Åkersberga'},
     'arbetstidTyp': {'namn': 'Heltid', 'varde': '6YE1_gAC_R2G'}, 'avpublicerad': False, 'ejKravSvenskaEngelska': False,
     'informationAnsokningssatt': None, 'ingenErfarenhetKravs': False, 'inkluderande': False,
     'kallaTyp': 'VIA_ANNONSERA', 'kontaktpersoner': [
        {'befattning': None, 'beskrivning': 'blabla', 'efternamn': 'Testsson', 'epost': 'test@jobtechdev.se',
         'fackligRepresentant': False, 'fornamn': 'Testy', 'telefonnummer': '+01011122233'}],
     'korkort': [{'namn': 'B', 'varde': 'VTK8_WRx_GcM'}],
     'lonTyp': {'namn': 'Fast månads- vecko- eller timlön', 'varde': 'oG8G_9cW_nRf'},
     'organisationsnummer': '5560428095', 'publiceringsdatum': '2021-03-18 00:00:00', 'referens': 'Banpersonal 2021',
     'sistaAnsokningsdatum': '2021-04-16 23:59:59', 'sistaPubliceringsdatum': '2022-06-14 14:33:12',
     'telefonnummer': None, 'tillgangTillEgenBil': True, 'uppdateradTid': 1615971812090,
     'varaktighetTyp': {'namn': '3 - 6 månader', 'varde': 'Xj7x_7yZ_jEn'}, 'version': '1.0',
     'webbadress': 'ullnagolf.se', 'yrkesroll': {'namn': 'Golfbanearbetare', 'varde': 'tArV_EVU_cFQ'}},

    {'annonsId': 24277938, 'annonsrubrik': 'Sjuksköterska till vårdplatsenheten',
     'annonstext': 'Kullbergska sjukhuset Katrineholm \r\n\r\nVälkommen att skapa Sveriges friskaste län tillsammans med oss!\n\nSjuksköterska till vårdplatsenheten, se hit!\n\nOm oss\nVårdplatsenheten har två vårdavdelningar varav den ena med inriktning hjärt -och medicin och den andra stroke- och medicin. Vårdplatsenheten består av ett härligt gäng medarbetare som nu behöver utöka sjuksköterskegruppen. \nAvdelningarna vårdar patienter med olika sjukdomstillstånd, vi söker dig som är intresserad av hjärtsjukvård och strokevård/ortopedrehab.\nDu ska vara nyfiken, flexibel och kunna anpassa dig till ett varierande arbete och tempo.\n\nVi jobbar i team bestående av sjuksköterskor, undersköterskor, utskrivningssamordnare och medicinska sekreterare. Läkare och paramedicinsk kompetens finns på avdelningen under vissa tider som stöd i arbetet. \n\nTillsammans arbetar vi målinriktat för att skapa en trygg och säker resa genom vården för våra kunder.\n\nDin kompetens\nLegitimerad sjuksköterska. Erfarenhet av slutenvård är meriterande men inget krav. Vi söker en flexibel  person som uppskattar en varierad arbetsdag med möjlighet att lära sig mycket i arbetet. Du ska ha lätt att samarbeta med olika professioner, och vara öppen för förändringar. Vi lägger stor vikt på att vara lyhörd och göra insatser där behovet är som störst. Personliga egenskaper lägger vi stor vikt vid.\n\nAnställningsform\nTillsvidareanställning / Vikariat på heltid eller enligt överenskommelse. Tillträde enligt överenskommelse.\nArbetstid dag/kväll/helg eller natt. Planeringsschema används på avdelningen med möjlighet att kunna påverka sina pass till viss del.\n\nInformation om tjänsten lämnas av\nVårdenhetschef FIRSTNAME LASTNAME (stroke- och medicin),  TELEPHONENO .\nVårdenhetschef Ann-FIRSTNAME LASTNAME (hjärtmedicin),  TELEPHONENO .\nFacklig företrädare Arleide Pereira FIRSTNAME LASTNAME,  TELEPHONENO och FIRSTNAME LASTNAME,  TELEPHONENO .\nÖvriga fackliga företrädare nås via kontaktcenter,  TELEPHONENO .\n\nKom och jobba hos oss på vårdplatsenheten!\n\nVälkommen med din ansökan, inklusive CV, senast 2021-03-29.\nIntervjuer kan komma att ske löpande.\n\nSe våra förmåner (http://regionsormland.se/jobb-och-utbildning/vara-formaner/)\n\nFölj oss gärna på Facebook (http://facebook.com/regionsormlandJobbahososs/)\n\r\n\nVi har i vissa fall skyldighet att kontrollera om en person förekommer i misstanke- och eller belastningsregistret. Det kan ske på två sätt, endera begär regionen ut uppgiften själv eller också uppmanas du att begära ut utdrag för att kunna visa upp innan anställning. Vi begär i undantagsfall att du visar upp registerutdrag även vid tillsättning av andra tjänster än de som avses ovan. Blir du aktuell för anställning kommer du att informeras om vad som gäller för den tjänst du sökt.\n\nVi gör aktiva val vid exponering och rekryteringsstöd och undanber oss därför direktkontakt av bemannings- och rekryteringsföretag.',
     'annonstextFormaterad': 'Kullbergska sjukhuset Katrineholm \r\n\r\nVälkommen att skapa Sveriges friskaste län tillsammans med oss!\n\nSjuksköterska till vårdplatsenheten, se hit!\n\nOm oss\nVårdplatsenheten har två vårdavdelningar varav den ena med inriktning hjärt -och medicin och den andra stroke- och medicin. Vårdplatsenheten består av ett härligt gäng medarbetare som nu behöver utöka sjuksköterskegruppen. \nAvdelningarna vårdar patienter med olika sjukdomstillstånd, vi söker dig som är intresserad av hjärtsjukvård och strokevård/ortopedrehab.\nDu ska vara nyfiken, flexibel och kunna anpassa dig till ett varierande arbete och tempo.\n\nVi jobbar i team bestående av sjuksköterskor, undersköterskor, utskrivningssamordnare och medicinska sekreterare. Läkare och paramedicinsk kompetens finns på avdelningen under vissa tider som stöd i arbetet. \n\nTillsammans arbetar vi målinriktat för att skapa en trygg och säker resa genom vården för våra kunder.\n\nDin kompetens\nLegitimerad sjuksköterska. Erfarenhet av slutenvård är meriterande men inget krav. Vi söker en flexibel  person som uppskattar en varierad arbetsdag med möjlighet att lära sig mycket i arbetet. Du ska ha lätt att samarbeta med olika professioner, och vara öppen för förändringar. Vi lägger stor vikt på att vara lyhörd och göra insatser där behovet är som störst. Personliga egenskaper lägger vi stor vikt vid.\n\nAnställningsform\nTillsvidareanställning / Vikariat på heltid eller enligt överenskommelse. Tillträde enligt överenskommelse.\nArbetstid dag/kväll/helg eller natt. Planeringsschema används på avdelningen med möjlighet att kunna påverka sina pass till viss del.\n\nInformation om tjänsten lämnas av\nVårdenhetschef FIRSTNAME LASTNAME (stroke- och medicin),  TELEPHONENO .\nVårdenhetschef Ann-FIRSTNAME LASTNAME (hjärtmedicin),  TELEPHONENO .\nFacklig företrädare Arleide Pereira FIRSTNAME LASTNAME,  TELEPHONENO och FIRSTNAME LASTNAME,  TELEPHONENO .\nÖvriga fackliga företrädare nås via kontaktcenter,  TELEPHONENO .\n\n\nKom och jobba hos oss på vårdplatsenheten!\n\n\nVälkommen med din ansökan, inklusive CV, senast 2021-03-29.\nIntervjuer kan komma att ske löpande.\n\nSe våra förmåner (http://regionsormland.se/jobb-och-utbildning/vara-formaner/)\n\nFölj oss gärna på Facebook (http://facebook.com/regionsormlandJobbahososs/)\n\r\n\nVi har i vissa fall skyldighet att kontrollera om en person förekommer i misstanke- och eller belastningsregistret. Det kan ske på två sätt, endera begär regionen ut uppgiften själv eller också uppmanas du att begära ut utdrag för att kunna visa upp innan anställning. Vi begär i undantagsfall att du visar upp registerutdrag även vid tillsättning av andra tjänster än de som avses ovan. Blir du aktuell för anställning kommer du att informeras om vad som gäller för den tjänst du sökt.\n\nVi gör aktiva val vid exponering och rekryteringsstöd och undanber oss därför direktkontakt av bemannings- och rekryteringsföretag.',
     'ansokningsadress': {'gatuadress': 'Repslagaregatan 19', 'land': None, 'mottagare': None, 'postnr': None,
                          'postort': None}, 'ansokningssattEpost': None, 'ansokningssattViaAF': False,
     'ansokningssattWebbadress': 'https://sormland.powerinit.com/Modules/Recruitments/Public/?JobPositionId=18351&RefNo=RLSV-20-564&Source=[JobPositionSource]',
     'anstallningTyp': {'namn': 'Vanlig anställning', 'varde': 'PFZr_Syz_cUq'}, 'antalPlatser': 1,
     'arbetsgivareId': '10820079', 'arbetsgivareNamn': 'REGION SÖRMLAND', 'arbetsplatsId': '0',
     'arbetsplatsNamn': 'Region Sörmland',
     'arbetsplatsadress': {'gatuadress': None, 'kommun': {'namn': 'Katrineholm', 'varde': 'snx9_qVD_Dr1'},
                           'koordinatPrecision': 'KOMMUN',
                           'lan': {'namn': 'Södermanlands län', 'varde': 's93u_BEb_sx2'},
                           'land': {'namn': 'Sverige', 'varde': 'i46j_HmG_v64'}, 'latitud': '58.995552',
                           'longitud': '16.205475', 'postnr': None, 'postort': None},
     'arbetstidTyp': {'namn': 'Heltid', 'varde': '6YE1_gAC_R2G'}, 'avpublicerad': False,
     'besoksadress': {'gatuadress': 'Repslagaregatan 19', 'land': None, 'postnr': None, 'postort': None},
     'ejKravSvenskaEngelska': False, 'externtAnnonsId': '46-232100-0032-18351', 'informationAnsokningssatt': None,
     'ingenErfarenhetKravs': False, 'inkluderande': False, 'kallaTyp': 'VIA_PLATSBANKEN_DXA', 'kontaktpersoner': [
        {'befattning': None, 'beskrivning': 'blabla', 'efternamn': 'Testsson', 'epost': 'test@jobtechdev.se',
         'fackligRepresentant': False, 'fornamn': 'Testy', 'telefonnummer': '+01011122233'}],
     'lonTyp': {'namn': 'Fast månads- vecko- eller timlön', 'varde': 'oG8G_9cW_nRf'}, 'lonebeskrivning': '-',
     'organisationsnummer': '2321000032',
     'postadress': {'gatuadress': 'Repslagaregatan 19', 'land': 'SE', 'postnr': '61188', 'postort': 'Nyköping'},
     'publiceringsdatum': '2020-09-30 14:35:20', 'referens': 'RLSV-20-564',
     'sistaAnsokningsdatum': '2021-03-29 23:59:59', 'sistaPubliceringsdatum': '2022-06-14 14:33:59',
     'telefonnummer': None, 'tillgangTillEgenBil': False, 'uppdateradTid': 1613993432534,
     'varaktighetTyp': {'namn': 'Tillsvidare', 'varde': 'a7uU_j21_mkL'}, 'version': '4.0',
     'villkorsbeskrivning': 'Tillsvidare, 100, Tillträde enligt överenskommelse\r\n\n-', 'yrkeserfarenheter': [
        {'erfarenhetsniva': {'namn': 'Mindre än 1 års erfarenhet', 'varde': 'yrAe_Fzi_E6u'},
         'namn': 'Sjuksköterska, grundutbildad', 'varde': 'bXNH_MNX_dUR', 'vikt': 10}],
     'yrkesroll': {'namn': 'Sjuksköterska, grundutbildad', 'varde': 'bXNH_MNX_dUR'}}]

ads_not_sweden = [

    {'annonsId': 24458381, 'annonsrubrik': 'Servicetekniker i Oslo sökes',
     'annonstext': 'Er du en handy og serviceinnstilt person som ønsker en spennende stilling i en fremoverlent og voksende bedrift?\n\nSom servicetekniker vil du ha ansvar for service og hjemmebesøk for Part Construction AB. Du vil behandle innkommende forespørsler, utføre service oppdrag på byggeprosjekter og reklamasjonsarbeid. Arbeidet foregår hovedsakelig på Østlandet, men vi har også leveranser til andre deler av landet. Noe reising vil derfor måtte påberegnes.\n\nOm deg:\n\n\tEn allsidig praktisk fagarbeider. Du er kanskje flislegger, rørlegger, elektriker eller har en annen relevant utdanning\n\tSnakker og skriver godt Norsk\n\tFørerkort klasse B/BE\n\tGod evne til kundebehandling\n\tEr selvstendig og strukturert\n\tGrunnleggende data kunnskaper.\n\tKjennskap til NS 3420, NS 8409 og Bustadoppføringslova.\n\nPart Construction AB kan tilby:\n\n\tEt dynamisk og spennende arbeidsmiljø\n\tUtfordrende og variert arbeidsoppgaver\n\tKonkurransedyktig betingelser\n\tServicebil, mobil telefon\n\tGode forsikringer og pensjons ordninger\n\nOm arbeidsgiveren \n\nPart Construction AB, en av Europas ledende produsenter av prefabrikkerte baderoms moduler. Part Construction AB tilbyr entreprenørfirmaer og eiendomsselskaper prefabrikkerte baderom til blant annet, hoteller, sykehjem og boliger. Bademodulene leveres komplett med fliser, gulv, møbler, inventar, helt etter kundens ønsker. rørleggerarbeid og elektriske installasjoner er også ferdig montert fra fabrikk, Part er et familieeid selskap fra Kalix, med ca. 300 ansatte og med en omsetning på ca. 600 millioner. Hovedkvarter og produksjon er i Kalix / Överkalix, Norrbotten. Part Construction AB er en del av konserngruppen PartGroup sammen med søsterselskapene PreBad AB, Altor Industri, Isolamin AB, Space Interior AB, og PCS Modulsystem.\n\nSektor\n\nPrivat\n\nSted\n\nVestbygata 55 2003 Lillestrøm\n\nBransje\n\nBygg og anlegg,\n\nStillingsfunksjon\n\nHåndverker / Andre montører,\n\nHåndverker,\n\nHåndverker / Flislegger, rørlegger, elektriker\n\n \n\nSøknad\n\nTiltredelse\n\nEtter avtale\n\nVarighet\n\nFast, heltid (6 måneders prøvetid)\n\nBosted\n\nOslo området\n\nSøknadsfrist\n\nSnarest, og innen 10.03.2021\n\nSpørsmål om stillingen rettes til FIRSTNAME LASTNAME Yttervik, Servicekoordinator Norge. Tel:  TELEPHONENO , e-post:  EMAIL \n\nSend søknad merket "Servicetekniker Norge" på e-post til:  EMAIL \n\nDenne rekrutteringen skjer helt i Part Constructions regi, og vi ønsker kun direkte kontakt med personlig søkere. \n\nVi gleder oss til å høre fra deg!',
     'annonstextFormaterad': '<p><strong>Er du en handy og serviceinnstilt person som ønsker en spennende stilling i en fremoverlent og voksende bedrift?</strong></p><p>Som servicetekniker vil du ha ansvar for service og hjemmebesøk for Part Construction AB. Du vil behandle innkommende forespørsler, utføre service oppdrag på byggeprosjekter og reklamasjonsarbeid. Arbeidet foregår hovedsakelig på Østlandet, men vi har også leveranser til andre deler av landet. Noe reising vil derfor måtte påberegnes.</p><p><br></p><p><strong>Om deg:</strong></p><ul><li>En allsidig praktisk fagarbeider. Du er kanskje flislegger, rørlegger, elektriker eller har en annen relevant utdanning</li><li>Snakker og skriver godt Norsk</li><li>Førerkort klasse B/BE</li><li>God evne til kundebehandling</li><li>Er selvstendig og strukturert</li><li>Grunnleggende data kunnskaper.</li><li>Kjennskap til NS 3420, NS 8409 og Bustadoppføringslova.</li></ul><p><br></p><p><strong>Part Construction AB kan tilby:</strong></p><ul><li>Et dynamisk og spennende arbeidsmiljø</li><li>Utfordrende og variert arbeidsoppgaver</li><li>Konkurransedyktig betingelser</li><li>Servicebil, mobil telefon</li><li>Gode forsikringer og pensjons ordninger</li></ul><p><br></p><p><strong>Om arbeidsgiveren </strong></p><p>Part Construction AB, en av Europas ledende produsenter av prefabrikkerte baderoms moduler. Part Construction AB tilbyr entreprenørfirmaer og eiendomsselskaper prefabrikkerte baderom til blant annet, hoteller, sykehjem og boliger. Bademodulene leveres komplett med fliser, gulv, møbler, inventar, helt etter kundens ønsker. rørleggerarbeid og elektriske installasjoner er også ferdig montert fra fabrikk, Part er et familieeid selskap fra Kalix, med ca. 300 ansatte og med en omsetning på ca. 600 millioner. Hovedkvarter og produksjon er i Kalix / Överkalix, Norrbotten. Part Construction AB er en del av konserngruppen PartGroup sammen med søsterselskapene PreBad AB, Altor Industri, Isolamin AB, Space Interior AB, og PCS Modulsystem.</p><p><br></p><p><strong>Sektor</strong></p><p>Privat</p><p><strong>Sted</strong></p><p>Vestbygata 55 2003 Lillestrøm</p><p><strong>Bransje</strong></p><p>Bygg og anlegg,</p><p><strong>Stillingsfunksjon</strong></p><p>Håndverker / Andre montører,</p><p>Håndverker,</p><p>Håndverker / Flislegger, rørlegger, elektriker</p><p> </p><p><strong><u>Søknad</u></strong></p><p><br></p><p><strong>Tiltredelse</strong></p><p>Etter avtale</p><p><strong>Varighet</strong></p><p>Fast, heltid (6 måneders prøvetid)</p><p><strong>Bosted</strong></p><p>Oslo området</p><p><strong>Søknadsfrist</strong></p><p>Snarest, og innen 10.03.2021</p><p><strong>Spørsmål </strong>om stillingen rettes til FIRSTNAME LASTNAME Yttervik, Servicekoordinator Norge. Tel:  TELEPHONENO , e-post:  EMAIL </p><p>Send søknad merket "Servicetekniker Norge" på e-post til:  EMAIL </p><p>Denne rekrutteringen skjer helt i Part Constructions regi, og vi ønsker kun direkte kontakt med personlig søkere. </p><p>Vi gleder oss til å høre fra deg!</p><p id="oppenforalla">Öppen för alla\nVi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.</p>',
     'ansokningssattEpost': ' EMAIL ', 'ansokningssattViaAF': False,
     'anstallningTyp': {'namn': 'Vanlig anställning', 'varde': 'PFZr_Syz_cUq'}, 'antalPlatser': 1,
     'arbetsgivareId': '20610987', 'arbetsgivareNamn': 'Partbyggen i Kalix AB', 'arbetsplatsId': '86440232',
     'arbetsplatsNamn': 'Part Construction',
     'arbetsplatsadress': {'gatuadress': None, 'kommun': None, 'koordinatPrecision': None, 'lan': None,
                           'land': {'namn': 'Norge', 'varde': 'QJgN_Zge_BzJ'}, 'latitud': None, 'longitud': None,
                           'postnr': None, 'postort': None},
     'arbetstidTyp': {'namn': 'Heltid', 'varde': '6YE1_gAC_R2G'}, 'avpublicerad': False, 'ejKravSvenskaEngelska': False,
     'informationAnsokningssatt': None, 'ingenErfarenhetKravs': False, 'inkluderande': True,
     'kallaTyp': 'VIA_ANNONSERA', 'kontaktpersoner': [
        {'befattning': None, 'beskrivning': 'blabla', 'efternamn': 'Testsson', 'epost': 'test@jobtechdev.se',
         'fackligRepresentant': False, 'fornamn': 'Testy', 'telefonnummer': '+01011122233'}],
     'korkort': [{'namn': 'B', 'varde': 'VTK8_WRx_GcM'}],
     'lonTyp': {'namn': 'Fast månads- vecko- eller timlön', 'varde': 'oG8G_9cW_nRf'},
     'organisationsnummer': '5564003357', 'publiceringsdatum': '2021-01-04 16:18:13',
     'referens': 'Servicetekniker Norge', 'sistaAnsokningsdatum': '2021-03-31 23:59:59',
     'sistaPubliceringsdatum': '2022-06-14 14:52:55',
     'sprak': [{'namn': 'Norska', 'varde': 'pnjj_2JX_Fub', 'vikt': 10}], 'telefonnummer': None,
     'tillgangTillEgenBil': False, 'uppdateradTid': 1609773493583,
     'varaktighetTyp': {'namn': 'Tillsvidare', 'varde': 'a7uU_j21_mkL'}, 'version': '1.0', 'webbadress': 'partab.nu',
     'yrkeserfarenheter': [
         {'erfarenhetsniva': {'namn': '1-2 års erfarenhet', 'varde': 'LLnd_5GJ_4ju'}, 'namn': 'VVS-montör',
          'varde': 'ZZTi_v6g_4cZ', 'vikt': 5}], 'yrkesroll': {'namn': 'VVS-montör', 'varde': 'ZZTi_v6g_4cZ'}},
    {'annonsId': 24458381, 'annonsrubrik': 'Servicetekniker i Oslo sökes',
     'annonstext': 'Er du en handy og serviceinnstilt person som ønsker en spennende stilling i en fremoverlent og voksende bedrift?\n\nSom servicetekniker vil du ha ansvar for service og hjemmebesøk for Part Construction AB. Du vil behandle innkommende forespørsler, utføre service oppdrag på byggeprosjekter og reklamasjonsarbeid. Arbeidet foregår hovedsakelig på Østlandet, men vi har også leveranser til andre deler av landet. Noe reising vil derfor måtte påberegnes.\n\nOm deg:\n\n\tEn allsidig praktisk fagarbeider. Du er kanskje flislegger, rørlegger, elektriker eller har en annen relevant utdanning\n\tSnakker og skriver godt Norsk\n\tFørerkort klasse B/BE\n\tGod evne til kundebehandling\n\tEr selvstendig og strukturert\n\tGrunnleggende data kunnskaper.\n\tKjennskap til NS 3420, NS 8409 og Bustadoppføringslova.\n\nPart Construction AB kan tilby:\n\n\tEt dynamisk og spennende arbeidsmiljø\n\tUtfordrende og variert arbeidsoppgaver\n\tKonkurransedyktig betingelser\n\tServicebil, mobil telefon\n\tGode forsikringer og pensjons ordninger\n\nOm arbeidsgiveren \n\nPart Construction AB, en av Europas ledende produsenter av prefabrikkerte baderoms moduler. Part Construction AB tilbyr entreprenørfirmaer og eiendomsselskaper prefabrikkerte baderom til blant annet, hoteller, sykehjem og boliger. Bademodulene leveres komplett med fliser, gulv, møbler, inventar, helt etter kundens ønsker. rørleggerarbeid og elektriske installasjoner er også ferdig montert fra fabrikk, Part er et familieeid selskap fra Kalix, med ca. 300 ansatte og med en omsetning på ca. 600 millioner. Hovedkvarter og produksjon er i Kalix / Överkalix, Norrbotten. Part Construction AB er en del av konserngruppen PartGroup sammen med søsterselskapene PreBad AB, Altor Industri, Isolamin AB, Space Interior AB, og PCS Modulsystem.\n\nSektor\n\nPrivat\n\nSted\n\nVestbygata 55 2003 Lillestrøm\n\nBransje\n\nBygg og anlegg,\n\nStillingsfunksjon\n\nHåndverker / Andre montører,\n\nHåndverker,\n\nHåndverker / Flislegger, rørlegger, elektriker\n\n \n\nSøknad\n\nTiltredelse\n\nEtter avtale\n\nVarighet\n\nFast, heltid (6 måneders prøvetid)\n\nBosted\n\nOslo området\n\nSøknadsfrist\n\nSnarest, og innen 10.03.2021\n\nSpørsmål om stillingen rettes til FIRSTNAME LASTNAME Yttervik, Servicekoordinator Norge. Tel:  TELEPHONENO , e-post:  EMAIL \n\nSend søknad merket "Servicetekniker Norge" på e-post til:  EMAIL \n\nDenne rekrutteringen skjer helt i Part Constructions regi, og vi ønsker kun direkte kontakt med personlig søkere. \n\nVi gleder oss til å høre fra deg!',
     'annonstextFormaterad': '<p><strong>Er du en handy og serviceinnstilt person som ønsker en spennende stilling i en fremoverlent og voksende bedrift?</strong></p><p>Som servicetekniker vil du ha ansvar for service og hjemmebesøk for Part Construction AB. Du vil behandle innkommende forespørsler, utføre service oppdrag på byggeprosjekter og reklamasjonsarbeid. Arbeidet foregår hovedsakelig på Østlandet, men vi har også leveranser til andre deler av landet. Noe reising vil derfor måtte påberegnes.</p><p><br></p><p><strong>Om deg:</strong></p><ul><li>En allsidig praktisk fagarbeider. Du er kanskje flislegger, rørlegger, elektriker eller har en annen relevant utdanning</li><li>Snakker og skriver godt Norsk</li><li>Førerkort klasse B/BE</li><li>God evne til kundebehandling</li><li>Er selvstendig og strukturert</li><li>Grunnleggende data kunnskaper.</li><li>Kjennskap til NS 3420, NS 8409 og Bustadoppføringslova.</li></ul><p><br></p><p><strong>Part Construction AB kan tilby:</strong></p><ul><li>Et dynamisk og spennende arbeidsmiljø</li><li>Utfordrende og variert arbeidsoppgaver</li><li>Konkurransedyktig betingelser</li><li>Servicebil, mobil telefon</li><li>Gode forsikringer og pensjons ordninger</li></ul><p><br></p><p><strong>Om arbeidsgiveren </strong></p><p>Part Construction AB, en av Europas ledende produsenter av prefabrikkerte baderoms moduler. Part Construction AB tilbyr entreprenørfirmaer og eiendomsselskaper prefabrikkerte baderom til blant annet, hoteller, sykehjem og boliger. Bademodulene leveres komplett med fliser, gulv, møbler, inventar, helt etter kundens ønsker. rørleggerarbeid og elektriske installasjoner er også ferdig montert fra fabrikk, Part er et familieeid selskap fra Kalix, med ca. 300 ansatte og med en omsetning på ca. 600 millioner. Hovedkvarter og produksjon er i Kalix / Överkalix, Norrbotten. Part Construction AB er en del av konserngruppen PartGroup sammen med søsterselskapene PreBad AB, Altor Industri, Isolamin AB, Space Interior AB, og PCS Modulsystem.</p><p><br></p><p><strong>Sektor</strong></p><p>Privat</p><p><strong>Sted</strong></p><p>Vestbygata 55 2003 Lillestrøm</p><p><strong>Bransje</strong></p><p>Bygg og anlegg,</p><p><strong>Stillingsfunksjon</strong></p><p>Håndverker / Andre montører,</p><p>Håndverker,</p><p>Håndverker / Flislegger, rørlegger, elektriker</p><p> </p><p><strong><u>Søknad</u></strong></p><p><br></p><p><strong>Tiltredelse</strong></p><p>Etter avtale</p><p><strong>Varighet</strong></p><p>Fast, heltid (6 måneders prøvetid)</p><p><strong>Bosted</strong></p><p>Oslo området</p><p><strong>Søknadsfrist</strong></p><p>Snarest, og innen 10.03.2021</p><p><strong>Spørsmål </strong>om stillingen rettes til FIRSTNAME LASTNAME Yttervik, Servicekoordinator Norge. Tel:  TELEPHONENO , e-post:  EMAIL </p><p>Send søknad merket "Servicetekniker Norge" på e-post til:  EMAIL </p><p>Denne rekrutteringen skjer helt i Part Constructions regi, og vi ønsker kun direkte kontakt med personlig søkere. </p><p>Vi gleder oss til å høre fra deg!</p><p id="oppenforalla">Öppen för alla\nVi fokuserar på din kompetens, inte dina övriga förutsättningar. Vi är öppna för att anpassa rollen eller arbetsplatsen efter dina behov.</p>',
     'ansokningssattEpost': ' EMAIL ', 'ansokningssattViaAF': False,
     'anstallningTyp': {'namn': 'Vanlig anställning', 'varde': 'PFZr_Syz_cUq'}, 'antalPlatser': 1,
     'arbetsgivareId': '20610987', 'arbetsgivareNamn': 'Partbyggen i Kalix AB', 'arbetsplatsId': '86440232',
     'arbetsplatsNamn': 'Part Construction',
     'arbetsplatsadress': {'gatuadress': None, 'kommun': None, 'koordinatPrecision': None, 'lan': None,
                           'land': {'namn': 'Norge', 'varde': 'QJgN_Zge_BzJ'}, 'latitud': None, 'longitud': None,
                           'postnr': None, 'postort': None},
     'arbetstidTyp': {'namn': 'Heltid', 'varde': '6YE1_gAC_R2G'}, 'avpublicerad': False, 'ejKravSvenskaEngelska': False,
     'informationAnsokningssatt': None, 'ingenErfarenhetKravs': False, 'inkluderande': True,
     'kallaTyp': 'VIA_ANNONSERA', 'kontaktpersoner': [
        {'befattning': None, 'beskrivning': 'blabla', 'efternamn': 'Testsson', 'epost': 'test@jobtechdev.se',
         'fackligRepresentant': False, 'fornamn': 'Testy', 'telefonnummer': '+01011122233'}],
     'korkort': [{'namn': 'B', 'varde': 'VTK8_WRx_GcM'}],
     'lonTyp': {'namn': 'Fast månads- vecko- eller timlön', 'varde': 'oG8G_9cW_nRf'},
     'organisationsnummer': '5564003357', 'publiceringsdatum': '2021-01-04 16:18:13',
     'referens': 'Servicetekniker Norge', 'sistaAnsokningsdatum': '2021-03-31 23:59:59',
     'sistaPubliceringsdatum': '2022-06-14 14:52:55',
     'sprak': [{'namn': 'Norska', 'varde': 'pnjj_2JX_Fub', 'vikt': 10}], 'telefonnummer': None,
     'tillgangTillEgenBil': False, 'uppdateradTid': 1609773493583,
     'varaktighetTyp': {'namn': 'Tillsvidare', 'varde': 'a7uU_j21_mkL'}, 'version': '1.0', 'webbadress': 'partab.nu',
     'yrkeserfarenheter': [
         {'erfarenhetsniva': {'namn': '1-2 års erfarenhet', 'varde': 'LLnd_5GJ_4ju'}, 'namn': 'VVS-montör',
          'varde': 'ZZTi_v6g_4cZ', 'vikt': 5}], 'yrkesroll': {'namn': 'VVS-montör', 'varde': 'ZZTi_v6g_4cZ'}},

    {'annonsId': 24513844, 'annonsrubrik': 'Svensktalande Mötesbokare till Soliga Malaga, Spanien!',
     'annonstext': 'Svensktalande B2B mötesbokare till soliga Benalmadena i Spanien!\n \nVi söker nu en mötesbokare som kan hjälpa vår partner att ytterligare stärka sin position på företagsmarknaden. Vi söker säljare som är bra med människor, som vill utvecklas vidare som säljare som har förmågan att stå på sig och ge lite extra för att nå resultat. Medarbetare med goda empatiska färdigheter och som kan socialisera och prata med många olika personligheter.\n  \nI din roll kommer du att boka möten för vår klient per telefon. Du kommer att arbeta för den svenska  marknaden och kontakta verksamheter för att kunna väcka intresse, och sätta upp ett möte för den potentiella nya klienten och säljteamet.\n \nOm dig\nDu kommunicerar väl i tal och skrift samt tycker om att arbeta med uppsökande försäljning mot nya kunder. Vidare drivs du av bra värderingar och arbetar för att alla ska sträva mot samma mål. Likaså har du förmågan att ge det lilla extra för att uppnå dina egna mål. Du har ett starkt driv att uppnå goda resultat och är lösningsorienterad med en god arbetsmoral. Därtill har du hög förmåga att förvärva ny kunskap samt har erfarenhet av och trivs med att ha telefonen som arbetsredskap. \n\nVår klient erbjuder\n-Säljutbildning och vägledning i att arbeta med ett välkänt och starkt varumärke\n-Möjlighet att vidareutveckla dig själv med fokus på kompetensutveckling. \n-En ambitiös och stabil arbetsgivare med långsiktiga mål. \n-Konkurrenskraftig lön. \n-En energisk och ambitiös arbetsmiljö där alla hjälper varandra.\n\nAndra detaljer:\n-Arbetstider: Måndag-fredag 08:00-17:00\n-Heltidsanställning\n-Kontor i Benalmadena, Spanien\n\nKompetenskrav: \n-Flytande svenska både i tal och skrift \n-Flytande engelska både i tal och skrift \n-God teknisk förståelse\n\nFlyttpaket: Återbetald flybjljett + fyra veckors boende vid start samt hjälp med NIE-nummer.\n\nBeskrivning av företaget:\nVår klient är en välkänd aktör på företagsmarknaden - detta är en mycket spännande och utvecklande position. Denna position är placerad i vackra och soliga Benalmadena i Spanien. \n\nVi vill inte bli kontaktade av rekryterings-/bemanningsföretag',
     'annonstextFormaterad': 'Svensktalande B2B mötesbokare till soliga Benalmadena i Spanien!\n \nVi söker nu en mötesbokare som kan hjälpa vår partner att ytterligare stärka sin position på företagsmarknaden. Vi söker säljare som är bra med människor, som vill utvecklas vidare som säljare som har förmågan att stå på sig och ge lite extra för att nå resultat. Medarbetare med goda empatiska färdigheter och som kan socialisera och prata med många olika personligheter.\n  \nI din roll kommer du att boka möten för vår klient per telefon. Du kommer att arbeta för den svenska  marknaden och kontakta verksamheter för att kunna väcka intresse, och sätta upp ett möte för den potentiella nya klienten och säljteamet.\n \nOm dig\nDu kommunicerar väl i tal och skrift samt tycker om att arbeta med uppsökande försäljning mot nya kunder. Vidare drivs du av bra värderingar och arbetar för att alla ska sträva mot samma mål. Likaså har du förmågan att ge det lilla extra för att uppnå dina egna mål. Du har ett starkt driv att uppnå goda resultat och är lösningsorienterad med en god arbetsmoral. Därtill har du hög förmåga att förvärva ny kunskap samt har erfarenhet av och trivs med att ha telefonen som arbetsredskap. \n\nVår klient erbjuder\n-Säljutbildning och vägledning i att arbeta med ett välkänt och starkt varumärke\n-Möjlighet att vidareutveckla dig själv med fokus på kompetensutveckling. \n-En ambitiös och stabil arbetsgivare med långsiktiga mål. \n-Konkurrenskraftig lön. \n-En energisk och ambitiös arbetsmiljö där alla hjälper varandra.\n\nAndra detaljer:\n-Arbetstider: Måndag-fredag 08:00-17:00\n-Heltidsanställning\n-Kontor i Benalmadena, Spanien\n\nKompetenskrav: \n-Flytande svenska både i tal och skrift \n-Flytande engelska både i tal och skrift \n-God teknisk förståelse\n\nFlyttpaket: Återbetald flybjljett + fyra veckors boende vid start samt hjälp med NIE-nummer.\n\nBeskrivning av företaget:\nVår klient är en välkänd aktör på företagsmarknaden - detta är en mycket spännande och utvecklande position. Denna position är placerad i vackra och soliga Benalmadena i Spanien. \n\nVi vill inte bli kontaktade av rekryterings-/bemanningsföretag',
     'ansokningssattEpost': ' EMAIL ', 'ansokningssattViaAF': False,
     'anstallningTyp': {'namn': 'Arbete utomlands', 'varde': '9Wuo_2Yb_36E'}, 'antalPlatser': 8, 'arbetsgivareId': '0',
     'arbetsgivareNamn': 'Nordic Jobs Worldwide AS', 'arbetsplatsId': '21052284',
     'arbetsplatsNamn': 'Nordic Jobs Worldwide AS',
     'arbetsplatsadress': {'gatuadress': None, 'kommun': None, 'koordinatPrecision': None, 'lan': None,
                           'land': {'namn': 'Spanien', 'varde': 'bN7k_4ka_YGQ'}, 'latitud': None, 'longitud': None,
                           'postnr': None, 'postort': None},
     'arbetstidTyp': {'namn': 'Heltid', 'varde': '6YE1_gAC_R2G'}, 'avpublicerad': False, 'ejKravSvenskaEngelska': False,
     'externtAnnonsId': '0021-045667', 'informationAnsokningssatt': None, 'ingenErfarenhetKravs': True,
     'inkluderande': False, 'kallaTyp': 'VIA_AIS', 'kontaktpersoner': [
        {'befattning': None, 'beskrivning': 'blabla', 'efternamn': 'Testsson', 'epost': 'test@jobtechdev.se',
         'fackligRepresentant': False, 'fornamn': 'Testy', 'telefonnummer': '+01011122233'}],
     'lonTyp': {'namn': 'Fast och rörlig lön', 'varde': 'asrX_9Df_ukn'}, 'lonebeskrivning': 'Fast lön + provision',
     'publiceringsdatum': '2021-02-01 10:57:26', 'sistaAnsokningsdatum': '2021-04-27 23:59:59',
     'sistaPubliceringsdatum': '2022-06-14 14:55:21',
     'sprak': [{'namn': 'Svenska', 'varde': 'zSLA_vw2_FXN', 'vikt': 10},
               {'namn': 'Engelska', 'varde': 'NVxJ_hLg_TYS', 'vikt': 10}], 'telefonnummer': None,
     'tillgangTillEgenBil': False, 'uppdateradTid': 1612173446756,
     'varaktighetTyp': {'namn': 'Tillsvidare', 'varde': 'a7uU_j21_mkL'}, 'version': '2.0',
     'villkorsbeskrivning': 'Arbetstider: Måndag-fredag 08:00-17:00',
     'yrkesroll': {'namn': 'Besöksbokare/Kundbokare', 'varde': 'w8eg_Ufq_B5X'}},

]


ads_no_address = [
    {'arbetsplatsadress': {'gatuadress': None, 'kommun': None, 'koordinatPrecision': None, 'lan': None,
                          'land': {'namn': None, 'varde': None}, 'latitud': None, 'longitud': None,
                          'postnr': None, 'postort': None}},




]