general_value_test_cases =  [( {'type': 'driving-licence', 'id': '4HpY_e2U_TUH', 'preferred_label': 'AM', 'deprecated_legacy_id': '16'},
'driving-license', {'legacy_ams_taxonomy_id': '16', 'type': 'driving-license', 'label': 'AM', 'concept_id': '4HpY_e2U_TUH', 'legacy_ams_taxonomy_num_id': 16}),

( {'type': 'driving-licence', 'id': 'bneY_V3g_Kog', 'preferred_label': 'A1', 'deprecated_legacy_id': '2'},
'driving-license', {'legacy_ams_taxonomy_id': '2', 'type': 'driving-license', 'label': 'A1', 'concept_id': 'bneY_V3g_Kog', 'legacy_ams_taxonomy_num_id': 2}),

( {'type': 'driving-licence', 'id': 'Eybh_j3s_SqF', 'preferred_label': 'A2', 'deprecated_legacy_id': '17'},
'driving-license', {'legacy_ams_taxonomy_id': '17', 'type': 'driving-license', 'label': 'A2', 'concept_id': 'Eybh_j3s_SqF', 'legacy_ams_taxonomy_num_id': 17}),

( {'type': 'driving-licence', 'id': 'hK8X_cX9_5P4', 'preferred_label': 'A', 'deprecated_legacy_id': '10'},
'driving-license', {'legacy_ams_taxonomy_id': '10', 'type': 'driving-license', 'label': 'A', 'concept_id': 'hK8X_cX9_5P4', 'legacy_ams_taxonomy_num_id': 10}),

( {'type': 'driving-licence', 'id': 'VTK8_WRx_GcM', 'preferred_label': 'B', 'deprecated_legacy_id': '3'},
'driving-license', {'legacy_ams_taxonomy_id': '3', 'type': 'driving-license', 'label': 'B', 'concept_id': 'VTK8_WRx_GcM', 'legacy_ams_taxonomy_num_id': 3}),

( {'type': 'driving-licence', 'id': 'ftCQ_gFu_L4b', 'preferred_label': 'Utökad B', 'deprecated_legacy_id': '18'},
'driving-license', {'legacy_ams_taxonomy_id': '18', 'type': 'driving-license', 'label': 'Utökad B', 'concept_id': 'ftCQ_gFu_L4b', 'legacy_ams_taxonomy_num_id': 18}),

( {'type': 'driving-licence', 'id': 'bcFd_Vkt_KXL', 'preferred_label': 'BE', 'deprecated_legacy_id': '6'},
'driving-license', {'legacy_ams_taxonomy_id': '6', 'type': 'driving-license', 'label': 'BE', 'concept_id': 'bcFd_Vkt_KXL', 'legacy_ams_taxonomy_num_id': 6}),

( {'type': 'driving-licence', 'id': 'swP6_psb_FCB', 'preferred_label': 'C1', 'deprecated_legacy_id': '12'},
'driving-license', {'legacy_ams_taxonomy_id': '12', 'type': 'driving-license', 'label': 'C1', 'concept_id': 'swP6_psb_FCB', 'legacy_ams_taxonomy_num_id': 12}),

( {'type': 'driving-licence', 'id': 'xRum_Wtt_T1e', 'preferred_label': 'C1E', 'deprecated_legacy_id': '14'},
'driving-license', {'legacy_ams_taxonomy_id': '14', 'type': 'driving-license', 'label': 'C1E', 'concept_id': 'xRum_Wtt_T1e', 'legacy_ams_taxonomy_num_id': 14}),

( {'type': 'driving-licence', 'id': 'BKCx_hST_Pcm', 'preferred_label': 'C', 'deprecated_legacy_id': '4'},
'driving-license', {'legacy_ams_taxonomy_id': '4', 'type': 'driving-license', 'label': 'C', 'concept_id': 'BKCx_hST_Pcm', 'legacy_ams_taxonomy_num_id': 4}),

( {'type': 'driving-licence', 'id': 'zZu8_iZ9_wMH', 'preferred_label': 'CE', 'deprecated_legacy_id': '7'},
'driving-license', {'legacy_ams_taxonomy_id': '7', 'type': 'driving-license', 'label': 'CE', 'concept_id': 'zZu8_iZ9_wMH', 'legacy_ams_taxonomy_num_id': 7}),

( {'type': 'driving-licence', 'id': '5L6k_p4w_8Wu', 'preferred_label': 'D1', 'deprecated_legacy_id': '19'},
'driving-license', {'legacy_ams_taxonomy_id': '19', 'type': 'driving-license', 'label': 'D1', 'concept_id': '5L6k_p4w_8Wu', 'legacy_ams_taxonomy_num_id': 19}),

( {'type': 'driving-licence', 'id': 'qZ2E_JSL_xAq', 'preferred_label': 'D1E', 'deprecated_legacy_id': '15'},
'driving-license', {'legacy_ams_taxonomy_id': '15', 'type': 'driving-license', 'label': 'D1E', 'concept_id': 'qZ2E_JSL_xAq', 'legacy_ams_taxonomy_num_id': 15}),

( {'type': 'driving-licence', 'id': 'hK1a_wsQ_4UG', 'preferred_label': 'D', 'deprecated_legacy_id': '5'},
'driving-license', {'legacy_ams_taxonomy_id': '5', 'type': 'driving-license', 'label': 'D', 'concept_id': 'hK1a_wsQ_4UG', 'legacy_ams_taxonomy_num_id': 5}),

( {'type': 'driving-licence', 'id': '6se7_AdD_nGw', 'preferred_label': 'DE', 'deprecated_legacy_id': '8'},
'driving-license', {'legacy_ams_taxonomy_id': '8', 'type': 'driving-license', 'label': 'DE', 'concept_id': '6se7_AdD_nGw', 'legacy_ams_taxonomy_num_id': 8}),

( {'type': 'country', 'id': 'kTtK_3Y3_Kha', 'preferred_label': 'Afghanistan', 'deprecated_legacy_id': '1'},
'country', {'legacy_ams_taxonomy_id': '1', 'type': 'country', 'label': 'Afghanistan', 'concept_id': 'kTtK_3Y3_Kha', 'legacy_ams_taxonomy_num_id': 1}),

( {'type': 'country', 'id': 'UinE_kZh_fyB', 'preferred_label': 'Albanien', 'deprecated_legacy_id': '2'},
'country', {'legacy_ams_taxonomy_id': '2', 'type': 'country', 'label': 'Albanien', 'concept_id': 'UinE_kZh_fyB', 'legacy_ams_taxonomy_num_id': 2}),

( {'type': 'country', 'id': 'RfvE_aUv_QNd', 'preferred_label': 'Algeriet', 'deprecated_legacy_id': '3'},
'country', {'legacy_ams_taxonomy_id': '3', 'type': 'country', 'label': 'Algeriet', 'concept_id': 'RfvE_aUv_QNd', 'legacy_ams_taxonomy_num_id': 3}),

( {'type': 'country', 'id': 'TQUR_jxt_5My', 'preferred_label': 'Amerikanska Samoa', 'deprecated_legacy_id': '4'},
'country', {'legacy_ams_taxonomy_id': '4', 'type': 'country', 'label': 'Amerikanska Samoa', 'concept_id': 'TQUR_jxt_5My', 'legacy_ams_taxonomy_num_id': 4}),

( {'type': 'country', 'id': 'vdpd_XS2_ZBZ', 'preferred_label': 'Amerikanska, mindre uteliggande öar', 'deprecated_legacy_id': '5'},
'country', {'legacy_ams_taxonomy_id': '5', 'type': 'country', 'label': 'Amerikanska, mindre uteliggande öar', 'concept_id': 'vdpd_XS2_ZBZ', 'legacy_ams_taxonomy_num_id': 5}),

( {'type': 'country', 'id': 'sweX_RLA_3e6', 'preferred_label': 'Andorra', 'deprecated_legacy_id': '6'},
'country', {'legacy_ams_taxonomy_id': '6', 'type': 'country', 'label': 'Andorra', 'concept_id': 'sweX_RLA_3e6', 'legacy_ams_taxonomy_num_id': 6}),

( {'type': 'country', 'id': 'kWK9_dqr_Mo7', 'preferred_label': 'Anguilla', 'deprecated_legacy_id': '8'},
'country', {'legacy_ams_taxonomy_id': '8', 'type': 'country', 'label': 'Anguilla', 'concept_id': 'kWK9_dqr_Mo7', 'legacy_ams_taxonomy_num_id': 8}),

( {'type': 'country', 'id': 'vc1T_oaU_Ctj', 'preferred_label': 'Antarktis', 'deprecated_legacy_id': '9'},
'country', {'legacy_ams_taxonomy_id': '9', 'type': 'country', 'label': 'Antarktis', 'concept_id': 'vc1T_oaU_Ctj', 'legacy_ams_taxonomy_num_id': 9}),

( {'type': 'country', 'id': 'bq7f_tFF_eHk', 'preferred_label': 'Antigua och Barbuda', 'deprecated_legacy_id': '10'},
'country', {'legacy_ams_taxonomy_id': '10', 'type': 'country', 'label': 'Antigua och Barbuda', 'concept_id': 'bq7f_tFF_eHk', 'legacy_ams_taxonomy_num_id': 10}),

( {'type': 'country', 'id': '5HZv_9h6_oEx', 'preferred_label': 'Förenade arabemiraten', 'deprecated_legacy_id': '11'},
'country', {'legacy_ams_taxonomy_id': '11', 'type': 'country', 'label': 'Förenade arabemiraten', 'concept_id': '5HZv_9h6_oEx', 'legacy_ams_taxonomy_num_id': 11}),

( {'type': 'country', 'id': '47CY_xjT_Xc2', 'preferred_label': 'Argentina', 'deprecated_legacy_id': '12'},
'country', {'legacy_ams_taxonomy_id': '12', 'type': 'country', 'label': 'Argentina', 'concept_id': '47CY_xjT_Xc2', 'legacy_ams_taxonomy_num_id': 12}),

( {'type': 'country', 'id': 'fy8S_VeD_ssD', 'preferred_label': 'Armenien', 'deprecated_legacy_id': '13'},
'country', {'legacy_ams_taxonomy_id': '13', 'type': 'country', 'label': 'Armenien', 'concept_id': 'fy8S_VeD_ssD', 'legacy_ams_taxonomy_num_id': 13}),

( {'type': 'country', 'id': 'ryVr_iWV_Fng', 'preferred_label': 'Aruba', 'deprecated_legacy_id': '14'},
'country', {'legacy_ams_taxonomy_id': '14', 'type': 'country', 'label': 'Aruba', 'concept_id': 'ryVr_iWV_Fng', 'legacy_ams_taxonomy_num_id': 14}),

( {'type': 'country', 'id': 'miKy_TpU_dxq', 'preferred_label': 'Australien', 'deprecated_legacy_id': '15'},
'country', {'legacy_ams_taxonomy_id': '15', 'type': 'country', 'label': 'Australien', 'concept_id': 'miKy_TpU_dxq', 'legacy_ams_taxonomy_num_id': 15}),

( {'type': 'country', 'id': 'aHrz_7Ra_BHD', 'preferred_label': 'Azerbajdzjan', 'deprecated_legacy_id': '16'},
'country', {'legacy_ams_taxonomy_id': '16', 'type': 'country', 'label': 'Azerbajdzjan', 'concept_id': 'aHrz_7Ra_BHD', 'legacy_ams_taxonomy_num_id': 16}),

( {'type': 'country', 'id': 'TkqL_i5G_onf', 'preferred_label': 'Bahamas', 'deprecated_legacy_id': '17'},
'country', {'legacy_ams_taxonomy_id': '17', 'type': 'country', 'label': 'Bahamas', 'concept_id': 'TkqL_i5G_onf', 'legacy_ams_taxonomy_num_id': 17}),

( {'type': 'country', 'id': 'nLkD_9YC_CbV', 'preferred_label': 'Bahrain', 'deprecated_legacy_id': '18'},
'country', {'legacy_ams_taxonomy_id': '18', 'type': 'country', 'label': 'Bahrain', 'concept_id': 'nLkD_9YC_CbV', 'legacy_ams_taxonomy_num_id': 18}),

( {'type': 'country', 'id': 'SntG_Zrn_SR3', 'preferred_label': 'Bangladesh', 'deprecated_legacy_id': '19'},
'country', {'legacy_ams_taxonomy_id': '19', 'type': 'country', 'label': 'Bangladesh', 'concept_id': 'SntG_Zrn_SR3', 'legacy_ams_taxonomy_num_id': 19}),

( {'type': 'country', 'id': 'BXzb_Wib_vNM', 'preferred_label': 'Barbados', 'deprecated_legacy_id': '20'},
'country', {'legacy_ams_taxonomy_id': '20', 'type': 'country', 'label': 'Barbados', 'concept_id': 'BXzb_Wib_vNM', 'legacy_ams_taxonomy_num_id': 20}),

( {'type': 'country', 'id': 'igN2_SxA_VJK', 'preferred_label': 'Belgien', 'deprecated_legacy_id': '21'},
'country', {'legacy_ams_taxonomy_id': '21', 'type': 'country', 'label': 'Belgien', 'concept_id': 'igN2_SxA_VJK', 'legacy_ams_taxonomy_num_id': 21}),

( {'type': 'country', 'id': 'ccCA_va6_Lx1', 'preferred_label': 'Belize', 'deprecated_legacy_id': '22'},
'country', {'legacy_ams_taxonomy_id': '22', 'type': 'country', 'label': 'Belize', 'concept_id': 'ccCA_va6_Lx1', 'legacy_ams_taxonomy_num_id': 22}),

( {'type': 'country', 'id': '99u3_XQB_jRk', 'preferred_label': 'Benin', 'deprecated_legacy_id': '23'},
'country', {'legacy_ams_taxonomy_id': '23', 'type': 'country', 'label': 'Benin', 'concept_id': '99u3_XQB_jRk', 'legacy_ams_taxonomy_num_id': 23}),

( {'type': 'country', 'id': 'EwAS_2UD_n2e', 'preferred_label': 'Bermuda', 'deprecated_legacy_id': '24'},
'country', {'legacy_ams_taxonomy_id': '24', 'type': 'country', 'label': 'Bermuda', 'concept_id': 'EwAS_2UD_n2e', 'legacy_ams_taxonomy_num_id': 24}),

( {'type': 'country', 'id': 'bhUZ_uVe_pLo', 'preferred_label': 'Bhutan', 'deprecated_legacy_id': '25'},
'country', {'legacy_ams_taxonomy_id': '25', 'type': 'country', 'label': 'Bhutan', 'concept_id': 'bhUZ_uVe_pLo', 'legacy_ams_taxonomy_num_id': 25}),

( {'type': 'country', 'id': 'v5g4_WPx_zE4', 'preferred_label': 'Bolivia', 'deprecated_legacy_id': '26'},
'country', {'legacy_ams_taxonomy_id': '26', 'type': 'country', 'label': 'Bolivia', 'concept_id': 'v5g4_WPx_zE4', 'legacy_ams_taxonomy_num_id': 26}),

( {'type': 'country', 'id': 'sdJ3_JHH_bHh', 'preferred_label': 'Bosnien och Hercegovina', 'deprecated_legacy_id': '27'},
'country', {'legacy_ams_taxonomy_id': '27', 'type': 'country', 'label': 'Bosnien och Hercegovina', 'concept_id': 'sdJ3_JHH_bHh', 'legacy_ams_taxonomy_num_id': 27}),

( {'type': 'country', 'id': 'f4zT_ajm_AT8', 'preferred_label': 'Botswana', 'deprecated_legacy_id': '28'},
'country', {'legacy_ams_taxonomy_id': '28', 'type': 'country', 'label': 'Botswana', 'concept_id': 'f4zT_ajm_AT8', 'legacy_ams_taxonomy_num_id': 28}),

( {'type': 'country', 'id': 'biXx_VWq_vrH', 'preferred_label': 'Bouvetön', 'deprecated_legacy_id': '29'},
'country', {'legacy_ams_taxonomy_id': '29', 'type': 'country', 'label': 'Bouvetön', 'concept_id': 'biXx_VWq_vrH', 'legacy_ams_taxonomy_num_id': 29}),

( {'type': 'country', 'id': 'KYRf_tqd_ndA', 'preferred_label': 'Brasilien', 'deprecated_legacy_id': '30'},
'country', {'legacy_ams_taxonomy_id': '30', 'type': 'country', 'label': 'Brasilien', 'concept_id': 'KYRf_tqd_ndA', 'legacy_ams_taxonomy_num_id': 30}),

( {'type': 'country', 'id': 'DtaS_uBt_nvb', 'preferred_label': 'Brittiska Indiska Oceanöarna', 'deprecated_legacy_id': '31'},
'country', {'legacy_ams_taxonomy_id': '31', 'type': 'country', 'label': 'Brittiska Indiska Oceanöarna', 'concept_id': 'DtaS_uBt_nvb', 'legacy_ams_taxonomy_num_id': 31}),

( {'type': 'country', 'id': 'tttG_4rv_nRZ', 'preferred_label': 'Brunei Darussalam', 'deprecated_legacy_id': '32'},
'country', {'legacy_ams_taxonomy_id': '32', 'type': 'country', 'label': 'Brunei Darussalam', 'concept_id': 'tttG_4rv_nRZ', 'legacy_ams_taxonomy_num_id': 32}),

( {'type': 'country', 'id': 'HnDR_EFB_MBp', 'preferred_label': 'Bulgarien', 'deprecated_legacy_id': '33'},
'country', {'legacy_ams_taxonomy_id': '33', 'type': 'country', 'label': 'Bulgarien', 'concept_id': 'HnDR_EFB_MBp', 'legacy_ams_taxonomy_num_id': 33}),

( {'type': 'country', 'id': '5huD_yut_1ro', 'preferred_label': 'Burkina Faso', 'deprecated_legacy_id': '34'},
'country', {'legacy_ams_taxonomy_id': '34', 'type': 'country', 'label': 'Burkina Faso', 'concept_id': '5huD_yut_1ro', 'legacy_ams_taxonomy_num_id': 34}),

( {'type': 'country', 'id': '2vPv_RfT_FHq', 'preferred_label': 'Burundi', 'deprecated_legacy_id': '35'},
'country', {'legacy_ams_taxonomy_id': '35', 'type': 'country', 'label': 'Burundi', 'concept_id': '2vPv_RfT_FHq', 'legacy_ams_taxonomy_num_id': 35}),

( {'type': 'country', 'id': 'MS1s_AYg_kZe', 'preferred_label': 'Caymanöarna', 'deprecated_legacy_id': '36'},
'country', {'legacy_ams_taxonomy_id': '36', 'type': 'country', 'label': 'Caymanöarna', 'concept_id': 'MS1s_AYg_kZe', 'legacy_ams_taxonomy_num_id': 36}),

( {'type': 'country', 'id': 'Jon9_cAq_w2p', 'preferred_label': 'Centralafrikanska republiken', 'deprecated_legacy_id': '37'},
'country', {'legacy_ams_taxonomy_id': '37', 'type': 'country', 'label': 'Centralafrikanska republiken', 'concept_id': 'Jon9_cAq_w2p', 'legacy_ams_taxonomy_num_id': 37}),

( {'type': 'country', 'id': 'yoHy_NiM_Hxi', 'preferred_label': 'Chile', 'deprecated_legacy_id': '38'},
'country', {'legacy_ams_taxonomy_id': '38', 'type': 'country', 'label': 'Chile', 'concept_id': 'yoHy_NiM_Hxi', 'legacy_ams_taxonomy_num_id': 38}),

( {'type': 'country', 'id': 'w4Mh_AHu_aUe', 'preferred_label': 'Colombia', 'deprecated_legacy_id': '39'},
'country', {'legacy_ams_taxonomy_id': '39', 'type': 'country', 'label': 'Colombia', 'concept_id': 'w4Mh_AHu_aUe', 'legacy_ams_taxonomy_num_id': 39}),

( {'type': 'country', 'id': 'Bw5r_quJ_jGb', 'preferred_label': 'Comorerna', 'deprecated_legacy_id': '40'},
'country', {'legacy_ams_taxonomy_id': '40', 'type': 'country', 'label': 'Comorerna', 'concept_id': 'Bw5r_quJ_jGb', 'legacy_ams_taxonomy_num_id': 40}),

( {'type': 'country', 'id': 'gBuN_LH5_1sS', 'preferred_label': 'Cooköarna', 'deprecated_legacy_id': '41'},
'country', {'legacy_ams_taxonomy_id': '41', 'type': 'country', 'label': 'Cooköarna', 'concept_id': 'gBuN_LH5_1sS', 'legacy_ams_taxonomy_num_id': 41}),

( {'type': 'country', 'id': '5gYb_FrC_N5L', 'preferred_label': 'Costa Rica', 'deprecated_legacy_id': '42'},
'country', {'legacy_ams_taxonomy_id': '42', 'type': 'country', 'label': 'Costa Rica', 'concept_id': '5gYb_FrC_N5L', 'legacy_ams_taxonomy_num_id': 42}),

( {'type': 'country', 'id': 'EDUL_5Zh_4bg', 'preferred_label': 'Cypern', 'deprecated_legacy_id': '43'},
'country', {'legacy_ams_taxonomy_id': '43', 'type': 'country', 'label': 'Cypern', 'concept_id': 'EDUL_5Zh_4bg', 'legacy_ams_taxonomy_num_id': 43}),

( {'type': 'country', 'id': 'Co3h_1xq_Cwb', 'preferred_label': 'Danmark', 'deprecated_legacy_id': '44'},
'country', {'legacy_ams_taxonomy_id': '44', 'type': 'country', 'label': 'Danmark', 'concept_id': 'Co3h_1xq_Cwb', 'legacy_ams_taxonomy_num_id': 44}),

( {'type': 'country', 'id': 'AH2Q_RGM_hFj', 'preferred_label': 'Djibouti', 'deprecated_legacy_id': '45'},
'country', {'legacy_ams_taxonomy_id': '45', 'type': 'country', 'label': 'Djibouti', 'concept_id': 'AH2Q_RGM_hFj', 'legacy_ams_taxonomy_num_id': 45}),

( {'type': 'country', 'id': 'SPjh_8RR_Go5', 'preferred_label': 'Dominica', 'deprecated_legacy_id': '46'},
'country', {'legacy_ams_taxonomy_id': '46', 'type': 'country', 'label': 'Dominica', 'concept_id': 'SPjh_8RR_Go5', 'legacy_ams_taxonomy_num_id': 46}),

( {'type': 'country', 'id': 'veGZ_ph6_D2z', 'preferred_label': 'Dominikanska republiken', 'deprecated_legacy_id': '47'},
'country', {'legacy_ams_taxonomy_id': '47', 'type': 'country', 'label': 'Dominikanska republiken', 'concept_id': 'veGZ_ph6_D2z', 'legacy_ams_taxonomy_num_id': 47}),

( {'type': 'country', 'id': 'prLG_gW1_iya', 'preferred_label': 'Ecuador', 'deprecated_legacy_id': '48'},
'country', {'legacy_ams_taxonomy_id': '48', 'type': 'country', 'label': 'Ecuador', 'concept_id': 'prLG_gW1_iya', 'legacy_ams_taxonomy_num_id': 48}),

( {'type': 'country', 'id': 'Q39b_6A6_feS', 'preferred_label': 'Egypten', 'deprecated_legacy_id': '49'},
'country', {'legacy_ams_taxonomy_id': '49', 'type': 'country', 'label': 'Egypten', 'concept_id': 'Q39b_6A6_feS', 'legacy_ams_taxonomy_num_id': 49}),

( {'type': 'country', 'id': 'N2MB_xDC_Dxk', 'preferred_label': 'Ekvatorialguinea', 'deprecated_legacy_id': '50'},
'country', {'legacy_ams_taxonomy_id': '50', 'type': 'country', 'label': 'Ekvatorialguinea', 'concept_id': 'N2MB_xDC_Dxk', 'legacy_ams_taxonomy_num_id': 50}),

( {'type': 'country', 'id': '2pi1_At7_eS8', 'preferred_label': 'El Salvador', 'deprecated_legacy_id': '51'},
'country', {'legacy_ams_taxonomy_id': '51', 'type': 'country', 'label': 'El Salvador', 'concept_id': '2pi1_At7_eS8', 'legacy_ams_taxonomy_num_id': 51}),

( {'type': 'country', 'id': 'caeu_xU5_Pr2', 'preferred_label': 'Elfenbenskusten', 'deprecated_legacy_id': '52'},
'country', {'legacy_ams_taxonomy_id': '52', 'type': 'country', 'label': 'Elfenbenskusten', 'concept_id': 'caeu_xU5_Pr2', 'legacy_ams_taxonomy_num_id': 52}),

( {'type': 'country', 'id': 'pyTe_kTE_QkJ', 'preferred_label': 'Eritrea', 'deprecated_legacy_id': '53'},
'country', {'legacy_ams_taxonomy_id': '53', 'type': 'country', 'label': 'Eritrea', 'concept_id': 'pyTe_kTE_QkJ', 'legacy_ams_taxonomy_num_id': 53}),

( {'type': 'country', 'id': 'EtpD_2Kn_dEP', 'preferred_label': 'Estland', 'deprecated_legacy_id': '54'},
'country', {'legacy_ams_taxonomy_id': '54', 'type': 'country', 'label': 'Estland', 'concept_id': 'EtpD_2Kn_dEP', 'legacy_ams_taxonomy_num_id': 54}),

( {'type': 'country', 'id': '3o66_NRC_qxC', 'preferred_label': 'Etiopien', 'deprecated_legacy_id': '55'},
'country', {'legacy_ams_taxonomy_id': '55', 'type': 'country', 'label': 'Etiopien', 'concept_id': '3o66_NRC_qxC', 'legacy_ams_taxonomy_num_id': 55}),

( {'type': 'country', 'id': '3UTC_vx3_ivC', 'preferred_label': 'Falklandsöarna', 'deprecated_legacy_id': '56'},
'country', {'legacy_ams_taxonomy_id': '56', 'type': 'country', 'label': 'Falklandsöarna', 'concept_id': '3UTC_vx3_ivC', 'legacy_ams_taxonomy_num_id': 56}),

( {'type': 'country', 'id': 'S2GW_3zC_U1C', 'preferred_label': 'Fijiöarna', 'deprecated_legacy_id': '57'},
'country', {'legacy_ams_taxonomy_id': '57', 'type': 'country', 'label': 'Fijiöarna', 'concept_id': 'S2GW_3zC_U1C', 'legacy_ams_taxonomy_num_id': 57}),

( {'type': 'country', 'id': '3PiS_KrW_bRW', 'preferred_label': 'Filippinerna', 'deprecated_legacy_id': '58'},
'country', {'legacy_ams_taxonomy_id': '58', 'type': 'country', 'label': 'Filippinerna', 'concept_id': '3PiS_KrW_bRW', 'legacy_ams_taxonomy_num_id': 58}),

( {'type': 'country', 'id': 'WgbG_whJ_E7J', 'preferred_label': 'Finland', 'deprecated_legacy_id': '59'},
'country', {'legacy_ams_taxonomy_id': '59', 'type': 'country', 'label': 'Finland', 'concept_id': 'WgbG_whJ_E7J', 'legacy_ams_taxonomy_num_id': 59}),

( {'type': 'country', 'id': 'GN3D_B21_gpD', 'preferred_label': 'Frankrike', 'deprecated_legacy_id': '60'},
'country', {'legacy_ams_taxonomy_id': '60', 'type': 'country', 'label': 'Frankrike', 'concept_id': 'GN3D_B21_gpD', 'legacy_ams_taxonomy_num_id': 60}),

( {'type': 'country', 'id': 'MZtV_bG4_drn', 'preferred_label': 'Guyana', 'deprecated_legacy_id': '62'},
'country', {'legacy_ams_taxonomy_id': '62', 'type': 'country', 'label': 'Guyana', 'concept_id': 'MZtV_bG4_drn', 'legacy_ams_taxonomy_num_id': 62}),

( {'type': 'country', 'id': 'FF6b_T3q_35V', 'preferred_label': 'Franska Polynesien', 'deprecated_legacy_id': '63'},
'country', {'legacy_ams_taxonomy_id': '63', 'type': 'country', 'label': 'Franska Polynesien', 'concept_id': 'FF6b_T3q_35V', 'legacy_ams_taxonomy_num_id': 63}),

( {'type': 'country', 'id': 'Vn3X_9zH_2yn', 'preferred_label': 'Franska Sydterritorierna', 'deprecated_legacy_id': '64'},
'country', {'legacy_ams_taxonomy_id': '64', 'type': 'country', 'label': 'Franska Sydterritorierna', 'concept_id': 'Vn3X_9zH_2yn', 'legacy_ams_taxonomy_num_id': 64}),

( {'type': 'country', 'id': 'QKFb_bvZ_gNY', 'preferred_label': 'Färöarna', 'deprecated_legacy_id': '65'},
'country', {'legacy_ams_taxonomy_id': '65', 'type': 'country', 'label': 'Färöarna', 'concept_id': 'QKFb_bvZ_gNY', 'legacy_ams_taxonomy_num_id': 65}),

( {'type': 'country', 'id': 'FY6e_EBz_sqn', 'preferred_label': 'Gabon', 'deprecated_legacy_id': '66'},
'country', {'legacy_ams_taxonomy_id': '66', 'type': 'country', 'label': 'Gabon', 'concept_id': 'FY6e_EBz_sqn', 'legacy_ams_taxonomy_num_id': 66}),

( {'type': 'country', 'id': 'vbtb_ju1_kUX', 'preferred_label': 'Gambia', 'deprecated_legacy_id': '67'},
'country', {'legacy_ams_taxonomy_id': '67', 'type': 'country', 'label': 'Gambia', 'concept_id': 'vbtb_ju1_kUX', 'legacy_ams_taxonomy_num_id': 67}),

( {'type': 'country', 'id': '7DEB_o2m_bc2', 'preferred_label': 'Georgien', 'deprecated_legacy_id': '68'},
'country', {'legacy_ams_taxonomy_id': '68', 'type': 'country', 'label': 'Georgien', 'concept_id': '7DEB_o2m_bc2', 'legacy_ams_taxonomy_num_id': 68}),

( {'type': 'country', 'id': 'Qgi9_tqh_b2X', 'preferred_label': 'Ghana', 'deprecated_legacy_id': '69'},
'country', {'legacy_ams_taxonomy_id': '69', 'type': 'country', 'label': 'Ghana', 'concept_id': 'Qgi9_tqh_b2X', 'legacy_ams_taxonomy_num_id': 69}),

( {'type': 'country', 'id': '9Lj6_Lrk_83M', 'preferred_label': 'Gibraltar', 'deprecated_legacy_id': '70'},
'country', {'legacy_ams_taxonomy_id': '70', 'type': 'country', 'label': 'Gibraltar', 'concept_id': '9Lj6_Lrk_83M', 'legacy_ams_taxonomy_num_id': 70}),

( {'type': 'country', 'id': 'BRi4_in4_xLc', 'preferred_label': 'Grekland', 'deprecated_legacy_id': '71'},
'country', {'legacy_ams_taxonomy_id': '71', 'type': 'country', 'label': 'Grekland', 'concept_id': 'BRi4_in4_xLc', 'legacy_ams_taxonomy_num_id': 71}),

( {'type': 'country', 'id': 't9U9_e8w_oQi', 'preferred_label': 'Grenada', 'deprecated_legacy_id': '72'},
'country', {'legacy_ams_taxonomy_id': '72', 'type': 'country', 'label': 'Grenada', 'concept_id': 't9U9_e8w_oQi', 'legacy_ams_taxonomy_num_id': 72}),

( {'type': 'country', 'id': '6JQy_Wpf_8RK', 'preferred_label': 'Grönland', 'deprecated_legacy_id': '73'},
'country', {'legacy_ams_taxonomy_id': '73', 'type': 'country', 'label': 'Grönland', 'concept_id': '6JQy_Wpf_8RK', 'legacy_ams_taxonomy_num_id': 73}),

( {'type': 'country', 'id': 'o8tZ_1V3_5do', 'preferred_label': 'Guadeloupe', 'deprecated_legacy_id': '74'},
'country', {'legacy_ams_taxonomy_id': '74', 'type': 'country', 'label': 'Guadeloupe', 'concept_id': 'o8tZ_1V3_5do', 'legacy_ams_taxonomy_num_id': 74}),

( {'type': 'country', 'id': 'UxZz_53F_fcb', 'preferred_label': 'Guam', 'deprecated_legacy_id': '75'},
'country', {'legacy_ams_taxonomy_id': '75', 'type': 'country', 'label': 'Guam', 'concept_id': 'UxZz_53F_fcb', 'legacy_ams_taxonomy_num_id': 75}),

( {'type': 'country', 'id': 'RxXm_KBi_sjp', 'preferred_label': 'Guatemala', 'deprecated_legacy_id': '76'},
'country', {'legacy_ams_taxonomy_id': '76', 'type': 'country', 'label': 'Guatemala', 'concept_id': 'RxXm_KBi_sjp', 'legacy_ams_taxonomy_num_id': 76}),

( {'type': 'country', 'id': 'SYb7_4Rj_9SB', 'preferred_label': 'Guinea', 'deprecated_legacy_id': '77'},
'country', {'legacy_ams_taxonomy_id': '77', 'type': 'country', 'label': 'Guinea', 'concept_id': 'SYb7_4Rj_9SB', 'legacy_ams_taxonomy_num_id': 77}),

( {'type': 'country', 'id': 'BCcY_Z35_kS6', 'preferred_label': 'Guinea-Bissau', 'deprecated_legacy_id': '78'},
'country', {'legacy_ams_taxonomy_id': '78', 'type': 'country', 'label': 'Guinea-Bissau', 'concept_id': 'BCcY_Z35_kS6', 'legacy_ams_taxonomy_num_id': 78}),

( {'type': 'country', 'id': 'dKCz_1XX_ays', 'preferred_label': 'Haiti', 'deprecated_legacy_id': '80'},
'country', {'legacy_ams_taxonomy_id': '80', 'type': 'country', 'label': 'Haiti', 'concept_id': 'dKCz_1XX_ays', 'legacy_ams_taxonomy_num_id': 80}),

( {'type': 'country', 'id': '3XFZ_Hte_nch', 'preferred_label': 'Heardön och McDonaldöarna', 'deprecated_legacy_id': '81'},
'country', {'legacy_ams_taxonomy_id': '81', 'type': 'country', 'label': 'Heardön och McDonaldöarna', 'concept_id': '3XFZ_Hte_nch', 'legacy_ams_taxonomy_num_id': 81}),

( {'type': 'country', 'id': 'is8V_V6o_iyS', 'preferred_label': 'Honduras', 'deprecated_legacy_id': '82'},
'country', {'legacy_ams_taxonomy_id': '82', 'type': 'country', 'label': 'Honduras', 'concept_id': 'is8V_V6o_iyS', 'legacy_ams_taxonomy_num_id': 82}),

( {'type': 'country', 'id': 'H1JH_rYH_LjG', 'preferred_label': 'Hongkong', 'deprecated_legacy_id': '83'},
'country', {'legacy_ams_taxonomy_id': '83', 'type': 'country', 'label': 'Hongkong', 'concept_id': 'H1JH_rYH_LjG', 'legacy_ams_taxonomy_num_id': 83}),

( {'type': 'country', 'id': 'mCdG_xuR_qwD', 'preferred_label': 'Indien', 'deprecated_legacy_id': '84'},
'country', {'legacy_ams_taxonomy_id': '84', 'type': 'country', 'label': 'Indien', 'concept_id': 'mCdG_xuR_qwD', 'legacy_ams_taxonomy_num_id': 84}),

( {'type': 'country', 'id': 'St4p_G3F_MTj', 'preferred_label': 'Indonesien', 'deprecated_legacy_id': '85'},
'country', {'legacy_ams_taxonomy_id': '85', 'type': 'country', 'label': 'Indonesien', 'concept_id': 'St4p_G3F_MTj', 'legacy_ams_taxonomy_num_id': 85}),

( {'type': 'country', 'id': 'LQar_3Jn_BGU', 'preferred_label': 'Irak', 'deprecated_legacy_id': '86'},
'country', {'legacy_ams_taxonomy_id': '86', 'type': 'country', 'label': 'Irak', 'concept_id': 'LQar_3Jn_BGU', 'legacy_ams_taxonomy_num_id': 86}),

( {'type': 'country', 'id': 'wZw5_AY7_RMg', 'preferred_label': 'Iran', 'deprecated_legacy_id': '87'},
'country', {'legacy_ams_taxonomy_id': '87', 'type': 'country', 'label': 'Iran', 'concept_id': 'wZw5_AY7_RMg', 'legacy_ams_taxonomy_num_id': 87}),

( {'type': 'country', 'id': 'cnZp_bhJ_JCZ', 'preferred_label': 'Irland', 'deprecated_legacy_id': '88'},
'country', {'legacy_ams_taxonomy_id': '88', 'type': 'country', 'label': 'Irland', 'concept_id': 'cnZp_bhJ_JCZ', 'legacy_ams_taxonomy_num_id': 88}),

( {'type': 'country', 'id': 'ZDbX_yJc_zin', 'preferred_label': 'Island', 'deprecated_legacy_id': '89'},
'country', {'legacy_ams_taxonomy_id': '89', 'type': 'country', 'label': 'Island', 'concept_id': 'ZDbX_yJc_zin', 'legacy_ams_taxonomy_num_id': 89}),

( {'type': 'country', 'id': 'L2vN_3YW_6jk', 'preferred_label': 'Israel', 'deprecated_legacy_id': '90'},
'country', {'legacy_ams_taxonomy_id': '90', 'type': 'country', 'label': 'Israel', 'concept_id': 'L2vN_3YW_6jk', 'legacy_ams_taxonomy_num_id': 90}),

( {'type': 'country', 'id': 'yTER_v5s_onb', 'preferred_label': 'Italien', 'deprecated_legacy_id': '91'},
'country', {'legacy_ams_taxonomy_id': '91', 'type': 'country', 'label': 'Italien', 'concept_id': 'yTER_v5s_onb', 'legacy_ams_taxonomy_num_id': 91}),

( {'type': 'country', 'id': 'zJ4U_hmd_yei', 'preferred_label': 'Jamaica', 'deprecated_legacy_id': '92'},
'country', {'legacy_ams_taxonomy_id': '92', 'type': 'country', 'label': 'Jamaica', 'concept_id': 'zJ4U_hmd_yei', 'legacy_ams_taxonomy_num_id': 92}),

( {'type': 'country', 'id': 'DBob_NQg_ayR', 'preferred_label': 'Japan', 'deprecated_legacy_id': '93'},
'country', {'legacy_ams_taxonomy_id': '93', 'type': 'country', 'label': 'Japan', 'concept_id': 'DBob_NQg_ayR', 'legacy_ams_taxonomy_num_id': 93}),

( {'type': 'country', 'id': 'nXoR_hvq_TLr', 'preferred_label': 'Jordanien', 'deprecated_legacy_id': '94'},
'country', {'legacy_ams_taxonomy_id': '94', 'type': 'country', 'label': 'Jordanien', 'concept_id': 'nXoR_hvq_TLr', 'legacy_ams_taxonomy_num_id': 94}),

( {'type': 'country', 'id': 'BZuK_352_yAD', 'preferred_label': 'Julön', 'deprecated_legacy_id': '96'},
'country', {'legacy_ams_taxonomy_id': '96', 'type': 'country', 'label': 'Julön', 'concept_id': 'BZuK_352_yAD', 'legacy_ams_taxonomy_num_id': 96}),

( {'type': 'country', 'id': 'cr4o_3L8_hoa', 'preferred_label': 'Jungfruöarna, Brittiska', 'deprecated_legacy_id': '97'},
'country', {'legacy_ams_taxonomy_id': '97', 'type': 'country', 'label': 'Jungfruöarna, Brittiska', 'concept_id': 'cr4o_3L8_hoa', 'legacy_ams_taxonomy_num_id': 97}),

( {'type': 'country', 'id': 's4H2_8bX_V5G', 'preferred_label': 'Jungfruöarna, Förenta Staternas', 'deprecated_legacy_id': '98'},
'country', {'legacy_ams_taxonomy_id': '98', 'type': 'country', 'label': 'Jungfruöarna, Förenta Staternas', 'concept_id': 's4H2_8bX_V5G', 'legacy_ams_taxonomy_num_id': 98}),

( {'type': 'country', 'id': 'a5yX_nAP_GVu', 'preferred_label': 'Kambodja', 'deprecated_legacy_id': '99'},
'country', {'legacy_ams_taxonomy_id': '99', 'type': 'country', 'label': 'Kambodja', 'concept_id': 'a5yX_nAP_GVu', 'legacy_ams_taxonomy_num_id': 99}),

( {'type': 'country', 'id': 'Gx97_r5T_5vJ', 'preferred_label': 'Kamerun', 'deprecated_legacy_id': '100'},
'country', {'legacy_ams_taxonomy_id': '100', 'type': 'country', 'label': 'Kamerun', 'concept_id': 'Gx97_r5T_5vJ', 'legacy_ams_taxonomy_num_id': 100}),

( {'type': 'country', 'id': '4pSd_nND_Buv', 'preferred_label': 'Kanada', 'deprecated_legacy_id': '101'},
'country', {'legacy_ams_taxonomy_id': '101', 'type': 'country', 'label': 'Kanada', 'concept_id': '4pSd_nND_Buv', 'legacy_ams_taxonomy_num_id': 101}),

( {'type': 'country', 'id': '7hV5_ej6_4Ry', 'preferred_label': 'Kap Verde', 'deprecated_legacy_id': '102'},
'country', {'legacy_ams_taxonomy_id': '102', 'type': 'country', 'label': 'Kap Verde', 'concept_id': '7hV5_ej6_4Ry', 'legacy_ams_taxonomy_num_id': 102}),

( {'type': 'country', 'id': 'Jf5g_omQ_YVY', 'preferred_label': 'Kazakstan', 'deprecated_legacy_id': '103'},
'country', {'legacy_ams_taxonomy_id': '103', 'type': 'country', 'label': 'Kazakstan', 'concept_id': 'Jf5g_omQ_YVY', 'legacy_ams_taxonomy_num_id': 103}),

( {'type': 'country', 'id': 'RvV6_URK_zjd', 'preferred_label': 'Kenya', 'deprecated_legacy_id': '104'},
'country', {'legacy_ams_taxonomy_id': '104', 'type': 'country', 'label': 'Kenya', 'concept_id': 'RvV6_URK_zjd', 'legacy_ams_taxonomy_num_id': 104}),

( {'type': 'country', 'id': 'hwyG_7pq_yHR', 'preferred_label': 'Kina', 'deprecated_legacy_id': '105'},
'country', {'legacy_ams_taxonomy_id': '105', 'type': 'country', 'label': 'Kina', 'concept_id': 'hwyG_7pq_yHR', 'legacy_ams_taxonomy_num_id': 105}),

( {'type': 'country', 'id': '6uZH_6X8_8u4', 'preferred_label': 'Kirgizistan', 'deprecated_legacy_id': '106'},
'country', {'legacy_ams_taxonomy_id': '106', 'type': 'country', 'label': 'Kirgizistan', 'concept_id': '6uZH_6X8_8u4', 'legacy_ams_taxonomy_num_id': 106}),

( {'type': 'country', 'id': 'YoNM_eCx_zat', 'preferred_label': 'Kiribati', 'deprecated_legacy_id': '107'},
'country', {'legacy_ams_taxonomy_id': '107', 'type': 'country', 'label': 'Kiribati', 'concept_id': 'YoNM_eCx_zat', 'legacy_ams_taxonomy_num_id': 107}),

( {'type': 'country', 'id': 'UZKB_8TU_Hkn', 'preferred_label': 'Kokosöarna', 'deprecated_legacy_id': '108'},
'country', {'legacy_ams_taxonomy_id': '108', 'type': 'country', 'label': 'Kokosöarna', 'concept_id': 'UZKB_8TU_Hkn', 'legacy_ams_taxonomy_num_id': 108}),

( {'type': 'country', 'id': 'Rgpn_h5Y_u6N', 'preferred_label': 'Kongo, republiken', 'deprecated_legacy_id': '109'},
'country', {'legacy_ams_taxonomy_id': '109', 'type': 'country', 'label': 'Kongo, republiken', 'concept_id': 'Rgpn_h5Y_u6N', 'legacy_ams_taxonomy_num_id': 109}),

( {'type': 'country', 'id': 'STUC_FzH_Ah2', 'preferred_label': 'Nordkorea', 'deprecated_legacy_id': '110'},
'country', {'legacy_ams_taxonomy_id': '110', 'type': 'country', 'label': 'Nordkorea', 'concept_id': 'STUC_FzH_Ah2', 'legacy_ams_taxonomy_num_id': 110}),

( {'type': 'country', 'id': '1dcY_7Gr_ucB', 'preferred_label': 'Sydkorea', 'deprecated_legacy_id': '111'},
'country', {'legacy_ams_taxonomy_id': '111', 'type': 'country', 'label': 'Sydkorea', 'concept_id': '1dcY_7Gr_ucB', 'legacy_ams_taxonomy_num_id': 111}),

( {'type': 'country', 'id': 'kzWw_cdp_PhU', 'preferred_label': 'Kroatien', 'deprecated_legacy_id': '112'},
'country', {'legacy_ams_taxonomy_id': '112', 'type': 'country', 'label': 'Kroatien', 'concept_id': 'kzWw_cdp_PhU', 'legacy_ams_taxonomy_num_id': 112}),

( {'type': 'country', 'id': 'BAGM_XF7_FpH', 'preferred_label': 'Kuba', 'deprecated_legacy_id': '113'},
'country', {'legacy_ams_taxonomy_id': '113', 'type': 'country', 'label': 'Kuba', 'concept_id': 'BAGM_XF7_FpH', 'legacy_ams_taxonomy_num_id': 113}),

( {'type': 'country', 'id': 'NJTY_G1b_vc6', 'preferred_label': 'Kuwait', 'deprecated_legacy_id': '114'},
'country', {'legacy_ams_taxonomy_id': '114', 'type': 'country', 'label': 'Kuwait', 'concept_id': 'NJTY_G1b_vc6', 'legacy_ams_taxonomy_num_id': 114}),

( {'type': 'country', 'id': 'invg_zpk_bHR', 'preferred_label': 'Laos', 'deprecated_legacy_id': '115'},
'country', {'legacy_ams_taxonomy_id': '115', 'type': 'country', 'label': 'Laos', 'concept_id': 'invg_zpk_bHR', 'legacy_ams_taxonomy_num_id': 115}),

( {'type': 'country', 'id': 'QGD8_mTz_JdA', 'preferred_label': 'Lesotho', 'deprecated_legacy_id': '116'},
'country', {'legacy_ams_taxonomy_id': '116', 'type': 'country', 'label': 'Lesotho', 'concept_id': 'QGD8_mTz_JdA', 'legacy_ams_taxonomy_num_id': 116}),

( {'type': 'country', 'id': 'FcoH_7D1_p8H', 'preferred_label': 'Lettland', 'deprecated_legacy_id': '117'},
'country', {'legacy_ams_taxonomy_id': '117', 'type': 'country', 'label': 'Lettland', 'concept_id': 'FcoH_7D1_p8H', 'legacy_ams_taxonomy_num_id': 117}),

( {'type': 'country', 'id': '7P48_RTM_NwH', 'preferred_label': 'Libanon', 'deprecated_legacy_id': '118'},
'country', {'legacy_ams_taxonomy_id': '118', 'type': 'country', 'label': 'Libanon', 'concept_id': '7P48_RTM_NwH', 'legacy_ams_taxonomy_num_id': 118}),

( {'type': 'country', 'id': '5Lt9_xKU_6cC', 'preferred_label': 'Liberia', 'deprecated_legacy_id': '119'},
'country', {'legacy_ams_taxonomy_id': '119', 'type': 'country', 'label': 'Liberia', 'concept_id': '5Lt9_xKU_6cC', 'legacy_ams_taxonomy_num_id': 119}),

( {'type': 'country', 'id': 'ZfuN_DVH_t2R', 'preferred_label': 'Libyen', 'deprecated_legacy_id': '120'},
'country', {'legacy_ams_taxonomy_id': '120', 'type': 'country', 'label': 'Libyen', 'concept_id': 'ZfuN_DVH_t2R', 'legacy_ams_taxonomy_num_id': 120}),

( {'type': 'country', 'id': 'dL9B_ubn_9KN', 'preferred_label': 'Liechtenstein', 'deprecated_legacy_id': '121'},
'country', {'legacy_ams_taxonomy_id': '121', 'type': 'country', 'label': 'Liechtenstein', 'concept_id': 'dL9B_ubn_9KN', 'legacy_ams_taxonomy_num_id': 121}),

( {'type': 'country', 'id': '37nR_YtF_158', 'preferred_label': 'Litauen', 'deprecated_legacy_id': '122'},
'country', {'legacy_ams_taxonomy_id': '122', 'type': 'country', 'label': 'Litauen', 'concept_id': '37nR_YtF_158', 'legacy_ams_taxonomy_num_id': 122}),

( {'type': 'country', 'id': 'eh3o_JnT_ZU7', 'preferred_label': 'Luxemburg', 'deprecated_legacy_id': '123'},
'country', {'legacy_ams_taxonomy_id': '123', 'type': 'country', 'label': 'Luxemburg', 'concept_id': 'eh3o_JnT_ZU7', 'legacy_ams_taxonomy_num_id': 123}),

( {'type': 'country', 'id': 'PRSy_b5B_piv', 'preferred_label': 'Macao', 'deprecated_legacy_id': '124'},
'country', {'legacy_ams_taxonomy_id': '124', 'type': 'country', 'label': 'Macao', 'concept_id': 'PRSy_b5B_piv', 'legacy_ams_taxonomy_num_id': 124}),

( {'type': 'country', 'id': 'Y3iU_QDa_5Js', 'preferred_label': 'Madagaskar', 'deprecated_legacy_id': '125'},
'country', {'legacy_ams_taxonomy_id': '125', 'type': 'country', 'label': 'Madagaskar', 'concept_id': 'Y3iU_QDa_5Js', 'legacy_ams_taxonomy_num_id': 125}),

( {'type': 'country', 'id': 'T3Ui_pa1_2Wc', 'preferred_label': 'Malawi', 'deprecated_legacy_id': '126'},
'country', {'legacy_ams_taxonomy_id': '126', 'type': 'country', 'label': 'Malawi', 'concept_id': 'T3Ui_pa1_2Wc', 'legacy_ams_taxonomy_num_id': 126}),

( {'type': 'country', 'id': 'MRfa_jpt_dmY', 'preferred_label': 'Malaysia', 'deprecated_legacy_id': '127'},
'country', {'legacy_ams_taxonomy_id': '127', 'type': 'country', 'label': 'Malaysia', 'concept_id': 'MRfa_jpt_dmY', 'legacy_ams_taxonomy_num_id': 127}),

( {'type': 'country', 'id': 'yrE7_cE9_cVF', 'preferred_label': 'Maldiverna', 'deprecated_legacy_id': '128'},
'country', {'legacy_ams_taxonomy_id': '128', 'type': 'country', 'label': 'Maldiverna', 'concept_id': 'yrE7_cE9_cVF', 'legacy_ams_taxonomy_num_id': 128}),

( {'type': 'country', 'id': 'dLVN_jSb_YjF', 'preferred_label': 'Mali', 'deprecated_legacy_id': '129'},
'country', {'legacy_ams_taxonomy_id': '129', 'type': 'country', 'label': 'Mali', 'concept_id': 'dLVN_jSb_YjF', 'legacy_ams_taxonomy_num_id': 129}),

( {'type': 'country', 'id': 'u8qF_qpq_R5W', 'preferred_label': 'Malta', 'deprecated_legacy_id': '130'},
'country', {'legacy_ams_taxonomy_id': '130', 'type': 'country', 'label': 'Malta', 'concept_id': 'u8qF_qpq_R5W', 'legacy_ams_taxonomy_num_id': 130}),

( {'type': 'country', 'id': 'wJGU_XXo_28H', 'preferred_label': 'Marocko', 'deprecated_legacy_id': '131'},
'country', {'legacy_ams_taxonomy_id': '131', 'type': 'country', 'label': 'Marocko', 'concept_id': 'wJGU_XXo_28H', 'legacy_ams_taxonomy_num_id': 131}),

( {'type': 'country', 'id': 'mdMy_Gru_gjg', 'preferred_label': 'Marshallöarna', 'deprecated_legacy_id': '132'},
'country', {'legacy_ams_taxonomy_id': '132', 'type': 'country', 'label': 'Marshallöarna', 'concept_id': 'mdMy_Gru_gjg', 'legacy_ams_taxonomy_num_id': 132}),

( {'type': 'country', 'id': 'H5my_Ues_ya3', 'preferred_label': 'Martinique', 'deprecated_legacy_id': '133'},
'country', {'legacy_ams_taxonomy_id': '133', 'type': 'country', 'label': 'Martinique', 'concept_id': 'H5my_Ues_ya3', 'legacy_ams_taxonomy_num_id': 133}),

( {'type': 'country', 'id': '6Er6_yt8_Kzk', 'preferred_label': 'Mauretanien', 'deprecated_legacy_id': '134'},
'country', {'legacy_ams_taxonomy_id': '134', 'type': 'country', 'label': 'Mauretanien', 'concept_id': '6Er6_yt8_Kzk', 'legacy_ams_taxonomy_num_id': 134}),

( {'type': 'country', 'id': 'wcHy_zcL_tUt', 'preferred_label': 'Mauritius', 'deprecated_legacy_id': '135'},
'country', {'legacy_ams_taxonomy_id': '135', 'type': 'country', 'label': 'Mauritius', 'concept_id': 'wcHy_zcL_tUt', 'legacy_ams_taxonomy_num_id': 135}),

( {'type': 'country', 'id': 'aBL1_wh8_qgu', 'preferred_label': 'Mayotte', 'deprecated_legacy_id': '136'},
'country', {'legacy_ams_taxonomy_id': '136', 'type': 'country', 'label': 'Mayotte', 'concept_id': 'aBL1_wh8_qgu', 'legacy_ams_taxonomy_num_id': 136}),

( {'type': 'country', 'id': 'T7Dy_wGz_9vv', 'preferred_label': 'Mexiko/Mexikos förenta stater', 'deprecated_legacy_id': '137'},
'country', {'legacy_ams_taxonomy_id': '137', 'type': 'country', 'label': 'Mexiko/Mexikos förenta stater', 'concept_id': 'T7Dy_wGz_9vv', 'legacy_ams_taxonomy_num_id': 137}),

( {'type': 'country', 'id': 'NXE2_wDm_jtg', 'preferred_label': 'Mikronesiska federationen', 'deprecated_legacy_id': '138'},
'country', {'legacy_ams_taxonomy_id': '138', 'type': 'country', 'label': 'Mikronesiska federationen', 'concept_id': 'NXE2_wDm_jtg', 'legacy_ams_taxonomy_num_id': 138}),

( {'type': 'country', 'id': 'LXPi_qGh_dXN', 'preferred_label': 'Moçambique', 'deprecated_legacy_id': '139'},
'country', {'legacy_ams_taxonomy_id': '139', 'type': 'country', 'label': 'Moçambique', 'concept_id': 'LXPi_qGh_dXN', 'legacy_ams_taxonomy_num_id': 139}),

( {'type': 'country', 'id': 'g8EG_tGy_Y9h', 'preferred_label': 'Moldavien', 'deprecated_legacy_id': '140'},
'country', {'legacy_ams_taxonomy_id': '140', 'type': 'country', 'label': 'Moldavien', 'concept_id': 'g8EG_tGy_Y9h', 'legacy_ams_taxonomy_num_id': 140}),

( {'type': 'country', 'id': 'Rhid_ebP_t7s', 'preferred_label': 'Monaco', 'deprecated_legacy_id': '141'},
'country', {'legacy_ams_taxonomy_id': '141', 'type': 'country', 'label': 'Monaco', 'concept_id': 'Rhid_ebP_t7s', 'legacy_ams_taxonomy_num_id': 141}),

( {'type': 'country', 'id': 'hGXE_Keq_uhh', 'preferred_label': 'Mongoliet', 'deprecated_legacy_id': '142'},
'country', {'legacy_ams_taxonomy_id': '142', 'type': 'country', 'label': 'Mongoliet', 'concept_id': 'hGXE_Keq_uhh', 'legacy_ams_taxonomy_num_id': 142}),

( {'type': 'country', 'id': 'BVxK_CPb_XDd', 'preferred_label': 'Montserrat', 'deprecated_legacy_id': '143'},
'country', {'legacy_ams_taxonomy_id': '143', 'type': 'country', 'label': 'Montserrat', 'concept_id': 'BVxK_CPb_XDd', 'legacy_ams_taxonomy_num_id': 143}),

( {'type': 'country', 'id': 'h8Nc_4cm_SVk', 'preferred_label': 'Myanmar (Burma)', 'deprecated_legacy_id': '144'},
'country', {'legacy_ams_taxonomy_id': '144', 'type': 'country', 'label': 'Myanmar (Burma)', 'concept_id': 'h8Nc_4cm_SVk', 'legacy_ams_taxonomy_num_id': 144}),

( {'type': 'country', 'id': 'UvNn_GZf_GKx', 'preferred_label': 'Namibia', 'deprecated_legacy_id': '145'},
'country', {'legacy_ams_taxonomy_id': '145', 'type': 'country', 'label': 'Namibia', 'concept_id': 'UvNn_GZf_GKx', 'legacy_ams_taxonomy_num_id': 145}),

( {'type': 'country', 'id': 'nrBz_LjD_fC2', 'preferred_label': 'Nauru', 'deprecated_legacy_id': '146'},
'country', {'legacy_ams_taxonomy_id': '146', 'type': 'country', 'label': 'Nauru', 'concept_id': 'nrBz_LjD_fC2', 'legacy_ams_taxonomy_num_id': 146}),

( {'type': 'country', 'id': 'mc3v_dHh_dzf', 'preferred_label': 'Nederländerna', 'deprecated_legacy_id': '147'},
'country', {'legacy_ams_taxonomy_id': '147', 'type': 'country', 'label': 'Nederländerna', 'concept_id': 'mc3v_dHh_dzf', 'legacy_ams_taxonomy_num_id': 147}),

( {'type': 'country', 'id': 'YfAA_stA_Wim', 'preferred_label': 'Nepal', 'deprecated_legacy_id': '149'},
'country', {'legacy_ams_taxonomy_id': '149', 'type': 'country', 'label': 'Nepal', 'concept_id': 'YfAA_stA_Wim', 'legacy_ams_taxonomy_num_id': 149}),

( {'type': 'country', 'id': '9aCc_E74_GW4', 'preferred_label': 'Nicaragua', 'deprecated_legacy_id': '150'},
'country', {'legacy_ams_taxonomy_id': '150', 'type': 'country', 'label': 'Nicaragua', 'concept_id': '9aCc_E74_GW4', 'legacy_ams_taxonomy_num_id': 150}),

( {'type': 'country', 'id': '6mBR_Vzm_hEy', 'preferred_label': 'Niger', 'deprecated_legacy_id': '151'},
'country', {'legacy_ams_taxonomy_id': '151', 'type': 'country', 'label': 'Niger', 'concept_id': '6mBR_Vzm_hEy', 'legacy_ams_taxonomy_num_id': 151}),

( {'type': 'country', 'id': 'pUgR_4E5_Lcb', 'preferred_label': 'Nigeria', 'deprecated_legacy_id': '152'},
'country', {'legacy_ams_taxonomy_id': '152', 'type': 'country', 'label': 'Nigeria', 'concept_id': 'pUgR_4E5_Lcb', 'legacy_ams_taxonomy_num_id': 152}),

( {'type': 'country', 'id': 'iW9P_8kt_BSS', 'preferred_label': 'Niue', 'deprecated_legacy_id': '153'},
'country', {'legacy_ams_taxonomy_id': '153', 'type': 'country', 'label': 'Niue', 'concept_id': 'iW9P_8kt_BSS', 'legacy_ams_taxonomy_num_id': 153}),

( {'type': 'country', 'id': 'JawQ_qCy_XVW', 'preferred_label': 'Norfolkön', 'deprecated_legacy_id': '154'},
'country', {'legacy_ams_taxonomy_id': '154', 'type': 'country', 'label': 'Norfolkön', 'concept_id': 'JawQ_qCy_XVW', 'legacy_ams_taxonomy_num_id': 154}),

( {'type': 'country', 'id': 'QJgN_Zge_BzJ', 'preferred_label': 'Norge', 'deprecated_legacy_id': '155'},
'country', {'legacy_ams_taxonomy_id': '155', 'type': 'country', 'label': 'Norge', 'concept_id': 'QJgN_Zge_BzJ', 'legacy_ams_taxonomy_num_id': 155}),

( {'type': 'country', 'id': 'KCpz_RKA_EYu', 'preferred_label': 'Norra Marianaöarna', 'deprecated_legacy_id': '156'},
'country', {'legacy_ams_taxonomy_id': '156', 'type': 'country', 'label': 'Norra Marianaöarna', 'concept_id': 'KCpz_RKA_EYu', 'legacy_ams_taxonomy_num_id': 156}),

( {'type': 'country', 'id': 'FELd_2Jy_ZCP', 'preferred_label': 'Nya Kaledonien', 'deprecated_legacy_id': '157'},
'country', {'legacy_ams_taxonomy_id': '157', 'type': 'country', 'label': 'Nya Kaledonien', 'concept_id': 'FELd_2Jy_ZCP', 'legacy_ams_taxonomy_num_id': 157}),

( {'type': 'country', 'id': 'Ue2v_DjR_ANf', 'preferred_label': 'Nya Zeeland', 'deprecated_legacy_id': '158'},
'country', {'legacy_ams_taxonomy_id': '158', 'type': 'country', 'label': 'Nya Zeeland', 'concept_id': 'Ue2v_DjR_ANf', 'legacy_ams_taxonomy_num_id': 158}),

( {'type': 'country', 'id': 'XHsS_76L_o2s', 'preferred_label': 'Oman', 'deprecated_legacy_id': '159'},
'country', {'legacy_ams_taxonomy_id': '159', 'type': 'country', 'label': 'Oman', 'concept_id': 'XHsS_76L_o2s', 'legacy_ams_taxonomy_num_id': 159}),

( {'type': 'country', 'id': 'P42X_cky_m8v', 'preferred_label': 'Pakistan', 'deprecated_legacy_id': '160'},
'country', {'legacy_ams_taxonomy_id': '160', 'type': 'country', 'label': 'Pakistan', 'concept_id': 'P42X_cky_m8v', 'legacy_ams_taxonomy_num_id': 160}),

( {'type': 'country', 'id': 'Jtka_2NV_XGQ', 'preferred_label': 'Palau', 'deprecated_legacy_id': '161'},
'country', {'legacy_ams_taxonomy_id': '161', 'type': 'country', 'label': 'Palau', 'concept_id': 'Jtka_2NV_XGQ', 'legacy_ams_taxonomy_num_id': 161}),

( {'type': 'country', 'id': 'knPC_Zwn_w2t', 'preferred_label': 'Panama', 'deprecated_legacy_id': '162'},
'country', {'legacy_ams_taxonomy_id': '162', 'type': 'country', 'label': 'Panama', 'concept_id': 'knPC_Zwn_w2t', 'legacy_ams_taxonomy_num_id': 162}),

( {'type': 'country', 'id': 'VkNj_E4f_m93', 'preferred_label': 'Papua Nya Guinea', 'deprecated_legacy_id': '163'},
'country', {'legacy_ams_taxonomy_id': '163', 'type': 'country', 'label': 'Papua Nya Guinea', 'concept_id': 'VkNj_E4f_m93', 'legacy_ams_taxonomy_num_id': 163}),

( {'type': 'country', 'id': 'LRBs_ePE_o5F', 'preferred_label': 'Paraguay', 'deprecated_legacy_id': '164'},
'country', {'legacy_ams_taxonomy_id': '164', 'type': 'country', 'label': 'Paraguay', 'concept_id': 'LRBs_ePE_o5F', 'legacy_ams_taxonomy_num_id': 164}),

( {'type': 'country', 'id': 'qiLW_VZG_x3k', 'preferred_label': 'Peru', 'deprecated_legacy_id': '165'},
'country', {'legacy_ams_taxonomy_id': '165', 'type': 'country', 'label': 'Peru', 'concept_id': 'qiLW_VZG_x3k', 'legacy_ams_taxonomy_num_id': 165}),

( {'type': 'country', 'id': 'VJGs_3wz_vLw', 'preferred_label': 'Pitcairn', 'deprecated_legacy_id': '166'},
'country', {'legacy_ams_taxonomy_id': '166', 'type': 'country', 'label': 'Pitcairn', 'concept_id': 'VJGs_3wz_vLw', 'legacy_ams_taxonomy_num_id': 166}),

( {'type': 'country', 'id': 'EV41_7cy_jw7', 'preferred_label': 'Polen', 'deprecated_legacy_id': '167'},
'country', {'legacy_ams_taxonomy_id': '167', 'type': 'country', 'label': 'Polen', 'concept_id': 'EV41_7cy_jw7', 'legacy_ams_taxonomy_num_id': 167}),

( {'type': 'country', 'id': 'crVW_7oo_nLm', 'preferred_label': 'Portugal', 'deprecated_legacy_id': '168'},
'country', {'legacy_ams_taxonomy_id': '168', 'type': 'country', 'label': 'Portugal', 'concept_id': 'crVW_7oo_nLm', 'legacy_ams_taxonomy_num_id': 168}),

( {'type': 'country', 'id': 'SEbV_xRE_ZCF', 'preferred_label': 'Puerto Rico', 'deprecated_legacy_id': '169'},
'country', {'legacy_ams_taxonomy_id': '169', 'type': 'country', 'label': 'Puerto Rico', 'concept_id': 'SEbV_xRE_ZCF', 'legacy_ams_taxonomy_num_id': 169}),

( {'type': 'country', 'id': '76AC_UMd_YfA', 'preferred_label': 'Qatar', 'deprecated_legacy_id': '170'},
'country', {'legacy_ams_taxonomy_id': '170', 'type': 'country', 'label': 'Qatar', 'concept_id': '76AC_UMd_YfA', 'legacy_ams_taxonomy_num_id': 170}),

( {'type': 'country', 'id': '65UW_uMB_qMm', 'preferred_label': 'Reunion', 'deprecated_legacy_id': '171'},
'country', {'legacy_ams_taxonomy_id': '171', 'type': 'country', 'label': 'Reunion', 'concept_id': '65UW_uMB_qMm', 'legacy_ams_taxonomy_num_id': 171}),

( {'type': 'country', 'id': 't76b_fb1_WCi', 'preferred_label': 'Rumänien', 'deprecated_legacy_id': '172'},
'country', {'legacy_ams_taxonomy_id': '172', 'type': 'country', 'label': 'Rumänien', 'concept_id': 't76b_fb1_WCi', 'legacy_ams_taxonomy_num_id': 172}),

( {'type': 'country', 'id': '3Y4P_Upu_aWq', 'preferred_label': 'Rwanda', 'deprecated_legacy_id': '173'},
'country', {'legacy_ams_taxonomy_id': '173', 'type': 'country', 'label': 'Rwanda', 'concept_id': '3Y4P_Upu_aWq', 'legacy_ams_taxonomy_num_id': 173}),

( {'type': 'country', 'id': '24fF_htV_bs1', 'preferred_label': 'Ryssland/Ryska Federationen', 'deprecated_legacy_id': '174'},
'country', {'legacy_ams_taxonomy_id': '174', 'type': 'country', 'label': 'Ryssland/Ryska Federationen', 'concept_id': '24fF_htV_bs1', 'legacy_ams_taxonomy_num_id': 174}),

( {'type': 'country', 'id': 'taYQ_K5H_yRi', 'preferred_label': 'Saint Helena', 'deprecated_legacy_id': '175'},
'country', {'legacy_ams_taxonomy_id': '175', 'type': 'country', 'label': 'Saint Helena', 'concept_id': 'taYQ_K5H_yRi', 'legacy_ams_taxonomy_num_id': 175}),

( {'type': 'country', 'id': 'yrcR_Pps_5hd', 'preferred_label': 'Saint Kitts och Nevis', 'deprecated_legacy_id': '176'},
'country', {'legacy_ams_taxonomy_id': '176', 'type': 'country', 'label': 'Saint Kitts och Nevis', 'concept_id': 'yrcR_Pps_5hd', 'legacy_ams_taxonomy_num_id': 176}),

( {'type': 'country', 'id': 'Nq2U_CeR_qvC', 'preferred_label': 'Saint Lucia', 'deprecated_legacy_id': '177'},
'country', {'legacy_ams_taxonomy_id': '177', 'type': 'country', 'label': 'Saint Lucia', 'concept_id': 'Nq2U_CeR_qvC', 'legacy_ams_taxonomy_num_id': 177}),

( {'type': 'country', 'id': 'umJ8_fX9_XMg', 'preferred_label': 'Saint Pierre och Miquelon', 'deprecated_legacy_id': '178'},
'country', {'legacy_ams_taxonomy_id': '178', 'type': 'country', 'label': 'Saint Pierre och Miquelon', 'concept_id': 'umJ8_fX9_XMg', 'legacy_ams_taxonomy_num_id': 178}),

( {'type': 'country', 'id': 'avAF_tuR_8sR', 'preferred_label': 'Saint Vincent och Grenadinerna', 'deprecated_legacy_id': '179'},
'country', {'legacy_ams_taxonomy_id': '179', 'type': 'country', 'label': 'Saint Vincent och Grenadinerna', 'concept_id': 'avAF_tuR_8sR', 'legacy_ams_taxonomy_num_id': 179}),

( {'type': 'country', 'id': 'JWYP_wan_dVQ', 'preferred_label': 'Västsahara', 'deprecated_legacy_id': '180'},
'country', {'legacy_ams_taxonomy_id': '180', 'type': 'country', 'label': 'Västsahara', 'concept_id': 'JWYP_wan_dVQ', 'legacy_ams_taxonomy_num_id': 180}),

( {'type': 'country', 'id': 'hhAn_wpZ_8YQ', 'preferred_label': 'Salomonöarna', 'deprecated_legacy_id': '181'},
'country', {'legacy_ams_taxonomy_id': '181', 'type': 'country', 'label': 'Salomonöarna', 'concept_id': 'hhAn_wpZ_8YQ', 'legacy_ams_taxonomy_num_id': 181}),

( {'type': 'country', 'id': 'Kme6_ujn_tD4', 'preferred_label': 'San Marino', 'deprecated_legacy_id': '182'},
'country', {'legacy_ams_taxonomy_id': '182', 'type': 'country', 'label': 'San Marino', 'concept_id': 'Kme6_ujn_tD4', 'legacy_ams_taxonomy_num_id': 182}),

( {'type': 'country', 'id': 'CpWu_dFG_Lw3', 'preferred_label': 'São Tomé och Príncipe', 'deprecated_legacy_id': '183'},
'country', {'legacy_ams_taxonomy_id': '183', 'type': 'country', 'label': 'São Tomé och Príncipe', 'concept_id': 'CpWu_dFG_Lw3', 'legacy_ams_taxonomy_num_id': 183}),

( {'type': 'country', 'id': 'nKbk_boM_G1U', 'preferred_label': 'Saudiarabien', 'deprecated_legacy_id': '184'},
'country', {'legacy_ams_taxonomy_id': '184', 'type': 'country', 'label': 'Saudiarabien', 'concept_id': 'nKbk_boM_G1U', 'legacy_ams_taxonomy_num_id': 184}),

( {'type': 'country', 'id': 'Q78b_oCw_Yq2', 'preferred_label': 'Schweiz', 'deprecated_legacy_id': '185'},
'country', {'legacy_ams_taxonomy_id': '185', 'type': 'country', 'label': 'Schweiz', 'concept_id': 'Q78b_oCw_Yq2', 'legacy_ams_taxonomy_num_id': 185}),

( {'type': 'country', 'id': 'XWA9_zm8_DJu', 'preferred_label': 'Senegal', 'deprecated_legacy_id': '186'},
'country', {'legacy_ams_taxonomy_id': '186', 'type': 'country', 'label': 'Senegal', 'concept_id': 'XWA9_zm8_DJu', 'legacy_ams_taxonomy_num_id': 186}),

( {'type': 'country', 'id': '6UUy_rcP_KSp', 'preferred_label': 'Seychellerna', 'deprecated_legacy_id': '187'},
'country', {'legacy_ams_taxonomy_id': '187', 'type': 'country', 'label': 'Seychellerna', 'concept_id': '6UUy_rcP_KSp', 'legacy_ams_taxonomy_num_id': 187}),

( {'type': 'country', 'id': 'f8JX_cnY_7vt', 'preferred_label': 'Sierra Leone', 'deprecated_legacy_id': '188'},
'country', {'legacy_ams_taxonomy_id': '188', 'type': 'country', 'label': 'Sierra Leone', 'concept_id': 'f8JX_cnY_7vt', 'legacy_ams_taxonomy_num_id': 188}),

( {'type': 'country', 'id': 'TJLA_9GT_Wm8', 'preferred_label': 'Singapore', 'deprecated_legacy_id': '189'},
'country', {'legacy_ams_taxonomy_id': '189', 'type': 'country', 'label': 'Singapore', 'concept_id': 'TJLA_9GT_Wm8', 'legacy_ams_taxonomy_num_id': 189}),

( {'type': 'country', 'id': 'zu3V_aT3_KzE', 'preferred_label': 'Slovakien', 'deprecated_legacy_id': '190'},
'country', {'legacy_ams_taxonomy_id': '190', 'type': 'country', 'label': 'Slovakien', 'concept_id': 'zu3V_aT3_KzE', 'legacy_ams_taxonomy_num_id': 190}),

( {'type': 'country', 'id': 'UGMH_x68_T6m', 'preferred_label': 'Slovenien', 'deprecated_legacy_id': '191'},
'country', {'legacy_ams_taxonomy_id': '191', 'type': 'country', 'label': 'Slovenien', 'concept_id': 'UGMH_x68_T6m', 'legacy_ams_taxonomy_num_id': 191}),

( {'type': 'country', 'id': '1WZA_XkL_92R', 'preferred_label': 'Somalia', 'deprecated_legacy_id': '192'},
'country', {'legacy_ams_taxonomy_id': '192', 'type': 'country', 'label': 'Somalia', 'concept_id': '1WZA_XkL_92R', 'legacy_ams_taxonomy_num_id': 192}),

( {'type': 'country', 'id': 'bN7k_4ka_YGQ', 'preferred_label': 'Spanien', 'deprecated_legacy_id': '193'},
'country', {'legacy_ams_taxonomy_id': '193', 'type': 'country', 'label': 'Spanien', 'concept_id': 'bN7k_4ka_YGQ', 'legacy_ams_taxonomy_num_id': 193}),

( {'type': 'country', 'id': 'pQK1_3Gn_hPg', 'preferred_label': 'Sri Lanka', 'deprecated_legacy_id': '194'},
'country', {'legacy_ams_taxonomy_id': '194', 'type': 'country', 'label': 'Sri Lanka', 'concept_id': 'pQK1_3Gn_hPg', 'legacy_ams_taxonomy_num_id': 194}),

( {'type': 'country', 'id': '7wHq_Mri_wCt', 'preferred_label': 'Storbritannien och Nordirland', 'deprecated_legacy_id': '195'},
'country', {'legacy_ams_taxonomy_id': '195', 'type': 'country', 'label': 'Storbritannien och Nordirland', 'concept_id': '7wHq_Mri_wCt', 'legacy_ams_taxonomy_num_id': 195}),

( {'type': 'country', 'id': 'xjRk_KwZ_FSx', 'preferred_label': 'Sudan', 'deprecated_legacy_id': '196'},
'country', {'legacy_ams_taxonomy_id': '196', 'type': 'country', 'label': 'Sudan', 'concept_id': 'xjRk_KwZ_FSx', 'legacy_ams_taxonomy_num_id': 196}),

( {'type': 'country', 'id': 'xDZc_yza_2Tn', 'preferred_label': 'Surinam', 'deprecated_legacy_id': '197'},
'country', {'legacy_ams_taxonomy_id': '197', 'type': 'country', 'label': 'Surinam', 'concept_id': 'xDZc_yza_2Tn', 'legacy_ams_taxonomy_num_id': 197}),

( {'type': 'country', 'id': '8gs6_Na1_VVw', 'preferred_label': 'Svalbard och Jan Mayen', 'deprecated_legacy_id': '198'},
'country', {'legacy_ams_taxonomy_id': '198', 'type': 'country', 'label': 'Svalbard och Jan Mayen', 'concept_id': '8gs6_Na1_VVw', 'legacy_ams_taxonomy_num_id': 198}),

( {'type': 'country', 'id': 'i46j_HmG_v64', 'preferred_label': 'Sverige', 'deprecated_legacy_id': '199'},
'country', {'legacy_ams_taxonomy_id': '199', 'type': 'country', 'label': 'Sverige', 'concept_id': 'i46j_HmG_v64', 'legacy_ams_taxonomy_num_id': 199}),

( {'type': 'country', 'id': '8uqC_JpJ_cbM', 'preferred_label': 'Swaziland', 'deprecated_legacy_id': '200'},
'country', {'legacy_ams_taxonomy_id': '200', 'type': 'country', 'label': 'Swaziland', 'concept_id': '8uqC_JpJ_cbM', 'legacy_ams_taxonomy_num_id': 200}),

( {'type': 'country', 'id': '8TSL_ZL7_E5p', 'preferred_label': 'Sydafrika', 'deprecated_legacy_id': '201'},
'country', {'legacy_ams_taxonomy_id': '201', 'type': 'country', 'label': 'Sydafrika', 'concept_id': '8TSL_ZL7_E5p', 'legacy_ams_taxonomy_num_id': 201}),

( {'type': 'country', 'id': 'KRyt_Bj8_vWZ', 'preferred_label': 'Sydgeorgien och Södra Sandwichöarna', 'deprecated_legacy_id': '202'},
'country', {'legacy_ams_taxonomy_id': '202', 'type': 'country', 'label': 'Sydgeorgien och Södra Sandwichöarna', 'concept_id': 'KRyt_Bj8_vWZ', 'legacy_ams_taxonomy_num_id': 202}),

( {'type': 'country', 'id': 'Vkhm_P1M_BzE', 'preferred_label': 'Syrien', 'deprecated_legacy_id': '203'},
'country', {'legacy_ams_taxonomy_id': '203', 'type': 'country', 'label': 'Syrien', 'concept_id': 'Vkhm_P1M_BzE', 'legacy_ams_taxonomy_num_id': 203}),

( {'type': 'country', 'id': 'MdqM_dHv_9Rd', 'preferred_label': 'Tadzjikistan', 'deprecated_legacy_id': '204'},
'country', {'legacy_ams_taxonomy_id': '204', 'type': 'country', 'label': 'Tadzjikistan', 'concept_id': 'MdqM_dHv_9Rd', 'legacy_ams_taxonomy_num_id': 204}),

( {'type': 'country', 'id': 'HdNx_QfU_ALG', 'preferred_label': 'Taiwan', 'deprecated_legacy_id': '205'},
'country', {'legacy_ams_taxonomy_id': '205', 'type': 'country', 'label': 'Taiwan', 'concept_id': 'HdNx_QfU_ALG', 'legacy_ams_taxonomy_num_id': 205}),

( {'type': 'country', 'id': 'A1hv_Ljz_fUi', 'preferred_label': 'Tanzania', 'deprecated_legacy_id': '206'},
'country', {'legacy_ams_taxonomy_id': '206', 'type': 'country', 'label': 'Tanzania', 'concept_id': 'A1hv_Ljz_fUi', 'legacy_ams_taxonomy_num_id': 206}),

( {'type': 'country', 'id': 'xscm_4AC_j8o', 'preferred_label': 'Tchad', 'deprecated_legacy_id': '207'},
'country', {'legacy_ams_taxonomy_id': '207', 'type': 'country', 'label': 'Tchad', 'concept_id': 'xscm_4AC_j8o', 'legacy_ams_taxonomy_num_id': 207}),

( {'type': 'country', 'id': 'Sxx2_SZe_Krs', 'preferred_label': 'Thailand', 'deprecated_legacy_id': '208'},
'country', {'legacy_ams_taxonomy_id': '208', 'type': 'country', 'label': 'Thailand', 'concept_id': 'Sxx2_SZe_Krs', 'legacy_ams_taxonomy_num_id': 208}),

( {'type': 'country', 'id': '7qR2_55U_iyh', 'preferred_label': 'Tjeckien', 'deprecated_legacy_id': '209'},
'country', {'legacy_ams_taxonomy_id': '209', 'type': 'country', 'label': 'Tjeckien', 'concept_id': '7qR2_55U_iyh', 'legacy_ams_taxonomy_num_id': 209}),

( {'type': 'country', 'id': 'yD6Z_Jiw_xm2', 'preferred_label': 'Togo', 'deprecated_legacy_id': '210'},
'country', {'legacy_ams_taxonomy_id': '210', 'type': 'country', 'label': 'Togo', 'concept_id': 'yD6Z_Jiw_xm2', 'legacy_ams_taxonomy_num_id': 210}),

( {'type': 'country', 'id': 'ywtd_6fW_Eue', 'preferred_label': 'Tokelau', 'deprecated_legacy_id': '211'},
'country', {'legacy_ams_taxonomy_id': '211', 'type': 'country', 'label': 'Tokelau', 'concept_id': 'ywtd_6fW_Eue', 'legacy_ams_taxonomy_num_id': 211}),

( {'type': 'country', 'id': 'd6ce_nB8_LaP', 'preferred_label': 'Tonga', 'deprecated_legacy_id': '212'},
'country', {'legacy_ams_taxonomy_id': '212', 'type': 'country', 'label': 'Tonga', 'concept_id': 'd6ce_nB8_LaP', 'legacy_ams_taxonomy_num_id': 212}),

( {'type': 'country', 'id': 'WyNg_Ae1_hWd', 'preferred_label': 'Trinidad och Tobago', 'deprecated_legacy_id': '213'},
'country', {'legacy_ams_taxonomy_id': '213', 'type': 'country', 'label': 'Trinidad och Tobago', 'concept_id': 'WyNg_Ae1_hWd', 'legacy_ams_taxonomy_num_id': 213}),

( {'type': 'country', 'id': 'oCfK_4fN_DEE', 'preferred_label': 'Tunisien', 'deprecated_legacy_id': '214'},
'country', {'legacy_ams_taxonomy_id': '214', 'type': 'country', 'label': 'Tunisien', 'concept_id': 'oCfK_4fN_DEE', 'legacy_ams_taxonomy_num_id': 214}),

( {'type': 'country', 'id': 'YpWA_xv7_Qmr', 'preferred_label': 'Turkiet', 'deprecated_legacy_id': '215'},
'country', {'legacy_ams_taxonomy_id': '215', 'type': 'country', 'label': 'Turkiet', 'concept_id': 'YpWA_xv7_Qmr', 'legacy_ams_taxonomy_num_id': 215}),

( {'type': 'country', 'id': 'gUPX_Q1E_aNc', 'preferred_label': 'Turkmenistan', 'deprecated_legacy_id': '216'},
'country', {'legacy_ams_taxonomy_id': '216', 'type': 'country', 'label': 'Turkmenistan', 'concept_id': 'gUPX_Q1E_aNc', 'legacy_ams_taxonomy_num_id': 216}),

( {'type': 'country', 'id': 'dHjg_a8H_kHA', 'preferred_label': 'Turks- och Caicosöarna', 'deprecated_legacy_id': '217'},
'country', {'legacy_ams_taxonomy_id': '217', 'type': 'country', 'label': 'Turks- och Caicosöarna', 'concept_id': 'dHjg_a8H_kHA', 'legacy_ams_taxonomy_num_id': 217}),

( {'type': 'country', 'id': 'eSp2_gR8_EBG', 'preferred_label': 'Tuvalu', 'deprecated_legacy_id': '218'},
'country', {'legacy_ams_taxonomy_id': '218', 'type': 'country', 'label': 'Tuvalu', 'concept_id': 'eSp2_gR8_EBG', 'legacy_ams_taxonomy_num_id': 218}),

( {'type': 'country', 'id': 'G3M7_959_8Pp', 'preferred_label': 'Tyskland', 'deprecated_legacy_id': '219'},
'country', {'legacy_ams_taxonomy_id': '219', 'type': 'country', 'label': 'Tyskland', 'concept_id': 'G3M7_959_8Pp', 'legacy_ams_taxonomy_num_id': 219}),

( {'type': 'country', 'id': 'GwGd_ukJ_qsx', 'preferred_label': 'Förenta staterna (Amerikas förenta stater)', 'deprecated_legacy_id': '220'},
'country', {'legacy_ams_taxonomy_id': '220', 'type': 'country', 'label': 'Förenta staterna (Amerikas förenta stater)', 'concept_id': 'GwGd_ukJ_qsx', 'legacy_ams_taxonomy_num_id': 220}),

( {'type': 'country', 'id': 'VXZG_btz_6N3', 'preferred_label': 'Uganda', 'deprecated_legacy_id': '221'},
'country', {'legacy_ams_taxonomy_id': '221', 'type': 'country', 'label': 'Uganda', 'concept_id': 'VXZG_btz_6N3', 'legacy_ams_taxonomy_num_id': 221}),

( {'type': 'country', 'id': 'GmdR_egb_EWJ', 'preferred_label': 'Ukraina', 'deprecated_legacy_id': '222'},
'country', {'legacy_ams_taxonomy_id': '222', 'type': 'country', 'label': 'Ukraina', 'concept_id': 'GmdR_egb_EWJ', 'legacy_ams_taxonomy_num_id': 222}),

( {'type': 'country', 'id': 'QWsJ_QBW_vGb', 'preferred_label': 'Ungern', 'deprecated_legacy_id': '223'},
'country', {'legacy_ams_taxonomy_id': '223', 'type': 'country', 'label': 'Ungern', 'concept_id': 'QWsJ_QBW_vGb', 'legacy_ams_taxonomy_num_id': 223}),

( {'type': 'country', 'id': 'QAsK_uc2_8yJ', 'preferred_label': 'Uruguay', 'deprecated_legacy_id': '224'},
'country', {'legacy_ams_taxonomy_id': '224', 'type': 'country', 'label': 'Uruguay', 'concept_id': 'QAsK_uc2_8yJ', 'legacy_ams_taxonomy_num_id': 224}),

( {'type': 'country', 'id': 'TAAP_kv5_V6g', 'preferred_label': 'Uzbekistan', 'deprecated_legacy_id': '225'},
'country', {'legacy_ams_taxonomy_id': '225', 'type': 'country', 'label': 'Uzbekistan', 'concept_id': 'TAAP_kv5_V6g', 'legacy_ams_taxonomy_num_id': 225}),

( {'type': 'country', 'id': 'm2x5_LAT_X2t', 'preferred_label': 'Vanuatu', 'deprecated_legacy_id': '226'},
'country', {'legacy_ams_taxonomy_id': '226', 'type': 'country', 'label': 'Vanuatu', 'concept_id': 'm2x5_LAT_X2t', 'legacy_ams_taxonomy_num_id': 226}),

( {'type': 'country', 'id': 'DDSg_45a_2Qi', 'preferred_label': 'Heliga stolen (Vatikanstaten)', 'deprecated_legacy_id': '227'},
'country', {'legacy_ams_taxonomy_id': '227', 'type': 'country', 'label': 'Heliga stolen (Vatikanstaten)', 'concept_id': 'DDSg_45a_2Qi', 'legacy_ams_taxonomy_num_id': 227}),

( {'type': 'country', 'id': '2YeU_EST_ykA', 'preferred_label': 'Venezuela', 'deprecated_legacy_id': '228'},
'country', {'legacy_ams_taxonomy_id': '228', 'type': 'country', 'label': 'Venezuela', 'concept_id': '2YeU_EST_ykA', 'legacy_ams_taxonomy_num_id': 228}),

( {'type': 'country', 'id': '7RXF_Wx9_mfY', 'preferred_label': 'Vietnam', 'deprecated_legacy_id': '229'},
'country', {'legacy_ams_taxonomy_id': '229', 'type': 'country', 'label': 'Vietnam', 'concept_id': '7RXF_Wx9_mfY', 'legacy_ams_taxonomy_num_id': 229}),

( {'type': 'country', 'id': '9QXG_KJN_ZVN', 'preferred_label': 'Vitryssland', 'deprecated_legacy_id': '230'},
'country', {'legacy_ams_taxonomy_id': '230', 'type': 'country', 'label': 'Vitryssland', 'concept_id': '9QXG_KJN_ZVN', 'legacy_ams_taxonomy_num_id': 230}),

( {'type': 'country', 'id': 'PpbT_Psw_2Qy', 'preferred_label': 'Jemen', 'deprecated_legacy_id': '232'},
'country', {'legacy_ams_taxonomy_id': '232', 'type': 'country', 'label': 'Jemen', 'concept_id': 'PpbT_Psw_2Qy', 'legacy_ams_taxonomy_num_id': 232}),

( {'type': 'country', 'id': 'buDY_D5p_DAc', 'preferred_label': 'Zambia', 'deprecated_legacy_id': '234'},
'country', {'legacy_ams_taxonomy_id': '234', 'type': 'country', 'label': 'Zambia', 'concept_id': 'buDY_D5p_DAc', 'legacy_ams_taxonomy_num_id': 234}),

( {'type': 'country', 'id': 'K2Kh_K4w_NLF', 'preferred_label': 'Zimbabwe', 'deprecated_legacy_id': '235'},
'country', {'legacy_ams_taxonomy_id': '235', 'type': 'country', 'label': 'Zimbabwe', 'concept_id': 'K2Kh_K4w_NLF', 'legacy_ams_taxonomy_num_id': 235}),

( {'type': 'country', 'id': 'uZex_hVg_jPf', 'preferred_label': 'Österrike', 'deprecated_legacy_id': '236'},
'country', {'legacy_ams_taxonomy_id': '236', 'type': 'country', 'label': 'Österrike', 'concept_id': 'uZex_hVg_jPf', 'legacy_ams_taxonomy_num_id': 236}),

( {'type': 'country', 'id': 'nziP_X84_aBA', 'preferred_label': 'Östtimor', 'deprecated_legacy_id': '237'},
'country', {'legacy_ams_taxonomy_id': '237', 'type': 'country', 'label': 'Östtimor', 'concept_id': 'nziP_X84_aBA', 'legacy_ams_taxonomy_num_id': 237}),

( {'type': 'country', 'id': 'Yfec_Cw6_Ejz', 'preferred_label': 'Kongo, demokratiska republiken', 'deprecated_legacy_id': '238'},
'country', {'legacy_ams_taxonomy_id': '238', 'type': 'country', 'label': 'Kongo, demokratiska republiken', 'concept_id': 'Yfec_Cw6_Ejz', 'legacy_ams_taxonomy_num_id': 238}),

( {'type': 'country', 'id': 'qi2S_K8c_Kn6', 'preferred_label': 'Nordmakedonien', 'deprecated_legacy_id': '239'},
'country', {'legacy_ams_taxonomy_id': '239', 'type': 'country', 'label': 'Nordmakedonien', 'concept_id': 'qi2S_K8c_Kn6', 'legacy_ams_taxonomy_num_id': 239}),

( {'type': 'country', 'id': 'qrQX_cxS_t7P', 'preferred_label': 'Palestina', 'deprecated_legacy_id': '240'},
'country', {'legacy_ams_taxonomy_id': '240', 'type': 'country', 'label': 'Palestina', 'concept_id': 'qrQX_cxS_t7P', 'legacy_ams_taxonomy_num_id': 240}),

( {'type': 'country', 'id': 'G7zY_fB5_zg4', 'preferred_label': 'Wallis och Futuna', 'deprecated_legacy_id': '241'},
'country', {'legacy_ams_taxonomy_id': '241', 'type': 'country', 'label': 'Wallis och Futuna', 'concept_id': 'G7zY_fB5_zg4', 'legacy_ams_taxonomy_num_id': 241}),

( {'type': 'country', 'id': 'nFvs_pbn_aia', 'preferred_label': 'Angola', 'deprecated_legacy_id': '242'},
'country', {'legacy_ams_taxonomy_id': '242', 'type': 'country', 'label': 'Angola', 'concept_id': 'nFvs_pbn_aia', 'legacy_ams_taxonomy_num_id': 242}),

( {'type': 'country', 'id': 'WNSF_XBA_piV', 'preferred_label': 'Franska Guyana', 'deprecated_legacy_id': '243'},
'country', {'legacy_ams_taxonomy_id': '243', 'type': 'country', 'label': 'Franska Guyana', 'concept_id': 'WNSF_XBA_piV', 'legacy_ams_taxonomy_num_id': 243}),

( {'type': 'country', 'id': 'K6sm_uW6_Lch', 'preferred_label': 'Samoa', 'deprecated_legacy_id': '244'},
'country', {'legacy_ams_taxonomy_id': '244', 'type': 'country', 'label': 'Samoa', 'concept_id': 'K6sm_uW6_Lch', 'legacy_ams_taxonomy_num_id': 244}),

( {'type': 'country', 'id': 'amTW_Ebm_ejQ', 'preferred_label': 'Serbien', 'deprecated_legacy_id': '245'},
'country', {'legacy_ams_taxonomy_id': '245', 'type': 'country', 'label': 'Serbien', 'concept_id': 'amTW_Ebm_ejQ', 'legacy_ams_taxonomy_num_id': 245}),

( {'type': 'country', 'id': 'bUNp_6HW_Vbn', 'preferred_label': 'Åland (tillhör Finland)', 'deprecated_legacy_id': '246'},
'country', {'legacy_ams_taxonomy_id': '246', 'type': 'country', 'label': 'Åland (tillhör Finland)', 'concept_id': 'bUNp_6HW_Vbn', 'legacy_ams_taxonomy_num_id': 246}),

( {'type': 'country', 'id': '5zrc_Su8_X4D', 'preferred_label': 'Guernsey', 'deprecated_legacy_id': '247'},
'country', {'legacy_ams_taxonomy_id': '247', 'type': 'country', 'label': 'Guernsey', 'concept_id': '5zrc_Su8_X4D', 'legacy_ams_taxonomy_num_id': 247}),

( {'type': 'country', 'id': 'BApY_3G4_Nsb', 'preferred_label': 'Isle of Man', 'deprecated_legacy_id': '248'},
'country', {'legacy_ams_taxonomy_id': '248', 'type': 'country', 'label': 'Isle of Man', 'concept_id': 'BApY_3G4_Nsb', 'legacy_ams_taxonomy_num_id': 248}),

( {'type': 'country', 'id': 'ihaN_Ru8_mPg', 'preferred_label': 'Jersey', 'deprecated_legacy_id': '249'},
'country', {'legacy_ams_taxonomy_id': '249', 'type': 'country', 'label': 'Jersey', 'concept_id': 'ihaN_Ru8_mPg', 'legacy_ams_taxonomy_num_id': 249}),

( {'type': 'country', 'id': 'vZtV_pRR_Evp', 'preferred_label': 'Montenegro', 'deprecated_legacy_id': '250'},
'country', {'legacy_ams_taxonomy_id': '250', 'type': 'country', 'label': 'Montenegro', 'concept_id': 'vZtV_pRR_Evp', 'legacy_ams_taxonomy_num_id': 250}),

( {'type': 'country', 'id': 'iJho_QUP_eaC', 'preferred_label': 'Kosovo', 'deprecated_legacy_id': '251'},
'country', {'legacy_ams_taxonomy_id': '251', 'type': 'country', 'label': 'Kosovo', 'concept_id': 'iJho_QUP_eaC', 'legacy_ams_taxonomy_num_id': 251}),

( {'type': 'country', 'id': 'tNTV_qrs_awt', 'preferred_label': 'Saint Barthélemy', 'deprecated_legacy_id': '252'},
'country', {'legacy_ams_taxonomy_id': '252', 'type': 'country', 'label': 'Saint Barthélemy', 'concept_id': 'tNTV_qrs_awt', 'legacy_ams_taxonomy_num_id': 252}),

( {'type': 'country', 'id': 'Ynjj_eeN_fM6', 'preferred_label': 'Saint Martin (franska delen)', 'deprecated_legacy_id': '253'},
'country', {'legacy_ams_taxonomy_id': '253', 'type': 'country', 'label': 'Saint Martin (franska delen)', 'concept_id': 'Ynjj_eeN_fM6', 'legacy_ams_taxonomy_num_id': 253}),

( {'type': 'country', 'id': 'CSwg_jk9_P5S', 'preferred_label': 'Bonaire, Sint Eustatius och Saba', 'deprecated_legacy_id': '254'},
'country', {'legacy_ams_taxonomy_id': '254', 'type': 'country', 'label': 'Bonaire, Sint Eustatius och Saba', 'concept_id': 'CSwg_jk9_P5S', 'legacy_ams_taxonomy_num_id': 254}),

( {'type': 'country', 'id': 'XPvX_7KR_UsM', 'preferred_label': 'Curaçao', 'deprecated_legacy_id': '255'},
'country', {'legacy_ams_taxonomy_id': '255', 'type': 'country', 'label': 'Curaçao', 'concept_id': 'XPvX_7KR_UsM', 'legacy_ams_taxonomy_num_id': 255}),

( {'type': 'country', 'id': 'sGcM_zCn_iGQ', 'preferred_label': 'Sint Maarten (nederländska delen)', 'deprecated_legacy_id': '256'},
'country', {'legacy_ams_taxonomy_id': '256', 'type': 'country', 'label': 'Sint Maarten (nederländska delen)', 'concept_id': 'sGcM_zCn_iGQ', 'legacy_ams_taxonomy_num_id': 256}),

( {'type': 'country', 'id': 'aqxj_t1i_SxL', 'preferred_label': 'Sydsudan', 'deprecated_legacy_id': '257'},
'country', {'legacy_ams_taxonomy_id': '257', 'type': 'country', 'label': 'Sydsudan', 'concept_id': 'aqxj_t1i_SxL', 'legacy_ams_taxonomy_num_id': 257}),

( {'type': 'worktime-extent', 'id': '6YE1_gAC_R2G', 'preferred_label': 'Heltid', 'deprecated_legacy_id': '1'},
'worktime-extent', {'legacy_ams_taxonomy_id': '1', 'type': 'worktime-extent', 'label': 'Heltid', 'concept_id': '6YE1_gAC_R2G', 'legacy_ams_taxonomy_num_id': 1}),

( {'type': 'worktime-extent', 'id': '947z_JGS_Uk2', 'preferred_label': 'Deltid', 'deprecated_legacy_id': '2'},
'worktime-extent', {'legacy_ams_taxonomy_id': '2', 'type': 'worktime-extent', 'label': 'Deltid', 'concept_id': '947z_JGS_Uk2', 'legacy_ams_taxonomy_num_id': 2}),

( {'type': 'language', 'id': '5vWL_41L_udS', 'preferred_label': 'Afar', 'deprecated_legacy_id': '239'},
'language', {'legacy_ams_taxonomy_id': '239', 'type': 'language', 'label': 'Afar', 'concept_id': '5vWL_41L_udS', 'legacy_ams_taxonomy_num_id': 239}),

( {'type': 'language', 'id': 'vitY_dty_VbD', 'preferred_label': 'Afrikaans', 'deprecated_legacy_id': '240'},
'language', {'legacy_ams_taxonomy_id': '240', 'type': 'language', 'label': 'Afrikaans', 'concept_id': 'vitY_dty_VbD', 'legacy_ams_taxonomy_num_id': 240}),

( {'type': 'language', 'id': '7xZB_9BJ_eLa', 'preferred_label': 'Akan', 'deprecated_legacy_id': '242'},
'language', {'legacy_ams_taxonomy_id': '242', 'type': 'language', 'label': 'Akan', 'concept_id': '7xZB_9BJ_eLa', 'legacy_ams_taxonomy_num_id': 242}),

( {'type': 'language', 'id': 'rod1_aPk_8V5', 'preferred_label': 'Albanska', 'deprecated_legacy_id': '243'},
'language', {'legacy_ams_taxonomy_id': '243', 'type': 'language', 'label': 'Albanska', 'concept_id': 'rod1_aPk_8V5', 'legacy_ams_taxonomy_num_id': 243}),

( {'type': 'language', 'id': 'EMhG_wnR_hYP', 'preferred_label': 'Amhariska', 'deprecated_legacy_id': '245'},
'language', {'legacy_ams_taxonomy_id': '245', 'type': 'language', 'label': 'Amhariska', 'concept_id': 'EMhG_wnR_hYP', 'legacy_ams_taxonomy_num_id': 245}),

( {'type': 'language', 'id': '3iEY_RGP_qXq', 'preferred_label': 'Arabiska', 'deprecated_legacy_id': '247'},
'language', {'legacy_ams_taxonomy_id': '247', 'type': 'language', 'label': 'Arabiska', 'concept_id': '3iEY_RGP_qXq', 'legacy_ams_taxonomy_num_id': 247}),

( {'type': 'language', 'id': 'Hk93_QrC_QXX', 'preferred_label': 'Arameiska', 'deprecated_legacy_id': '248'},
'language', {'legacy_ams_taxonomy_id': '248', 'type': 'language', 'label': 'Arameiska', 'concept_id': 'Hk93_QrC_QXX', 'legacy_ams_taxonomy_num_id': 248}),

( {'type': 'language', 'id': 'jsQe_Bra_Ety', 'preferred_label': 'Armeniska', 'deprecated_legacy_id': '249'},
'language', {'legacy_ams_taxonomy_id': '249', 'type': 'language', 'label': 'Armeniska', 'concept_id': 'jsQe_Bra_Ety', 'legacy_ams_taxonomy_num_id': 249}),

( {'type': 'language', 'id': 'zYgB_czd_R1x', 'preferred_label': 'Assamesiska', 'deprecated_legacy_id': '250'},
'language', {'legacy_ams_taxonomy_id': '250', 'type': 'language', 'label': 'Assamesiska', 'concept_id': 'zYgB_czd_R1x', 'legacy_ams_taxonomy_num_id': 250}),

( {'type': 'language', 'id': 'Lg2z_RbL_A8S', 'preferred_label': 'Aymara', 'deprecated_legacy_id': '251'},
'language', {'legacy_ams_taxonomy_id': '251', 'type': 'language', 'label': 'Aymara', 'concept_id': 'Lg2z_RbL_A8S', 'legacy_ams_taxonomy_num_id': 251}),

( {'type': 'language', 'id': 'J16c_pLE_QY4', 'preferred_label': 'Azerbajdzjanska', 'deprecated_legacy_id': '252'},
'language', {'legacy_ams_taxonomy_id': '252', 'type': 'language', 'label': 'Azerbajdzjanska', 'concept_id': 'J16c_pLE_QY4', 'legacy_ams_taxonomy_num_id': 252}),

( {'type': 'language', 'id': 'TJsR_11M_d72', 'preferred_label': 'Baluchiska', 'deprecated_legacy_id': '253'},
'language', {'legacy_ams_taxonomy_id': '253', 'type': 'language', 'label': 'Baluchiska', 'concept_id': 'TJsR_11M_d72', 'legacy_ams_taxonomy_num_id': 253}),

( {'type': 'language', 'id': 'T3Yk_ZEt_kLQ', 'preferred_label': 'Bambara', 'deprecated_legacy_id': '254'},
'language', {'legacy_ams_taxonomy_id': '254', 'type': 'language', 'label': 'Bambara', 'concept_id': 'T3Yk_ZEt_kLQ', 'legacy_ams_taxonomy_num_id': 254}),

( {'type': 'language', 'id': 'X5sT_hmN_m2S', 'preferred_label': 'Banda', 'deprecated_legacy_id': '255'},
'language', {'legacy_ams_taxonomy_id': '255', 'type': 'language', 'label': 'Banda', 'concept_id': 'X5sT_hmN_m2S', 'legacy_ams_taxonomy_num_id': 255}),

( {'type': 'language', 'id': 'udrA_ZMS_epP', 'preferred_label': 'Bantuspråk, andra', 'deprecated_legacy_id': '256'},
'language', {'legacy_ams_taxonomy_id': '256', 'type': 'language', 'label': 'Bantuspråk, andra', 'concept_id': 'udrA_ZMS_epP', 'legacy_ams_taxonomy_num_id': 256}),

( {'type': 'language', 'id': 'yPxq_rk8_QqY', 'preferred_label': 'Basjkiriska', 'deprecated_legacy_id': '257'},
'language', {'legacy_ams_taxonomy_id': '257', 'type': 'language', 'label': 'Basjkiriska', 'concept_id': 'yPxq_rk8_QqY', 'legacy_ams_taxonomy_num_id': 257}),

( {'type': 'language', 'id': 'rucj_CJq_mV8', 'preferred_label': 'Baskiska', 'deprecated_legacy_id': '258'},
'language', {'legacy_ams_taxonomy_id': '258', 'type': 'language', 'label': 'Baskiska', 'concept_id': 'rucj_CJq_mV8', 'legacy_ams_taxonomy_num_id': 258}),

( {'type': 'language', 'id': 'nc4p_BwA_2HH', 'preferred_label': 'Bemba', 'deprecated_legacy_id': '259'},
'language', {'legacy_ams_taxonomy_id': '259', 'type': 'language', 'label': 'Bemba', 'concept_id': 'nc4p_BwA_2HH', 'legacy_ams_taxonomy_num_id': 259}),

( {'type': 'language', 'id': 'GBG3_87R_ux1', 'preferred_label': 'Bengaliska', 'deprecated_legacy_id': '260'},
'language', {'legacy_ams_taxonomy_id': '260', 'type': 'language', 'label': 'Bengaliska', 'concept_id': 'GBG3_87R_ux1', 'legacy_ams_taxonomy_num_id': 260}),

( {'type': 'language', 'id': 'QTC1_ADL_T85', 'preferred_label': 'Berberspråk, andra', 'deprecated_legacy_id': '261'},
'language', {'legacy_ams_taxonomy_id': '261', 'type': 'language', 'label': 'Berberspråk, andra', 'concept_id': 'QTC1_ADL_T85', 'legacy_ams_taxonomy_num_id': 261}),

( {'type': 'language', 'id': 'Ur91_w7s_dP1', 'preferred_label': 'Bislama', 'deprecated_legacy_id': '262'},
'language', {'legacy_ams_taxonomy_id': '262', 'type': 'language', 'label': 'Bislama', 'concept_id': 'Ur91_w7s_dP1', 'legacy_ams_taxonomy_num_id': 262}),

( {'type': 'language', 'id': '9TLR_Ri9_34C', 'preferred_label': 'Bihariska', 'deprecated_legacy_id': '263'},
'language', {'legacy_ams_taxonomy_id': '263', 'type': 'language', 'label': 'Bihariska', 'concept_id': '9TLR_Ri9_34C', 'legacy_ams_taxonomy_num_id': 263}),

( {'type': 'language', 'id': 'AGKq_3n1_4sm', 'preferred_label': 'Bosniska', 'deprecated_legacy_id': '265'},
'language', {'legacy_ams_taxonomy_id': '265', 'type': 'language', 'label': 'Bosniska', 'concept_id': 'AGKq_3n1_4sm', 'legacy_ams_taxonomy_num_id': 265}),

( {'type': 'language', 'id': 'pW4U_gcW_VGv', 'preferred_label': 'Bulgariska', 'deprecated_legacy_id': '266'},
'language', {'legacy_ams_taxonomy_id': '266', 'type': 'language', 'label': 'Bulgariska', 'concept_id': 'pW4U_gcW_VGv', 'legacy_ams_taxonomy_num_id': 266}),

( {'type': 'language', 'id': 'yHFe_Te6_E6F', 'preferred_label': 'Burmesiska', 'deprecated_legacy_id': '267'},
'language', {'legacy_ams_taxonomy_id': '267', 'type': 'language', 'label': 'Burmesiska', 'concept_id': 'yHFe_Te6_E6F', 'legacy_ams_taxonomy_num_id': 267}),

( {'type': 'language', 'id': 'y89C_Uia_Y5j', 'preferred_label': 'Cebuano', 'deprecated_legacy_id': '269'},
'language', {'legacy_ams_taxonomy_id': '269', 'type': 'language', 'label': 'Cebuano', 'concept_id': 'y89C_Uia_Y5j', 'legacy_ams_taxonomy_num_id': 269}),

( {'type': 'language', 'id': 'yGZ2_39H_oPN', 'preferred_label': 'Danska', 'deprecated_legacy_id': '274'},
'language', {'legacy_ams_taxonomy_id': '274', 'type': 'language', 'label': 'Danska', 'concept_id': 'yGZ2_39H_oPN', 'legacy_ams_taxonomy_num_id': 274}),

( {'type': 'language', 'id': 'ov2k_GtY_wDw', 'preferred_label': 'Dinka', 'deprecated_legacy_id': '277'},
'language', {'legacy_ams_taxonomy_id': '277', 'type': 'language', 'label': 'Dinka', 'concept_id': 'ov2k_GtY_wDw', 'legacy_ams_taxonomy_num_id': 277}),

( {'type': 'language', 'id': 'XFGV_NHd_eaf', 'preferred_label': 'Divehi', 'deprecated_legacy_id': '278'},
'language', {'legacy_ams_taxonomy_id': '278', 'type': 'language', 'label': 'Divehi', 'concept_id': 'XFGV_NHd_eaf', 'legacy_ams_taxonomy_num_id': 278}),

( {'type': 'language', 'id': '1bwY_BGf_CH7', 'preferred_label': 'Dyula', 'deprecated_legacy_id': '280'},
'language', {'legacy_ams_taxonomy_id': '280', 'type': 'language', 'label': 'Dyula', 'concept_id': '1bwY_BGf_CH7', 'legacy_ams_taxonomy_num_id': 280}),

( {'type': 'language', 'id': '8RMF_7gP_NaH', 'preferred_label': 'Bhutanesiska', 'deprecated_legacy_id': '281'},
'language', {'legacy_ams_taxonomy_id': '281', 'type': 'language', 'label': 'Bhutanesiska', 'concept_id': '8RMF_7gP_NaH', 'legacy_ams_taxonomy_num_id': 281}),

( {'type': 'language', 'id': 'Vfz9_yba_MkG', 'preferred_label': 'Efik', 'deprecated_legacy_id': '282'},
'language', {'legacy_ams_taxonomy_id': '282', 'type': 'language', 'label': 'Efik', 'concept_id': 'Vfz9_yba_MkG', 'legacy_ams_taxonomy_num_id': 282}),

( {'type': 'language', 'id': 'NVxJ_hLg_TYS', 'preferred_label': 'Engelska', 'deprecated_legacy_id': '283'},
'language', {'legacy_ams_taxonomy_id': '283', 'type': 'language', 'label': 'Engelska', 'concept_id': 'NVxJ_hLg_TYS', 'legacy_ams_taxonomy_num_id': 283}),

( {'type': 'language', 'id': 'zg1P_hzq_eMR', 'preferred_label': 'Estniska', 'deprecated_legacy_id': '285'},
'language', {'legacy_ams_taxonomy_id': '285', 'type': 'language', 'label': 'Estniska', 'concept_id': 'zg1P_hzq_eMR', 'legacy_ams_taxonomy_num_id': 285}),

( {'type': 'language', 'id': '5FUu_wLF_tTf', 'preferred_label': 'Ewe', 'deprecated_legacy_id': '286'},
'language', {'legacy_ams_taxonomy_id': '286', 'type': 'language', 'label': 'Ewe', 'concept_id': '5FUu_wLF_tTf', 'legacy_ams_taxonomy_num_id': 286}),

( {'type': 'language', 'id': 'L4AS_T9o_VwB', 'preferred_label': 'Fang', 'deprecated_legacy_id': '287'},
'language', {'legacy_ams_taxonomy_id': '287', 'type': 'language', 'label': 'Fang', 'concept_id': 'L4AS_T9o_VwB', 'legacy_ams_taxonomy_num_id': 287}),

( {'type': 'language', 'id': 'WTZi_v28_Unh', 'preferred_label': 'Fanti', 'deprecated_legacy_id': '288'},
'language', {'legacy_ams_taxonomy_id': '288', 'type': 'language', 'label': 'Fanti', 'concept_id': 'WTZi_v28_Unh', 'legacy_ams_taxonomy_num_id': 288}),

( {'type': 'language', 'id': 'j4rH_cw8_s7g', 'preferred_label': 'Färöiska', 'deprecated_legacy_id': '289'},
'language', {'legacy_ams_taxonomy_id': '289', 'type': 'language', 'label': 'Färöiska', 'concept_id': 'j4rH_cw8_s7g', 'legacy_ams_taxonomy_num_id': 289}),

( {'type': 'language', 'id': 'ZMHx_hup_v34', 'preferred_label': 'Fidjianska', 'deprecated_legacy_id': '291'},
'language', {'legacy_ams_taxonomy_id': '291', 'type': 'language', 'label': 'Fidjianska', 'concept_id': 'ZMHx_hup_v34', 'legacy_ams_taxonomy_num_id': 291}),

( {'type': 'language', 'id': 'Swhq_4Fg_qfm', 'preferred_label': 'Finska', 'deprecated_legacy_id': '292'},
'language', {'legacy_ams_taxonomy_id': '292', 'type': 'language', 'label': 'Finska', 'concept_id': 'Swhq_4Fg_qfm', 'legacy_ams_taxonomy_num_id': 292}),

( {'type': 'language', 'id': 'KPgw_t5P_759', 'preferred_label': 'Fon', 'deprecated_legacy_id': '293'},
'language', {'legacy_ams_taxonomy_id': '293', 'type': 'language', 'label': 'Fon', 'concept_id': 'KPgw_t5P_759', 'legacy_ams_taxonomy_num_id': 293}),

( {'type': 'language', 'id': 'AJf5_p3a_b64', 'preferred_label': 'Franska', 'deprecated_legacy_id': '295'},
'language', {'legacy_ams_taxonomy_id': '295', 'type': 'language', 'label': 'Franska', 'concept_id': 'AJf5_p3a_b64', 'legacy_ams_taxonomy_num_id': 295}),

( {'type': 'language', 'id': 'XXq9_Nq8_55j', 'preferred_label': 'Frisiska', 'deprecated_legacy_id': '296'},
'language', {'legacy_ams_taxonomy_id': '296', 'type': 'language', 'label': 'Frisiska', 'concept_id': 'XXq9_Nq8_55j', 'legacy_ams_taxonomy_num_id': 296}),

( {'type': 'language', 'id': 'nUfQ_bem_j5n', 'preferred_label': 'Fulfulde', 'deprecated_legacy_id': '298'},
'language', {'legacy_ams_taxonomy_id': '298', 'type': 'language', 'label': 'Fulfulde', 'concept_id': 'nUfQ_bem_j5n', 'legacy_ams_taxonomy_num_id': 298}),

( {'type': 'language', 'id': '6vC5_KFf_rKT', 'preferred_label': 'Ga', 'deprecated_legacy_id': '300'},
'language', {'legacy_ams_taxonomy_id': '300', 'type': 'language', 'label': 'Ga', 'concept_id': '6vC5_KFf_rKT', 'legacy_ams_taxonomy_num_id': 300}),

( {'type': 'language', 'id': 'ySYP_i3K_WZ2', 'preferred_label': 'Galiciska', 'deprecated_legacy_id': '301'},
'language', {'legacy_ams_taxonomy_id': '301', 'type': 'language', 'label': 'Galiciska', 'concept_id': 'ySYP_i3K_WZ2', 'legacy_ams_taxonomy_num_id': 301}),

( {'type': 'language', 'id': '69WT_Knh_hLs', 'preferred_label': 'Gaeliska (Skotsk)', 'deprecated_legacy_id': '304'},
'language', {'legacy_ams_taxonomy_id': '304', 'type': 'language', 'label': 'Gaeliska (Skotsk)', 'concept_id': '69WT_Knh_hLs', 'legacy_ams_taxonomy_num_id': 304}),

( {'type': 'language', 'id': 'jALF_55z_yDE', 'preferred_label': 'Georgiska', 'deprecated_legacy_id': '305'},
'language', {'legacy_ams_taxonomy_id': '305', 'type': 'language', 'label': 'Georgiska', 'concept_id': 'jALF_55z_yDE', 'legacy_ams_taxonomy_num_id': 305}),

( {'type': 'language', 'id': 'RKH6_hwa_6nk', 'preferred_label': 'Grekiska', 'deprecated_legacy_id': '306'},
'language', {'legacy_ams_taxonomy_id': '306', 'type': 'language', 'label': 'Grekiska', 'concept_id': 'RKH6_hwa_6nk', 'legacy_ams_taxonomy_num_id': 306}),

( {'type': 'language', 'id': 'a1wy_pzH_aQv', 'preferred_label': 'Grönländska', 'deprecated_legacy_id': '307'},
'language', {'legacy_ams_taxonomy_id': '307', 'type': 'language', 'label': 'Grönländska', 'concept_id': 'a1wy_pzH_aQv', 'legacy_ams_taxonomy_num_id': 307}),

( {'type': 'language', 'id': 'dWqu_Ldo_jcD', 'preferred_label': 'Guarani', 'deprecated_legacy_id': '308'},
'language', {'legacy_ams_taxonomy_id': '308', 'type': 'language', 'label': 'Guarani', 'concept_id': 'dWqu_Ldo_jcD', 'legacy_ams_taxonomy_num_id': 308}),

( {'type': 'language', 'id': 'D91H_7iz_Qbn', 'preferred_label': 'Gujarati', 'deprecated_legacy_id': '309'},
'language', {'legacy_ams_taxonomy_id': '309', 'type': 'language', 'label': 'Gujarati', 'concept_id': 'D91H_7iz_Qbn', 'legacy_ams_taxonomy_num_id': 309}),

( {'type': 'language', 'id': 'LaYc_2hR_4i1', 'preferred_label': 'Zenaga', 'deprecated_legacy_id': '312'},
'language', {'legacy_ams_taxonomy_id': '312', 'type': 'language', 'label': 'Zenaga', 'concept_id': 'LaYc_2hR_4i1', 'legacy_ams_taxonomy_num_id': 312}),

( {'type': 'language', 'id': 'ViTs_TnK_DzW', 'preferred_label': 'Hausa', 'deprecated_legacy_id': '313'},
'language', {'legacy_ams_taxonomy_id': '313', 'type': 'language', 'label': 'Hausa', 'concept_id': 'ViTs_TnK_DzW', 'legacy_ams_taxonomy_num_id': 313}),

( {'type': 'language', 'id': 'uWA3_QLq_nu3', 'preferred_label': 'Hebreiska', 'deprecated_legacy_id': '314'},
'language', {'legacy_ams_taxonomy_id': '314', 'type': 'language', 'label': 'Hebreiska', 'concept_id': 'uWA3_QLq_nu3', 'legacy_ams_taxonomy_num_id': 314}),

( {'type': 'language', 'id': 'DFr5_o1T_Bop', 'preferred_label': 'Herero', 'deprecated_legacy_id': '315'},
'language', {'legacy_ams_taxonomy_id': '315', 'type': 'language', 'label': 'Herero', 'concept_id': 'DFr5_o1T_Bop', 'legacy_ams_taxonomy_num_id': 315}),

( {'type': 'language', 'id': 'PwxQ_XQT_MnB', 'preferred_label': 'Hindi', 'deprecated_legacy_id': '316'},
'language', {'legacy_ams_taxonomy_id': '316', 'type': 'language', 'label': 'Hindi', 'concept_id': 'PwxQ_XQT_MnB', 'legacy_ams_taxonomy_num_id': 316}),

( {'type': 'language', 'id': 'kNtt_ZNR_N23', 'preferred_label': 'Igbo', 'deprecated_legacy_id': '317'},
'language', {'legacy_ams_taxonomy_id': '317', 'type': 'language', 'label': 'Igbo', 'concept_id': 'kNtt_ZNR_N23', 'legacy_ams_taxonomy_num_id': 317}),

( {'type': 'language', 'id': 'br9N_CBK_BCz', 'preferred_label': 'Gilbertesiska', 'deprecated_legacy_id': '318'},
'language', {'legacy_ams_taxonomy_id': '318', 'type': 'language', 'label': 'Gilbertesiska', 'concept_id': 'br9N_CBK_BCz', 'legacy_ams_taxonomy_num_id': 318}),

( {'type': 'language', 'id': 'Nojx_VyS_nLf', 'preferred_label': 'Indonesiska', 'deprecated_legacy_id': '319'},
'language', {'legacy_ams_taxonomy_id': '319', 'type': 'language', 'label': 'Indonesiska', 'concept_id': 'Nojx_VyS_nLf', 'legacy_ams_taxonomy_num_id': 319}),

( {'type': 'language', 'id': 'yXoA_zAJ_RoN', 'preferred_label': 'Iriska', 'deprecated_legacy_id': '320'},
'language', {'legacy_ams_taxonomy_id': '320', 'type': 'language', 'label': 'Iriska', 'concept_id': 'yXoA_zAJ_RoN', 'legacy_ams_taxonomy_num_id': 320}),

( {'type': 'language', 'id': 'bvPv_3kf_EQK', 'preferred_label': 'Isländska', 'deprecated_legacy_id': '321'},
'language', {'legacy_ams_taxonomy_id': '321', 'type': 'language', 'label': 'Isländska', 'concept_id': 'bvPv_3kf_EQK', 'legacy_ams_taxonomy_num_id': 321}),

( {'type': 'language', 'id': 'b9cE_KXy_Yxv', 'preferred_label': 'Italienska', 'deprecated_legacy_id': '322'},
'language', {'legacy_ams_taxonomy_id': '322', 'type': 'language', 'label': 'Italienska', 'concept_id': 'b9cE_KXy_Yxv', 'legacy_ams_taxonomy_num_id': 322}),

( {'type': 'language', 'id': 'qmex_uW7_kJw', 'preferred_label': 'Japanska', 'deprecated_legacy_id': '323'},
'language', {'legacy_ams_taxonomy_id': '323', 'type': 'language', 'label': 'Japanska', 'concept_id': 'qmex_uW7_kJw', 'legacy_ams_taxonomy_num_id': 323}),

( {'type': 'language', 'id': 'NNHs_R1H_ESq', 'preferred_label': 'Javanesiska', 'deprecated_legacy_id': '324'},
'language', {'legacy_ams_taxonomy_id': '324', 'type': 'language', 'label': 'Javanesiska', 'concept_id': 'NNHs_R1H_ESq', 'legacy_ams_taxonomy_num_id': 324}),

( {'type': 'language', 'id': 'obg3_pig_x58', 'preferred_label': 'Jiddisch', 'deprecated_legacy_id': '325'},
'language', {'legacy_ams_taxonomy_id': '325', 'type': 'language', 'label': 'Jiddisch', 'concept_id': 'obg3_pig_x58', 'legacy_ams_taxonomy_num_id': 325}),

( {'type': 'language', 'id': 'j4JW_5P4_BGA', 'preferred_label': 'Kabyliska', 'deprecated_legacy_id': '327'},
'language', {'legacy_ams_taxonomy_id': '327', 'type': 'language', 'label': 'Kabyliska', 'concept_id': 'j4JW_5P4_BGA', 'legacy_ams_taxonomy_num_id': 327}),

( {'type': 'language', 'id': '8DQh_CaG_Jsk', 'preferred_label': 'Kamba', 'deprecated_legacy_id': '329'},
'language', {'legacy_ams_taxonomy_id': '329', 'type': 'language', 'label': 'Kamba', 'concept_id': '8DQh_CaG_Jsk', 'legacy_ams_taxonomy_num_id': 329}),

( {'type': 'language', 'id': 'T5kG_Krh_ZBZ', 'preferred_label': 'Kanaresiska', 'deprecated_legacy_id': '330'},
'language', {'legacy_ams_taxonomy_id': '330', 'type': 'language', 'label': 'Kanaresiska', 'concept_id': 'T5kG_Krh_ZBZ', 'legacy_ams_taxonomy_num_id': 330}),

( {'type': 'language', 'id': 'uC3q_W2M_BzA', 'preferred_label': 'Kanuri', 'deprecated_legacy_id': '332'},
'language', {'legacy_ams_taxonomy_id': '332', 'type': 'language', 'label': 'Kanuri', 'concept_id': 'uC3q_W2M_BzA', 'legacy_ams_taxonomy_num_id': 332}),

( {'type': 'language', 'id': 'hq1g_5Md_6PZ', 'preferred_label': 'Karen', 'deprecated_legacy_id': '333'},
'language', {'legacy_ams_taxonomy_id': '333', 'type': 'language', 'label': 'Karen', 'concept_id': 'hq1g_5Md_6PZ', 'legacy_ams_taxonomy_num_id': 333}),

( {'type': 'language', 'id': 'kpf7_mGV_EWk', 'preferred_label': 'Kasmiriska', 'deprecated_legacy_id': '334'},
'language', {'legacy_ams_taxonomy_id': '334', 'type': 'language', 'label': 'Kasmiriska', 'concept_id': 'kpf7_mGV_EWk', 'legacy_ams_taxonomy_num_id': 334}),

( {'type': 'language', 'id': 'YPKq_LRQ_CHT', 'preferred_label': 'Katalanska', 'deprecated_legacy_id': '335'},
'language', {'legacy_ams_taxonomy_id': '335', 'type': 'language', 'label': 'Katalanska', 'concept_id': 'YPKq_LRQ_CHT', 'legacy_ams_taxonomy_num_id': 335}),

( {'type': 'language', 'id': 'mdWX_4PC_wY7', 'preferred_label': 'Kazakiska', 'deprecated_legacy_id': '337'},
'language', {'legacy_ams_taxonomy_id': '337', 'type': 'language', 'label': 'Kazakiska', 'concept_id': 'mdWX_4PC_wY7', 'legacy_ams_taxonomy_num_id': 337}),

( {'type': 'language', 'id': 'LrSS_yQQ_w6c', 'preferred_label': 'Khmer', 'deprecated_legacy_id': '338'},
'language', {'legacy_ams_taxonomy_id': '338', 'type': 'language', 'label': 'Khmer', 'concept_id': 'LrSS_yQQ_w6c', 'legacy_ams_taxonomy_num_id': 338}),

( {'type': 'language', 'id': 'pgjM_8KQ_RZT', 'preferred_label': 'Kinesiska', 'deprecated_legacy_id': '339'},
'language', {'legacy_ams_taxonomy_id': '339', 'type': 'language', 'label': 'Kinesiska', 'concept_id': 'pgjM_8KQ_RZT', 'legacy_ams_taxonomy_num_id': 339}),

( {'type': 'language', 'id': 'TKMX_Mgp_roJ', 'preferred_label': 'Kinyarwanda', 'deprecated_legacy_id': '340'},
'language', {'legacy_ams_taxonomy_id': '340', 'type': 'language', 'label': 'Kinyarwanda', 'concept_id': 'TKMX_Mgp_roJ', 'legacy_ams_taxonomy_num_id': 340}),

( {'type': 'language', 'id': 'H3Mn_eaX_XbQ', 'preferred_label': 'Kirgisiska', 'deprecated_legacy_id': '341'},
'language', {'legacy_ams_taxonomy_id': '341', 'type': 'language', 'label': 'Kirgisiska', 'concept_id': 'H3Mn_eaX_XbQ', 'legacy_ams_taxonomy_num_id': 341}),

( {'type': 'language', 'id': 'i2A1_KpM_osQ', 'preferred_label': 'Kirundi', 'deprecated_legacy_id': '342'},
'language', {'legacy_ams_taxonomy_id': '342', 'type': 'language', 'label': 'Kirundi', 'concept_id': 'i2A1_KpM_osQ', 'legacy_ams_taxonomy_num_id': 342}),

( {'type': 'language', 'id': 'rZYz_myk_W5T', 'preferred_label': 'Kongo', 'deprecated_legacy_id': '343'},
'language', {'legacy_ams_taxonomy_id': '343', 'type': 'language', 'label': 'Kongo', 'concept_id': 'rZYz_myk_W5T', 'legacy_ams_taxonomy_num_id': 343}),

( {'type': 'language', 'id': 'xwTw_Ykx_oMZ', 'preferred_label': 'Konkani', 'deprecated_legacy_id': '344'},
'language', {'legacy_ams_taxonomy_id': '344', 'type': 'language', 'label': 'Konkani', 'concept_id': 'xwTw_Ykx_oMZ', 'legacy_ams_taxonomy_num_id': 344}),

( {'type': 'language', 'id': 'Wj1F_2BF_vtm', 'preferred_label': 'Koreanska', 'deprecated_legacy_id': '345'},
'language', {'legacy_ams_taxonomy_id': '345', 'type': 'language', 'label': 'Koreanska', 'concept_id': 'Wj1F_2BF_vtm', 'legacy_ams_taxonomy_num_id': 345}),

( {'type': 'language', 'id': 'wREW_gh2_Bxu', 'preferred_label': 'Korsikanska', 'deprecated_legacy_id': '346'},
'language', {'legacy_ams_taxonomy_id': '346', 'type': 'language', 'label': 'Korsikanska', 'concept_id': 'wREW_gh2_Bxu', 'legacy_ams_taxonomy_num_id': 346}),

( {'type': 'language', 'id': 'JFiv_95F_eNB', 'preferred_label': 'Kreolska o pidginspråk, portugisisk-baserade,andra', 'deprecated_legacy_id': '348'},
'language', {'legacy_ams_taxonomy_id': '348', 'type': 'language', 'label': 'Kreolska o pidginspråk, portugisisk-baserade,andra', 'concept_id': 'JFiv_95F_eNB', 'legacy_ams_taxonomy_num_id': 348}),

( {'type': 'language', 'id': 'ZUf8_t85_NC6', 'preferred_label': 'Kroatiska', 'deprecated_legacy_id': '350'},
'language', {'legacy_ams_taxonomy_id': '350', 'type': 'language', 'label': 'Kroatiska', 'concept_id': 'ZUf8_t85_NC6', 'legacy_ams_taxonomy_num_id': 350}),

( {'type': 'language', 'id': 'Sgvv_5Vd_xro', 'preferred_label': 'Kru', 'deprecated_legacy_id': '351'},
'language', {'legacy_ams_taxonomy_id': '351', 'type': 'language', 'label': 'Kru', 'concept_id': 'Sgvv_5Vd_xro', 'legacy_ams_taxonomy_num_id': 351}),

( {'type': 'language', 'id': 'JxfJ_kyR_CQZ', 'preferred_label': 'Kurdiska', 'deprecated_legacy_id': '352'},
'language', {'legacy_ams_taxonomy_id': '352', 'type': 'language', 'label': 'Kurdiska', 'concept_id': 'JxfJ_kyR_CQZ', 'legacy_ams_taxonomy_num_id': 352}),

( {'type': 'language', 'id': '95r9_3rM_KWz', 'preferred_label': 'Kikuyu', 'deprecated_legacy_id': '353'},
'language', {'legacy_ams_taxonomy_id': '353', 'type': 'language', 'label': 'Kikuyu', 'concept_id': '95r9_3rM_KWz', 'legacy_ams_taxonomy_num_id': 353}),

( {'type': 'language', 'id': 'FNQx_3PN_eiT', 'preferred_label': 'Lao', 'deprecated_legacy_id': '355'},
'language', {'legacy_ams_taxonomy_id': '355', 'type': 'language', 'label': 'Lao', 'concept_id': 'FNQx_3PN_eiT', 'legacy_ams_taxonomy_num_id': 355}),

( {'type': 'language', 'id': 'YiQE_ERm_7kh', 'preferred_label': 'Lettiska', 'deprecated_legacy_id': '356'},
'language', {'legacy_ams_taxonomy_id': '356', 'type': 'language', 'label': 'Lettiska', 'concept_id': 'YiQE_ERm_7kh', 'legacy_ams_taxonomy_num_id': 356}),

( {'type': 'language', 'id': 'w7am_ASn_5uK', 'preferred_label': 'Lamba', 'deprecated_legacy_id': '357'},
'language', {'legacy_ams_taxonomy_id': '357', 'type': 'language', 'label': 'Lamba', 'concept_id': 'w7am_ASn_5uK', 'legacy_ams_taxonomy_num_id': 357}),

( {'type': 'language', 'id': 'NBKm_cvX_fGo', 'preferred_label': 'Lingala', 'deprecated_legacy_id': '358'},
'language', {'legacy_ams_taxonomy_id': '358', 'type': 'language', 'label': 'Lingala', 'concept_id': 'NBKm_cvX_fGo', 'legacy_ams_taxonomy_num_id': 358}),

( {'type': 'language', 'id': '14pF_wSZ_fnK', 'preferred_label': 'Litauiska', 'deprecated_legacy_id': '359'},
'language', {'legacy_ams_taxonomy_id': '359', 'type': 'language', 'label': 'Litauiska', 'concept_id': '14pF_wSZ_fnK', 'legacy_ams_taxonomy_num_id': 359}),

( {'type': 'language', 'id': 'eW8E_sSU_sbS', 'preferred_label': 'Lozi', 'deprecated_legacy_id': '360'},
'language', {'legacy_ams_taxonomy_id': '360', 'type': 'language', 'label': 'Lozi', 'concept_id': 'eW8E_sSU_sbS', 'legacy_ams_taxonomy_num_id': 360}),

( {'type': 'language', 'id': 'QhX2_SiT_PYf', 'preferred_label': 'Luba-Katanga', 'deprecated_legacy_id': '361'},
'language', {'legacy_ams_taxonomy_id': '361', 'type': 'language', 'label': 'Luba-Katanga', 'concept_id': 'QhX2_SiT_PYf', 'legacy_ams_taxonomy_num_id': 361}),

( {'type': 'language', 'id': 'f3zb_p1z_JDE', 'preferred_label': 'Ganda', 'deprecated_legacy_id': '362'},
'language', {'legacy_ams_taxonomy_id': '362', 'type': 'language', 'label': 'Ganda', 'concept_id': 'f3zb_p1z_JDE', 'legacy_ams_taxonomy_num_id': 362}),

( {'type': 'language', 'id': 'cMMJ_usJ_o6P', 'preferred_label': 'Lunda', 'deprecated_legacy_id': '364'},
'language', {'legacy_ams_taxonomy_id': '364', 'type': 'language', 'label': 'Lunda', 'concept_id': 'cMMJ_usJ_o6P', 'legacy_ams_taxonomy_num_id': 364}),

( {'type': 'language', 'id': '4Vfp_k1i_j59', 'preferred_label': 'Luo (Kenya och Tanzania)', 'deprecated_legacy_id': '365'},
'language', {'legacy_ams_taxonomy_id': '365', 'type': 'language', 'label': 'Luo (Kenya och Tanzania)', 'concept_id': '4Vfp_k1i_j59', 'legacy_ams_taxonomy_num_id': 365}),

( {'type': 'language', 'id': 'PZa9_wAR_1BZ', 'preferred_label': 'Luxemburgiska', 'deprecated_legacy_id': '368'},
'language', {'legacy_ams_taxonomy_id': '368', 'type': 'language', 'label': 'Luxemburgiska', 'concept_id': 'PZa9_wAR_1BZ', 'legacy_ams_taxonomy_num_id': 368}),

( {'type': 'language', 'id': '518r_2Eg_WKo', 'preferred_label': 'Maduresiska', 'deprecated_legacy_id': '369'},
'language', {'legacy_ams_taxonomy_id': '369', 'type': 'language', 'label': 'Maduresiska', 'concept_id': '518r_2Eg_WKo', 'legacy_ams_taxonomy_num_id': 369}),

( {'type': 'language', 'id': 'X8kF_rvX_yuj', 'preferred_label': 'Makedonska', 'deprecated_legacy_id': '370'},
'language', {'legacy_ams_taxonomy_id': '370', 'type': 'language', 'label': 'Makedonska', 'concept_id': 'X8kF_rvX_yuj', 'legacy_ams_taxonomy_num_id': 370}),

( {'type': 'language', 'id': 'dVEU_LsK_m8b', 'preferred_label': 'Malagassiska', 'deprecated_legacy_id': '372'},
'language', {'legacy_ams_taxonomy_id': '372', 'type': 'language', 'label': 'Malagassiska', 'concept_id': 'dVEU_LsK_m8b', 'legacy_ams_taxonomy_num_id': 372}),

( {'type': 'language', 'id': 'TrvD_6oy_oYt', 'preferred_label': 'Malajiska', 'deprecated_legacy_id': '373'},
'language', {'legacy_ams_taxonomy_id': '373', 'type': 'language', 'label': 'Malajiska', 'concept_id': 'TrvD_6oy_oYt', 'legacy_ams_taxonomy_num_id': 373}),

( {'type': 'language', 'id': 'QsrB_4Kx_K7y', 'preferred_label': 'Malayalam', 'deprecated_legacy_id': '374'},
'language', {'legacy_ams_taxonomy_id': '374', 'type': 'language', 'label': 'Malayalam', 'concept_id': 'QsrB_4Kx_K7y', 'legacy_ams_taxonomy_num_id': 374}),

( {'type': 'language', 'id': 'oJMr_g3f_9Z9', 'preferred_label': 'Maltesiska', 'deprecated_legacy_id': '376'},
'language', {'legacy_ams_taxonomy_id': '376', 'type': 'language', 'label': 'Maltesiska', 'concept_id': 'oJMr_g3f_9Z9', 'legacy_ams_taxonomy_num_id': 376}),

( {'type': 'language', 'id': 'cxiY_CBR_1Ve', 'preferred_label': 'Mandarin', 'deprecated_legacy_id': '377'},
'language', {'legacy_ams_taxonomy_id': '377', 'type': 'language', 'label': 'Mandarin', 'concept_id': 'cxiY_CBR_1Ve', 'legacy_ams_taxonomy_num_id': 377}),

( {'type': 'language', 'id': '32J8_SCe_3gR', 'preferred_label': 'Manx', 'deprecated_legacy_id': '381'},
'language', {'legacy_ams_taxonomy_id': '381', 'type': 'language', 'label': 'Manx', 'concept_id': '32J8_SCe_3gR', 'legacy_ams_taxonomy_num_id': 381}),

( {'type': 'language', 'id': 'vG2n_EAN_SwC', 'preferred_label': 'Maori', 'deprecated_legacy_id': '382'},
'language', {'legacy_ams_taxonomy_id': '382', 'type': 'language', 'label': 'Maori', 'concept_id': 'vG2n_EAN_SwC', 'legacy_ams_taxonomy_num_id': 382}),

( {'type': 'language', 'id': 'XadF_xEF_Hvb', 'preferred_label': 'Marathi', 'deprecated_legacy_id': '383'},
'language', {'legacy_ams_taxonomy_id': '383', 'type': 'language', 'label': 'Marathi', 'concept_id': 'XadF_xEF_Hvb', 'legacy_ams_taxonomy_num_id': 383}),

( {'type': 'language', 'id': 'Qgzp_mLJ_4tr', 'preferred_label': 'Mariska', 'deprecated_legacy_id': '384'},
'language', {'legacy_ams_taxonomy_id': '384', 'type': 'language', 'label': 'Mariska', 'concept_id': 'Qgzp_mLJ_4tr', 'legacy_ams_taxonomy_num_id': 384}),

( {'type': 'language', 'id': 'vSm2_7L7_xvL', 'preferred_label': 'Marwariska', 'deprecated_legacy_id': '385'},
'language', {'legacy_ams_taxonomy_id': '385', 'type': 'language', 'label': 'Marwariska', 'concept_id': 'vSm2_7L7_xvL', 'legacy_ams_taxonomy_num_id': 385}),

( {'type': 'language', 'id': 'BHDa_r1G_5Sv', 'preferred_label': 'Masai', 'deprecated_legacy_id': '386'},
'language', {'legacy_ams_taxonomy_id': '386', 'type': 'language', 'label': 'Masai', 'concept_id': 'BHDa_r1G_5Sv', 'legacy_ams_taxonomy_num_id': 386}),

( {'type': 'language', 'id': '53mz_row_sAP', 'preferred_label': 'Maya', 'deprecated_legacy_id': '387'},
'language', {'legacy_ams_taxonomy_id': '387', 'type': 'language', 'label': 'Maya', 'concept_id': '53mz_row_sAP', 'legacy_ams_taxonomy_num_id': 387}),

( {'type': 'language', 'id': 'F6YW_uCL_xCN', 'preferred_label': 'Mende', 'deprecated_legacy_id': '388'},
'language', {'legacy_ams_taxonomy_id': '388', 'type': 'language', 'label': 'Mende', 'concept_id': 'F6YW_uCL_xCN', 'legacy_ams_taxonomy_num_id': 388}),

( {'type': 'language', 'id': 'vWfd_Xrt_hPJ', 'preferred_label': 'Moldaviska', 'deprecated_legacy_id': '390'},
'language', {'legacy_ams_taxonomy_id': '390', 'type': 'language', 'label': 'Moldaviska', 'concept_id': 'vWfd_Xrt_hPJ', 'legacy_ams_taxonomy_num_id': 390}),

( {'type': 'language', 'id': 'J6dh_R8z_Ja4', 'preferred_label': 'Mongolska', 'deprecated_legacy_id': '391'},
'language', {'legacy_ams_taxonomy_id': '391', 'type': 'language', 'label': 'Mongolska', 'concept_id': 'J6dh_R8z_Ja4', 'legacy_ams_taxonomy_num_id': 391}),

( {'type': 'language', 'id': 'T3kB_EuD_ReH', 'preferred_label': 'Mossi', 'deprecated_legacy_id': '393'},
'language', {'legacy_ams_taxonomy_id': '393', 'type': 'language', 'label': 'Mossi', 'concept_id': 'T3kB_EuD_ReH', 'legacy_ams_taxonomy_num_id': 393}),

( {'type': 'language', 'id': 'UNA7_isN_UzK', 'preferred_label': 'Nauhatl', 'deprecated_legacy_id': '397'},
'language', {'legacy_ams_taxonomy_id': '397', 'type': 'language', 'label': 'Nauhatl', 'concept_id': 'UNA7_isN_UzK', 'legacy_ams_taxonomy_num_id': 397}),

( {'type': 'language', 'id': 'jvFH_zK8_KhS', 'preferred_label': 'Nauru', 'deprecated_legacy_id': '398'},
'language', {'legacy_ams_taxonomy_id': '398', 'type': 'language', 'label': 'Nauru', 'concept_id': 'jvFH_zK8_KhS', 'legacy_ams_taxonomy_num_id': 398}),

( {'type': 'language', 'id': 'niMN_h39_tm3', 'preferred_label': 'Nauriska', 'deprecated_legacy_id': '399'},
'language', {'legacy_ams_taxonomy_id': '399', 'type': 'language', 'label': 'Nauriska', 'concept_id': 'niMN_h39_tm3', 'legacy_ams_taxonomy_num_id': 399}),

( {'type': 'language', 'id': '3rPm_Muj_kJT', 'preferred_label': 'Ndebele Nord', 'deprecated_legacy_id': '400'},
'language', {'legacy_ams_taxonomy_id': '400', 'type': 'language', 'label': 'Ndebele Nord', 'concept_id': '3rPm_Muj_kJT', 'legacy_ams_taxonomy_num_id': 400}),

( {'type': 'language', 'id': '6S6h_c57_VnV', 'preferred_label': 'Nederländska', 'deprecated_legacy_id': '401'},
'language', {'legacy_ams_taxonomy_id': '401', 'type': 'language', 'label': 'Nederländska', 'concept_id': '6S6h_c57_VnV', 'legacy_ams_taxonomy_num_id': 401}),

( {'type': 'language', 'id': 'Lwot_W1k_dFW', 'preferred_label': 'Nepalesiska', 'deprecated_legacy_id': '402'},
'language', {'legacy_ams_taxonomy_id': '402', 'type': 'language', 'label': 'Nepalesiska', 'concept_id': 'Lwot_W1k_dFW', 'legacy_ams_taxonomy_num_id': 402}),

( {'type': 'language', 'id': 'pnjj_2JX_Fub', 'preferred_label': 'Norska', 'deprecated_legacy_id': '404'},
'language', {'legacy_ams_taxonomy_id': '404', 'type': 'language', 'label': 'Norska', 'concept_id': 'pnjj_2JX_Fub', 'legacy_ams_taxonomy_num_id': 404}),

( {'type': 'language', 'id': 'Ygzd_HiY_Ksk', 'preferred_label': 'Nyamwezi', 'deprecated_legacy_id': '405'},
'language', {'legacy_ams_taxonomy_id': '405', 'type': 'language', 'label': 'Nyamwezi', 'concept_id': 'Ygzd_HiY_Ksk', 'legacy_ams_taxonomy_num_id': 405}),

( {'type': 'language', 'id': 'Lcsi_xHg_8UW', 'preferred_label': 'Nyanja', 'deprecated_legacy_id': '406'},
'language', {'legacy_ams_taxonomy_id': '406', 'type': 'language', 'label': 'Nyanja', 'concept_id': 'Lcsi_xHg_8UW', 'legacy_ams_taxonomy_num_id': 406}),

( {'type': 'language', 'id': 'MzzG_eb7_ojB', 'preferred_label': 'Occitanska', 'deprecated_legacy_id': '407'},
'language', {'legacy_ams_taxonomy_id': '407', 'type': 'language', 'label': 'Occitanska', 'concept_id': 'MzzG_eb7_ojB', 'legacy_ams_taxonomy_num_id': 407}),

( {'type': 'language', 'id': 'qrBQ_mhN_W7D', 'preferred_label': 'Oriya', 'deprecated_legacy_id': '408'},
'language', {'legacy_ams_taxonomy_id': '408', 'type': 'language', 'label': 'Oriya', 'concept_id': 'qrBQ_mhN_W7D', 'legacy_ams_taxonomy_num_id': 408}),

( {'type': 'language', 'id': 'qDZT_5H6_ZET', 'preferred_label': 'Oromo', 'deprecated_legacy_id': '409'},
'language', {'legacy_ams_taxonomy_id': '409', 'type': 'language', 'label': 'Oromo', 'concept_id': 'qDZT_5H6_ZET', 'legacy_ams_taxonomy_num_id': 409}),

( {'type': 'language', 'id': 'g6Up_z8D_oAY', 'preferred_label': 'Pashto', 'deprecated_legacy_id': '412'},
'language', {'legacy_ams_taxonomy_id': '412', 'type': 'language', 'label': 'Pashto', 'concept_id': 'g6Up_z8D_oAY', 'legacy_ams_taxonomy_num_id': 412}),

( {'type': 'language', 'id': 'sNoC_CTL_8Ph', 'preferred_label': 'Persiska (Farsi)', 'deprecated_legacy_id': '414'},
'language', {'legacy_ams_taxonomy_id': '414', 'type': 'language', 'label': 'Persiska (Farsi)', 'concept_id': 'sNoC_CTL_8Ph', 'legacy_ams_taxonomy_num_id': 414}),

( {'type': 'language', 'id': 'Gp9q_gTW_XAw', 'preferred_label': 'Polska', 'deprecated_legacy_id': '415'},
'language', {'legacy_ams_taxonomy_id': '415', 'type': 'language', 'label': 'Polska', 'concept_id': 'Gp9q_gTW_XAw', 'legacy_ams_taxonomy_num_id': 415}),

( {'type': 'language', 'id': 'W1EH_yKC_wMJ', 'preferred_label': 'Portugisiska', 'deprecated_legacy_id': '416'},
'language', {'legacy_ams_taxonomy_id': '416', 'type': 'language', 'label': 'Portugisiska', 'concept_id': 'W1EH_yKC_wMJ', 'legacy_ams_taxonomy_num_id': 416}),

( {'type': 'language', 'id': 'so8d_dR6_E8j', 'preferred_label': 'Punjabi/Panjabo', 'deprecated_legacy_id': '417'},
'language', {'legacy_ams_taxonomy_id': '417', 'type': 'language', 'label': 'Punjabi/Panjabo', 'concept_id': 'so8d_dR6_E8j', 'legacy_ams_taxonomy_num_id': 417}),

( {'type': 'language', 'id': 'gBd9_PJY_gbT', 'preferred_label': 'Quechua', 'deprecated_legacy_id': '418'},
'language', {'legacy_ams_taxonomy_id': '418', 'type': 'language', 'label': 'Quechua', 'concept_id': 'gBd9_PJY_gbT', 'legacy_ams_taxonomy_num_id': 418}),

( {'type': 'language', 'id': 'ocLM_QVf_661', 'preferred_label': 'Retoromanska', 'deprecated_legacy_id': '420'},
'language', {'legacy_ams_taxonomy_id': '420', 'type': 'language', 'label': 'Retoromanska', 'concept_id': 'ocLM_QVf_661', 'legacy_ams_taxonomy_num_id': 420}),

( {'type': 'language', 'id': 'EgN8_Usb_2f6', 'preferred_label': 'Romani', 'deprecated_legacy_id': '421'},
'language', {'legacy_ams_taxonomy_id': '421', 'type': 'language', 'label': 'Romani', 'concept_id': 'EgN8_Usb_2f6', 'legacy_ams_taxonomy_num_id': 421}),

( {'type': 'language', 'id': 'ZDoY_SrN_mRm', 'preferred_label': 'Rumänska', 'deprecated_legacy_id': '422'},
'language', {'legacy_ams_taxonomy_id': '422', 'type': 'language', 'label': 'Rumänska', 'concept_id': 'ZDoY_SrN_mRm', 'legacy_ams_taxonomy_num_id': 422}),

( {'type': 'language', 'id': 'mHSV_SUD_wXF', 'preferred_label': 'Ryska', 'deprecated_legacy_id': '424'},
'language', {'legacy_ams_taxonomy_id': '424', 'type': 'language', 'label': 'Ryska', 'concept_id': 'mHSV_SUD_wXF', 'legacy_ams_taxonomy_num_id': 424}),

( {'type': 'language', 'id': 'oRUY_CU4_s9t', 'preferred_label': 'Samiska', 'deprecated_legacy_id': '425'},
'language', {'legacy_ams_taxonomy_id': '425', 'type': 'language', 'label': 'Samiska', 'concept_id': 'oRUY_CU4_s9t', 'legacy_ams_taxonomy_num_id': 425}),

( {'type': 'language', 'id': 'qhwA_Zhw_XLy', 'preferred_label': 'Samoanska', 'deprecated_legacy_id': '426'},
'language', {'legacy_ams_taxonomy_id': '426', 'type': 'language', 'label': 'Samoanska', 'concept_id': 'qhwA_Zhw_XLy', 'legacy_ams_taxonomy_num_id': 426}),

( {'type': 'language', 'id': 'wspL_wxs_ud5', 'preferred_label': 'Sango', 'deprecated_legacy_id': '427'},
'language', {'legacy_ams_taxonomy_id': '427', 'type': 'language', 'label': 'Sango', 'concept_id': 'wspL_wxs_ud5', 'legacy_ams_taxonomy_num_id': 427}),

( {'type': 'language', 'id': 'xLXn_NYc_2Zv', 'preferred_label': 'Santali', 'deprecated_legacy_id': '428'},
'language', {'legacy_ams_taxonomy_id': '428', 'type': 'language', 'label': 'Santali', 'concept_id': 'xLXn_NYc_2Zv', 'legacy_ams_taxonomy_num_id': 428}),

( {'type': 'language', 'id': 'jcL9_M9C_mAE', 'preferred_label': 'Sardiska/Sardiniska', 'deprecated_legacy_id': '430'},
'language', {'legacy_ams_taxonomy_id': '430', 'type': 'language', 'label': 'Sardiska/Sardiniska', 'concept_id': 'jcL9_M9C_mAE', 'legacy_ams_taxonomy_num_id': 430}),

( {'type': 'language', 'id': 'CKpA_mgw_jgi', 'preferred_label': 'Serbiska', 'deprecated_legacy_id': '432'},
'language', {'legacy_ams_taxonomy_id': '432', 'type': 'language', 'label': 'Serbiska', 'concept_id': 'CKpA_mgw_jgi', 'legacy_ams_taxonomy_num_id': 432}),

( {'type': 'language', 'id': 'ECrL_6pW_Zuz', 'preferred_label': 'Sesotho/Sesuto/Sotho', 'deprecated_legacy_id': '434'},
'language', {'legacy_ams_taxonomy_id': '434', 'type': 'language', 'label': 'Sesotho/Sesuto/Sotho', 'concept_id': 'ECrL_6pW_Zuz', 'legacy_ams_taxonomy_num_id': 434}),

( {'type': 'language', 'id': 'b7BB_6HR_vNr', 'preferred_label': 'Tswana', 'deprecated_legacy_id': '435'},
'language', {'legacy_ams_taxonomy_id': '435', 'type': 'language', 'label': 'Tswana', 'concept_id': 'b7BB_6HR_vNr', 'legacy_ams_taxonomy_num_id': 435}),

( {'type': 'language', 'id': 'duKn_okc_J3Q', 'preferred_label': 'Shona', 'deprecated_legacy_id': '438'},
'language', {'legacy_ams_taxonomy_id': '438', 'type': 'language', 'label': 'Shona', 'concept_id': 'duKn_okc_J3Q', 'legacy_ams_taxonomy_num_id': 438}),

( {'type': 'language', 'id': '97Zk_gEu_4XD', 'preferred_label': 'Sindhi', 'deprecated_legacy_id': '440'},
'language', {'legacy_ams_taxonomy_id': '440', 'type': 'language', 'label': 'Sindhi', 'concept_id': '97Zk_gEu_4XD', 'legacy_ams_taxonomy_num_id': 440}),

( {'type': 'language', 'id': 'PKJu_KCn_zAV', 'preferred_label': 'Singalesiska/Sinhala', 'deprecated_legacy_id': '441'},
'language', {'legacy_ams_taxonomy_id': '441', 'type': 'language', 'label': 'Singalesiska/Sinhala', 'concept_id': 'PKJu_KCn_zAV', 'legacy_ams_taxonomy_num_id': 441}),

( {'type': 'language', 'id': 'tVYE_xuR_B75', 'preferred_label': 'Swazi/Swati', 'deprecated_legacy_id': '442'},
'language', {'legacy_ams_taxonomy_id': '442', 'type': 'language', 'label': 'Swazi/Swati', 'concept_id': 'tVYE_xuR_B75', 'legacy_ams_taxonomy_num_id': 442}),

( {'type': 'language', 'id': 'rcVk_VU4_zay', 'preferred_label': 'Skotska', 'deprecated_legacy_id': '444'},
'language', {'legacy_ams_taxonomy_id': '444', 'type': 'language', 'label': 'Skotska', 'concept_id': 'rcVk_VU4_zay', 'legacy_ams_taxonomy_num_id': 444}),

( {'type': 'language', 'id': 'mLRm_6VG_s3c', 'preferred_label': 'Slovakiska', 'deprecated_legacy_id': '445'},
'language', {'legacy_ams_taxonomy_id': '445', 'type': 'language', 'label': 'Slovakiska', 'concept_id': 'mLRm_6VG_s3c', 'legacy_ams_taxonomy_num_id': 445}),

( {'type': 'language', 'id': 'H9bz_4TN_vL5', 'preferred_label': 'Slovenska', 'deprecated_legacy_id': '446'},
'language', {'legacy_ams_taxonomy_id': '446', 'type': 'language', 'label': 'Slovenska', 'concept_id': 'H9bz_4TN_vL5', 'legacy_ams_taxonomy_num_id': 446}),

( {'type': 'language', 'id': 'F6dn_vX1_Qnm', 'preferred_label': 'Somaliska', 'deprecated_legacy_id': '447'},
'language', {'legacy_ams_taxonomy_id': '447', 'type': 'language', 'label': 'Somaliska', 'concept_id': 'F6dn_vX1_Qnm', 'legacy_ams_taxonomy_num_id': 447}),

( {'type': 'language', 'id': 'n69w_Nza_N6w', 'preferred_label': 'Songhai', 'deprecated_legacy_id': '448'},
'language', {'legacy_ams_taxonomy_id': '448', 'type': 'language', 'label': 'Songhai', 'concept_id': 'n69w_Nza_N6w', 'legacy_ams_taxonomy_num_id': 448}),

( {'type': 'language', 'id': 'cqJm_R5Q_Gpr', 'preferred_label': 'Sorbiska språk', 'deprecated_legacy_id': '450'},
'language', {'legacy_ams_taxonomy_id': '450', 'type': 'language', 'label': 'Sorbiska språk', 'concept_id': 'cqJm_R5Q_Gpr', 'legacy_ams_taxonomy_num_id': 450}),

( {'type': 'language', 'id': 'i9Hd_bVm_Z3c', 'preferred_label': 'Spanska', 'deprecated_legacy_id': '451'},
'language', {'legacy_ams_taxonomy_id': '451', 'type': 'language', 'label': 'Spanska', 'concept_id': 'i9Hd_bVm_Z3c', 'legacy_ams_taxonomy_num_id': 451}),

( {'type': 'language', 'id': 'popv_w4d_5uB', 'preferred_label': 'Susu', 'deprecated_legacy_id': '453'},
'language', {'legacy_ams_taxonomy_id': '453', 'type': 'language', 'label': 'Susu', 'concept_id': 'popv_w4d_5uB', 'legacy_ams_taxonomy_num_id': 453}),

( {'type': 'language', 'id': 'Hbzs_PLN_Vo8', 'preferred_label': 'Swahili', 'deprecated_legacy_id': '455'},
'language', {'legacy_ams_taxonomy_id': '455', 'type': 'language', 'label': 'Swahili', 'concept_id': 'Hbzs_PLN_Vo8', 'legacy_ams_taxonomy_num_id': 455}),

( {'type': 'language', 'id': 'vYua_sYT_JEg', 'preferred_label': 'Syrianska', 'deprecated_legacy_id': '457'},
'language', {'legacy_ams_taxonomy_id': '457', 'type': 'language', 'label': 'Syrianska', 'concept_id': 'vYua_sYT_JEg', 'legacy_ams_taxonomy_num_id': 457}),

( {'type': 'language', 'id': 'ChZf_oUX_fKG', 'preferred_label': 'Tadjikiska', 'deprecated_legacy_id': '458'},
'language', {'legacy_ams_taxonomy_id': '458', 'type': 'language', 'label': 'Tadjikiska', 'concept_id': 'ChZf_oUX_fKG', 'legacy_ams_taxonomy_num_id': 458}),

( {'type': 'language', 'id': '7EYg_JZj_aU8', 'preferred_label': 'Tagalog', 'deprecated_legacy_id': '459'},
'language', {'legacy_ams_taxonomy_id': '459', 'type': 'language', 'label': 'Tagalog', 'concept_id': '7EYg_JZj_aU8', 'legacy_ams_taxonomy_num_id': 459}),

( {'type': 'language', 'id': 'YAw2_WRr_SAZ', 'preferred_label': 'Tahitiska', 'deprecated_legacy_id': '460'},
'language', {'legacy_ams_taxonomy_id': '460', 'type': 'language', 'label': 'Tahitiska', 'concept_id': 'YAw2_WRr_SAZ', 'legacy_ams_taxonomy_num_id': 460}),

( {'type': 'language', 'id': 'BWtj_uTy_nUS', 'preferred_label': 'Tamashek', 'deprecated_legacy_id': '461'},
'language', {'legacy_ams_taxonomy_id': '461', 'type': 'language', 'label': 'Tamashek', 'concept_id': 'BWtj_uTy_nUS', 'legacy_ams_taxonomy_num_id': 461}),

( {'type': 'language', 'id': 'u4QL_MM1_3zA', 'preferred_label': 'Tamil/Tamilska', 'deprecated_legacy_id': '463'},
'language', {'legacy_ams_taxonomy_id': '463', 'type': 'language', 'label': 'Tamil/Tamilska', 'concept_id': 'u4QL_MM1_3zA', 'legacy_ams_taxonomy_num_id': 463}),

( {'type': 'language', 'id': 'WmiH_iwb_S95', 'preferred_label': 'Tatariska', 'deprecated_legacy_id': '465'},
'language', {'legacy_ams_taxonomy_id': '465', 'type': 'language', 'label': 'Tatariska', 'concept_id': 'WmiH_iwb_S95', 'legacy_ams_taxonomy_num_id': 465}),

( {'type': 'language', 'id': '4N2S_MYT_nt7', 'preferred_label': 'Telugu', 'deprecated_legacy_id': '466'},
'language', {'legacy_ams_taxonomy_id': '466', 'type': 'language', 'label': 'Telugu', 'concept_id': '4N2S_MYT_nt7', 'legacy_ams_taxonomy_num_id': 466}),

( {'type': 'language', 'id': 'YQs2_RvS_UjV', 'preferred_label': 'Timne', 'deprecated_legacy_id': '467'},
'language', {'legacy_ams_taxonomy_id': '467', 'type': 'language', 'label': 'Timne', 'concept_id': 'YQs2_RvS_UjV', 'legacy_ams_taxonomy_num_id': 467}),

( {'type': 'language', 'id': 'ECih_Cev_hwd', 'preferred_label': 'Thailändska', 'deprecated_legacy_id': '469'},
'language', {'legacy_ams_taxonomy_id': '469', 'type': 'language', 'label': 'Thailändska', 'concept_id': 'ECih_Cev_hwd', 'legacy_ams_taxonomy_num_id': 469}),

( {'type': 'language', 'id': 'SXCC_X3z_zYJ', 'preferred_label': 'Tibetanska', 'deprecated_legacy_id': '470'},
'language', {'legacy_ams_taxonomy_id': '470', 'type': 'language', 'label': 'Tibetanska', 'concept_id': 'SXCC_X3z_zYJ', 'legacy_ams_taxonomy_num_id': 470}),

( {'type': 'language', 'id': '4Qvn_5N7_AVk', 'preferred_label': 'Tigre', 'deprecated_legacy_id': '471'},
'language', {'legacy_ams_taxonomy_id': '471', 'type': 'language', 'label': 'Tigre', 'concept_id': '4Qvn_5N7_AVk', 'legacy_ams_taxonomy_num_id': 471}),

( {'type': 'language', 'id': 'nPzh_1oc_EKz', 'preferred_label': 'Tigrinja/Tigrinska', 'deprecated_legacy_id': '472'},
'language', {'legacy_ams_taxonomy_id': '472', 'type': 'language', 'label': 'Tigrinja/Tigrinska', 'concept_id': 'nPzh_1oc_EKz', 'legacy_ams_taxonomy_num_id': 472}),

( {'type': 'language', 'id': 'aAje_qMU_iU9', 'preferred_label': 'Tjeckiska', 'deprecated_legacy_id': '473'},
'language', {'legacy_ams_taxonomy_id': '473', 'type': 'language', 'label': 'Tjeckiska', 'concept_id': 'aAje_qMU_iU9', 'legacy_ams_taxonomy_num_id': 473}),

( {'type': 'language', 'id': 'heWF_H1y_48o', 'preferred_label': 'Tjuvasjiska', 'deprecated_legacy_id': '475'},
'language', {'legacy_ams_taxonomy_id': '475', 'type': 'language', 'label': 'Tjuvasjiska', 'concept_id': 'heWF_H1y_48o', 'legacy_ams_taxonomy_num_id': 475}),

( {'type': 'language', 'id': 'wL3C_poK_fbi', 'preferred_label': 'Tok Pisin', 'deprecated_legacy_id': '476'},
'language', {'legacy_ams_taxonomy_id': '476', 'type': 'language', 'label': 'Tok Pisin', 'concept_id': 'wL3C_poK_fbi', 'legacy_ams_taxonomy_num_id': 476}),

( {'type': 'language', 'id': 'sVLN_wnV_MxS', 'preferred_label': 'Tonga (Nyasa)', 'deprecated_legacy_id': '477'},
'language', {'legacy_ams_taxonomy_id': '477', 'type': 'language', 'label': 'Tonga (Nyasa)', 'concept_id': 'sVLN_wnV_MxS', 'legacy_ams_taxonomy_num_id': 477}),

( {'type': 'language', 'id': 'UaRX_FvG_rKF', 'preferred_label': 'Turkiska', 'deprecated_legacy_id': '481'},
'language', {'legacy_ams_taxonomy_id': '481', 'type': 'language', 'label': 'Turkiska', 'concept_id': 'UaRX_FvG_rKF', 'legacy_ams_taxonomy_num_id': 481}),

( {'type': 'language', 'id': 'HFys_DBy_E5b', 'preferred_label': 'Turkmenska', 'deprecated_legacy_id': '482'},
'language', {'legacy_ams_taxonomy_id': '482', 'type': 'language', 'label': 'Turkmenska', 'concept_id': 'HFys_DBy_E5b', 'legacy_ams_taxonomy_num_id': 482}),

( {'type': 'language', 'id': 'k3AV_Asg_TWU', 'preferred_label': 'Twi', 'deprecated_legacy_id': '483'},
'language', {'legacy_ams_taxonomy_id': '483', 'type': 'language', 'label': 'Twi', 'concept_id': 'k3AV_Asg_TWU', 'legacy_ams_taxonomy_num_id': 483}),

( {'type': 'language', 'id': 'S368_jFS_2hP', 'preferred_label': 'Tyska', 'deprecated_legacy_id': '484'},
'language', {'legacy_ams_taxonomy_id': '484', 'type': 'language', 'label': 'Tyska', 'concept_id': 'S368_jFS_2hP', 'legacy_ams_taxonomy_num_id': 484}),

( {'type': 'language', 'id': 'tAM8_qNg_sW5', 'preferred_label': 'Uiguriska', 'deprecated_legacy_id': '485'},
'language', {'legacy_ams_taxonomy_id': '485', 'type': 'language', 'label': 'Uiguriska', 'concept_id': 'tAM8_qNg_sW5', 'legacy_ams_taxonomy_num_id': 485}),

( {'type': 'language', 'id': 'zkfD_Qwv_kTk', 'preferred_label': 'Ukrainska', 'deprecated_legacy_id': '486'},
'language', {'legacy_ams_taxonomy_id': '486', 'type': 'language', 'label': 'Ukrainska', 'concept_id': 'zkfD_Qwv_kTk', 'legacy_ams_taxonomy_num_id': 486}),

( {'type': 'language', 'id': 'TWvN_hko_Uk5', 'preferred_label': 'Umbundu', 'deprecated_legacy_id': '487'},
'language', {'legacy_ams_taxonomy_id': '487', 'type': 'language', 'label': 'Umbundu', 'concept_id': 'TWvN_hko_Uk5', 'legacy_ams_taxonomy_num_id': 487}),

( {'type': 'language', 'id': 'Cs9j_jSN_wNH', 'preferred_label': 'Ungerska', 'deprecated_legacy_id': '488'},
'language', {'legacy_ams_taxonomy_id': '488', 'type': 'language', 'label': 'Ungerska', 'concept_id': 'Cs9j_jSN_wNH', 'legacy_ams_taxonomy_num_id': 488}),

( {'type': 'language', 'id': 'Jw1X_vyk_SbY', 'preferred_label': 'Urdu', 'deprecated_legacy_id': '489'},
'language', {'legacy_ams_taxonomy_id': '489', 'type': 'language', 'label': 'Urdu', 'concept_id': 'Jw1X_vyk_SbY', 'legacy_ams_taxonomy_num_id': 489}),

( {'type': 'language', 'id': '3Xxo_J8v_rez', 'preferred_label': 'Uzbekiska', 'deprecated_legacy_id': '491'},
'language', {'legacy_ams_taxonomy_id': '491', 'type': 'language', 'label': 'Uzbekiska', 'concept_id': '3Xxo_J8v_rez', 'legacy_ams_taxonomy_num_id': 491}),

( {'type': 'language', 'id': 'RqHU_aEE_tJA', 'preferred_label': 'Vietnamesiska', 'deprecated_legacy_id': '492'},
'language', {'legacy_ams_taxonomy_id': '492', 'type': 'language', 'label': 'Vietnamesiska', 'concept_id': 'RqHU_aEE_tJA', 'legacy_ams_taxonomy_num_id': 492}),

( {'type': 'language', 'id': 'i2wN_KYK_kRT', 'preferred_label': 'Vitryska', 'deprecated_legacy_id': '493'},
'language', {'legacy_ams_taxonomy_id': '493', 'type': 'language', 'label': 'Vitryska', 'concept_id': 'i2wN_KYK_kRT', 'legacy_ams_taxonomy_num_id': 493}),

( {'type': 'language', 'id': 'jFod_528_29i', 'preferred_label': 'Walesiska', 'deprecated_legacy_id': '494'},
'language', {'legacy_ams_taxonomy_id': '494', 'type': 'language', 'label': 'Walesiska', 'concept_id': 'jFod_528_29i', 'legacy_ams_taxonomy_num_id': 494}),

( {'type': 'language', 'id': 'GF87_h5U_r6r', 'preferred_label': 'Wolof', 'deprecated_legacy_id': '495'},
'language', {'legacy_ams_taxonomy_id': '495', 'type': 'language', 'label': 'Wolof', 'concept_id': 'GF87_h5U_r6r', 'legacy_ams_taxonomy_num_id': 495}),

( {'type': 'language', 'id': 'ZExV_tjM_Nbo', 'preferred_label': 'Xhosa/Isixhosa', 'deprecated_legacy_id': '496'},
'language', {'legacy_ams_taxonomy_id': '496', 'type': 'language', 'label': 'Xhosa/Isixhosa', 'concept_id': 'ZExV_tjM_Nbo', 'legacy_ams_taxonomy_num_id': 496}),

( {'type': 'language', 'id': 'hWuY_56Y_gEA', 'preferred_label': 'Yoruba', 'deprecated_legacy_id': '498'},
'language', {'legacy_ams_taxonomy_id': '498', 'type': 'language', 'label': 'Yoruba', 'concept_id': 'hWuY_56Y_gEA', 'legacy_ams_taxonomy_num_id': 498}),

( {'type': 'language', 'id': '5sps_VbV_7DK', 'preferred_label': 'Zande/Azande', 'deprecated_legacy_id': '499'},
'language', {'legacy_ams_taxonomy_id': '499', 'type': 'language', 'label': 'Zande/Azande', 'concept_id': '5sps_VbV_7DK', 'legacy_ams_taxonomy_num_id': 499}),

( {'type': 'language', 'id': 'RD8Y_xdr_zdh', 'preferred_label': 'Zhuang', 'deprecated_legacy_id': '500'},
'language', {'legacy_ams_taxonomy_id': '500', 'type': 'language', 'label': 'Zhuang', 'concept_id': 'RD8Y_xdr_zdh', 'legacy_ams_taxonomy_num_id': 500}),

( {'type': 'language', 'id': 'Mhbn_hgq_sa6', 'preferred_label': 'Zulu', 'deprecated_legacy_id': '501'},
'language', {'legacy_ams_taxonomy_id': '501', 'type': 'language', 'label': 'Zulu', 'concept_id': 'Mhbn_hgq_sa6', 'legacy_ams_taxonomy_num_id': 501}),

( {'type': 'language', 'id': 'zSLA_vw2_FXN', 'preferred_label': 'Svenska', 'deprecated_legacy_id': '502'},
'language', {'legacy_ams_taxonomy_id': '502', 'type': 'language', 'label': 'Svenska', 'concept_id': 'zSLA_vw2_FXN', 'legacy_ams_taxonomy_num_id': 502}),

( {'type': 'language', 'id': 'fYiW_4KZ_MHa', 'preferred_label': 'Teckenspråk', 'deprecated_legacy_id': '503'},
'language', {'legacy_ams_taxonomy_id': '503', 'type': 'language', 'label': 'Teckenspråk', 'concept_id': 'fYiW_4KZ_MHa', 'legacy_ams_taxonomy_num_id': 503}),

( {'type': 'language', 'id': '43Dr_HRx_rRx', 'preferred_label': 'Adangme', 'deprecated_legacy_id': '10015'},
'language', {'legacy_ams_taxonomy_id': '10015', 'type': 'language', 'label': 'Adangme', 'concept_id': '43Dr_HRx_rRx', 'legacy_ams_taxonomy_num_id': 10015}),

( {'type': 'language', 'id': 'uDkg_vvB_DsN', 'preferred_label': 'Afrihili', 'deprecated_legacy_id': '10017'},
'language', {'legacy_ams_taxonomy_id': '10017', 'type': 'language', 'label': 'Afrihili', 'concept_id': 'uDkg_vvB_DsN', 'legacy_ams_taxonomy_num_id': 10017}),

( {'type': 'language', 'id': 'RfoX_qzv_t7i', 'preferred_label': 'Afro-asiatiska språk', 'deprecated_legacy_id': '10265'},
'language', {'legacy_ams_taxonomy_id': '10265', 'type': 'language', 'label': 'Afro-asiatiska språk', 'concept_id': 'RfoX_qzv_t7i', 'legacy_ams_taxonomy_num_id': 10265}),

( {'type': 'language', 'id': 'pYrC_AU4_73N', 'preferred_label': 'Akkadiska', 'deprecated_legacy_id': '10266'},
'language', {'legacy_ams_taxonomy_id': '10266', 'type': 'language', 'label': 'Akkadiska', 'concept_id': 'pYrC_AU4_73N', 'legacy_ams_taxonomy_num_id': 10266}),

( {'type': 'language', 'id': 'c2mi_duS_MDX', 'preferred_label': 'Aleutiska', 'deprecated_legacy_id': '10267'},
'language', {'legacy_ams_taxonomy_id': '10267', 'type': 'language', 'label': 'Aleutiska', 'concept_id': 'c2mi_duS_MDX', 'legacy_ams_taxonomy_num_id': 10267}),

( {'type': 'language', 'id': 'N6Hi_gMb_u2D', 'preferred_label': 'Algokinspråk', 'deprecated_legacy_id': '10268'},
'language', {'legacy_ams_taxonomy_id': '10268', 'type': 'language', 'label': 'Algokinspråk', 'concept_id': 'N6Hi_gMb_u2D', 'legacy_ams_taxonomy_num_id': 10268}),

( {'type': 'language', 'id': 'jTzC_UTB_m7d', 'preferred_label': 'Altaiska språk, andra', 'deprecated_legacy_id': '10269'},
'language', {'legacy_ams_taxonomy_id': '10269', 'type': 'language', 'label': 'Altaiska språk, andra', 'concept_id': 'jTzC_UTB_m7d', 'legacy_ams_taxonomy_num_id': 10269}),

( {'type': 'language', 'id': 'b6Pn_p3r_nDz', 'preferred_label': 'Apachespråk', 'deprecated_legacy_id': '10270'},
'language', {'legacy_ams_taxonomy_id': '10270', 'type': 'language', 'label': 'Apachespråk', 'concept_id': 'b6Pn_p3r_nDz', 'legacy_ams_taxonomy_num_id': 10270}),

( {'type': 'language', 'id': 'Z19f_uKB_oTc', 'preferred_label': 'Arapaho', 'deprecated_legacy_id': '10271'},
'language', {'legacy_ams_taxonomy_id': '10271', 'type': 'language', 'label': 'Arapaho', 'concept_id': 'Z19f_uKB_oTc', 'legacy_ams_taxonomy_num_id': 10271}),

( {'type': 'language', 'id': 'cZpA_5RH_23F', 'preferred_label': 'Araukiska', 'deprecated_legacy_id': '10272'},
'language', {'legacy_ams_taxonomy_id': '10272', 'type': 'language', 'label': 'Araukiska', 'concept_id': 'cZpA_5RH_23F', 'legacy_ams_taxonomy_num_id': 10272}),

( {'type': 'language', 'id': 'J4aC_yPX_Sgt', 'preferred_label': 'Arawak', 'deprecated_legacy_id': '10273'},
'language', {'legacy_ams_taxonomy_id': '10273', 'type': 'language', 'label': 'Arawak', 'concept_id': 'J4aC_yPX_Sgt', 'legacy_ams_taxonomy_num_id': 10273}),

( {'type': 'language', 'id': 'XVwz_HH4_PjB', 'preferred_label': 'Artificiella, andra språk', 'deprecated_legacy_id': '10274'},
'language', {'legacy_ams_taxonomy_id': '10274', 'type': 'language', 'label': 'Artificiella, andra språk', 'concept_id': 'XVwz_HH4_PjB', 'legacy_ams_taxonomy_num_id': 10274}),

( {'type': 'language', 'id': 'A2Ts_vri_15G', 'preferred_label': 'Athapaskspråk', 'deprecated_legacy_id': '10275'},
'language', {'legacy_ams_taxonomy_id': '10275', 'type': 'language', 'label': 'Athapaskspråk', 'concept_id': 'A2Ts_vri_15G', 'legacy_ams_taxonomy_num_id': 10275}),

( {'type': 'language', 'id': 'xPeV_5HK_buN', 'preferred_label': 'Australiska språk', 'deprecated_legacy_id': '10276'},
'language', {'legacy_ams_taxonomy_id': '10276', 'type': 'language', 'label': 'Australiska språk', 'concept_id': 'xPeV_5HK_buN', 'legacy_ams_taxonomy_num_id': 10276}),

( {'type': 'language', 'id': '6RTo_v81_XdA', 'preferred_label': 'Austronesiska språk, andra', 'deprecated_legacy_id': '10277'},
'language', {'legacy_ams_taxonomy_id': '10277', 'type': 'language', 'label': 'Austronesiska språk, andra', 'concept_id': '6RTo_v81_XdA', 'legacy_ams_taxonomy_num_id': 10277}),

( {'type': 'language', 'id': 'WzGi_zgx_pbJ', 'preferred_label': 'Avariska', 'deprecated_legacy_id': '10278'},
'language', {'legacy_ams_taxonomy_id': '10278', 'type': 'language', 'label': 'Avariska', 'concept_id': 'WzGi_zgx_pbJ', 'legacy_ams_taxonomy_num_id': 10278}),

( {'type': 'language', 'id': 'V5kR_n15_m1D', 'preferred_label': 'Avestiska', 'deprecated_legacy_id': '10279'},
'language', {'legacy_ams_taxonomy_id': '10279', 'type': 'language', 'label': 'Avestiska', 'concept_id': 'V5kR_n15_m1D', 'legacy_ams_taxonomy_num_id': 10279}),

( {'type': 'language', 'id': 'fwrn_bpj_4vM', 'preferred_label': 'Awadhi', 'deprecated_legacy_id': '10280'},
'language', {'legacy_ams_taxonomy_id': '10280', 'type': 'language', 'label': 'Awadhi', 'concept_id': 'fwrn_bpj_4vM', 'legacy_ams_taxonomy_num_id': 10280}),

( {'type': 'language', 'id': 'DKjf_DVV_vxC', 'preferred_label': 'Acoli', 'deprecated_legacy_id': '10282'},
'language', {'legacy_ams_taxonomy_id': '10282', 'type': 'language', 'label': 'Acoli', 'concept_id': 'DKjf_DVV_vxC', 'legacy_ams_taxonomy_num_id': 10282}),

( {'type': 'language', 'id': '4tXr_2NX_SSy', 'preferred_label': 'Abkhasiska', 'deprecated_legacy_id': '10283'},
'language', {'legacy_ams_taxonomy_id': '10283', 'type': 'language', 'label': 'Abkhasiska', 'concept_id': '4tXr_2NX_SSy', 'legacy_ams_taxonomy_num_id': 10283}),

( {'type': 'language', 'id': 'YJcD_PeA_u4M', 'preferred_label': 'Akinesiska', 'deprecated_legacy_id': '10284'},
'language', {'legacy_ams_taxonomy_id': '10284', 'type': 'language', 'label': 'Akinesiska', 'concept_id': 'YJcD_PeA_u4M', 'legacy_ams_taxonomy_num_id': 10284}),

( {'type': 'language', 'id': 'GcJV_gXD_7YU', 'preferred_label': 'Balinesiska', 'deprecated_legacy_id': '10285'},
'language', {'legacy_ams_taxonomy_id': '10285', 'type': 'language', 'label': 'Balinesiska', 'concept_id': 'GcJV_gXD_7YU', 'legacy_ams_taxonomy_num_id': 10285}),

( {'type': 'language', 'id': 'gFSD_xzk_PfW', 'preferred_label': 'Baltiska språk, andra', 'deprecated_legacy_id': '10286'},
'language', {'legacy_ams_taxonomy_id': '10286', 'type': 'language', 'label': 'Baltiska språk, andra', 'concept_id': 'gFSD_xzk_PfW', 'legacy_ams_taxonomy_num_id': 10286}),

( {'type': 'language', 'id': 'c8iN_jaP_6xc', 'preferred_label': 'Bamilekespråk', 'deprecated_legacy_id': '10288'},
'language', {'legacy_ams_taxonomy_id': '10288', 'type': 'language', 'label': 'Bamilekespråk', 'concept_id': 'c8iN_jaP_6xc', 'legacy_ams_taxonomy_num_id': 10288}),

( {'type': 'language', 'id': 'SRr4_baB_iSZ', 'preferred_label': 'Basa', 'deprecated_legacy_id': '10289'},
'language', {'legacy_ams_taxonomy_id': '10289', 'type': 'language', 'label': 'Basa', 'concept_id': 'SRr4_baB_iSZ', 'legacy_ams_taxonomy_num_id': 10289}),

( {'type': 'language', 'id': 'YHen_8sx_MDd', 'preferred_label': 'Batak (Indonésien)', 'deprecated_legacy_id': '10290'},
'language', {'legacy_ams_taxonomy_id': '10290', 'type': 'language', 'label': 'Batak (Indonésien)', 'concept_id': 'YHen_8sx_MDd', 'legacy_ams_taxonomy_num_id': 10290}),

( {'type': 'language', 'id': 'Tzjp_quT_tX2', 'preferred_label': 'Bedja', 'deprecated_legacy_id': '10291'},
'language', {'legacy_ams_taxonomy_id': '10291', 'type': 'language', 'label': 'Bedja', 'concept_id': 'Tzjp_quT_tX2', 'legacy_ams_taxonomy_num_id': 10291}),

( {'type': 'language', 'id': 'czZH_fp8_mbz', 'preferred_label': 'Bodjpuri', 'deprecated_legacy_id': '10292'},
'language', {'legacy_ams_taxonomy_id': '10292', 'type': 'language', 'label': 'Bodjpuri', 'concept_id': 'czZH_fp8_mbz', 'legacy_ams_taxonomy_num_id': 10292}),

( {'type': 'language', 'id': 'fyTh_C2w_gsy', 'preferred_label': 'Bikol', 'deprecated_legacy_id': '10293'},
'language', {'legacy_ams_taxonomy_id': '10293', 'type': 'language', 'label': 'Bikol', 'concept_id': 'fyTh_C2w_gsy', 'legacy_ams_taxonomy_num_id': 10293}),

( {'type': 'language', 'id': 'QZv8_dXK_dw8', 'preferred_label': 'Bini', 'deprecated_legacy_id': '10294'},
'language', {'legacy_ams_taxonomy_id': '10294', 'type': 'language', 'label': 'Bini', 'concept_id': 'QZv8_dXK_dw8', 'legacy_ams_taxonomy_num_id': 10294}),

( {'type': 'language', 'id': 'XdkL_b18_aE3', 'preferred_label': 'Braj', 'deprecated_legacy_id': '10295'},
'language', {'legacy_ams_taxonomy_id': '10295', 'type': 'language', 'label': 'Braj', 'concept_id': 'XdkL_b18_aE3', 'legacy_ams_taxonomy_num_id': 10295}),

( {'type': 'language', 'id': 'txVD_z3R_P4q', 'preferred_label': 'Bretonska', 'deprecated_legacy_id': '10296'},
'language', {'legacy_ams_taxonomy_id': '10296', 'type': 'language', 'label': 'Bretonska', 'concept_id': 'txVD_z3R_P4q', 'legacy_ams_taxonomy_num_id': 10296}),

( {'type': 'language', 'id': 'becB_LRA_kuX', 'preferred_label': 'Buginesiska', 'deprecated_legacy_id': '10297'},
'language', {'legacy_ams_taxonomy_id': '10297', 'type': 'language', 'label': 'Buginesiska', 'concept_id': 'becB_LRA_kuX', 'legacy_ams_taxonomy_num_id': 10297}),

( {'type': 'language', 'id': '6VZ8_y2a_ViA', 'preferred_label': 'Burjatiska', 'deprecated_legacy_id': '10298'},
'language', {'legacy_ams_taxonomy_id': '10298', 'type': 'language', 'label': 'Burjatiska', 'concept_id': '6VZ8_y2a_ViA', 'legacy_ams_taxonomy_num_id': 10298}),

( {'type': 'language', 'id': 'A5So_xQX_T59', 'preferred_label': 'Caddo', 'deprecated_legacy_id': '10299'},
'language', {'legacy_ams_taxonomy_id': '10299', 'type': 'language', 'label': 'Caddo', 'concept_id': 'A5So_xQX_T59', 'legacy_ams_taxonomy_num_id': 10299}),

( {'type': 'language', 'id': 'QFad_Hdf_EqL', 'preferred_label': 'Karibiska', 'deprecated_legacy_id': '10300'},
'language', {'legacy_ams_taxonomy_id': '10300', 'type': 'language', 'label': 'Karibiska', 'concept_id': 'QFad_Hdf_EqL', 'legacy_ams_taxonomy_num_id': 10300}),

( {'type': 'language', 'id': 'VpQH_nua_x5a', 'preferred_label': 'Kaukasiska språk, andra', 'deprecated_legacy_id': '10302'},
'language', {'legacy_ams_taxonomy_id': '10302', 'type': 'language', 'label': 'Kaukasiska språk, andra', 'concept_id': 'VpQH_nua_x5a', 'legacy_ams_taxonomy_num_id': 10302}),

( {'type': 'language', 'id': 'yy2q_Zsg_6Ei', 'preferred_label': 'Keltiska språk, andra', 'deprecated_legacy_id': '10303'},
'language', {'legacy_ams_taxonomy_id': '10303', 'type': 'language', 'label': 'Keltiska språk, andra', 'concept_id': 'yy2q_Zsg_6Ei', 'legacy_ams_taxonomy_num_id': 10303}),

( {'type': 'language', 'id': 'DUK5_RYX_sRe', 'preferred_label': 'Centralamerikanska indianspråk, andra', 'deprecated_legacy_id': '10304'},
'language', {'legacy_ams_taxonomy_id': '10304', 'type': 'language', 'label': 'Centralamerikanska indianspråk, andra', 'concept_id': 'DUK5_RYX_sRe', 'legacy_ams_taxonomy_num_id': 10304}),

( {'type': 'language', 'id': 'FnFC_4cc_heD', 'preferred_label': 'Chagatai', 'deprecated_legacy_id': '10305'},
'language', {'legacy_ams_taxonomy_id': '10305', 'type': 'language', 'label': 'Chagatai', 'concept_id': 'FnFC_4cc_heD', 'legacy_ams_taxonomy_num_id': 10305}),

( {'type': 'language', 'id': 'uyoM_hFZ_kh1', 'preferred_label': 'Chamiska språk', 'deprecated_legacy_id': '10306'},
'language', {'legacy_ams_taxonomy_id': '10306', 'type': 'language', 'label': 'Chamiska språk', 'concept_id': 'uyoM_hFZ_kh1', 'legacy_ams_taxonomy_num_id': 10306}),

( {'type': 'language', 'id': 'FyZY_BJZ_yz6', 'preferred_label': 'Chamorro', 'deprecated_legacy_id': '10307'},
'language', {'legacy_ams_taxonomy_id': '10307', 'type': 'language', 'label': 'Chamorro', 'concept_id': 'FyZY_BJZ_yz6', 'legacy_ams_taxonomy_num_id': 10307}),

( {'type': 'language', 'id': 'wfdT_62G_T49', 'preferred_label': 'Tjetjenska', 'deprecated_legacy_id': '10308'},
'language', {'legacy_ams_taxonomy_id': '10308', 'type': 'language', 'label': 'Tjetjenska', 'concept_id': 'wfdT_62G_T49', 'legacy_ams_taxonomy_num_id': 10308}),

( {'type': 'language', 'id': 'p8af_zMR_sbN', 'preferred_label': 'Cherokee', 'deprecated_legacy_id': '10309'},
'language', {'legacy_ams_taxonomy_id': '10309', 'type': 'language', 'label': 'Cherokee', 'concept_id': 'p8af_zMR_sbN', 'legacy_ams_taxonomy_num_id': 10309}),

( {'type': 'language', 'id': 'NaRV_tAk_xx2', 'preferred_label': 'Cheyenne', 'deprecated_legacy_id': '10310'},
'language', {'legacy_ams_taxonomy_id': '10310', 'type': 'language', 'label': 'Cheyenne', 'concept_id': 'NaRV_tAk_xx2', 'legacy_ams_taxonomy_num_id': 10310}),

( {'type': 'language', 'id': 'mxQt_Uiv_X9B', 'preferred_label': 'Chibcha', 'deprecated_legacy_id': '10311'},
'language', {'legacy_ams_taxonomy_id': '10311', 'type': 'language', 'label': 'Chibcha', 'concept_id': 'mxQt_Uiv_X9B', 'legacy_ams_taxonomy_num_id': 10311}),

( {'type': 'language', 'id': 'tfxQ_bTn_jVT', 'preferred_label': 'Tsjinuksjargong', 'deprecated_legacy_id': '10312'},
'language', {'legacy_ams_taxonomy_id': '10312', 'type': 'language', 'label': 'Tsjinuksjargong', 'concept_id': 'tfxQ_bTn_jVT', 'legacy_ams_taxonomy_num_id': 10312}),

( {'type': 'language', 'id': 'tKF5_Dm1_PL5', 'preferred_label': 'Chipewyan', 'deprecated_legacy_id': '10313'},
'language', {'legacy_ams_taxonomy_id': '10313', 'type': 'language', 'label': 'Chipewyan', 'concept_id': 'tKF5_Dm1_PL5', 'legacy_ams_taxonomy_num_id': 10313}),

( {'type': 'language', 'id': 'CFaA_CXW_ZLx', 'preferred_label': 'Choctaw', 'deprecated_legacy_id': '10314'},
'language', {'legacy_ams_taxonomy_id': '10314', 'type': 'language', 'label': 'Choctaw', 'concept_id': 'CFaA_CXW_ZLx', 'legacy_ams_taxonomy_num_id': 10314}),

( {'type': 'language', 'id': 'WfH2_RjG_CyW', 'preferred_label': 'Kyrkslaviska', 'deprecated_legacy_id': '10315'},
'language', {'legacy_ams_taxonomy_id': '10315', 'type': 'language', 'label': 'Kyrkslaviska', 'concept_id': 'WfH2_RjG_CyW', 'legacy_ams_taxonomy_num_id': 10315}),

( {'type': 'language', 'id': 'Utfp_DGP_qCS', 'preferred_label': 'Chuukesiska', 'deprecated_legacy_id': '10316'},
'language', {'legacy_ams_taxonomy_id': '10316', 'type': 'language', 'label': 'Chuukesiska', 'concept_id': 'Utfp_DGP_qCS', 'legacy_ams_taxonomy_num_id': 10316}),

( {'type': 'language', 'id': 'gnmK_CdW_viG', 'preferred_label': 'Koptiska', 'deprecated_legacy_id': '10317'},
'language', {'legacy_ams_taxonomy_id': '10317', 'type': 'language', 'label': 'Koptiska', 'concept_id': 'gnmK_CdW_viG', 'legacy_ams_taxonomy_num_id': 10317}),

( {'type': 'language', 'id': 'vk8M_mQD_JtZ', 'preferred_label': 'Korniska', 'deprecated_legacy_id': '10318'},
'language', {'legacy_ams_taxonomy_id': '10318', 'type': 'language', 'label': 'Korniska', 'concept_id': 'vk8M_mQD_JtZ', 'legacy_ams_taxonomy_num_id': 10318}),

( {'type': 'language', 'id': 'Y3nz_qvd_DBW', 'preferred_label': 'Cree', 'deprecated_legacy_id': '10319'},
'language', {'legacy_ams_taxonomy_id': '10319', 'type': 'language', 'label': 'Cree', 'concept_id': 'Y3nz_qvd_DBW', 'legacy_ams_taxonomy_num_id': 10319}),

( {'type': 'language', 'id': 'FzWY_WsG_LUy', 'preferred_label': 'Muskogee', 'deprecated_legacy_id': '10320'},
'language', {'legacy_ams_taxonomy_id': '10320', 'type': 'language', 'label': 'Muskogee', 'concept_id': 'FzWY_WsG_LUy', 'legacy_ams_taxonomy_num_id': 10320}),

( {'type': 'language', 'id': 'e5AH_3JG_yc1', 'preferred_label': 'Kreolska och pidginspråk, andra', 'deprecated_legacy_id': '10321'},
'language', {'legacy_ams_taxonomy_id': '10321', 'type': 'language', 'label': 'Kreolska och pidginspråk, andra', 'concept_id': 'e5AH_3JG_yc1', 'legacy_ams_taxonomy_num_id': 10321}),

( {'type': 'language', 'id': 'h35H_jBx_XFy', 'preferred_label': 'Kreolska och pidginspråk, engelsk-baserade, andra', 'deprecated_legacy_id': '10322'},
'language', {'legacy_ams_taxonomy_id': '10322', 'type': 'language', 'label': 'Kreolska och pidginspråk, engelsk-baserade, andra', 'concept_id': 'h35H_jBx_XFy', 'legacy_ams_taxonomy_num_id': 10322}),

( {'type': 'language', 'id': 'R8WU_fQb_Y4E', 'preferred_label': 'Kreolska och pidginspråk, fransk-baserade, andra', 'deprecated_legacy_id': '10323'},
'language', {'legacy_ams_taxonomy_id': '10323', 'type': 'language', 'label': 'Kreolska och pidginspråk, fransk-baserade, andra', 'concept_id': 'R8WU_fQb_Y4E', 'legacy_ams_taxonomy_num_id': 10323}),

( {'type': 'language', 'id': 'LrDr_WmD_Ljb', 'preferred_label': 'Kusjittiska språk, andra', 'deprecated_legacy_id': '10324'},
'language', {'legacy_ams_taxonomy_id': '10324', 'type': 'language', 'label': 'Kusjittiska språk, andra', 'concept_id': 'LrDr_WmD_Ljb', 'legacy_ams_taxonomy_num_id': 10324}),

( {'type': 'language', 'id': 'MXbX_9Du_nkp', 'preferred_label': 'Dakota', 'deprecated_legacy_id': '10325'},
'language', {'legacy_ams_taxonomy_id': '10325', 'type': 'language', 'label': 'Dakota', 'concept_id': 'MXbX_9Du_nkp', 'legacy_ams_taxonomy_num_id': 10325}),

( {'type': 'language', 'id': 'AvDT_nvZ_uig', 'preferred_label': 'Dajak', 'deprecated_legacy_id': '10326'},
'language', {'legacy_ams_taxonomy_id': '10326', 'type': 'language', 'label': 'Dajak', 'concept_id': 'AvDT_nvZ_uig', 'legacy_ams_taxonomy_num_id': 10326}),

( {'type': 'language', 'id': 'Kpju_m4q_31i', 'preferred_label': 'Delaware', 'deprecated_legacy_id': '10327'},
'language', {'legacy_ams_taxonomy_id': '10327', 'type': 'language', 'label': 'Delaware', 'concept_id': 'Kpju_m4q_31i', 'legacy_ams_taxonomy_num_id': 10327}),

( {'type': 'language', 'id': 'ttMr_bG7_Go7', 'preferred_label': 'Dogri', 'deprecated_legacy_id': '10328'},
'language', {'legacy_ams_taxonomy_id': '10328', 'type': 'language', 'label': 'Dogri', 'concept_id': 'ttMr_bG7_Go7', 'legacy_ams_taxonomy_num_id': 10328}),

( {'type': 'language', 'id': 'utaN_9Tq_yM3', 'preferred_label': 'Dogrib', 'deprecated_legacy_id': '10329'},
'language', {'legacy_ams_taxonomy_id': '10329', 'type': 'language', 'label': 'Dogrib', 'concept_id': 'utaN_9Tq_yM3', 'legacy_ams_taxonomy_num_id': 10329}),

( {'type': 'language', 'id': '9AnT_Sut_QBs', 'preferred_label': 'Dravidiska språk, andra', 'deprecated_legacy_id': '10330'},
'language', {'legacy_ams_taxonomy_id': '10330', 'type': 'language', 'label': 'Dravidiska språk, andra', 'concept_id': '9AnT_Sut_QBs', 'legacy_ams_taxonomy_num_id': 10330}),

( {'type': 'language', 'id': 'HnUQ_rah_Akr', 'preferred_label': 'Duala', 'deprecated_legacy_id': '10331'},
'language', {'legacy_ams_taxonomy_id': '10331', 'type': 'language', 'label': 'Duala', 'concept_id': 'HnUQ_rah_Akr', 'legacy_ams_taxonomy_num_id': 10331}),

( {'type': 'language', 'id': '7a3p_ktX_vgP', 'preferred_label': 'Medelnederländska', 'deprecated_legacy_id': '10332'},
'language', {'legacy_ams_taxonomy_id': '10332', 'type': 'language', 'label': 'Medelnederländska', 'concept_id': '7a3p_ktX_vgP', 'legacy_ams_taxonomy_num_id': 10332}),

( {'type': 'language', 'id': 'bJXz_kjT_LfC', 'preferred_label': 'Egyptiska', 'deprecated_legacy_id': '10333'},
'language', {'legacy_ams_taxonomy_id': '10333', 'type': 'language', 'label': 'Egyptiska', 'concept_id': 'bJXz_kjT_LfC', 'legacy_ams_taxonomy_num_id': 10333}),

( {'type': 'language', 'id': 'uhGZ_Xif_8jX', 'preferred_label': 'Ekajuk', 'deprecated_legacy_id': '10334'},
'language', {'legacy_ams_taxonomy_id': '10334', 'type': 'language', 'label': 'Ekajuk', 'concept_id': 'uhGZ_Xif_8jX', 'legacy_ams_taxonomy_num_id': 10334}),

( {'type': 'language', 'id': 'NCju_o5B_Qhv', 'preferred_label': 'Elamitiska', 'deprecated_legacy_id': '10335'},
'language', {'legacy_ams_taxonomy_id': '10335', 'type': 'language', 'label': 'Elamitiska', 'concept_id': 'NCju_o5B_Qhv', 'legacy_ams_taxonomy_num_id': 10335}),

( {'type': 'language', 'id': 'ww2N_GEd_Pc7', 'preferred_label': 'Medelengelska', 'deprecated_legacy_id': '10336'},
'language', {'legacy_ams_taxonomy_id': '10336', 'type': 'language', 'label': 'Medelengelska', 'concept_id': 'ww2N_GEd_Pc7', 'legacy_ams_taxonomy_num_id': 10336}),

( {'type': 'language', 'id': 'hUGZ_TrC_T7v', 'preferred_label': 'Gammelengelska', 'deprecated_legacy_id': '10337'},
'language', {'legacy_ams_taxonomy_id': '10337', 'type': 'language', 'label': 'Gammelengelska', 'concept_id': 'hUGZ_TrC_T7v', 'legacy_ams_taxonomy_num_id': 10337}),

( {'type': 'language', 'id': 'aqBv_yoE_YVf', 'preferred_label': 'Esperanto', 'deprecated_legacy_id': '10338'},
'language', {'legacy_ams_taxonomy_id': '10338', 'type': 'language', 'label': 'Esperanto', 'concept_id': 'aqBv_yoE_YVf', 'legacy_ams_taxonomy_num_id': 10338}),

( {'type': 'language', 'id': 'aRMQ_MKY_kwp', 'preferred_label': 'Éwondo', 'deprecated_legacy_id': '10339'},
'language', {'legacy_ams_taxonomy_id': '10339', 'type': 'language', 'label': 'Éwondo', 'concept_id': 'aRMQ_MKY_kwp', 'legacy_ams_taxonomy_num_id': 10339}),

( {'type': 'language', 'id': 'RRwx_hXb_bRK', 'preferred_label': 'Finsk-ugriska språk, andra', 'deprecated_legacy_id': '10340'},
'language', {'legacy_ams_taxonomy_id': '10340', 'type': 'language', 'label': 'Finsk-ugriska språk, andra', 'concept_id': 'RRwx_hXb_bRK', 'legacy_ams_taxonomy_num_id': 10340}),

( {'type': 'language', 'id': 'k9XX_SU3_PiE', 'preferred_label': 'Medelfranska', 'deprecated_legacy_id': '10341'},
'language', {'legacy_ams_taxonomy_id': '10341', 'type': 'language', 'label': 'Medelfranska', 'concept_id': 'k9XX_SU3_PiE', 'legacy_ams_taxonomy_num_id': 10341}),

( {'type': 'language', 'id': 'LHyw_YUi_539', 'preferred_label': 'Friuliska', 'deprecated_legacy_id': '10342'},
'language', {'legacy_ams_taxonomy_id': '10342', 'type': 'language', 'label': 'Friuliska', 'concept_id': 'LHyw_YUi_539', 'legacy_ams_taxonomy_num_id': 10342}),

( {'type': 'language', 'id': 'XDyM_jnY_2XB', 'preferred_label': 'Gayo', 'deprecated_legacy_id': '10344'},
'language', {'legacy_ams_taxonomy_id': '10344', 'type': 'language', 'label': 'Gayo', 'concept_id': 'XDyM_jnY_2XB', 'legacy_ams_taxonomy_num_id': 10344}),

( {'type': 'language', 'id': 'aayg_wup_G3z', 'preferred_label': 'Gbaya', 'deprecated_legacy_id': '10345'},
'language', {'legacy_ams_taxonomy_id': '10345', 'type': 'language', 'label': 'Gbaya', 'concept_id': 'aayg_wup_G3z', 'legacy_ams_taxonomy_num_id': 10345}),

( {'type': 'language', 'id': 'nX5K_BFZ_CGM', 'preferred_label': 'Geez', 'deprecated_legacy_id': '10346'},
'language', {'legacy_ams_taxonomy_id': '10346', 'type': 'language', 'label': 'Geez', 'concept_id': 'nX5K_BFZ_CGM', 'legacy_ams_taxonomy_num_id': 10346}),

( {'type': 'language', 'id': 'kMxH_v1Y_jcW', 'preferred_label': 'Medelhögtyska', 'deprecated_legacy_id': '10347'},
'language', {'legacy_ams_taxonomy_id': '10347', 'type': 'language', 'label': 'Medelhögtyska', 'concept_id': 'kMxH_v1Y_jcW', 'legacy_ams_taxonomy_num_id': 10347}),

( {'type': 'language', 'id': '2s87_4M9_mF8', 'preferred_label': 'Gammalhögtyska', 'deprecated_legacy_id': '10348'},
'language', {'legacy_ams_taxonomy_id': '10348', 'type': 'language', 'label': 'Gammalhögtyska', 'concept_id': '2s87_4M9_mF8', 'legacy_ams_taxonomy_num_id': 10348}),

( {'type': 'language', 'id': 'oNtF_Wh5_nL3', 'preferred_label': 'Germanska språk, andra', 'deprecated_legacy_id': '10349'},
'language', {'legacy_ams_taxonomy_id': '10349', 'type': 'language', 'label': 'Germanska språk, andra', 'concept_id': 'oNtF_Wh5_nL3', 'legacy_ams_taxonomy_num_id': 10349}),

( {'type': 'language', 'id': 'Sta6_5fE_6YC', 'preferred_label': 'Gondi', 'deprecated_legacy_id': '10351'},
'language', {'legacy_ams_taxonomy_id': '10351', 'type': 'language', 'label': 'Gondi', 'concept_id': 'Sta6_5fE_6YC', 'legacy_ams_taxonomy_num_id': 10351}),

( {'type': 'language', 'id': 'WvNj_WvF_CMS', 'preferred_label': 'Gorontalo', 'deprecated_legacy_id': '10352'},
'language', {'legacy_ams_taxonomy_id': '10352', 'type': 'language', 'label': 'Gorontalo', 'concept_id': 'WvNj_WvF_CMS', 'legacy_ams_taxonomy_num_id': 10352}),

( {'type': 'language', 'id': 'GB2g_Bhp_zh7', 'preferred_label': 'Gotiska', 'deprecated_legacy_id': '10353'},
'language', {'legacy_ams_taxonomy_id': '10353', 'type': 'language', 'label': 'Gotiska', 'concept_id': 'GB2g_Bhp_zh7', 'legacy_ams_taxonomy_num_id': 10353}),

( {'type': 'language', 'id': '1V1A_8LC_9FL', 'preferred_label': 'Grebo', 'deprecated_legacy_id': '10354'},
'language', {'legacy_ams_taxonomy_id': '10354', 'type': 'language', 'label': 'Grebo', 'concept_id': '1V1A_8LC_9FL', 'legacy_ams_taxonomy_num_id': 10354}),

( {'type': 'language', 'id': 'sMa9_mkN_w7L', 'preferred_label': 'Gammalgrekiska', 'deprecated_legacy_id': '10355'},
'language', {'legacy_ams_taxonomy_id': '10355', 'type': 'language', 'label': 'Gammalgrekiska', 'concept_id': 'sMa9_mkN_w7L', 'legacy_ams_taxonomy_num_id': 10355}),

( {'type': 'language', 'id': 'pLK6_YGd_bDZ', 'preferred_label': "Gwich'in", 'deprecated_legacy_id': '10356'},
'language', {'legacy_ams_taxonomy_id': '10356', 'type': 'language', 'label': "Gwich'in", 'concept_id': 'pLK6_YGd_bDZ', 'legacy_ams_taxonomy_num_id': 10356}),

( {'type': 'language', 'id': 'ufzT_NXJ_Gbx', 'preferred_label': 'Haida', 'deprecated_legacy_id': '10357'},
'language', {'legacy_ams_taxonomy_id': '10357', 'type': 'language', 'label': 'Haida', 'concept_id': 'ufzT_NXJ_Gbx', 'legacy_ams_taxonomy_num_id': 10357}),

( {'type': 'language', 'id': 'Xfhf_EwK_2ej', 'preferred_label': 'Hawaiiska', 'deprecated_legacy_id': '10358'},
'language', {'legacy_ams_taxonomy_id': '10358', 'type': 'language', 'label': 'Hawaiiska', 'concept_id': 'Xfhf_EwK_2ej', 'legacy_ams_taxonomy_num_id': 10358}),

( {'type': 'language', 'id': 'H2Le_YR5_AnM', 'preferred_label': 'Hiligaynon', 'deprecated_legacy_id': '10359'},
'language', {'legacy_ams_taxonomy_id': '10359', 'type': 'language', 'label': 'Hiligaynon', 'concept_id': 'H2Le_YR5_AnM', 'legacy_ams_taxonomy_num_id': 10359}),

( {'type': 'language', 'id': 'iNVQ_m3Y_oxH', 'preferred_label': 'Himachali', 'deprecated_legacy_id': '10360'},
'language', {'legacy_ams_taxonomy_id': '10360', 'type': 'language', 'label': 'Himachali', 'concept_id': 'iNVQ_m3Y_oxH', 'legacy_ams_taxonomy_num_id': 10360}),

( {'type': 'language', 'id': 'hYDU_LrU_QCt', 'preferred_label': 'Hiri Motu', 'deprecated_legacy_id': '10361'},
'language', {'legacy_ams_taxonomy_id': '10361', 'type': 'language', 'label': 'Hiri Motu', 'concept_id': 'hYDU_LrU_QCt', 'legacy_ams_taxonomy_num_id': 10361}),

( {'type': 'language', 'id': 'D8L1_GrU_UEk', 'preferred_label': 'Hettittiska', 'deprecated_legacy_id': '10362'},
'language', {'legacy_ams_taxonomy_id': '10362', 'type': 'language', 'label': 'Hettittiska', 'concept_id': 'D8L1_GrU_UEk', 'legacy_ams_taxonomy_num_id': 10362}),

( {'type': 'language', 'id': 'CejU_23x_JPS', 'preferred_label': 'Hmong', 'deprecated_legacy_id': '10363'},
'language', {'legacy_ams_taxonomy_id': '10363', 'type': 'language', 'label': 'Hmong', 'concept_id': 'CejU_23x_JPS', 'legacy_ams_taxonomy_num_id': 10363}),

( {'type': 'language', 'id': '5G9f_Xs6_R9R', 'preferred_label': 'Hupa', 'deprecated_legacy_id': '10364'},
'language', {'legacy_ams_taxonomy_id': '10364', 'type': 'language', 'label': 'Hupa', 'concept_id': '5G9f_Xs6_R9R', 'legacy_ams_taxonomy_num_id': 10364}),

( {'type': 'language', 'id': 'sggr_4ba_xWP', 'preferred_label': 'Iban', 'deprecated_legacy_id': '10365'},
'language', {'legacy_ams_taxonomy_id': '10365', 'type': 'language', 'label': 'Iban', 'concept_id': 'sggr_4ba_xWP', 'legacy_ams_taxonomy_num_id': 10365}),

( {'type': 'language', 'id': '3dD8_TTW_5wh', 'preferred_label': 'Ijo', 'deprecated_legacy_id': '10366'},
'language', {'legacy_ams_taxonomy_id': '10366', 'type': 'language', 'label': 'Ijo', 'concept_id': '3dD8_TTW_5wh', 'legacy_ams_taxonomy_num_id': 10366}),

( {'type': 'language', 'id': 'yBCa_NHj_TcN', 'preferred_label': 'Iloko', 'deprecated_legacy_id': '10367'},
'language', {'legacy_ams_taxonomy_id': '10367', 'type': 'language', 'label': 'Iloko', 'concept_id': 'yBCa_NHj_TcN', 'legacy_ams_taxonomy_num_id': 10367}),

( {'type': 'language', 'id': 'P69L_FCv_sf9', 'preferred_label': 'Indo-iranska språk, andra', 'deprecated_legacy_id': '10368'},
'language', {'legacy_ams_taxonomy_id': '10368', 'type': 'language', 'label': 'Indo-iranska språk, andra', 'concept_id': 'P69L_FCv_sf9', 'legacy_ams_taxonomy_num_id': 10368}),

( {'type': 'language', 'id': 'kwP5_3pt_hk9', 'preferred_label': 'Indo-europeiska språk, andra', 'deprecated_legacy_id': '10369'},
'language', {'legacy_ams_taxonomy_id': '10369', 'type': 'language', 'label': 'Indo-europeiska språk, andra', 'concept_id': 'kwP5_3pt_hk9', 'legacy_ams_taxonomy_num_id': 10369}),

( {'type': 'language', 'id': 'dWU4_ed7_jX6', 'preferred_label': 'Interlingua', 'deprecated_legacy_id': '10370'},
'language', {'legacy_ams_taxonomy_id': '10370', 'type': 'language', 'label': 'Interlingua', 'concept_id': 'dWU4_ed7_jX6', 'legacy_ams_taxonomy_num_id': 10370}),

( {'type': 'language', 'id': 'r2HQ_7Dt_N2N', 'preferred_label': 'Inuktitut', 'deprecated_legacy_id': '10371'},
'language', {'legacy_ams_taxonomy_id': '10371', 'type': 'language', 'label': 'Inuktitut', 'concept_id': 'r2HQ_7Dt_N2N', 'legacy_ams_taxonomy_num_id': 10371}),

( {'type': 'language', 'id': 'gXVC_bNa_VdA', 'preferred_label': 'Inupiak', 'deprecated_legacy_id': '10372'},
'language', {'legacy_ams_taxonomy_id': '10372', 'type': 'language', 'label': 'Inupiak', 'concept_id': 'gXVC_bNa_VdA', 'legacy_ams_taxonomy_num_id': 10372}),

( {'type': 'language', 'id': 'kafF_y2Q_Md2', 'preferred_label': 'Iranska språk, andra', 'deprecated_legacy_id': '10373'},
'language', {'legacy_ams_taxonomy_id': '10373', 'type': 'language', 'label': 'Iranska språk, andra', 'concept_id': 'kafF_y2Q_Md2', 'legacy_ams_taxonomy_num_id': 10373}),

( {'type': 'language', 'id': '8G95_iDg_rtV', 'preferred_label': 'Medelirisk', 'deprecated_legacy_id': '10374'},
'language', {'legacy_ams_taxonomy_id': '10374', 'type': 'language', 'label': 'Medelirisk', 'concept_id': '8G95_iDg_rtV', 'legacy_ams_taxonomy_num_id': 10374}),

( {'type': 'language', 'id': '8VDp_XA9_dBF', 'preferred_label': 'Gammaliriska', 'deprecated_legacy_id': '10375'},
'language', {'legacy_ams_taxonomy_id': '10375', 'type': 'language', 'label': 'Gammaliriska', 'concept_id': '8VDp_XA9_dBF', 'legacy_ams_taxonomy_num_id': 10375}),

( {'type': 'language', 'id': 'rdv1_9u8_wCM', 'preferred_label': 'Irokesarspråk', 'deprecated_legacy_id': '10376'},
'language', {'legacy_ams_taxonomy_id': '10376', 'type': 'language', 'label': 'Irokesarspråk', 'concept_id': 'rdv1_9u8_wCM', 'legacy_ams_taxonomy_num_id': 10376}),

( {'type': 'language', 'id': '64Rg_xyJ_bmN', 'preferred_label': 'Judeo-arabisk', 'deprecated_legacy_id': '10377'},
'language', {'legacy_ams_taxonomy_id': '10377', 'type': 'language', 'label': 'Judeo-arabisk', 'concept_id': '64Rg_xyJ_bmN', 'legacy_ams_taxonomy_num_id': 10377}),

( {'type': 'language', 'id': 'W5rh_1Ec_sox', 'preferred_label': 'Judeo-persiska', 'deprecated_legacy_id': '10378'},
'language', {'legacy_ams_taxonomy_id': '10378', 'type': 'language', 'label': 'Judeo-persiska', 'concept_id': 'W5rh_1Ec_sox', 'legacy_ams_taxonomy_num_id': 10378}),

( {'type': 'language', 'id': 'ZsK2_s92_xaW', 'preferred_label': 'Kachin', 'deprecated_legacy_id': '10379'},
'language', {'legacy_ams_taxonomy_id': '10379', 'type': 'language', 'label': 'Kachin', 'concept_id': 'ZsK2_s92_xaW', 'legacy_ams_taxonomy_num_id': 10379}),

( {'type': 'language', 'id': 'brZn_27W_A9K', 'preferred_label': 'Karakalpak', 'deprecated_legacy_id': '10381'},
'language', {'legacy_ams_taxonomy_id': '10381', 'type': 'language', 'label': 'Karakalpak', 'concept_id': 'brZn_27W_A9K', 'legacy_ams_taxonomy_num_id': 10381}),

( {'type': 'language', 'id': 'DuPf_Z28_3Jv', 'preferred_label': 'Kawi', 'deprecated_legacy_id': '10383'},
'language', {'legacy_ams_taxonomy_id': '10383', 'type': 'language', 'label': 'Kawi', 'concept_id': 'DuPf_Z28_3Jv', 'legacy_ams_taxonomy_num_id': 10383}),

( {'type': 'language', 'id': 'e84R_V8f_fXS', 'preferred_label': 'Khasi', 'deprecated_legacy_id': '10384'},
'language', {'legacy_ams_taxonomy_id': '10384', 'type': 'language', 'label': 'Khasi', 'concept_id': 'e84R_V8f_fXS', 'legacy_ams_taxonomy_num_id': 10384}),

( {'type': 'language', 'id': 'Vdz7_pdG_bTJ', 'preferred_label': 'Khoisanspråk, andra', 'deprecated_legacy_id': '10385'},
'language', {'legacy_ams_taxonomy_id': '10385', 'type': 'language', 'label': 'Khoisanspråk, andra', 'concept_id': 'Vdz7_pdG_bTJ', 'legacy_ams_taxonomy_num_id': 10385}),

( {'type': 'language', 'id': 'aF96_PaA_VAs', 'preferred_label': 'Khotanesiska', 'deprecated_legacy_id': '10386'},
'language', {'legacy_ams_taxonomy_id': '10386', 'type': 'language', 'label': 'Khotanesiska', 'concept_id': 'aF96_PaA_VAs', 'legacy_ams_taxonomy_num_id': 10386}),

( {'type': 'language', 'id': 'PZH4_EGP_Dz3', 'preferred_label': 'Kimbundu', 'deprecated_legacy_id': '10388'},
'language', {'legacy_ams_taxonomy_id': '10388', 'type': 'language', 'label': 'Kimbundu', 'concept_id': 'PZH4_EGP_Dz3', 'legacy_ams_taxonomy_num_id': 10388}),

( {'type': 'language', 'id': 'KVSQ_4pu_RSs', 'preferred_label': 'Komi', 'deprecated_legacy_id': '10389'},
'language', {'legacy_ams_taxonomy_id': '10389', 'type': 'language', 'label': 'Komi', 'concept_id': 'KVSQ_4pu_RSs', 'legacy_ams_taxonomy_num_id': 10389}),

( {'type': 'language', 'id': 'gsoZ_3LM_C79', 'preferred_label': 'Kosraeiska', 'deprecated_legacy_id': '10390'},
'language', {'legacy_ams_taxonomy_id': '10390', 'type': 'language', 'label': 'Kosraeiska', 'concept_id': 'gsoZ_3LM_C79', 'legacy_ams_taxonomy_num_id': 10390}),

( {'type': 'language', 'id': 'nT65_Jya_ocT', 'preferred_label': 'Kpelle', 'deprecated_legacy_id': '10391'},
'language', {'legacy_ams_taxonomy_id': '10391', 'type': 'language', 'label': 'Kpelle', 'concept_id': 'nT65_Jya_ocT', 'legacy_ams_taxonomy_num_id': 10391}),

( {'type': 'language', 'id': 'sXAB_vhU_zN8', 'preferred_label': 'Kuanyama', 'deprecated_legacy_id': '10392'},
'language', {'legacy_ams_taxonomy_id': '10392', 'type': 'language', 'label': 'Kuanyama', 'concept_id': 'sXAB_vhU_zN8', 'legacy_ams_taxonomy_num_id': 10392}),

( {'type': 'language', 'id': 'MZ9f_S5d_6Wo', 'preferred_label': 'Kumykiska', 'deprecated_legacy_id': '10393'},
'language', {'legacy_ams_taxonomy_id': '10393', 'type': 'language', 'label': 'Kumykiska', 'concept_id': 'MZ9f_S5d_6Wo', 'legacy_ams_taxonomy_num_id': 10393}),

( {'type': 'language', 'id': 'qTKS_snK_ezT', 'preferred_label': 'Kurukh', 'deprecated_legacy_id': '10394'},
'language', {'legacy_ams_taxonomy_id': '10394', 'type': 'language', 'label': 'Kurukh', 'concept_id': 'qTKS_snK_ezT', 'legacy_ams_taxonomy_num_id': 10394}),

( {'type': 'language', 'id': 'fLPj_PDh_SQJ', 'preferred_label': 'Kutenai', 'deprecated_legacy_id': '10395'},
'language', {'legacy_ams_taxonomy_id': '10395', 'type': 'language', 'label': 'Kutenai', 'concept_id': 'fLPj_PDh_SQJ', 'legacy_ams_taxonomy_num_id': 10395}),

( {'type': 'language', 'id': 'B9id_Ecv_qji', 'preferred_label': 'Ladino', 'deprecated_legacy_id': '10396'},
'language', {'legacy_ams_taxonomy_id': '10396', 'type': 'language', 'label': 'Ladino', 'concept_id': 'B9id_Ecv_qji', 'legacy_ams_taxonomy_num_id': 10396}),

( {'type': 'language', 'id': '73sF_ejf_2Vx', 'preferred_label': 'Lahnda', 'deprecated_legacy_id': '10397'},
'language', {'legacy_ams_taxonomy_id': '10397', 'type': 'language', 'label': 'Lahnda', 'concept_id': '73sF_ejf_2Vx', 'legacy_ams_taxonomy_num_id': 10397}),

( {'type': 'language', 'id': '7nWL_Bzc_RFQ', 'preferred_label': 'Latin', 'deprecated_legacy_id': '10400'},
'language', {'legacy_ams_taxonomy_id': '10400', 'type': 'language', 'label': 'Latin', 'concept_id': '7nWL_Bzc_RFQ', 'legacy_ams_taxonomy_num_id': 10400}),

( {'type': 'language', 'id': '9ig9_ypn_cVw', 'preferred_label': 'Lesgiska', 'deprecated_legacy_id': '10401'},
'language', {'legacy_ams_taxonomy_id': '10401', 'type': 'language', 'label': 'Lesgiska', 'concept_id': '9ig9_ypn_cVw', 'legacy_ams_taxonomy_num_id': 10401}),

( {'type': 'language', 'id': 'yA84_KQv_pKJ', 'preferred_label': 'Luba-Lulua', 'deprecated_legacy_id': '10402'},
'language', {'legacy_ams_taxonomy_id': '10402', 'type': 'language', 'label': 'Luba-Lulua', 'concept_id': 'yA84_KQv_pKJ', 'legacy_ams_taxonomy_num_id': 10402}),

( {'type': 'language', 'id': 'rEi5_NuN_9xC', 'preferred_label': 'Luiseno', 'deprecated_legacy_id': '10403'},
'language', {'legacy_ams_taxonomy_id': '10403', 'type': 'language', 'label': 'Luiseno', 'concept_id': 'rEi5_NuN_9xC', 'legacy_ams_taxonomy_num_id': 10403}),

( {'type': 'language', 'id': 'qeKC_PXn_cHU', 'preferred_label': 'Lushai', 'deprecated_legacy_id': '10404'},
'language', {'legacy_ams_taxonomy_id': '10404', 'type': 'language', 'label': 'Lushai', 'concept_id': 'qeKC_PXn_cHU', 'legacy_ams_taxonomy_num_id': 10404}),

( {'type': 'language', 'id': 'rJah_bYL_qdW', 'preferred_label': 'Magahi', 'deprecated_legacy_id': '10405'},
'language', {'legacy_ams_taxonomy_id': '10405', 'type': 'language', 'label': 'Magahi', 'concept_id': 'rJah_bYL_qdW', 'legacy_ams_taxonomy_num_id': 10405}),

( {'type': 'language', 'id': 'AzXn_4ic_B5Y', 'preferred_label': 'Maithili', 'deprecated_legacy_id': '10406'},
'language', {'legacy_ams_taxonomy_id': '10406', 'type': 'language', 'label': 'Maithili', 'concept_id': 'AzXn_4ic_B5Y', 'legacy_ams_taxonomy_num_id': 10406}),

( {'type': 'language', 'id': 'ER1i_tUG_y4e', 'preferred_label': 'Makassar', 'deprecated_legacy_id': '10407'},
'language', {'legacy_ams_taxonomy_id': '10407', 'type': 'language', 'label': 'Makassar', 'concept_id': 'ER1i_tUG_y4e', 'legacy_ams_taxonomy_num_id': 10407}),

( {'type': 'language', 'id': 'PvjG_Ak1_L71', 'preferred_label': 'Mandingo', 'deprecated_legacy_id': '10408'},
'language', {'legacy_ams_taxonomy_id': '10408', 'type': 'language', 'label': 'Mandingo', 'concept_id': 'PvjG_Ak1_L71', 'legacy_ams_taxonomy_num_id': 10408}),

( {'type': 'language', 'id': '4xVc_tVS_85F', 'preferred_label': 'Manipuriska', 'deprecated_legacy_id': '10409'},
'language', {'legacy_ams_taxonomy_id': '10409', 'type': 'language', 'label': 'Manipuriska', 'concept_id': '4xVc_tVS_85F', 'legacy_ams_taxonomy_num_id': 10409}),

( {'type': 'language', 'id': 'A4h7_WDJ_Qzf', 'preferred_label': 'Manobospråk', 'deprecated_legacy_id': '10410'},
'language', {'legacy_ams_taxonomy_id': '10410', 'type': 'language', 'label': 'Manobospråk', 'concept_id': 'A4h7_WDJ_Qzf', 'legacy_ams_taxonomy_num_id': 10410}),

( {'type': 'language', 'id': 'JDk9_Yia_DPQ', 'preferred_label': 'Marshalliska', 'deprecated_legacy_id': '10411'},
'language', {'legacy_ams_taxonomy_id': '10411', 'type': 'language', 'label': 'Marshalliska', 'concept_id': 'JDk9_Yia_DPQ', 'legacy_ams_taxonomy_num_id': 10411}),

( {'type': 'language', 'id': '3ueP_nhZ_1em', 'preferred_label': 'Micmac', 'deprecated_legacy_id': '10412'},
'language', {'legacy_ams_taxonomy_id': '10412', 'type': 'language', 'label': 'Micmac', 'concept_id': '3ueP_nhZ_1em', 'legacy_ams_taxonomy_num_id': 10412}),

( {'type': 'language', 'id': 'hB5i_2eY_Kur', 'preferred_label': 'Minangkabau', 'deprecated_legacy_id': '10413'},
'language', {'legacy_ams_taxonomy_id': '10413', 'type': 'language', 'label': 'Minangkabau', 'concept_id': 'hB5i_2eY_Kur', 'legacy_ams_taxonomy_num_id': 10413}),

( {'type': 'language', 'id': '8GQ1_Avf_vdR', 'preferred_label': 'Diverse språk', 'deprecated_legacy_id': '10414'},
'language', {'legacy_ams_taxonomy_id': '10414', 'type': 'language', 'label': 'Diverse språk', 'concept_id': '8GQ1_Avf_vdR', 'legacy_ams_taxonomy_num_id': 10414}),

( {'type': 'language', 'id': 's6ka_gDZ_Ycb', 'preferred_label': 'Mohawk', 'deprecated_legacy_id': '10415'},
'language', {'legacy_ams_taxonomy_id': '10415', 'type': 'language', 'label': 'Mohawk', 'concept_id': 's6ka_gDZ_Ycb', 'legacy_ams_taxonomy_num_id': 10415}),

( {'type': 'language', 'id': 'wcNi_yzQ_n9V', 'preferred_label': 'Monkhmerspråk, andra', 'deprecated_legacy_id': '10416'},
'language', {'legacy_ams_taxonomy_id': '10416', 'type': 'language', 'label': 'Monkhmerspråk, andra', 'concept_id': 'wcNi_yzQ_n9V', 'legacy_ams_taxonomy_num_id': 10416}),

( {'type': 'language', 'id': 'En9t_Siu_7yx', 'preferred_label': 'Mongo', 'deprecated_legacy_id': '10417'},
'language', {'legacy_ams_taxonomy_id': '10417', 'type': 'language', 'label': 'Mongo', 'concept_id': 'En9t_Siu_7yx', 'legacy_ams_taxonomy_num_id': 10417}),

( {'type': 'language', 'id': 'o8Xp_wvY_5pu', 'preferred_label': 'Flera språk', 'deprecated_legacy_id': '10419'},
'language', {'legacy_ams_taxonomy_id': '10419', 'type': 'language', 'label': 'Flera språk', 'concept_id': 'o8Xp_wvY_5pu', 'legacy_ams_taxonomy_num_id': 10419}),

( {'type': 'language', 'id': 'EUGX_wJT_qxE', 'preferred_label': 'Mundspråk', 'deprecated_legacy_id': '10420'},
'language', {'legacy_ams_taxonomy_id': '10420', 'type': 'language', 'label': 'Mundspråk', 'concept_id': 'EUGX_wJT_qxE', 'legacy_ams_taxonomy_num_id': 10420}),

( {'type': 'language', 'id': '8ZiC_PNc_hkY', 'preferred_label': 'Ndebele Syd', 'deprecated_legacy_id': '10422'},
'language', {'legacy_ams_taxonomy_id': '10422', 'type': 'language', 'label': 'Ndebele Syd', 'concept_id': '8ZiC_PNc_hkY', 'legacy_ams_taxonomy_num_id': 10422}),

( {'type': 'language', 'id': 'zAgM_Myz_xeD', 'preferred_label': 'Ndonga', 'deprecated_legacy_id': '10423'},
'language', {'legacy_ams_taxonomy_id': '10423', 'type': 'language', 'label': 'Ndonga', 'concept_id': 'zAgM_Myz_xeD', 'legacy_ams_taxonomy_num_id': 10423}),

( {'type': 'language', 'id': 'dS3F_4gn_htW', 'preferred_label': 'Newari', 'deprecated_legacy_id': '10424'},
'language', {'legacy_ams_taxonomy_id': '10424', 'type': 'language', 'label': 'Newari', 'concept_id': 'dS3F_4gn_htW', 'legacy_ams_taxonomy_num_id': 10424}),

( {'type': 'language', 'id': 'vNJX_dvg_1aq', 'preferred_label': 'Nias', 'deprecated_legacy_id': '10425'},
'language', {'legacy_ams_taxonomy_id': '10425', 'type': 'language', 'label': 'Nias', 'concept_id': 'vNJX_dvg_1aq', 'legacy_ams_taxonomy_num_id': 10425}),

( {'type': 'language', 'id': 'Kmib_KAp_WkF', 'preferred_label': 'Niger-Kordofanspråk, andra', 'deprecated_legacy_id': '10426'},
'language', {'legacy_ams_taxonomy_id': '10426', 'type': 'language', 'label': 'Niger-Kordofanspråk, andra', 'concept_id': 'Kmib_KAp_WkF', 'legacy_ams_taxonomy_num_id': 10426}),

( {'type': 'language', 'id': 'MAWi_wjM_Qrb', 'preferred_label': 'Nilsahariska, andra', 'deprecated_legacy_id': '10427'},
'language', {'legacy_ams_taxonomy_id': '10427', 'type': 'language', 'label': 'Nilsahariska, andra', 'concept_id': 'MAWi_wjM_Qrb', 'legacy_ams_taxonomy_num_id': 10427}),

( {'type': 'language', 'id': 'e44f_RYG_BZd', 'preferred_label': 'Niuea', 'deprecated_legacy_id': '10428'},
'language', {'legacy_ams_taxonomy_id': '10428', 'type': 'language', 'label': 'Niuea', 'concept_id': 'e44f_RYG_BZd', 'legacy_ams_taxonomy_num_id': 10428}),

( {'type': 'language', 'id': 'Xgng_L5L_T89', 'preferred_label': 'Gammalnorska', 'deprecated_legacy_id': '10429'},
'language', {'legacy_ams_taxonomy_id': '10429', 'type': 'language', 'label': 'Gammalnorska', 'concept_id': 'Xgng_L5L_T89', 'legacy_ams_taxonomy_num_id': 10429}),

( {'type': 'language', 'id': 'rztR_jw7_vez', 'preferred_label': 'Nordamerikanska indianerspråk, andra', 'deprecated_legacy_id': '10430'},
'language', {'legacy_ams_taxonomy_id': '10430', 'type': 'language', 'label': 'Nordamerikanska indianerspråk, andra', 'concept_id': 'rztR_jw7_vez', 'legacy_ams_taxonomy_num_id': 10430}),

( {'type': 'language', 'id': '5gu3_xFf_Mo8', 'preferred_label': 'Nubiska språk', 'deprecated_legacy_id': '10431'},
'language', {'legacy_ams_taxonomy_id': '10431', 'type': 'language', 'label': 'Nubiska språk', 'concept_id': '5gu3_xFf_Mo8', 'legacy_ams_taxonomy_num_id': 10431}),

( {'type': 'language', 'id': 'yh9H_efB_UFJ', 'preferred_label': 'Nyankole', 'deprecated_legacy_id': '10432'},
'language', {'legacy_ams_taxonomy_id': '10432', 'type': 'language', 'label': 'Nyankole', 'concept_id': 'yh9H_efB_UFJ', 'legacy_ams_taxonomy_num_id': 10432}),

( {'type': 'language', 'id': 'EwW1_aQH_EXu', 'preferred_label': 'Nyoro', 'deprecated_legacy_id': '10433'},
'language', {'legacy_ams_taxonomy_id': '10433', 'type': 'language', 'label': 'Nyoro', 'concept_id': 'EwW1_aQH_EXu', 'legacy_ams_taxonomy_num_id': 10433}),

( {'type': 'language', 'id': 'Agwy_4Hu_NTY', 'preferred_label': 'Nzima', 'deprecated_legacy_id': '10434'},
'language', {'legacy_ams_taxonomy_id': '10434', 'type': 'language', 'label': 'Nzima', 'concept_id': 'Agwy_4Hu_NTY', 'legacy_ams_taxonomy_num_id': 10434}),

( {'type': 'language', 'id': 'MPVv_14T_88n', 'preferred_label': 'Ojibwa', 'deprecated_legacy_id': '10435'},
'language', {'legacy_ams_taxonomy_id': '10435', 'type': 'language', 'label': 'Ojibwa', 'concept_id': 'MPVv_14T_88n', 'legacy_ams_taxonomy_num_id': 10435}),

( {'type': 'language', 'id': 'Ev2g_L1i_D62', 'preferred_label': 'Osage', 'deprecated_legacy_id': '10436'},
'language', {'legacy_ams_taxonomy_id': '10436', 'type': 'language', 'label': 'Osage', 'concept_id': 'Ev2g_L1i_D62', 'legacy_ams_taxonomy_num_id': 10436}),

( {'type': 'language', 'id': 'Ji47_WT1_NKN', 'preferred_label': 'Ossetiska', 'deprecated_legacy_id': '10437'},
'language', {'legacy_ams_taxonomy_id': '10437', 'type': 'language', 'label': 'Ossetiska', 'concept_id': 'Ji47_WT1_NKN', 'legacy_ams_taxonomy_num_id': 10437}),

( {'type': 'language', 'id': 'QdLQ_Ti9_UP3', 'preferred_label': 'Oto-manguespråk', 'deprecated_legacy_id': '10438'},
'language', {'legacy_ams_taxonomy_id': '10438', 'type': 'language', 'label': 'Oto-manguespråk', 'concept_id': 'QdLQ_Ti9_UP3', 'legacy_ams_taxonomy_num_id': 10438}),

( {'type': 'language', 'id': '9RmN_zLz_k9H', 'preferred_label': 'Pahlavi', 'deprecated_legacy_id': '10439'},
'language', {'legacy_ams_taxonomy_id': '10439', 'type': 'language', 'label': 'Pahlavi', 'concept_id': '9RmN_zLz_k9H', 'legacy_ams_taxonomy_num_id': 10439}),

( {'type': 'language', 'id': '3ofn_Vft_AFK', 'preferred_label': 'Palauiska', 'deprecated_legacy_id': '10440'},
'language', {'legacy_ams_taxonomy_id': '10440', 'type': 'language', 'label': 'Palauiska', 'concept_id': '3ofn_Vft_AFK', 'legacy_ams_taxonomy_num_id': 10440}),

( {'type': 'language', 'id': 'uVrS_Lx3_7N2', 'preferred_label': 'Pali', 'deprecated_legacy_id': '10441'},
'language', {'legacy_ams_taxonomy_id': '10441', 'type': 'language', 'label': 'Pali', 'concept_id': 'uVrS_Lx3_7N2', 'legacy_ams_taxonomy_num_id': 10441}),

( {'type': 'language', 'id': 'wAEM_WR9_cmv', 'preferred_label': 'Pampanggo', 'deprecated_legacy_id': '10442'},
'language', {'legacy_ams_taxonomy_id': '10442', 'type': 'language', 'label': 'Pampanggo', 'concept_id': 'wAEM_WR9_cmv', 'legacy_ams_taxonomy_num_id': 10442}),

( {'type': 'language', 'id': 'yymy_7L8_LDE', 'preferred_label': 'Pangasinan', 'deprecated_legacy_id': '10443'},
'language', {'legacy_ams_taxonomy_id': '10443', 'type': 'language', 'label': 'Pangasinan', 'concept_id': 'yymy_7L8_LDE', 'legacy_ams_taxonomy_num_id': 10443}),

( {'type': 'language', 'id': 'QWLc_9BV_8RS', 'preferred_label': 'Papiamento', 'deprecated_legacy_id': '10444'},
'language', {'legacy_ams_taxonomy_id': '10444', 'type': 'language', 'label': 'Papiamento', 'concept_id': 'QWLc_9BV_8RS', 'legacy_ams_taxonomy_num_id': 10444}),

( {'type': 'language', 'id': '7HE9_DoQ_a56', 'preferred_label': 'Papuanska språk, andra', 'deprecated_legacy_id': '10445'},
'language', {'legacy_ams_taxonomy_id': '10445', 'type': 'language', 'label': 'Papuanska språk, andra', 'concept_id': '7HE9_DoQ_a56', 'legacy_ams_taxonomy_num_id': 10445}),

( {'type': 'language', 'id': 'WA3P_FfD_nmy', 'preferred_label': 'Gammelpersiska', 'deprecated_legacy_id': '10446'},
'language', {'legacy_ams_taxonomy_id': '10446', 'type': 'language', 'label': 'Gammelpersiska', 'concept_id': 'WA3P_FfD_nmy', 'legacy_ams_taxonomy_num_id': 10446}),

( {'type': 'language', 'id': 'SUTA_7JX_JRw', 'preferred_label': 'Filippinska språk, andra', 'deprecated_legacy_id': '10447'},
'language', {'legacy_ams_taxonomy_id': '10447', 'type': 'language', 'label': 'Filippinska språk, andra', 'concept_id': 'SUTA_7JX_JRw', 'legacy_ams_taxonomy_num_id': 10447}),

( {'type': 'language', 'id': 'CM6d_taZ_vVL', 'preferred_label': 'Fönikiska', 'deprecated_legacy_id': '10448'},
'language', {'legacy_ams_taxonomy_id': '10448', 'type': 'language', 'label': 'Fönikiska', 'concept_id': 'CM6d_taZ_vVL', 'legacy_ams_taxonomy_num_id': 10448}),

( {'type': 'language', 'id': 'zASV_5vH_5Yr', 'preferred_label': 'Pohnpei', 'deprecated_legacy_id': '10449'},
'language', {'legacy_ams_taxonomy_id': '10449', 'type': 'language', 'label': 'Pohnpei', 'concept_id': 'zASV_5vH_5Yr', 'legacy_ams_taxonomy_num_id': 10449}),

( {'type': 'language', 'id': 'HPb5_vo6_Dmp', 'preferred_label': 'Prakrit', 'deprecated_legacy_id': '10450'},
'language', {'legacy_ams_taxonomy_id': '10450', 'type': 'language', 'label': 'Prakrit', 'concept_id': 'HPb5_vo6_Dmp', 'legacy_ams_taxonomy_num_id': 10450}),

( {'type': 'language', 'id': '1zy3_huH_ic1', 'preferred_label': 'Gammelprovecanska', 'deprecated_legacy_id': '10451'},
'language', {'legacy_ams_taxonomy_id': '10451', 'type': 'language', 'label': 'Gammelprovecanska', 'concept_id': '1zy3_huH_ic1', 'legacy_ams_taxonomy_num_id': 10451}),

( {'type': 'language', 'id': 'bUdf_dcV_r3c', 'preferred_label': 'Rajasthanisja; marwariska', 'deprecated_legacy_id': '10453'},
'language', {'legacy_ams_taxonomy_id': '10453', 'type': 'language', 'label': 'Rajasthanisja; marwariska', 'concept_id': 'bUdf_dcV_r3c', 'legacy_ams_taxonomy_num_id': 10453}),

( {'type': 'language', 'id': 'VEPz_1v9_Rvu', 'preferred_label': 'Rapanui', 'deprecated_legacy_id': '10454'},
'language', {'legacy_ams_taxonomy_id': '10454', 'type': 'language', 'label': 'Rapanui', 'concept_id': 'VEPz_1v9_Rvu', 'legacy_ams_taxonomy_num_id': 10454}),

( {'type': 'language', 'id': 'anUm_Nr6_gpN', 'preferred_label': 'Rarotonga', 'deprecated_legacy_id': '10455'},
'language', {'legacy_ams_taxonomy_id': '10455', 'type': 'language', 'label': 'Rarotonga', 'concept_id': 'anUm_Nr6_gpN', 'legacy_ams_taxonomy_num_id': 10455}),

( {'type': 'language', 'id': 'xxAm_o17_Sf3', 'preferred_label': 'Romanska språk, andra', 'deprecated_legacy_id': '10456'},
'language', {'legacy_ams_taxonomy_id': '10456', 'type': 'language', 'label': 'Romanska språk, andra', 'concept_id': 'xxAm_o17_Sf3', 'legacy_ams_taxonomy_num_id': 10456}),

( {'type': 'language', 'id': 'GXwq_W4u_cvX', 'preferred_label': 'Saliska språk', 'deprecated_legacy_id': '10458'},
'language', {'legacy_ams_taxonomy_id': '10458', 'type': 'language', 'label': 'Saliska språk', 'concept_id': 'GXwq_W4u_cvX', 'legacy_ams_taxonomy_num_id': 10458}),

( {'type': 'language', 'id': 'kQvM_k1N_nne', 'preferred_label': 'Samaritansk aramaiska', 'deprecated_legacy_id': '10459'},
'language', {'legacy_ams_taxonomy_id': '10459', 'type': 'language', 'label': 'Samaritansk aramaiska', 'concept_id': 'kQvM_k1N_nne', 'legacy_ams_taxonomy_num_id': 10459}),

( {'type': 'language', 'id': 'FCsC_s5t_dU2', 'preferred_label': 'Sandawe', 'deprecated_legacy_id': '10460'},
'language', {'legacy_ams_taxonomy_id': '10460', 'type': 'language', 'label': 'Sandawe', 'concept_id': 'FCsC_s5t_dU2', 'legacy_ams_taxonomy_num_id': 10460}),

( {'type': 'language', 'id': 'WV6s_E7d_hBp', 'preferred_label': 'Sanskrit', 'deprecated_legacy_id': '10461'},
'language', {'legacy_ams_taxonomy_id': '10461', 'type': 'language', 'label': 'Sanskrit', 'concept_id': 'WV6s_E7d_hBp', 'legacy_ams_taxonomy_num_id': 10461}),

( {'type': 'language', 'id': 'anHt_dxL_Yvo', 'preferred_label': 'Sasak', 'deprecated_legacy_id': '10462'},
'language', {'legacy_ams_taxonomy_id': '10462', 'type': 'language', 'label': 'Sasak', 'concept_id': 'anHt_dxL_Yvo', 'legacy_ams_taxonomy_num_id': 10462}),

( {'type': 'language', 'id': 'agM1_4uk_nHZ', 'preferred_label': 'Selkupiska', 'deprecated_legacy_id': '10463'},
'language', {'legacy_ams_taxonomy_id': '10463', 'type': 'language', 'label': 'Selkupiska', 'concept_id': 'agM1_4uk_nHZ', 'legacy_ams_taxonomy_num_id': 10463}),

( {'type': 'language', 'id': 'sCUz_F8E_pKZ', 'preferred_label': 'Semitiska språk, andra', 'deprecated_legacy_id': '10464'},
'language', {'legacy_ams_taxonomy_id': '10464', 'type': 'language', 'label': 'Semitiska språk, andra', 'concept_id': 'sCUz_F8E_pKZ', 'legacy_ams_taxonomy_num_id': 10464}),

( {'type': 'language', 'id': 'Xajo_j9B_XWW', 'preferred_label': 'Serer', 'deprecated_legacy_id': '10465'},
'language', {'legacy_ams_taxonomy_id': '10465', 'type': 'language', 'label': 'Serer', 'concept_id': 'Xajo_j9B_XWW', 'legacy_ams_taxonomy_num_id': 10465}),

( {'type': 'language', 'id': 'ZRA5_7p5_vY7', 'preferred_label': 'Shan', 'deprecated_legacy_id': '10466'},
'language', {'legacy_ams_taxonomy_id': '10466', 'type': 'language', 'label': 'Shan', 'concept_id': 'ZRA5_7p5_vY7', 'legacy_ams_taxonomy_num_id': 10466}),

( {'type': 'language', 'id': 'G8e4_Sid_vFd', 'preferred_label': 'Sidamo', 'deprecated_legacy_id': '10467'},
'language', {'legacy_ams_taxonomy_id': '10467', 'type': 'language', 'label': 'Sidamo', 'concept_id': 'G8e4_Sid_vFd', 'legacy_ams_taxonomy_num_id': 10467}),

( {'type': 'language', 'id': 'DJfZ_4fk_kwb', 'preferred_label': 'Siksika', 'deprecated_legacy_id': '10468'},
'language', {'legacy_ams_taxonomy_id': '10468', 'type': 'language', 'label': 'Siksika', 'concept_id': 'DJfZ_4fk_kwb', 'legacy_ams_taxonomy_num_id': 10468}),

( {'type': 'language', 'id': 'dQvb_BHK_Yxk', 'preferred_label': 'Sinotibetanska språk, andra', 'deprecated_legacy_id': '10469'},
'language', {'legacy_ams_taxonomy_id': '10469', 'type': 'language', 'label': 'Sinotibetanska språk, andra', 'concept_id': 'dQvb_BHK_Yxk', 'legacy_ams_taxonomy_num_id': 10469}),

( {'type': 'language', 'id': '443s_ZvN_1F4', 'preferred_label': 'Sioux-språk', 'deprecated_legacy_id': '10470'},
'language', {'legacy_ams_taxonomy_id': '10470', 'type': 'language', 'label': 'Sioux-språk', 'concept_id': '443s_ZvN_1F4', 'legacy_ams_taxonomy_num_id': 10470}),

( {'type': 'language', 'id': 'u9Rr_v5c_iT2', 'preferred_label': 'Slaviska (Athapascaniska)', 'deprecated_legacy_id': '10471'},
'language', {'legacy_ams_taxonomy_id': '10471', 'type': 'language', 'label': 'Slaviska (Athapascaniska)', 'concept_id': 'u9Rr_v5c_iT2', 'legacy_ams_taxonomy_num_id': 10471}),

( {'type': 'language', 'id': 'PMLh_Nss_Ehg', 'preferred_label': 'Slaviska språk, andra', 'deprecated_legacy_id': '10472'},
'language', {'legacy_ams_taxonomy_id': '10472', 'type': 'language', 'label': 'Slaviska språk, andra', 'concept_id': 'PMLh_Nss_Ehg', 'legacy_ams_taxonomy_num_id': 10472}),

( {'type': 'language', 'id': 'kdXZ_zHa_KTB', 'preferred_label': 'Sogdiska', 'deprecated_legacy_id': '10473'},
'language', {'legacy_ams_taxonomy_id': '10473', 'type': 'language', 'label': 'Sogdiska', 'concept_id': 'kdXZ_zHa_KTB', 'legacy_ams_taxonomy_num_id': 10473}),

( {'type': 'language', 'id': 'Gi82_GNa_Uvu', 'preferred_label': 'Soninke', 'deprecated_legacy_id': '10474'},
'language', {'legacy_ams_taxonomy_id': '10474', 'type': 'language', 'label': 'Soninke', 'concept_id': 'Gi82_GNa_Uvu', 'legacy_ams_taxonomy_num_id': 10474}),

( {'type': 'language', 'id': 'HgMp_vQ1_7ed', 'preferred_label': 'Nordsotho', 'deprecated_legacy_id': '10475'},
'language', {'legacy_ams_taxonomy_id': '10475', 'type': 'language', 'label': 'Nordsotho', 'concept_id': 'HgMp_vQ1_7ed', 'legacy_ams_taxonomy_num_id': 10475}),

( {'type': 'language', 'id': 'AtJY_V9b_8Ae', 'preferred_label': 'Sydamerikanska indianspråk, andra', 'deprecated_legacy_id': '10476'},
'language', {'legacy_ams_taxonomy_id': '10476', 'type': 'language', 'label': 'Sydamerikanska indianspråk, andra', 'concept_id': 'AtJY_V9b_8Ae', 'legacy_ams_taxonomy_num_id': 10476}),

( {'type': 'language', 'id': 'HJSu_QbW_htd', 'preferred_label': 'Sukuma', 'deprecated_legacy_id': '10477'},
'language', {'legacy_ams_taxonomy_id': '10477', 'type': 'language', 'label': 'Sukuma', 'concept_id': 'HJSu_QbW_htd', 'legacy_ams_taxonomy_num_id': 10477}),

( {'type': 'language', 'id': 's6D9_CAD_eMB', 'preferred_label': 'Sumeriska', 'deprecated_legacy_id': '10478'},
'language', {'legacy_ams_taxonomy_id': '10478', 'type': 'language', 'label': 'Sumeriska', 'concept_id': 's6D9_CAD_eMB', 'legacy_ams_taxonomy_num_id': 10478}),

( {'type': 'language', 'id': 'qLfm_7QZ_37r', 'preferred_label': 'Sundanesiska', 'deprecated_legacy_id': '10479'},
'language', {'legacy_ams_taxonomy_id': '10479', 'type': 'language', 'label': 'Sundanesiska', 'concept_id': 'qLfm_7QZ_37r', 'legacy_ams_taxonomy_num_id': 10479}),

( {'type': 'language', 'id': 'rt8T_8UB_wTq', 'preferred_label': 'Thaispråk, andra', 'deprecated_legacy_id': '10480'},
'language', {'legacy_ams_taxonomy_id': '10480', 'type': 'language', 'label': 'Thaispråk, andra', 'concept_id': 'rt8T_8UB_wTq', 'legacy_ams_taxonomy_num_id': 10480}),

( {'type': 'language', 'id': 'Zrmd_kmh_AtN', 'preferred_label': 'Tereno', 'deprecated_legacy_id': '10481'},
'language', {'legacy_ams_taxonomy_id': '10481', 'type': 'language', 'label': 'Tereno', 'concept_id': 'Zrmd_kmh_AtN', 'legacy_ams_taxonomy_num_id': 10481}),

( {'type': 'language', 'id': 'JGyZ_VXD_V8b', 'preferred_label': 'Tetum', 'deprecated_legacy_id': '10482'},
'language', {'legacy_ams_taxonomy_id': '10482', 'type': 'language', 'label': 'Tetum', 'concept_id': 'JGyZ_VXD_V8b', 'legacy_ams_taxonomy_num_id': 10482}),

( {'type': 'language', 'id': 'yTWE_vfZ_HNB', 'preferred_label': 'Tiv', 'deprecated_legacy_id': '10484'},
'language', {'legacy_ams_taxonomy_id': '10484', 'type': 'language', 'label': 'Tiv', 'concept_id': 'yTWE_vfZ_HNB', 'legacy_ams_taxonomy_num_id': 10484}),

( {'type': 'language', 'id': 'mZVe_5Ug_ncD', 'preferred_label': 'Tlingit', 'deprecated_legacy_id': '10485'},
'language', {'legacy_ams_taxonomy_id': '10485', 'type': 'language', 'label': 'Tlingit', 'concept_id': 'mZVe_5Ug_ncD', 'legacy_ams_taxonomy_num_id': 10485}),

( {'type': 'language', 'id': 'cnGc_92Q_oex', 'preferred_label': 'Tokelau', 'deprecated_legacy_id': '10486'},
'language', {'legacy_ams_taxonomy_id': '10486', 'type': 'language', 'label': 'Tokelau', 'concept_id': 'cnGc_92Q_oex', 'legacy_ams_taxonomy_num_id': 10486}),

( {'type': 'language', 'id': 'RUJ6_xGA_eUN', 'preferred_label': 'Tonganska', 'deprecated_legacy_id': '10487'},
'language', {'legacy_ams_taxonomy_id': '10487', 'type': 'language', 'label': 'Tonganska', 'concept_id': 'RUJ6_xGA_eUN', 'legacy_ams_taxonomy_num_id': 10487}),

( {'type': 'language', 'id': 'qzy8_5NW_TDP', 'preferred_label': 'Tsimshian', 'deprecated_legacy_id': '10488'},
'language', {'legacy_ams_taxonomy_id': '10488', 'type': 'language', 'label': 'Tsimshian', 'concept_id': 'qzy8_5NW_TDP', 'legacy_ams_taxonomy_num_id': 10488}),

( {'type': 'language', 'id': '2tws_aRJ_FC8', 'preferred_label': 'Tsonga', 'deprecated_legacy_id': '10489'},
'language', {'legacy_ams_taxonomy_id': '10489', 'type': 'language', 'label': 'Tsonga', 'concept_id': '2tws_aRJ_FC8', 'legacy_ams_taxonomy_num_id': 10489}),

( {'type': 'language', 'id': 'VPVt_rxK_6JP', 'preferred_label': 'Tumbuka', 'deprecated_legacy_id': '10491'},
'language', {'legacy_ams_taxonomy_id': '10491', 'type': 'language', 'label': 'Tumbuka', 'concept_id': 'VPVt_rxK_6JP', 'legacy_ams_taxonomy_num_id': 10491}),

( {'type': 'language', 'id': '4eqr_GrX_XM6', 'preferred_label': 'Ottomanturkiska', 'deprecated_legacy_id': '10492'},
'language', {'legacy_ams_taxonomy_id': '10492', 'type': 'language', 'label': 'Ottomanturkiska', 'concept_id': '4eqr_GrX_XM6', 'legacy_ams_taxonomy_num_id': 10492}),

( {'type': 'language', 'id': 'zunG_TL8_koW', 'preferred_label': 'Tuvalu', 'deprecated_legacy_id': '10493'},
'language', {'legacy_ams_taxonomy_id': '10493', 'type': 'language', 'label': 'Tuvalu', 'concept_id': 'zunG_TL8_koW', 'legacy_ams_taxonomy_num_id': 10493}),

( {'type': 'language', 'id': 'jE6j_5nL_t9U', 'preferred_label': 'Tuvinska', 'deprecated_legacy_id': '10494'},
'language', {'legacy_ams_taxonomy_id': '10494', 'type': 'language', 'label': 'Tuvinska', 'concept_id': 'jE6j_5nL_t9U', 'legacy_ams_taxonomy_num_id': 10494}),

( {'type': 'language', 'id': 'hP5c_JJk_riU', 'preferred_label': 'Ugaritiska', 'deprecated_legacy_id': '10495'},
'language', {'legacy_ams_taxonomy_id': '10495', 'type': 'language', 'label': 'Ugaritiska', 'concept_id': 'hP5c_JJk_riU', 'legacy_ams_taxonomy_num_id': 10495}),

( {'type': 'language', 'id': 'h3RJ_dgP_MxH', 'preferred_label': 'Vai', 'deprecated_legacy_id': '10496'},
'language', {'legacy_ams_taxonomy_id': '10496', 'type': 'language', 'label': 'Vai', 'concept_id': 'h3RJ_dgP_MxH', 'legacy_ams_taxonomy_num_id': 10496}),

( {'type': 'language', 'id': 'sGCA_SoQ_VDz', 'preferred_label': 'Venda', 'deprecated_legacy_id': '10497'},
'language', {'legacy_ams_taxonomy_id': '10497', 'type': 'language', 'label': 'Venda', 'concept_id': 'sGCA_SoQ_VDz', 'legacy_ams_taxonomy_num_id': 10497}),

( {'type': 'language', 'id': 'SMdj_4DK_D7D', 'preferred_label': 'Volapük', 'deprecated_legacy_id': '10498'},
'language', {'legacy_ams_taxonomy_id': '10498', 'type': 'language', 'label': 'Volapük', 'concept_id': 'SMdj_4DK_D7D', 'legacy_ams_taxonomy_num_id': 10498}),

( {'type': 'language', 'id': 'QHQc_Thx_KyY', 'preferred_label': 'Votiska', 'deprecated_legacy_id': '10499'},
'language', {'legacy_ams_taxonomy_id': '10499', 'type': 'language', 'label': 'Votiska', 'concept_id': 'QHQc_Thx_KyY', 'legacy_ams_taxonomy_num_id': 10499}),

( {'type': 'language', 'id': 'Su2Y_KnM_Nm3', 'preferred_label': 'Wakashan språk', 'deprecated_legacy_id': '10500'},
'language', {'legacy_ams_taxonomy_id': '10500', 'type': 'language', 'label': 'Wakashan språk', 'concept_id': 'Su2Y_KnM_Nm3', 'legacy_ams_taxonomy_num_id': 10500}),

( {'type': 'language', 'id': 'pKgd_UJA_ahC', 'preferred_label': 'Walamo', 'deprecated_legacy_id': '10501'},
'language', {'legacy_ams_taxonomy_id': '10501', 'type': 'language', 'label': 'Walamo', 'concept_id': 'pKgd_UJA_ahC', 'legacy_ams_taxonomy_num_id': 10501}),

( {'type': 'language', 'id': 'vm4L_9sD_6Dd', 'preferred_label': 'Waray', 'deprecated_legacy_id': '10502'},
'language', {'legacy_ams_taxonomy_id': '10502', 'type': 'language', 'label': 'Waray', 'concept_id': 'vm4L_9sD_6Dd', 'legacy_ams_taxonomy_num_id': 10502}),

( {'type': 'language', 'id': 'ZLh2_Qtx_GQm', 'preferred_label': 'Washo', 'deprecated_legacy_id': '10503'},
'language', {'legacy_ams_taxonomy_id': '10503', 'type': 'language', 'label': 'Washo', 'concept_id': 'ZLh2_Qtx_GQm', 'legacy_ams_taxonomy_num_id': 10503}),

( {'type': 'language', 'id': 'PsHc_32K_PEz', 'preferred_label': 'Jakutiska', 'deprecated_legacy_id': '10506'},
'language', {'legacy_ams_taxonomy_id': '10506', 'type': 'language', 'label': 'Jakutiska', 'concept_id': 'PsHc_32K_PEz', 'legacy_ams_taxonomy_num_id': 10506}),

( {'type': 'language', 'id': 'rYw9_SHt_yWk', 'preferred_label': 'Yao', 'deprecated_legacy_id': '10507'},
'language', {'legacy_ams_taxonomy_id': '10507', 'type': 'language', 'label': 'Yao', 'concept_id': 'rYw9_SHt_yWk', 'legacy_ams_taxonomy_num_id': 10507}),

( {'type': 'language', 'id': 'PWs8_ctN_ewk', 'preferred_label': 'Yapese', 'deprecated_legacy_id': '10508'},
'language', {'legacy_ams_taxonomy_id': '10508', 'type': 'language', 'label': 'Yapese', 'concept_id': 'PWs8_ctN_ewk', 'legacy_ams_taxonomy_num_id': 10508}),

( {'type': 'language', 'id': 'KBWH_gdK_uVB', 'preferred_label': 'Zapotekiska', 'deprecated_legacy_id': '10510'},
'language', {'legacy_ams_taxonomy_id': '10510', 'type': 'language', 'label': 'Zapotekiska', 'concept_id': 'KBWH_gdK_uVB', 'legacy_ams_taxonomy_num_id': 10510}),

( {'type': 'language', 'id': 'B9Dx_jN8_QZg', 'preferred_label': 'Zuni', 'deprecated_legacy_id': '10512'},
'language', {'legacy_ams_taxonomy_id': '10512', 'type': 'language', 'label': 'Zuni', 'concept_id': 'B9Dx_jN8_QZg', 'legacy_ams_taxonomy_num_id': 10512}),

( {'type': 'language', 'id': 'ASKN_a1S_Uqt', 'preferred_label': 'Yupik språk', 'deprecated_legacy_id': '10513'},
'language', {'legacy_ams_taxonomy_id': '10513', 'type': 'language', 'label': 'Yupik språk', 'concept_id': 'ASKN_a1S_Uqt', 'legacy_ams_taxonomy_num_id': 10513}),

( {'type': 'language', 'id': 'vQHk_Y7j_NVM', 'preferred_label': 'Gammelfranska', 'deprecated_legacy_id': '10514'},
'language', {'legacy_ams_taxonomy_id': '10514', 'type': 'language', 'label': 'Gammelfranska', 'concept_id': 'vQHk_Y7j_NVM', 'legacy_ams_taxonomy_num_id': 10514}),

( {'type': 'language', 'id': 'ujrF_MeF_LSB', 'preferred_label': 'Meänkieli/Tornedalsfinska', 'deprecated_legacy_id': '10515'},
'language', {'legacy_ams_taxonomy_id': '10515', 'type': 'language', 'label': 'Meänkieli/Tornedalsfinska', 'concept_id': 'ujrF_MeF_LSB', 'legacy_ams_taxonomy_num_id': 10515}),

( {'type': 'language', 'id': 'Lozg_qhA_r3y', 'preferred_label': 'Dari', 'deprecated_legacy_id': '10516'},
'language', {'legacy_ams_taxonomy_id': '10516', 'type': 'language', 'label': 'Dari', 'concept_id': 'Lozg_qhA_r3y', 'legacy_ams_taxonomy_num_id': 10516}),

( {'type': 'language', 'id': 'xYJh_6Ez_4Md', 'preferred_label': 'Assyriska', 'deprecated_legacy_id': '10517'},
'language', {'legacy_ams_taxonomy_id': '10517', 'type': 'language', 'label': 'Assyriska', 'concept_id': 'xYJh_6Ez_4Md', 'legacy_ams_taxonomy_num_id': 10517}),

( {'type': 'language', 'id': '8VkC_Gwt_jHy', 'preferred_label': 'Kaldeiska', 'deprecated_legacy_id': '10518'},
'language', {'legacy_ams_taxonomy_id': '10518', 'type': 'language', 'label': 'Kaldeiska', 'concept_id': '8VkC_Gwt_jHy', 'legacy_ams_taxonomy_num_id': 10518}),

( {'type': 'language', 'id': 'w9hf_iyH_FCH', 'preferred_label': 'Saho', 'deprecated_legacy_id': '10519'},
'language', {'legacy_ams_taxonomy_id': '10519', 'type': 'language', 'label': 'Saho', 'concept_id': 'w9hf_iyH_FCH', 'legacy_ams_taxonomy_num_id': 10519}),

( {'type': 'language', 'id': 'nTYu_SG6_CNa', 'preferred_label': 'Kurmanji', 'deprecated_legacy_id': None},
'language', {'legacy_ams_taxonomy_id': None, 'type': 'language', 'label': 'Kurmanji', 'concept_id': 'nTYu_SG6_CNa', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'language', 'id': '49Hh_qTx_aj5', 'preferred_label': 'Sorani', 'deprecated_legacy_id': None},
'language', {'legacy_ams_taxonomy_id': None, 'type': 'language', 'label': 'Sorani', 'concept_id': '49Hh_qTx_aj5', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'language', 'id': 'M3e1_w6W_iLW', 'preferred_label': 'Kantonesiska', 'deprecated_legacy_id': None},
'language', {'legacy_ams_taxonomy_id': None, 'type': 'language', 'label': 'Kantonesiska', 'concept_id': 'M3e1_w6W_iLW', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'wage-type', 'id': 'oG8G_9cW_nRf', 'preferred_label': 'Fast månads- vecko- eller timlön', 'deprecated_legacy_id': '1'},
'wage-type', {'legacy_ams_taxonomy_id': '1', 'type': 'wage-type', 'label': 'Fast månads- vecko- eller timlön', 'concept_id': 'oG8G_9cW_nRf', 'legacy_ams_taxonomy_num_id': 1}),

( {'type': 'wage-type', 'id': 'vVtj_qm6_GQu', 'preferred_label': 'Rörlig ackords- eller provisionslön', 'deprecated_legacy_id': '8'},
'wage-type', {'legacy_ams_taxonomy_id': '8', 'type': 'wage-type', 'label': 'Rörlig ackords- eller provisionslön', 'concept_id': 'vVtj_qm6_GQu', 'legacy_ams_taxonomy_num_id': 8}),

( {'type': 'wage-type', 'id': 'asrX_9Df_ukn', 'preferred_label': 'Fast och rörlig lön', 'deprecated_legacy_id': '7'},
'wage-type', {'legacy_ams_taxonomy_id': '7', 'type': 'wage-type', 'label': 'Fast och rörlig lön', 'concept_id': 'asrX_9Df_ukn', 'legacy_ams_taxonomy_num_id': 7}),

( {'type': 'sun-education-field-1', 'id': 'U925_qMh_KzG', 'preferred_label': 'Allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Allmän utbildning', 'concept_id': 'U925_qMh_KzG', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'Umn1_P6Q_vPp', 'preferred_label': 'Pedagogik och lärarutbildning', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Pedagogik och lärarutbildning', 'concept_id': 'Umn1_P6Q_vPp', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'KmhN_geK_3Gd', 'preferred_label': 'Humaniora och konst', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Humaniora och konst', 'concept_id': 'KmhN_geK_3Gd', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'Mga4_MQZ_aRW', 'preferred_label': 'Samhällsvetenskap, juridik, handel, administration', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Samhällsvetenskap, juridik, handel, administration', 'concept_id': 'Mga4_MQZ_aRW', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'mQSY_Afj_Bv4', 'preferred_label': 'Naturvetenskap, matematik och informations- och kommunikationsteknik (IKT)', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Naturvetenskap, matematik och informations- och kommunikationsteknik (IKT)', 'concept_id': 'mQSY_Afj_Bv4', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'xsQC_69i_d1X', 'preferred_label': 'Teknik och tillverkning', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Teknik och tillverkning', 'concept_id': 'xsQC_69i_d1X', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'WtZG_YJq_m3B', 'preferred_label': 'Lant- och skogsbruk samt djursjukvård', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Lant- och skogsbruk samt djursjukvård', 'concept_id': 'WtZG_YJq_m3B', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'bV8r_EjP_wxB', 'preferred_label': 'Hälso- och sjukvård samt social omsorg', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Hälso- och sjukvård samt social omsorg', 'concept_id': 'bV8r_EjP_wxB', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'SmQC_tL9_6Vb', 'preferred_label': 'Tjänster', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Tjänster', 'concept_id': 'SmQC_tL9_6Vb', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-1', 'id': 'SvNP_hTj_bHG', 'preferred_label': 'Okänd', 'deprecated_legacy_id': None},
'sun-education-field-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-1', 'label': 'Okänd', 'concept_id': 'SvNP_hTj_bHG', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'n4aT_1x4_oFw', 'preferred_label': 'Bred, generell utbildning', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Bred, generell utbildning', 'concept_id': 'n4aT_1x4_oFw', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 's6jT_CW8_viT', 'preferred_label': 'Läs- och skrivinlärning för vuxna', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Läs- och skrivinlärning för vuxna', 'concept_id': 's6jT_CW8_viT', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'CESx_ZDS_4bY', 'preferred_label': 'Personlig utveckling', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Personlig utveckling', 'concept_id': 'CESx_ZDS_4bY', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'dvre_Duj_oyv', 'preferred_label': 'Pedagogik och lärarutbildning', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Pedagogik och lärarutbildning', 'concept_id': 'dvre_Duj_oyv', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': '49hp_SsT_G6u', 'preferred_label': 'Konst och media', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Konst och media', 'concept_id': '49hp_SsT_G6u', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'XMgK_vMp_xJp', 'preferred_label': 'Humaniora', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Humaniora', 'concept_id': 'XMgK_vMp_xJp', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'bLS3_m9V_c41', 'preferred_label': 'Samhälls- och beteendevetenskap', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Samhälls- och beteendevetenskap', 'concept_id': 'bLS3_m9V_c41', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': '4CNy_4r7_Kqk', 'preferred_label': 'Journalistik och information', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Journalistik och information', 'concept_id': '4CNy_4r7_Kqk', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': '1nfZ_zkU_RyS', 'preferred_label': 'Företagsekonomi, handel och administration', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Företagsekonomi, handel och administration', 'concept_id': '1nfZ_zkU_RyS', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'H5iv_5v9_t4o', 'preferred_label': 'Juridik och rättsvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Juridik och rättsvetenskap', 'concept_id': 'H5iv_5v9_t4o', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': '4rRq_Nyp_tCB', 'preferred_label': 'Biologi och miljövetenskap', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Biologi och miljövetenskap', 'concept_id': '4rRq_Nyp_tCB', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': '4uY2_MQQ_XQy', 'preferred_label': 'Fysik, kemi och geovetenskap', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Fysik, kemi och geovetenskap', 'concept_id': '4uY2_MQQ_XQy', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'jvrj_RjH_Xzz', 'preferred_label': 'Matematik och övrig naturvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Matematik och övrig naturvetenskap', 'concept_id': 'jvrj_RjH_Xzz', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'Bktq_dkL_8fH', 'preferred_label': 'Informations- och kommunikationsteknik (IKT)', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Informations- och kommunikationsteknik (IKT)', 'concept_id': 'Bktq_dkL_8fH', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'n6zo_VDi_gPi', 'preferred_label': 'Teknik och teknisk industri', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Teknik och teknisk industri', 'concept_id': 'n6zo_VDi_gPi', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'JjwK_ULz_LyY', 'preferred_label': 'Material och tillverkning', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Material och tillverkning', 'concept_id': 'JjwK_ULz_LyY', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'fPFH_YgR_2MH', 'preferred_label': 'Samhällsbyggnad och byggnadsteknik', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Samhällsbyggnad och byggnadsteknik', 'concept_id': 'fPFH_YgR_2MH', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'yDGz_siE_K7e', 'preferred_label': 'Lantbruk, trädgård, skog och fiske', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Lantbruk, trädgård, skog och fiske', 'concept_id': 'yDGz_siE_K7e', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'Fkrh_L3n_g84', 'preferred_label': 'Djursjukvård', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Djursjukvård', 'concept_id': 'Fkrh_L3n_g84', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'xPJU_eQm_ehe', 'preferred_label': 'Hälso- och sjukvård', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Hälso- och sjukvård', 'concept_id': 'xPJU_eQm_ehe', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'Cbrh_8aE_CcP', 'preferred_label': 'Socialt arbete och omsorg', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Socialt arbete och omsorg', 'concept_id': 'Cbrh_8aE_CcP', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'Fxpk_ePT_hMZ', 'preferred_label': 'Personliga tjänster', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Personliga tjänster', 'concept_id': 'Fxpk_ePT_hMZ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'QgQc_N3Y_Xrh', 'preferred_label': 'Transporttjänster', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Transporttjänster', 'concept_id': 'QgQc_N3Y_Xrh', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': '1joZ_PHC_wU7', 'preferred_label': 'Arbetsmiljö och renhållning', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Arbetsmiljö och renhållning', 'concept_id': '1joZ_PHC_wU7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': '16Ri_bLX_Wug', 'preferred_label': 'Säkerhetstjänster', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Säkerhetstjänster', 'concept_id': '16Ri_bLX_Wug', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-2', 'id': 'Jc4o_RgJ_ddf', 'preferred_label': 'Okänd', 'deprecated_legacy_id': None},
'sun-education-field-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-2', 'label': 'Okänd', 'concept_id': 'Jc4o_RgJ_ddf', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'wES4_1rw_TEH', 'preferred_label': 'Bred, generell utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Bred, generell utbildning', 'concept_id': 'wES4_1rw_TEH', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'K4t9_4gZ_MgJ', 'preferred_label': 'Läs- och skrivinlärning för vuxna', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Läs- och skrivinlärning för vuxna', 'concept_id': 'K4t9_4gZ_MgJ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'VQPP_GZg_xkU', 'preferred_label': 'Personlig utveckling', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Personlig utveckling', 'concept_id': 'VQPP_GZg_xkU', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'NpSa_78c_ma1', 'preferred_label': 'Pedagogik och lärarutbildning, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Pedagogik och lärarutbildning, allmän utbildning', 'concept_id': 'NpSa_78c_ma1', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'vp9A_NEv_dbU', 'preferred_label': 'Pedagogik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Pedagogik', 'concept_id': 'vp9A_NEv_dbU', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'ybAL_uBD_x7v', 'preferred_label': 'Lärarutbildning för förskola och fritidsverksamhet', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Lärarutbildning för förskola och fritidsverksamhet', 'concept_id': 'ybAL_uBD_x7v', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'DGWJ_xnW_S3L', 'preferred_label': 'Lärarutbildning för grundskolans tidiga åldrar', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Lärarutbildning för grundskolans tidiga åldrar', 'concept_id': 'DGWJ_xnW_S3L', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'VkYx_GTn_tWF', 'preferred_label': 'Ämneslärarutbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Ämneslärarutbildning', 'concept_id': 'VkYx_GTn_tWF', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '1wAW_w4s_Gif', 'preferred_label': 'Lärarutbildning i yrkesämne och praktisk-estetiska ämnen', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Lärarutbildning i yrkesämne och praktisk-estetiska ämnen', 'concept_id': '1wAW_w4s_Gif', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'F3Zd_KX4_orz', 'preferred_label': 'Pedagogik och lärarutbildning, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Pedagogik och lärarutbildning, övrig och ospecificerad utbildning', 'concept_id': 'F3Zd_KX4_orz', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '3oX1_N64_PLW', 'preferred_label': 'Konst och media, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Konst och media, allmän utbildning', 'concept_id': '3oX1_N64_PLW', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'vd5y_Ppm_TuU', 'preferred_label': 'Bild- och formkonst', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Bild- och formkonst', 'concept_id': 'vd5y_Ppm_TuU', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'WXYK_9T2_unn', 'preferred_label': 'Musik, dans och dramatik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Musik, dans och dramatik', 'concept_id': 'WXYK_9T2_unn', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '8Eas_LAZ_wW7', 'preferred_label': 'Medieproduktion', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Medieproduktion', 'concept_id': '8Eas_LAZ_wW7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'zHeT_FtE_yXn', 'preferred_label': 'Formgivning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Formgivning', 'concept_id': 'zHeT_FtE_yXn', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'UX9d_rbD_FT7', 'preferred_label': 'Konsthantverk', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Konsthantverk', 'concept_id': 'UX9d_rbD_FT7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '4U55_wx6_Q3u', 'preferred_label': 'Konst och media, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Konst och media, övrig och ospecificerad utbildning', 'concept_id': '4U55_wx6_Q3u', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'bHms_Q4W_uQo', 'preferred_label': 'Humaniora, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Humaniora, allmän utbildning', 'concept_id': 'bHms_Q4W_uQo', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '8SMP_CNZ_U75', 'preferred_label': 'Religion', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Religion', 'concept_id': '8SMP_CNZ_U75', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '4jr1_KRN_wpq', 'preferred_label': 'Främmande språk och språkvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Främmande språk och språkvetenskap', 'concept_id': '4jr1_KRN_wpq', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'gw6Z_hLp_Hvk', 'preferred_label': 'Svenska och litteraturvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Svenska och litteraturvetenskap', 'concept_id': 'gw6Z_hLp_Hvk', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'RnPd_5mc_E9G', 'preferred_label': 'Historia och arkeologi', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Historia och arkeologi', 'concept_id': 'RnPd_5mc_E9G', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'iLjG_2Q5_Bxb', 'preferred_label': 'Filosofi och logik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Filosofi och logik', 'concept_id': 'iLjG_2Q5_Bxb', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '7kiw_F1A_kd5', 'preferred_label': 'Humaniora, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Humaniora, övrig och ospecificerad utbildning', 'concept_id': '7kiw_F1A_kd5', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'cAKc_bo8_vZa', 'preferred_label': 'Samhälls- och beteendevetenskap, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Samhälls- och beteendevetenskap, allmän utbildning', 'concept_id': 'cAKc_bo8_vZa', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '2tSR_mug_Ero', 'preferred_label': 'Psykologi', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Psykologi', 'concept_id': '2tSR_mug_Ero', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'zRKE_r3q_U1s', 'preferred_label': 'Sociologi, etnologi och kulturgeografi', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Sociologi, etnologi och kulturgeografi', 'concept_id': 'zRKE_r3q_U1s', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'RuTA_kv2_NAp', 'preferred_label': 'Statsvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Statsvetenskap', 'concept_id': 'RuTA_kv2_NAp', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'riPZ_vW8_Dep', 'preferred_label': 'Nationalekonomi och ekonomisk historia', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Nationalekonomi och ekonomisk historia', 'concept_id': 'riPZ_vW8_Dep', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'LXUe_hM4_nXf', 'preferred_label': 'Samhälls- och beteendevetenskap, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Samhälls- och beteendevetenskap, övrig och ospecificerad utbildning', 'concept_id': 'LXUe_hM4_nXf', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'CYsz_5Ck_v1n', 'preferred_label': 'Journalistik och information, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Journalistik och information, allmän utbildning', 'concept_id': 'CYsz_5Ck_v1n', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'FeRX_Xc2_8b7', 'preferred_label': 'Journalistik och medievetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Journalistik och medievetenskap', 'concept_id': 'FeRX_Xc2_8b7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '6d9j_JKg_wun', 'preferred_label': 'Biblioteks- och dokumentationsvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Biblioteks- och dokumentationsvetenskap', 'concept_id': '6d9j_JKg_wun', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'LhAm_khy_7P7', 'preferred_label': 'Journalistik och information, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Journalistik och information, övrig och ospecificerad utbildning', 'concept_id': 'LhAm_khy_7P7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'uCs9_xRZ_jgp', 'preferred_label': 'Företagsekonomi, handel och administration, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Företagsekonomi, handel och administration, allmän utbildning', 'concept_id': 'uCs9_xRZ_jgp', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'VkhU_pFB_PXA', 'preferred_label': 'Inköp, försäljning och distribution', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Inköp, försäljning och distribution', 'concept_id': 'VkhU_pFB_PXA', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'myT2_Uu2_Xbr', 'preferred_label': 'Marknadsföring', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Marknadsföring', 'concept_id': 'myT2_Uu2_Xbr', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'Ta9V_DMN_999', 'preferred_label': 'Bank, försäkring och finansiering', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Bank, försäkring och finansiering', 'concept_id': 'Ta9V_DMN_999', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'MbeC_Ty2_YfL', 'preferred_label': 'Redovisning och beskattning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Redovisning och beskattning', 'concept_id': 'MbeC_Ty2_YfL', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'eXzj_Jdo_xGQ', 'preferred_label': 'Ledning och administration', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Ledning och administration', 'concept_id': 'eXzj_Jdo_xGQ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'MJKe_ubg_wms', 'preferred_label': 'Kontorsservice och sekreterartjänster', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Kontorsservice och sekreterartjänster', 'concept_id': 'MJKe_ubg_wms', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'YTUT_rQj_u5V', 'preferred_label': 'Arbetsplatsrelaterad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Arbetsplatsrelaterad utbildning', 'concept_id': 'YTUT_rQj_u5V', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'AVZz_6QN_qyM', 'preferred_label': 'Företagsekonomi, handel och administration, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Företagsekonomi, handel och administration, övrig och ospecificerad utbildning', 'concept_id': 'AVZz_6QN_qyM', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'yrf9_Zzw_FLx', 'preferred_label': 'Juridik och rättsvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Juridik och rättsvetenskap', 'concept_id': 'yrf9_Zzw_FLx', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '3q44_gdP_4fx', 'preferred_label': 'Biologi och miljövetenskap, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Biologi och miljövetenskap, allmän utbildning', 'concept_id': '3q44_gdP_4fx', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'tfmL_QCu_mdD', 'preferred_label': 'Biologi och biokemi', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Biologi och biokemi', 'concept_id': 'tfmL_QCu_mdD', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'cs1N_Zp4_KgB', 'preferred_label': 'Miljövetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Miljövetenskap', 'concept_id': 'cs1N_Zp4_KgB', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'qa2V_GWE_bnK', 'preferred_label': 'Miljövård och miljöskydd', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Miljövård och miljöskydd', 'concept_id': 'qa2V_GWE_bnK', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'K8FW_Fct_zJU', 'preferred_label': 'Naturvård och djurskydd', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Naturvård och djurskydd', 'concept_id': 'K8FW_Fct_zJU', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'sTMt_rKA_EHs', 'preferred_label': 'Biologi och miljövetenskap, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Biologi och miljövetenskap, övrig och ospecificerad utbildning', 'concept_id': 'sTMt_rKA_EHs', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'bpAf_YP9_qRf', 'preferred_label': 'Fysik, kemi och geovetenskap, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Fysik, kemi och geovetenskap, allmän utbildning', 'concept_id': 'bpAf_YP9_qRf', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'NMwf_TFi_j57', 'preferred_label': 'Fysik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Fysik', 'concept_id': 'NMwf_TFi_j57', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'vpZY_6iv_NPL', 'preferred_label': 'Kemi', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Kemi', 'concept_id': 'vpZY_6iv_NPL', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'R9em_jvo_Rge', 'preferred_label': 'Geovetenskap och naturgeografi', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Geovetenskap och naturgeografi', 'concept_id': 'R9em_jvo_Rge', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'ZjBK_44x_xkr', 'preferred_label': 'Fysik, kemi och geovetenskap, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Fysik, kemi och geovetenskap, övrig och ospecificerad utbildning', 'concept_id': 'ZjBK_44x_xkr', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'RqCj_yhw_GFc', 'preferred_label': 'Matematik och naturvetenskap, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Matematik och naturvetenskap, allmän utbildning', 'concept_id': 'RqCj_yhw_GFc', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'WcVD_tdd_7mN', 'preferred_label': 'Matematik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Matematik', 'concept_id': 'WcVD_tdd_7mN', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'VDZC_YeP_k1P', 'preferred_label': 'Statistik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Statistik', 'concept_id': 'VDZC_YeP_k1P', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'vQdy_ySp_GzS', 'preferred_label': 'Matematik och naturvetenskap, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Matematik och naturvetenskap, övrig och ospecificerad utbildning', 'concept_id': 'vQdy_ySp_GzS', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'jfpH_VdC_DM3', 'preferred_label': 'Informations- och kommunikationsteknik (IKT), allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Informations- och kommunikationsteknik (IKT), allmän utbildning', 'concept_id': 'jfpH_VdC_DM3', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'Ai2t_uy2_1KN', 'preferred_label': 'Datavetenskap och systemvetenskap', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Datavetenskap och systemvetenskap', 'concept_id': 'Ai2t_uy2_1KN', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'A2fS_GsT_bSC', 'preferred_label': 'Datoranvändning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Datoranvändning', 'concept_id': 'A2fS_GsT_bSC', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'kXWz_6uh_krq', 'preferred_label': 'Informations- och kommunikationsteknik (IKT), övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Informations- och kommunikationsteknik (IKT), övrig och ospecificerad utbildning', 'concept_id': 'kXWz_6uh_krq', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'LDWD_zzm_jeY', 'preferred_label': 'Teknik och teknisk industri, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Teknik och teknisk industri, allmän utbildning', 'concept_id': 'LDWD_zzm_jeY', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'GRcU_Soh_rg5', 'preferred_label': 'Maskinteknik och verkstadsteknik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Maskinteknik och verkstadsteknik', 'concept_id': 'GRcU_Soh_rg5', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'wuXX_ZG5_31i', 'preferred_label': 'Energi- och elektroteknik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Energi- och elektroteknik', 'concept_id': 'wuXX_ZG5_31i', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'fDye_THC_7i7', 'preferred_label': 'Elektronik, datateknik och automation', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Elektronik, datateknik och automation', 'concept_id': 'fDye_THC_7i7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '2rjL_bUS_fk3', 'preferred_label': 'Kemi- och bioteknik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Kemi- och bioteknik', 'concept_id': '2rjL_bUS_fk3', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '4VCJ_KsY_p5G', 'preferred_label': 'Fordons- och farkostteknik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Fordons- och farkostteknik', 'concept_id': '4VCJ_KsY_p5G', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'qe9k_qAm_uCN', 'preferred_label': 'Industriell ekonomi och organisation', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Industriell ekonomi och organisation', 'concept_id': 'qe9k_qAm_uCN', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'kyMw_LZF_AGG', 'preferred_label': 'Miljöteknik och miljökontroll', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Miljöteknik och miljökontroll', 'concept_id': 'kyMw_LZF_AGG', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'ex8F_PzU_aqT', 'preferred_label': 'Teknik och teknisk industri, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Teknik och teknisk industri, övrig och ospecificerad utbildning', 'concept_id': 'ex8F_PzU_aqT', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'HNek_Sui_NhZ', 'preferred_label': 'Material och tillverkning, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Material och tillverkning, allmän utbildning', 'concept_id': 'HNek_Sui_NhZ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'fLKd_6Dg_ohE', 'preferred_label': 'Tillverkning och hantering av livsmedel', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Tillverkning och hantering av livsmedel', 'concept_id': 'fLKd_6Dg_ohE', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'MSyp_oyY_5ZE', 'preferred_label': 'Tillverkning av textilier, konfektion och lädervaror', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Tillverkning av textilier, konfektion och lädervaror', 'concept_id': 'MSyp_oyY_5ZE', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'WsF2_1YU_i5R', 'preferred_label': 'Tillverkning av produkter av trä, papper, glas, porslin och plast', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Tillverkning av produkter av trä, papper, glas, porslin och plast', 'concept_id': 'WsF2_1YU_i5R', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'Bth1_2AW_wKh', 'preferred_label': 'Berg- och mineralteknik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Berg- och mineralteknik', 'concept_id': 'Bth1_2AW_wKh', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'jMGW_KY7_NCA', 'preferred_label': 'Material och tillverkning, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Material och tillverkning, övrig och ospecificerad utbildning', 'concept_id': 'jMGW_KY7_NCA', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'YXPg_MRw_bpB', 'preferred_label': 'Samhällsbyggnad och byggnadsteknik, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Samhällsbyggnad och byggnadsteknik, allmän utbildning', 'concept_id': 'YXPg_MRw_bpB', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '5v1g_CoF_VE6', 'preferred_label': 'Samhällsbyggnad och arkitektur', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Samhällsbyggnad och arkitektur', 'concept_id': '5v1g_CoF_VE6', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'TbLq_8FH_cNN', 'preferred_label': 'Byggnadsteknik och anläggningsteknik', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Byggnadsteknik och anläggningsteknik', 'concept_id': 'TbLq_8FH_cNN', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'qxHd_cS3_339', 'preferred_label': 'Samhällsbyggnad och byggnadsteknik, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Samhällsbyggnad och byggnadsteknik, övrig och ospecificerad utbildning', 'concept_id': 'qxHd_cS3_339', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '6whE_Hgj_o82', 'preferred_label': 'Lantbruk, trädgård, skog och fiske, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Lantbruk, trädgård, skog och fiske, allmän utbildning', 'concept_id': '6whE_Hgj_o82', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'RXMT_9Ui_TJJ', 'preferred_label': 'Lantbruk', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Lantbruk', 'concept_id': 'RXMT_9Ui_TJJ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'Vr5b_b9P_3mS', 'preferred_label': 'Trädgård', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Trädgård', 'concept_id': 'Vr5b_b9P_3mS', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'UAAw_NYh_3uY', 'preferred_label': 'Skog', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Skog', 'concept_id': 'UAAw_NYh_3uY', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'MbV3_bZe_HHF', 'preferred_label': 'Fiske och vattenbruk', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Fiske och vattenbruk', 'concept_id': 'MbV3_bZe_HHF', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'uX8X_4yf_Mm6', 'preferred_label': 'Lantbruk, trädgård, skog och fiske, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Lantbruk, trädgård, skog och fiske, övrig och ospecificerad utbildning', 'concept_id': 'uX8X_4yf_Mm6', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'fw6h_X4w_5iP', 'preferred_label': 'Djursjukvård', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Djursjukvård', 'concept_id': 'fw6h_X4w_5iP', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'ePB9_rjD_WYM', 'preferred_label': 'Hälso- och sjukvård, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Hälso- och sjukvård, allmän utbildning', 'concept_id': 'ePB9_rjD_WYM', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'EBEB_9iM_UH5', 'preferred_label': 'Medicin', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Medicin', 'concept_id': 'EBEB_9iM_UH5', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'fQMU_GjY_AkC', 'preferred_label': 'Omvårdnad', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Omvårdnad', 'concept_id': 'fQMU_GjY_AkC', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'LXNM_AzU_2G5', 'preferred_label': 'Tandvård', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Tandvård', 'concept_id': 'LXNM_AzU_2G5', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '12KX_UT8_y22', 'preferred_label': 'Tekniskt inriktad vårdutbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Tekniskt inriktad vårdutbildning', 'concept_id': '12KX_UT8_y22', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'bXtJ_oB1_5w6', 'preferred_label': 'Terapi, rehabilitering och kostbehandling', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Terapi, rehabilitering och kostbehandling', 'concept_id': 'bXtJ_oB1_5w6', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '8iie_en2_ff7', 'preferred_label': 'Farmaci', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Farmaci', 'concept_id': '8iie_en2_ff7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'nDbs_yS5_4ku', 'preferred_label': 'Hälso- och sjukvård, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Hälso- och sjukvård, övrig och ospecificerad utbildning', 'concept_id': 'nDbs_yS5_4ku', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'qPrp_ZgA_vgH', 'preferred_label': 'Socialt arbete och socialt omsorgsarbete, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Socialt arbete och socialt omsorgsarbete, allmän utbildning', 'concept_id': 'qPrp_ZgA_vgH', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'p2Ky_NBB_N4Y', 'preferred_label': 'Barn och ungdom', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Barn och ungdom', 'concept_id': 'p2Ky_NBB_N4Y', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 't1wZ_E9z_Uz6', 'preferred_label': 'Socialt arbete', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Socialt arbete', 'concept_id': 't1wZ_E9z_Uz6', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'uZJ8_6KZ_fbE', 'preferred_label': 'Socialt arbete och socialt omsorgsarbete, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Socialt arbete och socialt omsorgsarbete, övrig och ospecificerad utbildning', 'concept_id': 'uZJ8_6KZ_fbE', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 't4kE_UxW_WxC', 'preferred_label': 'Personliga tjänster, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Personliga tjänster, allmän utbildning', 'concept_id': 't4kE_UxW_WxC', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'iGn1_QvT_Asi', 'preferred_label': 'Hotell, restaurang och storhushåll', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Hotell, restaurang och storhushåll', 'concept_id': 'iGn1_QvT_Asi', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'jZ9y_Vyg_Kvp', 'preferred_label': 'Turism, resor och fritid', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Turism, resor och fritid', 'concept_id': 'jZ9y_Vyg_Kvp', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'JFAV_cy3_b6a', 'preferred_label': 'Idrott och friskvård', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Idrott och friskvård', 'concept_id': 'JFAV_cy3_b6a', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'NCdQ_RwY_EbN', 'preferred_label': 'Hushållstjänster och lokalvård', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Hushållstjänster och lokalvård', 'concept_id': 'NCdQ_RwY_EbN', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': '8xVi_MZF_dQL', 'preferred_label': 'Hårvård, skönhetsvård och massage', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Hårvård, skönhetsvård och massage', 'concept_id': '8xVi_MZF_dQL', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'Q9yA_4XU_KHo', 'preferred_label': 'Personliga tjänster, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Personliga tjänster, övrig och ospecificerad utbildning', 'concept_id': 'Q9yA_4XU_KHo', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'yEEt_gg6_XBK', 'preferred_label': 'Transporttjänster', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Transporttjänster', 'concept_id': 'yEEt_gg6_XBK', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'PiJ7_1y5_ztn', 'preferred_label': 'Arbetsmiljö och renhållning, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Arbetsmiljö och renhållning, allmän utbildning', 'concept_id': 'PiJ7_1y5_ztn', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'eC23_WkY_KKd', 'preferred_label': 'Arbetsmiljö och arbetarskydd', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Arbetsmiljö och arbetarskydd', 'concept_id': 'eC23_WkY_KKd', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'TKTt_U3W_xpF', 'preferred_label': 'Renhållning och avfallshantering', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Renhållning och avfallshantering', 'concept_id': 'TKTt_U3W_xpF', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'k2vb_jdE_dFL', 'preferred_label': 'Arbetsmiljö och renhållning, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Arbetsmiljö och renhållning, övrig och ospecificerad utbildning', 'concept_id': 'k2vb_jdE_dFL', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'CHpR_CZt_yFH', 'preferred_label': 'Säkerhetstjänster, allmän utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Säkerhetstjänster, allmän utbildning', 'concept_id': 'CHpR_CZt_yFH', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'gFgy_6ge_5y4', 'preferred_label': 'Säkerhet i samhället', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Säkerhet i samhället', 'concept_id': 'gFgy_6ge_5y4', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'GweQ_WZy_ns4', 'preferred_label': 'Militär utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Militär utbildning', 'concept_id': 'GweQ_WZy_ns4', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'Brzn_3gx_ynL', 'preferred_label': 'Säkerhetstjänster, övrig och ospecificerad utbildning', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Säkerhetstjänster, övrig och ospecificerad utbildning', 'concept_id': 'Brzn_3gx_ynL', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-field-3', 'id': 'iVQn_SPL_8hc', 'preferred_label': 'Okänd', 'deprecated_legacy_id': None},
'sun-education-field-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-field-3', 'label': 'Okänd', 'concept_id': 'iVQn_SPL_8hc', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-1', 'id': 'VQCo_PgG_39T', 'preferred_label': 'Förskoleutbildning', 'deprecated_legacy_id': None},
'sun-education-level-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-1', 'label': 'Förskoleutbildning', 'concept_id': 'VQCo_PgG_39T', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-1', 'id': 'p7d8_c57_h7X', 'preferred_label': 'Förgymnasial utbildning, kortare än 9 år', 'deprecated_legacy_id': None},
'sun-education-level-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-1', 'label': 'Förgymnasial utbildning, kortare än 9 år', 'concept_id': 'p7d8_c57_h7X', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-1', 'id': 'm5ZS_gKU_d4v', 'preferred_label': 'Förgymnasial utbildning, 9 (10) år', 'deprecated_legacy_id': None},
'sun-education-level-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-1', 'label': 'Förgymnasial utbildning, 9 (10) år', 'concept_id': 'm5ZS_gKU_d4v', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-1', 'id': 'wyLL_K3a_HRC', 'preferred_label': 'Gymnasial utbildning', 'deprecated_legacy_id': None},
'sun-education-level-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-1', 'label': 'Gymnasial utbildning', 'concept_id': 'wyLL_K3a_HRC', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-1', 'id': 'GD66_5wp_MSh', 'preferred_label': 'Eftergymnasial utbildning, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-1', 'label': 'Eftergymnasial utbildning, kortare än 2 år', 'concept_id': 'GD66_5wp_MSh', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-1', 'id': 'KBjB_gpS_ZJL', 'preferred_label': 'Eftergymnasial utbildning, minst 2 år', 'deprecated_legacy_id': None},
'sun-education-level-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-1', 'label': 'Eftergymnasial utbildning, minst 2 år', 'concept_id': 'KBjB_gpS_ZJL', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-1', 'id': 'ygjL_oek_A2F', 'preferred_label': 'Forskarutbildning', 'deprecated_legacy_id': None},
'sun-education-level-1', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-1', 'label': 'Forskarutbildning', 'concept_id': 'ygjL_oek_A2F', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'UY9b_iWP_8Zw', 'preferred_label': 'Förskoleutbildning', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Förskoleutbildning', 'concept_id': 'UY9b_iWP_8Zw', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': '9XKT_8kH_gi7', 'preferred_label': 'Förgymnasial utbildning kortare än 9 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Förgymnasial utbildning kortare än 9 år', 'concept_id': '9XKT_8kH_gi7', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': '9WtB_jpA_rXE', 'preferred_label': 'Förgymnasial utbildning, 9 (10) år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Förgymnasial utbildning, 9 (10) år', 'concept_id': '9WtB_jpA_rXE', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'XMhw_at9_A3J', 'preferred_label': 'Gymnasial utbildning, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Gymnasial utbildning, kortare än 2 år', 'concept_id': 'XMhw_at9_A3J', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'GNkn_NLL_SFQ', 'preferred_label': 'Gymnasial utbildning, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Gymnasial utbildning, 2 år', 'concept_id': 'GNkn_NLL_SFQ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': '7wEe_uui_q7S', 'preferred_label': 'Gymnasial utbildning, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Gymnasial utbildning, 3 år', 'concept_id': '7wEe_uui_q7S', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': '5zyL_72s_9Dq', 'preferred_label': 'Eftergymnasial utbildning, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Eftergymnasial utbildning, kortare än 2 år', 'concept_id': '5zyL_72s_9Dq', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'ou37_iEy_U9G', 'preferred_label': 'Eftergymnasial utbildning, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Eftergymnasial utbildning, 2 år', 'concept_id': 'ou37_iEy_U9G', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'NQLe_3u4_E3H', 'preferred_label': 'Eftergymnasial utbildning, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Eftergymnasial utbildning, 3 år', 'concept_id': 'NQLe_3u4_E3H', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'yXeS_uF8_bcJ', 'preferred_label': 'Eftergymnasial utbildning, 4 år', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Eftergymnasial utbildning, 4 år', 'concept_id': 'yXeS_uF8_bcJ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': '63Y9_Y6H_MRS', 'preferred_label': 'Eftergymnasial utbildning, 5 år eller längre', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Eftergymnasial utbildning, 5 år eller längre', 'concept_id': '63Y9_Y6H_MRS', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'Tgwr_PnN_eXh', 'preferred_label': 'Övrig forskarutbildning', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Övrig forskarutbildning', 'concept_id': 'Tgwr_PnN_eXh', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': 'dbU6_yuv_FBz', 'preferred_label': 'Licentiatutbildning', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Licentiatutbildning', 'concept_id': 'dbU6_yuv_FBz', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-2', 'id': '212f_zrd_YnB', 'preferred_label': 'Doktorsutbildning', 'deprecated_legacy_id': None},
'sun-education-level-2', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-2', 'label': 'Doktorsutbildning', 'concept_id': '212f_zrd_YnB', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'TzAp_hjo_A8a', 'preferred_label': 'Övrig förskoleutbildning', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig förskoleutbildning', 'concept_id': 'TzAp_hjo_A8a', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'p3Nu_zzc_L7T', 'preferred_label': 'Förskola', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Förskola', 'concept_id': 'p3Nu_zzc_L7T', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'pCha_vWk_pbR', 'preferred_label': 'Förskoleklass', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Förskoleklass', 'concept_id': 'pCha_vWk_pbR', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'WWUK_YR9_tiB', 'preferred_label': 'Övrig förgymnasial utbildning, kortare än 9 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig förgymnasial utbildning, kortare än 9 år', 'concept_id': 'WWUK_YR9_tiB', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'aqCP_3Sv_hbt', 'preferred_label': 'Grundskoleutbildning, årskurs 1–6', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Grundskoleutbildning, årskurs 1–6', 'concept_id': 'aqCP_3Sv_hbt', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'kZCr_aNc_xrE', 'preferred_label': 'Folkskoleutbildning', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Folkskoleutbildning', 'concept_id': 'kZCr_aNc_xrE', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'gxFs_gGU_1AM', 'preferred_label': 'Övrig förgymnasial utbildning, 9 (10) år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig förgymnasial utbildning, 9 (10) år', 'concept_id': 'gxFs_gGU_1AM', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'rprK_LHm_mqY', 'preferred_label': 'Realskoleutbildning', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Realskoleutbildning', 'concept_id': 'rprK_LHm_mqY', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'wkuu_1Vj_bjf', 'preferred_label': 'Grundskoleutbildning, årskurs 7–9 (10)', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Grundskoleutbildning, årskurs 7–9 (10)', 'concept_id': 'wkuu_1Vj_bjf', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'iGTR_7gg_kkU', 'preferred_label': 'Övrig gymnasial utbildning, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig gymnasial utbildning, kortare än 2 år', 'concept_id': 'iGTR_7gg_kkU', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'iztQ_c73_SFg', 'preferred_label': 'Gymnasial utbildning, studieförberedande, ej examen, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, studieförberedande, ej examen, kortare än 2 år', 'concept_id': 'iztQ_c73_SFg', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'yaxP_FTp_8tY', 'preferred_label': 'Gymnasial utbildning, yrkesinriktad, ej examen, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, yrkesinriktad, ej examen, kortare än 2 år', 'concept_id': 'yaxP_FTp_8tY', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '7FFZ_yZV_63D', 'preferred_label': 'Gymnasial utbildning, studieförberedande, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, studieförberedande, kortare än 2 år', 'concept_id': '7FFZ_yZV_63D', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'FqU9_fvY_7aG', 'preferred_label': 'Gymnasial utbildning, yrkesinriktad, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, yrkesinriktad, kortare än 2 år', 'concept_id': 'FqU9_fvY_7aG', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'eykP_7QJ_HP4', 'preferred_label': 'Övrig gymnasial utbildning, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig gymnasial utbildning, 2 år', 'concept_id': 'eykP_7QJ_HP4', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'uzND_WKS_EvF', 'preferred_label': 'Gymnasial utbildning, studieförberedande, ej examen, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, studieförberedande, ej examen, 2 år', 'concept_id': 'uzND_WKS_EvF', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'rsT8_uBz_7GD', 'preferred_label': 'Gymnasial utbildning, yrkesinriktad, ej examen, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, yrkesinriktad, ej examen, 2 år', 'concept_id': 'rsT8_uBz_7GD', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '51zD_KTU_BvN', 'preferred_label': 'Gymnasial utbildning, studieförberedande, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, studieförberedande, 2 år', 'concept_id': '51zD_KTU_BvN', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '5zXG_hBr_f5r', 'preferred_label': 'Gymnasial utbildning, yrkesinriktad, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, yrkesinriktad, 2 år', 'concept_id': '5zXG_hBr_f5r', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'dQJv_EFZ_ARk', 'preferred_label': 'Övrig gymnasial utbildning, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig gymnasial utbildning, 3 år', 'concept_id': 'dQJv_EFZ_ARk', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '24uD_BB2_iNj', 'preferred_label': 'Gymnasial utbildning, studieförberedande, ej examen, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, studieförberedande, ej examen, 3 år', 'concept_id': '24uD_BB2_iNj', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'quGU_7iM_NmM', 'preferred_label': 'Gymnasial utbildning, yrkesinriktad, ej examen, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, yrkesinriktad, ej examen, 3 år', 'concept_id': 'quGU_7iM_NmM', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'b69Q_2ZV_Wmf', 'preferred_label': 'Gymnasial utbildning, studieförberedande, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, studieförberedande, 3 år', 'concept_id': 'b69Q_2ZV_Wmf', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'mLC7_n2W_KzP', 'preferred_label': 'Gymnasial utbildning, yrkesinriktad, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial utbildning, yrkesinriktad, 3 år', 'concept_id': 'mLC7_n2W_KzP', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'B1Mh_UAh_mKJ', 'preferred_label': 'Övrig eftergymnasial utbildning, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig eftergymnasial utbildning, kortare än 2 år', 'concept_id': 'B1Mh_UAh_mKJ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'q8Mm_HQR_FNu', 'preferred_label': 'Högskoleutbildning, minst 30 högskolepoäng, ej examen, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, minst 30 högskolepoäng, ej examen, kortare än 2 år', 'concept_id': 'q8Mm_HQR_FNu', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'qrw9_w7a_aaA', 'preferred_label': 'Gymnasial påbyggnadsutbildning, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Gymnasial påbyggnadsutbildning, kortare än 2 år', 'concept_id': 'qrw9_w7a_aaA', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'qc85_uPq_tp1', 'preferred_label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, kortare än 2 år', 'concept_id': 'qc85_uPq_tp1', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'oroB_8GN_Fe5', 'preferred_label': 'Högskoleutbildning, yrkesinriktad, kortare än 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, yrkesinriktad, kortare än 2 år', 'concept_id': 'oroB_8GN_Fe5', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'awNi_fMS_NKM', 'preferred_label': 'Övrig eftergymnasial utbildning, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig eftergymnasial utbildning, 2 år', 'concept_id': 'awNi_fMS_NKM', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'xrpD_CxF_DUA', 'preferred_label': 'Högskoleutbildning, 120 högskolepoäng, ej examen, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, 120 högskolepoäng, ej examen, 2 år', 'concept_id': 'xrpD_CxF_DUA', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'CSRP_pA2_nFg', 'preferred_label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 2 år', 'concept_id': 'CSRP_pA2_nFg', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'rqLj_s2C_LTJ', 'preferred_label': 'Högskoleutbildning, generell, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, generell, 2 år', 'concept_id': 'rqLj_s2C_LTJ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'zdKB_pTL_HMS', 'preferred_label': 'Högskoleutbildning, yrkesinriktad, 2 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, yrkesinriktad, 2 år', 'concept_id': 'zdKB_pTL_HMS', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'rEsJ_SeR_PeH', 'preferred_label': 'Övrig eftergymnasial utbildning, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig eftergymnasial utbildning, 3 år', 'concept_id': 'rEsJ_SeR_PeH', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '6oeu_fV9_XGd', 'preferred_label': 'Högskoleutbildning, minst 180 högskolepoäng, ej examen, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, minst 180 högskolepoäng, ej examen, 3 år', 'concept_id': '6oeu_fV9_XGd', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'Dxdt_jZu_Je3', 'preferred_label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 3 år', 'concept_id': 'Dxdt_jZu_Je3', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'u9Qr_2wJ_nVu', 'preferred_label': 'Högskoleutbildning, generell, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, generell, 3 år', 'concept_id': 'u9Qr_2wJ_nVu', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '9soG_8dX_jNa', 'preferred_label': 'Högskoleutbildning, yrkesinriktad, 3 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, yrkesinriktad, 3 år', 'concept_id': '9soG_8dX_jNa', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'gbpT_d3p_KJf', 'preferred_label': 'Övrig eftergymnasial utbildning, 4 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig eftergymnasial utbildning, 4 år', 'concept_id': 'gbpT_d3p_KJf', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'kDKD_HRg_vJ9', 'preferred_label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 4 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 4 år', 'concept_id': 'kDKD_HRg_vJ9', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'P5XD_6Qj_hEk', 'preferred_label': 'Högskoleutbildning, generell, 4 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, generell, 4 år', 'concept_id': 'P5XD_6Qj_hEk', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'HPPA_wKg_Zpp', 'preferred_label': 'Högskoleutbildning, yrkesinriktad, 4 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, yrkesinriktad, 4 år', 'concept_id': 'HPPA_wKg_Zpp', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '8tej_hpa_8CH', 'preferred_label': 'Övrig eftergymnasial utbildning, 5 år eller längre', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig eftergymnasial utbildning, 5 år eller längre', 'concept_id': '8tej_hpa_8CH', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'iy5K_qmx_bco', 'preferred_label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 5 år', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Eftergymnasial utbildning, ej vid universitet och högskola, yrkesinriktad, 5 år', 'concept_id': 'iy5K_qmx_bco', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'mJj4_3Dz_V2z', 'preferred_label': 'Högskoleutbildning, generell, 5 år eller längre', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, generell, 5 år eller längre', 'concept_id': 'mJj4_3Dz_V2z', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': '9wiU_o3Z_BfD', 'preferred_label': 'Högskoleutbildning, yrkesinriktad, 5 år eller längre', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Högskoleutbildning, yrkesinriktad, 5 år eller längre', 'concept_id': '9wiU_o3Z_BfD', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'kfJY_KdG_Bmi', 'preferred_label': 'Övrig forskarutbildning', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Övrig forskarutbildning', 'concept_id': 'kfJY_KdG_Bmi', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'jP4Q_5Co_vWN', 'preferred_label': 'Licentiatutbildning', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Licentiatutbildning', 'concept_id': 'jP4Q_5Co_vWN', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'sun-education-level-3', 'id': 'FAhQ_vze_BYQ', 'preferred_label': 'Doktorsutbildning', 'deprecated_legacy_id': None},
'sun-education-level-3', {'legacy_ams_taxonomy_id': None, 'type': 'sun-education-level-3', 'label': 'Doktorsutbildning', 'concept_id': 'FAhQ_vze_BYQ', 'legacy_ams_taxonomy_num_id': None}),

( {'type': 'employment-duration', 'id': 'a7uU_j21_mkL', 'preferred_label': 'Tills vidare', 'deprecated_legacy_id': '1'},
'employment-duration', {'legacy_ams_taxonomy_id': '1', 'type': 'employment-duration', 'label': 'Tills vidare', 'concept_id': 'a7uU_j21_mkL', 'legacy_ams_taxonomy_num_id': 1}),

( {'type': 'employment-duration', 'id': '9RGe_UxD_FZw', 'preferred_label': '12 månader - upp till 2 år', 'deprecated_legacy_id': '9'},
'employment-duration', {'legacy_ams_taxonomy_id': '9', 'type': 'employment-duration', 'label': '12 månader - upp till 2 år', 'concept_id': '9RGe_UxD_FZw', 'legacy_ams_taxonomy_num_id': 9}),

( {'type': 'employment-duration', 'id': 'gJRb_akA_95y', 'preferred_label': '6 månader – upp till 12 månader', 'deprecated_legacy_id': '0'},
'employment-duration', {'legacy_ams_taxonomy_id': '0', 'type': 'employment-duration', 'label': '6 månader – upp till 12 månader', 'concept_id': 'gJRb_akA_95y', 'legacy_ams_taxonomy_num_id': 0}),

( {'type': 'employment-duration', 'id': 'Xj7x_7yZ_jEn', 'preferred_label': '3 månader – upp till 6 månader', 'deprecated_legacy_id': '3'},
'employment-duration', {'legacy_ams_taxonomy_id': '3', 'type': 'employment-duration', 'label': '3 månader – upp till 6 månader', 'concept_id': 'Xj7x_7yZ_jEn', 'legacy_ams_taxonomy_num_id': 3}),

( {'type': 'employment-duration', 'id': 'Sy9J_aRd_ALx', 'preferred_label': '11 dagar - upp till 3 månader', 'deprecated_legacy_id': '7'},
'employment-duration', {'legacy_ams_taxonomy_id': '7', 'type': 'employment-duration', 'label': '11 dagar - upp till 3 månader', 'concept_id': 'Sy9J_aRd_ALx', 'legacy_ams_taxonomy_num_id': 7}),

( {'type': 'employment-duration', 'id': 'cAQ8_TpB_Tdv', 'preferred_label': 'Upp till 10 dagar', 'deprecated_legacy_id': '8'},
'employment-duration', {'legacy_ams_taxonomy_id': '8', 'type': 'employment-duration', 'label': 'Upp till 10 dagar', 'concept_id': 'cAQ8_TpB_Tdv', 'legacy_ams_taxonomy_num_id': 8}),

]