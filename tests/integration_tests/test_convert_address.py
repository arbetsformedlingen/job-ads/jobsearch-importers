import pytest

from tests.integration_tests.test_resources.new_test_ads import test_ads, ads_not_sweden
from importers.current_ads import converter


@pytest.mark.parametrize('annons', test_ads)
def test_convert_ad_workplace_address_sweden(annons):
    if arbetsplats_address := annons.get('arbetsplatsadress', None):
        wp_address = converter._build_workplace_address(arbetsplats_address)
        assert wp_address['municipality'] == arbetsplats_address.get('kommun', {}).get('namn', None)
        assert wp_address['municipality_concept_id'] == arbetsplats_address.get('kommun', {}).get('varde', None)

        assert wp_address['region'] == arbetsplats_address.get('lan', {}).get('namn', None)
        assert wp_address['region_concept_id'] == arbetsplats_address.get('lan', {}).get('varde', None)

        assert wp_address['country'] == arbetsplats_address.get('land', {}).get('namn', None)
        assert wp_address['country_concept_id'] == arbetsplats_address.get('land', {}).get('varde', None)

        if (long := float(arbetsplats_address.get('longitud', None))) and (
                lat := float(arbetsplats_address.get('latitud', None))):
            assert wp_address['coordinates'] == [long, lat]


@pytest.mark.parametrize('annons', ads_not_sweden)
def test_not_sweden(annons):
    if arbetsplats_address := annons.get('arbetsplatsadress', None):
        wp_address = converter._build_workplace_address(arbetsplats_address)
        arbetsplats_address = annons.get('arbetsplatsadress', None)
        assert wp_address['country'] == arbetsplats_address.get('land', {}).get('namn', None)
        assert wp_address['country_concept_id'] == arbetsplats_address.get('land', {}).get('varde', None)
        # These values should be None
        for key in ['municipality_concept_id', 'municipality', 'region_code', 'region_concept_id', 'region',
                    'street_address', 'postcode', 'city']:
            assert wp_address[key] is None
        assert wp_address['coordinates'] == [None, None]
