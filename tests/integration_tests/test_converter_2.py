import pytest
from importers.common.helpers import clean_html
from importers.current_ads import converter
from tests.integration_tests.test_resources.new_test_ads import test_ads, ads_not_sweden
from tests.integration_tests.test_resources.original_value import get_original_value

ALL_ADS = test_ads + ads_not_sweden


@pytest.mark.parametrize("annons", ALL_ADS)
def test_application(annons):
    converted_ad = converter.convert_ad(annons)
    assert converted_ad['source_type'] == annons['kallaTyp']
    assert converted_ad['application_details']['email'] == annons['ansokningssattEpost']
    assert converted_ad['application_details']['via_af'] == annons['ansokningssattViaAF']
    assert (original_employment_type := get_original_value(converted_ad['employment_type']))
    assert original_employment_type['concept_id'] == annons['anstallningTyp']['varde']
    assert original_employment_type['label'] == annons['anstallningTyp']['namn']


@pytest.mark.parametrize('annons', ALL_ADS)
def test_convert_ad_description(annons):
    converted_ad = converter.convert_ad(annons)

    assert converted_ad['description'] == {
        'text': clean_html(annons.get('annonstextFormaterad')),
        'text_formatted': annons.get('annonstextFormaterad'),
        'company_information': annons.get('ftgInfo'),
        'needs': annons.get('beskrivningBehov'),
        'requirements': annons.get('beskrivningKrav'),
        'conditions': annons.get('villkorsbeskrivning'),
    }
