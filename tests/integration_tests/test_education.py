# TODO: Add correct module tests.integration_tests.test_resources.education_test_cases or delete this test
# When running, the following exception is thrown:
# ModuleNotFoundError: No module named 'tests.integration_tests.test_resources.education_test_cases'

# import pytest
#
# from importers.current_ads.converter import _set_education
# from tests.integration_tests.test_resources.education_test_cases import test_cases
#
#
# @pytest.mark.parametrize('tc', test_cases)
# def test_education(tc):
#     """
#     Test conversion of education data
#     """
#     annons = tc[0]
#     expected = tc[1]
#     ad = {
#         'must_have': {
#             'education': None,
#             'education_level': None
#         },
#         'nice_to_have': {
#             'education': None,
#             'education_level': None
#         }
#     }
#     _set_education(ad, annons)
#     assert ad == expected
