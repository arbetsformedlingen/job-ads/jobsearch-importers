import logging
import pytest
from valuestore.taxonomy import get_term
import importers.common.helpers
from importers.current_ads import converter
from importers.common import taxonomy
from importers.common.helpers import clean_html
from importers.common import elastic
# TODO: from tests.test_data import test_ads
from tests.integration_tests.test_resources.new_test_ads import test_ads

log = logging.getLogger(__name__)


@pytest.mark.integration
@pytest.mark.parametrize("annons_key", ['anstallningstyp', '', None, 'sprak'])
@pytest.mark.parametrize("message_key", ['anstallningTyp', 'mkey', '', None, 'sprak'])
@pytest.mark.parametrize("message_dict", [{"anstallningTyp": {"varde": "1"}}, {"anstallningTyp": {"varde": "2"}},
                                          {'': {'varde': 'v2'}}, {'mkey': {'EJvarde': 'v1'}}, {}, None,
                                          {"sprak": {"varde": "424"}}])
def test_expand_taxonomy_value(annons_key, message_key, message_dict):
    d = converter._expand_taxonomy_value(annons_key, message_key, message_dict)
    print(d)
    if not message_dict:
        # message_dict is empty
        assert d is None
        return
    message_value = message_dict.get(message_key, {}).get('label')
    if message_value:
        assert d['legacy_ams_taxonomy_id'] == message_value
        assert d['label'] == get_term(elastic.es, annons_key, message_value)
    else:
        assert d is None





@pytest.mark.parametrize('annons', test_ads)
def test_convert_ad_description(annons):
    converted_ad = converter.convert_ad(annons)
    assert converted_ad['description'] == {
        'text': clean_html(annons.get('annonstextFormaterad')),
        'text_formatted': annons.get('annonstextFormaterad'),
        'company_information': annons.get('ftgInfo'),
        'needs': annons.get('beskrivningBehov'),
        'requirements': annons.get('beskrivningKrav'),
        'conditions': annons.get('villkorsbeskrivning'),
    }


def _have_wish_entry(taxonomy_type, source_list, is_must):
    def weight_criterion(item):
        if is_must:
            return item.get('vikt', 0) >= converter.MUST_HAVE_WEIGHT
        else:
            return item.get('vikt', 0) < converter.MUST_HAVE_WEIGHT

    return [
        {'concept_id': item.get('varde'),
         'label': item.get('namn'),
         'weight': item.get('vikt'),
         'legacy_ams_taxonomy_id': taxonomy.get_legacy_by_concept_id(taxonomy_type, item.get('varde')).get(
             'legacy_ams_taxonomy_id')
         }
        for item in source_list if item and weight_criterion(item)
    ]


@pytest.mark.parametrize('annons', test_ads)
def test_convert_ad_must_have(annons):
    converted_ad = converter.convert_ad(annons)
    # TODO: skills can have replaced_by!
    assert converted_ad['must_have'].get('skills') == _have_wish_entry('kompetenser', annons.get('kompetenser', []),
                                                                       True)
    assert converted_ad['must_have'].get('languages') == _have_wish_entry('sprak', annons.get('sprak', []), True)
    assert converted_ad['must_have'].get('work_experiences') == _have_wish_entry('yrkesroll',
                                                                                 annons.get('yrkeserfarenheter', []),
                                                                                 True)
    assert converted_ad['must_have'].get('education') == _have_wish_entry(
        ['sun-education-field-1', 'sun-education-field-2', 'sun-education-field-3'],
        [annons.get('utbildningsinriktning', {})], True)
    assert converted_ad['must_have'].get('education_level') == _have_wish_entry(
        ['sun-education-level-1', 'sun-education-level-2', 'sun-education-level-3'],
        [annons.get('utbildningsniva', {})], True)


# failing because of a bug
@pytest.mark.parametrize('annons', test_ads)
def test_convert_ad_nice_to_have(annons):
    converted_ad = converter.convert_ad(annons)
    # TODO: skills can have replaced_by!
    assert converted_ad['nice_to_have'].get('skills') == _have_wish_entry('kompetenser', annons.get('kompetenser', []),
                                                                          False)
    assert converted_ad['nice_to_have'].get('languages') == _have_wish_entry('sprak', annons.get('sprak', []), False)
    assert converted_ad['nice_to_have'].get('work_experiences') == _have_wish_entry('yrkesroll',
                                                                                    annons.get('yrkeserfarenheter', []),
                                                                                    False)
    assert converted_ad['nice_to_have'].get('education') == _have_wish_entry(
        ['sun-education-field-1', 'sun-education-field-2', 'sun-education-field-3'],
        [annons.get('utbildningsinriktning', {})], False)
    assert converted_ad['nice_to_have'].get('education_level') == _have_wish_entry(
        ['sun-education-level-1', 'sun-education-level-2', 'sun-education-level-3'],
        [annons.get('utbildningsniva', {})], False)


@pytest.mark.parametrize('annons', test_ads)
def test_convert_ad_workplace_address(annons):
    converted_ad = converter.convert_ad(annons)
    # TODO: fix legacy ids later
    filtered_workplace_address = {key: value for (key, value) in converted_ad['workplace_address'].items() if
                                  '_code' not in key}
    if arbetsplats_adress := annons.get('arbetsplatsadress'):
        assert filtered_workplace_address == {
            'municipality': arbetsplats_adress.get('kommun', {}).get('namn'),
            'municipality_concept_id': arbetsplats_adress.get('kommun', {}).get('varde'),
            'region': arbetsplats_adress.get('lan', {}).get('namn'),
            'region_concept_id': arbetsplats_adress.get('lan', {}).get('varde'),
            'street_address': arbetsplats_adress.get('gatuadress'),
            'postcode': arbetsplats_adress.get('postnr'),
            'city': arbetsplats_adress.get('postort'),
            'country': arbetsplats_adress.get('land', {}).get('namn', "Sverige"),
            'country_concept_id': arbetsplats_adress.get('land', {}).get('varde', "i46j_HmG_v64"),
            'coordinates': [float(arbetsplats_adress.get('longitud')),
                            float(arbetsplats_adress.get('latitud'))],

        }
    else:
        assert filtered_workplace_address == {'municipality': None,
                                              'municipality_concept_id': None,
                                              'region': None,
                                              'region_concept_id': None,
                                              'country': None,
                                              'country_concept_id': None,
                                              'street_address': None,
                                              'postcode': None,
                                              'city': None,
                                              'coordinates': None
                                              }


@pytest.mark.integration
@pytest.mark.parametrize('annons', test_ads)
def test_convert_message(annons):
    converted_ad = converter.convert_ad(annons)
    assert converted_ad['id'] == annons.get('annonsId')
    assert converted_ad['external_id'] == annons.get('externtAnnonsId')
    assert converted_ad['headline'] == annons.get('annonsrubrik')
    assert converted_ad['application_deadline'] == importers.common.helpers.isodate(annons.get('sistaAnsokningsdatum'))
    assert converted_ad['number_of_vacancies'] == annons.get('antalPlatser')
    assert converted_ad['publication_date'] == importers.common.helpers.isodate(annons.get('publiceringsdatum'))

    # TODO: assert converted_ad['employment_type'] == converter._expand_taxonomy_value('anstallningstyp', 'anstallningTyp', annons)

    assert converted_ad['salary_type'] == converter._expand_taxonomy_value('lonetyp', 'lonTyp', annons)
    assert converted_ad['salary_description'] == annons.get('lonebeskrivning')
    assert converted_ad['duration'] == converter._expand_taxonomy_value('varaktighet', 'varaktighetTyp', annons)
    assert converted_ad['working_hours_type'] == converter._expand_taxonomy_value('arbetstidstyp', 'arbetstidTyp',
                                                                                  annons)
    default_min_omf, default_max_omf = converter._get_default_scope_of_work(annons.get('arbetstidTyp', {}).get('varde'))
    assert converted_ad['scope_of_work'] == {
        'min': annons.get('arbetstidOmfattningFran', default_min_omf),
        'max': annons.get('arbetstidOmfattningTill', default_max_omf)
    }
    assert converted_ad['access'] == annons.get('tilltrade')

    assert converted_ad['experience_required'] is not annons.get('ingenErfarenhetKravs', False)
    assert converted_ad['access_to_own_car'] == annons.get('tillgangTillEgenBil', False)

    if annons.get('korkort'):
        assert converted_ad['driving_license_required'] is True
        assert converted_ad['driving_license'] == converter.parse_driving_licence(annons)
    else:
        assert converted_ad['driving_license_required'] is False

    assert converted_ad['employer'] == {
        # TODO should this be included?  'id': annons.get('arbetsgivareId'),
        'phone_number': annons.get('telefonnummer'),
        'email': annons.get('epost'),
        'url': annons.get('webbadress'),
        'organization_number': annons.get('organisationsnummer'),
        'name': annons.get('arbetsgivareNamn'),
        'workplace': annons.get('arbetsplatsNamn'),
        'workplace_id': annons.get('arbetsplatsId')
    }

    assert converted_ad['application_details'] == {
        'information': annons.get('informationAnsokningssatt'),
        'reference': annons.get('referens'),
        'email': annons.get('ansokningssattEpost'),
        'via_af': annons.get('ansokningssattViaAF'),
        'url': annons.get('ansokningssattWebbadress'),
        'other': annons.get('ansokningssattAnnatSatt')
    }
    if 'yrkesroll' in annons:
        yrkesroll = taxonomy.get_legacy_by_concept_id('yrkesroll', annons.get('yrkesroll', {}).get('varde'))
        if yrkesroll and 'parent' in yrkesroll:
            yrkesgrupp = yrkesroll.pop('parent')
            yrkesomrade = yrkesgrupp.pop('parent')
            # TODO: fix for replaced_by etc later
            assert converted_ad['occupation'][0] == {
                'concept_id': yrkesroll['concept_id'],
                'label': yrkesroll['label'],
                'legacy_ams_taxonomy_id': yrkesroll['legacy_ams_taxonomy_id'],
                'original_value': True
            }
            assert converted_ad['occupation_group'][0] == {
                'concept_id': yrkesgrupp['concept_id'],
                'label': yrkesgrupp['label'],
                'legacy_ams_taxonomy_id': yrkesgrupp['legacy_ams_taxonomy_id'],
            }
            assert converted_ad['occupation_field'][0] == {
                'concept_id': yrkesomrade['concept_id'],
                'label': yrkesomrade['label'],
                'legacy_ams_taxonomy_id': yrkesomrade['legacy_ams_taxonomy_id'],
            }
