import json
import logging
import os

import jmespath
import pytest

from importers.common import enricher
from importers.current_ads import converter

MESSAGE_UNSUPPORTED_CONFIGURATION = "unsupported configuration. Set env variable URL_ENRICH_TEXTDOCS_SERVICE to run test."

current_dir = os.path.dirname(os.path.realpath(__file__)) + '/'

log = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def source_ads():
    return get_source_ads_from_file()


@pytest.fixture(scope="module")
def target_ads():
    return get_target_ads_from_file()


@pytest.mark.integration
@pytest.mark.parametrize("annons_id", ['22884504', '26699859'])
def test_keys_in_converted_ad(annons_id, source_ads, target_ads):
    check_test_configuration()

    source_ad = get_source_ad(annons_id, source_ads)
    target_ad = get_target_ad(annons_id, target_ads)
    message_envelope = source_ad
    message_envelope['version'] = 1
    message_envelope['timestamp'] = target_ad['timestamp']
    conv_ad = converter.convert_ad(message_envelope)

    for key, val in target_ad.items():
        if key not in ['keywords', 'timestamp', 'employer']:
            assert key in conv_ad.keys()


@pytest.mark.integration
def test_swedish_ad_missing_text_formatted(source_ads, target_ads):
    check_test_configuration()

    annons_id = '20159374'
    source_ad = get_source_ad(annons_id, source_ads)
    target_ad = get_target_ad(annons_id, target_ads)
    message_envelope = source_ad
    message_envelope['version'] = 1
    message_envelope['timestamp'] = target_ad['timestamp']
    conv_ad = converter.convert_ad(message_envelope)

    enriched_ads = enricher.enrich([conv_ad])

    log.info(json.dumps(enriched_ads, indent=4))

    assert len(enriched_ads) >= 1


@pytest.mark.integration
def test_swedish_ad_wanted_enriched_values(source_ads, target_ads):
    check_test_configuration()

    annons_id = '22884504'
    source_ad = get_source_ad(annons_id, source_ads)
    target_ad = get_target_ad(annons_id, target_ads)
    message_envelope = source_ad
    message_envelope['version'] = 1
    message_envelope['timestamp'] = target_ad['timestamp']
    conv_ad = converter.convert_ad(message_envelope)

    enriched_ads = enricher.enrich([conv_ad])

    # log.info(json.dumps(enriched_ads, indent=4))

    assert len(enriched_ads) >= 1
    enriched_ad = enriched_ads[0]

    enriched_occupations = jmespath.search('keywords.enriched.occupation', enriched_ad)
    assert len(enriched_occupations) > 0, "Enriched occupations should be greater than 0"
    for term_to_check in ['trafikledare', 'chaufför']:
        assert term_to_check in enriched_occupations

    enriched_skills = jmespath.search('keywords.enriched.skill', enriched_ad)
    assert len(enriched_skills) > 0, "Enriched skills should be greater than 0"
    for term_to_check in ['engelska', 'svenska']:
        assert term_to_check in enriched_skills

    enriched_traits = jmespath.search('keywords.enriched.trait', enriched_ad)
    assert len(enriched_traits) > 0, "Enriched traits should be greater than 0"
    for term_to_check in ['flexibel', 'självsäker']:
        assert term_to_check in enriched_traits

    enriched_locations = jmespath.search('keywords.enriched.location', enriched_ad)
    assert len(enriched_locations) > 0, "Enriched locations should be greater than 0"
    for term_to_check in ['kiruna']:
        assert term_to_check in enriched_locations


@pytest.mark.integration
def test_swedish_ad_unwanted_enriched_values(source_ads, target_ads):
    check_test_configuration()

    annons_id = '22884504'
    source_ad = get_source_ad(annons_id, source_ads)
    target_ad = get_target_ad(annons_id, target_ads)
    message_envelope = source_ad
    message_envelope['version'] = 1
    message_envelope['timestamp'] = target_ad['timestamp']
    conv_ad = converter.convert_ad(message_envelope)

    enriched_ads = enricher.enrich([conv_ad])

    # log.info(json.dumps(enriched_ads, indent=4))

    assert len(enriched_ads) >= 1
    enriched_ad = enriched_ads[0]

    enriched_occupations = jmespath.search('keywords.enriched.occupation', enriched_ad)
    for term_to_check in ['mentor']:
        assert term_to_check not in enriched_occupations

    enriched_skills = jmespath.search('keywords.enriched.skill', enriched_ad)
    for term_to_check in ['transportföretag']:
        assert term_to_check not in enriched_skills

    enriched_traits = jmespath.search('keywords.enriched.trait', enriched_ad)
    for term_to_check in ['professionell']:
        assert term_to_check not in enriched_traits

    enriched_locations = jmespath.search('keywords.enriched.location', enriched_ad)
    for term_to_check in ['alsike', 'jukkasjärvi', 'björkliden']:
        assert term_to_check not in enriched_locations


@pytest.mark.integration
def test_english_ad_wanted_enriched_values(source_ads, target_ads):
    check_test_configuration()

    annons_id = '26699859'
    source_ad = get_source_ad(annons_id, source_ads)
    target_ad = get_target_ad(annons_id, target_ads)
    message_envelope = source_ad
    message_envelope['version'] = 1
    message_envelope['timestamp'] = target_ad['timestamp']
    conv_ad = converter.convert_ad(message_envelope)

    enriched_ads = enricher.enrich([conv_ad])

    # log.info(json.dumps(enriched_ads, indent=4))

    assert len(enriched_ads) >= 1
    enriched_ad = enriched_ads[0]

    enriched_skills = jmespath.search('keywords.enriched.skill', enriched_ad)

    for term_to_check in ['kubernetes', 'powershell', 'c#']:
        assert term_to_check in enriched_skills

    # Note: Make sure that skills that are below enrich threshold are present since the ad is written in english.
    for term_to_check in ['deployment', 'innovation', 'script']:
        assert term_to_check in enriched_skills

    # Note: Make sure that occupations that are found in the headline are included (but not those that are in the description)
    enriched_occupations = jmespath.search('keywords.enriched.occupation', enriched_ad)
    for term_to_check in ['backend-utvecklare', 'systemutvecklare']:
        assert term_to_check in enriched_occupations

    # Note: Make sure that locations that are found in the headline are included (but not those that are in the description)
    enriched_locations = jmespath.search('keywords.enriched.location', enriched_ad)
    for term_to_check in ['ystad']:
        assert term_to_check in enriched_locations


@pytest.mark.integration
def test_unwanted_enriched_values(source_ads, target_ads):
    check_test_configuration()

    annons_id = '26699859'
    source_ad = get_source_ad(annons_id, source_ads)
    target_ad = get_target_ad(annons_id, target_ads)
    message_envelope = source_ad
    message_envelope['version'] = 1
    message_envelope['timestamp'] = target_ad['timestamp']
    conv_ad = converter.convert_ad(message_envelope)

    enriched_ads = enricher.enrich([conv_ad])

    # log.info(json.dumps(enriched_ads, indent=4))

    assert len(enriched_ads) >= 1
    enriched_ad = enriched_ads[0]

    # Note: Make sure that skills are not included in the enriched result if they are (blacklisted) stopwords
    enriched_skills = jmespath.search('keywords.enriched.skill', enriched_ad)
    for term_to_check in ['can', 'its', 'must', 'show']:
        assert term_to_check not in enriched_skills

    # Note: Make sure that occupations are not included in the enriched result if they are (blacklisted) stopwords
    # or not mentioned in the headline
    enriched_occupations = jmespath.search('keywords.enriched.occupation', enriched_ad)
    for term_to_check in ['devops', 'professor']:
        assert term_to_check not in enriched_occupations

    # Note: Make sure that locations are not included in the enriched result if they are (blacklisted) stopwords
    # or not mentioned in the headline
    enriched_locations = jmespath.search('keywords.enriched.location', enriched_ad)
    for term_to_check in ['stockholm']:
        assert term_to_check not in enriched_locations


def check_test_configuration():
    if not os.environ.get('URL_ENRICH_TEXTDOCS_SERVICE'):
        pytest.skip(MESSAGE_UNSUPPORTED_CONFIGURATION)


def get_source_ads_from_file():
    with open(current_dir + 'test_resources/platsannonser_source_test_import.json', encoding='utf-8') as f:
        result = json.load(f)
        return result['testannonser']


def get_target_ads_from_file():
    with open(current_dir + 'test_resources/platsannonser_expected_after_conversion.json', encoding='utf-8') as f:
        result = json.load(f)
        return result['hits']['hits']


def get_source_ad(annons_id, ads):
    ads_with_id = [ad for ad in ads if str(ad['annonsId']) == str(annons_id)]
    ad = None if len(ads_with_id) == 0 else ads_with_id[0]
    ad['annonsId'] = str(ad['annonsId'])
    return ad


def get_target_ad(annons_id, ads):
    ads_with_id = [ad['_source'] for ad in ads if str(ad['_source']['id']) == str(annons_id)]
    return None if len(ads_with_id) == 0 else ads_with_id[0]
