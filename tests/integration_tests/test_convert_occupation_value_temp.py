import pytest

from importers.taxonomy import converter
from tests.unit_tests.test_taxonomy_converter import select_test_cases
from tests.integration_tests.test_resources import occupation_value


@pytest.mark.parametrize("test_case", select_test_cases(occupation_value.occupation_test_data))
def test_convert_occupation_value_2(test_case):
    assert converter.convert_occupation_value(test_case['input']) == test_case['expected']