import pytest
from importers.current_ads import converter
from tests.integration_tests.test_resources.new_test_ads import test_ads, ads_not_sweden
from tests.integration_tests.test_resources.original_value import get_original_value
from importers.common import elastic
from valuestore.taxonomy import get_entity


ALL_ADS = test_ads + ads_not_sweden


@pytest.mark.parametrize('annons', ALL_ADS)
def test_convert_occupation(annons):
    converted_ad = converter.convert_ad(annons)

    yrkesroll = get_entity(elastic.es, 'yrkesroll', annons.get('yrkesroll', {}).get('varde'))
    yrkesgrupp = yrkesroll['parent']
    yrkesomrade = yrkesgrupp['parent']

    assert (expected := get_expected(annons['annonsId']))
    original_occupation = get_original_value(converted_ad['occupation'])
    assert original_occupation['concept_id'] == expected['occupation'][0]['concept_id']
    assert original_occupation['concept_id'] == annons['yrkesroll']['varde']

    assert converted_ad['occupation_group'][0]['concept_id'] == yrkesgrupp['concept_id']
    assert converted_ad['occupation_field'][0]['concept_id'] == yrkesomrade['concept_id']


@pytest.mark.parametrize("yrkesroll", [
    {'namn': 'Utesäljare', 'varde': 'p17k_znk_osi'},
    {'namn': 'Golfbanearbetare', 'varde': 'tArV_EVU_cFQ'},
    {'namn': 'Sjuksköterska, grundutbildad', 'varde': 'bXNH_MNX_dUR'},
    {'namn': 'VVS-montör', 'varde': 'ZZTi_v6g_4cZ'},
    {'namn': 'Besöksbokare/Kundbokare', 'varde': 'w8eg_Ufq_B5X'}
])
def test_taxonomy_get_entity(yrkesroll):
    """
    Checks that the value has parent (field) and grand parent (field)
    """
    expanded = get_entity(elastic.es, 'yrkesroll', yrkesroll['varde'])
    group = expanded['parent']
    field = expanded['parent']['parent']


def get_expected(ad_id):
    occupation_expected = [
        {'ad_id': 23448227, 'occupation': [
            {'concept_id': 'p17k_znk_osi', 'label': 'Utesäljare', 'legacy_ams_taxonomy_id': '7117',
             'original_value': True}],
         'occupation_group': [
             {'concept_id': 'oXSW_fbY_XrY', 'label': 'Företagssäljare', 'legacy_ams_taxonomy_id': '3322'}],
         'occupation_field': [
             {'concept_id': 'RPTn_bxG_ExZ', 'label': 'Försäljning, inköp, marknadsföring',
              'legacy_ams_taxonomy_id': '5'}]},
        {'ad_id': 23699999, 'occupation': [
            {'concept_id': 'tArV_EVU_cFQ', 'label': 'Golfbanearbetare', 'legacy_ams_taxonomy_id': '4570',
             'original_value': True}], 'occupation_group': [
            {'concept_id': 'XBh3_Xrm_C4R', 'label': 'Trädgårdsanläggare m.fl.', 'legacy_ams_taxonomy_id': '6113'}],
         'occupation_field': [{'concept_id': 'VuuL_7CH_adj', 'label': 'Naturbruk', 'legacy_ams_taxonomy_id': '13'}]},
        {'ad_id': 24277938, 'occupation': [
            {'concept_id': 'bXNH_MNX_dUR', 'label': 'Sjuksköterska, grundutbildad', 'legacy_ams_taxonomy_id': '7296',
             'original_value': True}], 'occupation_group': [
            {'concept_id': 'Z8ci_bBE_tmx', 'label': 'Grundutbildade sjuksköterskor', 'legacy_ams_taxonomy_id': '2221'}],
         'occupation_field': [
             {'concept_id': 'NYW6_mP6_vwf', 'label': 'Hälso- och sjukvård', 'legacy_ams_taxonomy_id': '8'}]},
        {'ad_id': 24458381, 'occupation': [
            {'concept_id': 'ZZTi_v6g_4cZ', 'label': 'VVS-montör', 'legacy_ams_taxonomy_id': '5817',
             'original_value': True}], 'occupation_group': [
            {'concept_id': 'fLKb_bJ3_69p', 'label': 'VVS-montörer m.fl.', 'legacy_ams_taxonomy_id': '7125'}],
         'occupation_field': [
             {'concept_id': 'j7Cq_ZJe_GkT', 'label': 'Bygg och anläggning', 'legacy_ams_taxonomy_id': '2'}]},
        {'ad_id': 24513844, 'occupation': [
            {'concept_id': 'w8eg_Ufq_B5X', 'label': 'Besöksbokare/Kundbokare', 'legacy_ams_taxonomy_id': '5375',
             'original_value': True}], 'occupation_group': [
            {'concept_id': 'pwRH_MT1_8nR', 'label': 'Kundtjänstpersonal', 'legacy_ams_taxonomy_id': '4222'}],
         'occupation_field': [
             {'concept_id': 'RPTn_bxG_ExZ', 'label': 'Försäljning, inköp, marknadsföring',
              'legacy_ams_taxonomy_id': '5'}]},
    ]
    for item in occupation_expected:
        if item['ad_id'] == ad_id:
            return item
    return None
