import os
import json
import logging

import jmespath
import pytest

import importers.common.helpers
from importers.current_ads import converter

current_dir = os.path.dirname(os.path.realpath(__file__)) + '/'
log = logging.getLogger(__name__)

def get_source_ads_from_file():
    with open(current_dir + 'test_resources/platsannonser_source_test_import.json', encoding='utf-8') as f:
        result = json.load(f)
        return result['testannonser']


def get_target_ads_from_file():
    with open(current_dir + 'test_resources/platsannonser_expected_after_conversion.json', encoding='utf-8') as f:
        result = json.load(f)
        return result['hits']['hits']


def get_source_ad(annons_id, ads):
    ads_with_id = [ad for ad in ads if str(ad['annonsId']) == str(annons_id)]
    ad = None if len(ads_with_id) == 0 else ads_with_id[0]
    ad['annonsId'] = str(ad['annonsId'])
    return ad


def get_target_ad(annons_id, ads):
    ads_with_id = [ad['_source'] for ad in ads if str(ad['_source']['id']) == str(annons_id)]
    return None if len(ads_with_id) == 0 else ads_with_id[0]


@pytest.mark.integration
def test_platsannons_conversion():
    source_ads = get_source_ads_from_file()
    target_ads = get_target_ads_from_file()

    annons_id = '22884504'
    source_ad = get_source_ad(annons_id, source_ads)
    target_ad = get_target_ad(annons_id, target_ads)
    assert_ad_properties(annons_id, source_ad, target_ad)



@pytest.mark.integration
def test_platsannons_conversion_old_taxonomy_occupation_to_new():
    source_ads = get_source_ads_from_file()

    annons_id = '27601240'
    source_ad = get_source_ad(annons_id, source_ads)
    # assert_converted_occupations(annons_id, source_ad, target_ad, 2)
    print('Testing convert_ad for id: %s' % annons_id)
    converted_ad = convert_test_ad(source_ad, None)
    # log.info(json.dumps(converted_ad, indent=4))
    occupations = jmespath.search("occupation", converted_ad)

    expected_concept_ids = ['CPKw_L5C_W69', 'h4wH_7kG_UTN']
    assert len(occupations) == len(expected_concept_ids)

    for occupation in occupations:
        assert occupation['concept_id'] in expected_concept_ids

@pytest.mark.integration
def test_platsannons_conversion_new_taxonomy_occupation_to_many_old():
    source_ads = get_source_ads_from_file()

    annons_id = '24488210'
    source_ad = get_source_ad(annons_id, source_ads)

    print('Testing convert_ad for id: %s' % annons_id)
    converted_ad = convert_test_ad(source_ad, None)
    # log.info(json.dumps(converted_ad, indent=4))
    occupations = jmespath.search("occupation", converted_ad)

    expected_concept_ids = ['xMbt_y3G_BrD', 'dCVP_nzp_jDd', 'fW8X_APE_3rq', '22Gn_bYP_Vz4', 'XLx7_TNH_5yA', 'EJBd_55M_4kc']
    assert len(occupations) == len(expected_concept_ids)


def assert_ad_properties(annons_id, source_ad, target_ad):
    print('Testing convert_ad for id: %s' % annons_id)
    converted_ad = convert_test_ad(source_ad, target_ad)
    # log.info(json.dumps(converted_ad, indent=4))

    for key, val in target_ad.items():
        if key not in ['keywords', 'timestamp', 'employer']:
            assert key in converted_ad.keys()
            assert type(val) == type(converted_ad[key])
            assert val == converted_ad[key], f"Expected {val} but got {converted_ad[key]}"
        elif key == 'keywords':
            company_node = converted_ad['keywords']['extracted']['employer']
            assert 'fazer food services' in company_node
            assert 'gateau' in company_node
            assert 'fazer food services ab' not in company_node
            assert 'gateau ab' not in company_node
        elif key == 'employer':
            employer_node = converted_ad['employer']

            employer_node_keys = employer_node.keys()
            assert 'phone_number' in employer_node_keys
            assert 'email' in employer_node_keys
            assert 'url' in employer_node_keys
            assert 'organization_number' in employer_node_keys
            assert 'name' in employer_node_keys
            assert 'workplace' in employer_node_keys
            assert 'workplace_id' in employer_node_keys

    #########################
    # Removed
    #########################
    assert 'status' not in converted_ad
    assert 'publiceringskanaler' not in converted_ad

    #########################
    # New and changed
    #########################
    assert 'removed' in converted_ad
    assert converted_ad['removed'] == source_ad['avpublicerad']
    assert 'last_publication_date' in converted_ad
    assert importers.common.helpers.isodate(source_ad['sistaPubliceringsdatum']) == converted_ad['last_publication_date']

    removed_date_key = 'removed_date'
    assert removed_date_key in converted_ad
    if 'avpubliceringsdatum' in source_ad:
        assert importers.common.helpers.isodate(source_ad['avpubliceringsdatum']) == converted_ad[removed_date_key]
    else:
        assert converted_ad[removed_date_key] is None


def convert_test_ad(source_ad, target_ad):
    source_envelope = source_ad
    source_envelope['version'] = 1
    if target_ad:
        source_envelope['timestamp'] = target_ad['timestamp']
    else:
        import time
        source_envelope['timestamp'] = time.time()
    conv_ad = converter.convert_ad(source_envelope)
    return conv_ad


@pytest.mark.integration
def test_corrupt_platsannons():
    source_ads = get_source_ads_from_file()

    annons_id = '20159374'
    source_ad = get_source_ad(annons_id, source_ads)
    message_envelope = source_ad

    message_envelope['version'] = 1
    message_envelope['timestamp'] = 1539958052330
    conv_ad = converter.convert_ad(message_envelope)

    assert len(conv_ad['must_have']['languages']) == 0
    assert len(conv_ad['must_have']['skills']) == 0
    assert len(conv_ad['must_have']['work_experiences']) == 0

    assert len(conv_ad['nice_to_have']['languages']) == 2
    assert len(conv_ad['nice_to_have']['skills']) == 2
    assert len(conv_ad['nice_to_have']['work_experiences']) == 2
