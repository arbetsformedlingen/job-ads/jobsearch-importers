import json
import os
import sys

# Run as:
# python count_all_historical_ads.py 2*.json


def start():
    all_list = []
    total_ads = 0
    print(os.getcwd())
    print(sys.argv)
    for filename in sys.argv[1:]:
        print(filename)
        json_name = os.path.splitext(filename)[0]
        with open(filename, 'r') as file:
            list_of_ads = json.load(file)
        nr_of_ads = len(list_of_ads)
        all_list.append((json_name, nr_of_ads))
        total_ads += nr_of_ads
    for tup in sorted(all_list):
        print(f"{tup[0]}: {tup[1]} ads")
    print('Total: ', total_ads)


if __name__ == '__main__':
    start()
