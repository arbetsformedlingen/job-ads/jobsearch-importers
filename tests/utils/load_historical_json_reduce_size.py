#  -*- coding: utf-8 -*-
import os
import glob
import json
import random

percent = 1
temp_file_name ='tmp.json'

def check_ads(ads_to_write):
    errors = []
    ok_ads = []
    for ad in ads_to_write:
        if desc := ad['description']['text']:
            ad['description']['text'] = desc  # .encode('ascii','ignore')
            try:
                with open(temp_file_name, 'w') as f:
                    json.dump(ad, f, ensure_ascii=False)
            except UnicodeEncodeError as e:
                errors.append(ad)
            else:
                ok_ads.append(ad)
    print()
    print(f"UnicodeEncodeError in {len(errors)} of {len(ads_to_write)} ads")
    return ok_ads


def write_json_file(data, year):
    cleaned_data = check_ads(data)
    filename = f"{year}_short.json"
    print(f"Writing {filename}")
    with open(filename, 'w') as f:
        json.dump(cleaned_data, f, ensure_ascii=False)


def select_random_ads(all_ads, percent):
    percentage = percent / 100
    desired_number = int(len(all_ads) * percentage) + 1
    return random.sample(all_ads, desired_number)

def cleanup():
    os.remove(temp_file_name)

def start():
    number_of_selected_ads = 0
    json_files = glob.glob("2*.json")
    for f in json_files:
        pct = percent
        year = os.path.splitext(f)[0]

        print(f"Loading file {f}")
        with open(f, 'r', encoding='utf8') as file:  # cp1252 for Windows
            list_of_ads_from_file = json.load(file)

        print(f"Loaded {len(list_of_ads_from_file)} ads from file {f}")
        random_ads = select_random_ads(list_of_ads_from_file, pct)
        print(f"{len(random_ads)} random ads from {year}")
        write_json_file(random_ads, year)
        number_of_selected_ads += len(random_ads)

    print('Number of selected random ads: ', number_of_selected_ads)
    cleanup()


if __name__ == '__main__':
    start()
