#  -*- coding: utf-8 -*-
import datetime
import pathlib
import glob
import json

files_with_path = pathlib.Path.cwd() / "*historical*.json"


def open_file(file_name):
    with open(file_name, 'r') as f:
        list_of_ads_from_file = json.load(f)
        return list_of_ads_from_file


def file_list(pattern):
    json_files = glob.glob(str(pattern))
    json_files.sort()
    return json_files


def current_year():
    return datetime.datetime.now().year


def year_from_file(file):
    year = file[-11:-7]
    return year


def start():
    json_files = file_list(files_with_path)
    all_results = []

    for y in range(2006, current_year()):
        # select files from for year y
        files_for_this_year = []
        for f in json_files:
            file_year = year_from_file(f)
            if str(y) == file_year:
                files_for_this_year.append(f)

        total_no_city = 0
        total_has_city = 0
        total_remote = 0
        total = 0

        for file in files_for_this_year:
            list_of_ads_from_file = open_file(file)
            no_city_tmp = 0
            has_city_tmp = 0
            remote_tmp = 0
            total_tmp = 0

            for ad in list_of_ads_from_file:
                if ad.get('workplace_address', {}).get('city') or ad.get('workplace_address', {}).get('municipality'):
                    has_city_tmp += 1
                else:
                    no_city_tmp += 1
                if ad.get('remote_work', None):
                    remote_tmp += 1
                total_tmp = len(list_of_ads_from_file)

            total_no_city += no_city_tmp
            total_has_city += has_city_tmp
            total_remote += remote_tmp
            total += total_tmp

        year_result = f"{y}, {total} ,  {total_no_city},    {total_has_city},   {total_remote}"
        all_results.append(year_result)

    with open(f"results.csv", mode="w") as f:
        header = f"year, total,    no city,  city,     remote  "
        f.write(f"{header}\n")
        for r in all_results:
            f.write(f"{r}\n")


if __name__ == '__main__':
    """
    Input:
    json files converted by the convert-historical function in elastic-importers
    These files are expected to be in the same directory as this program, and follow this naming convention: "*historical*.json"
    The output of this program is a .csv file with the results grouped per year
    """
    start()
