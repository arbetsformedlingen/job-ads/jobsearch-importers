#  -*- coding: utf-8 -*-
import os

from tests.utils.common import open_file, file_list

#  *short.json is the output from the program that
#  takes a small percentage of random ads from the complete years
file_name_pattern = "*short.json"


def start():
    json_files = file_list(file_name_pattern)
    for f in json_files:
        year = os.path.splitext(f)[0][0:4]
        list_of_ads_from_file = open_file(f)
        all_attributes = []
        for ad in list_of_ads_from_file:
            attributes = []
            for key in ad:
                attributes.append(key)
                all_attributes.append((year, sorted(attributes)))

        print(f"{year}: {len(list_of_ads_from_file)} ads")


if __name__ == '__main__':
    start()
