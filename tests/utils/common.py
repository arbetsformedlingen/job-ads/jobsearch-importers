import json
import glob


def open_file(file_name):
    with open(file_name, 'r') as f:
        list_of_ads_from_file = json.load(f)
        return list_of_ads_from_file


def file_list(pattern):
    json_files = glob.glob(pattern)
    json_files.sort()
    return json_files
