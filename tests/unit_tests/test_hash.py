from importers.historical.historical_common import hash_ad


def test_hash():
    """
    Check that hash function has not changed
    (e.g. hashing algorithm changes)
    """
    test_ad = {
        "id": "0001111222333",
        "webpage_url": "https://arbetsformedlingen.se/platsbanken/annonser/000000000",
        "headline": "Chaufförer sökes till sommaren",
        "description": {
            "text": "Vi söker ett antal semestervikarier som Sopchaufförer på Renhållningen i Ronneby. ... därför dig som tycker om att möta människor och ge dem en god service",
        },
        "occupation": {
            "concept_id": "bhk6_aNU_xRe",
            "label": "Renhållningsförare",
            "legacy_ams_taxonomy_id": "6691"
        },
        "publication_date": "2022-02-23T07:36:14",
        "last_publication_date": "2022-03-25T23:59:59",
        "application_deadline": "2022-03-25T23:59:59",

    }
    expected_hash = '9dc6f2bb046ea2692533b29b9f61baf8c18c0c68'

    assert expected_hash == hash_ad(test_ad)
