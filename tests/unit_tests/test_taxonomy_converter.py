import random
import pytest

from importers.taxonomy import converter
from tests.integration_tests.test_resources import standard_format, replaced_format, region_value, municipality_value, \
    value_with_replaced
from tests.integration_tests.test_resources import general_value

LIMIT_TEST_CASES = True


def select_test_cases(test_case_list):
    if not LIMIT_TEST_CASES:
        return test_case_list
    number_of_test_cases = 500
    if len(test_case_list) < number_of_test_cases:
        return test_case_list
    return random.sample(test_case_list, number_of_test_cases)


@pytest.mark.parametrize("in_value, expected", select_test_cases(standard_format.standard_format_test_cases))
def test_standard_format(in_value, expected):
    formatted = converter._standard_format(legacy_id=in_value['legacy_id'],
                                           taxonomy_type=in_value['type'],
                                           label=in_value['label'],
                                           concept_id=in_value['concept_id'])
    assert formatted == expected


@pytest.mark.parametrize("in_value, expected", select_test_cases(replaced_format.replaced_format_test_cases))
def test_add_replaced_format(in_value, expected):
    result = converter._add_replaced_format(legacy_id=in_value['legacy_id'],
                                            taxonomy_type=in_value['type'],
                                            label=in_value['label'],
                                            concept_id=in_value['concept_id'],
                                            replaced_by=in_value['replaced_by'])
    assert result == expected


@pytest.mark.parametrize("in_value, expected", select_test_cases(region_value.region_value_test_cases))
def test_convert_region_value(in_value, expected):
    assert converter.convert_region_value(in_value) == expected


@pytest.mark.parametrize("in_value, expected", select_test_cases(municipality_value.municipality_test_cases))
def test_convert_municipality_value(in_value, expected):
    converted = converter.convert_municipality_value(in_value)
    assert converted == expected


@pytest.mark.parametrize("in_value, type, expected", select_test_cases(general_value.general_value_test_cases))
def test_convert_general_value(in_value, type, expected):
    assert converter.convert_general_value(in_value, type) == expected


@pytest.mark.parametrize("in_value,type, expected", select_test_cases(value_with_replaced.convert_value_test_cases))
def test_convert_value_with_replaced(in_value, type, expected):
    assert converter.convert_value_with_replaced(in_value, type) == expected
