import unittest

from importers.common.thread_processor import ProcessJsonLineFile


class MockFile:

    def __init__(self):
        self.lines = []
        self.lines_read = 0
        self.out = ""

    def readline(self):
        if self.lines_read == len(self.lines):
            return None
        else:
            self.lines_read += 1
            return self.lines[self.lines_read-1]

    def write(self, chars):
        self.out += chars


class ProcessorTestCase(unittest.TestCase):

    def test_multiple_records_filling_batches_can_be_processed(self):
        to_test = ProcessJsonLineFile(processes=2, batch_size=2)
        in_file = MockFile()
        in_file.lines = [
            '{"foo": "bar1"}',
            '{"foo": "bar2"}',
            '{"foo": "bar3"}',
            '{"foo": "bar4"}',
            '{"foo": "bar5"}'
        ]
        out_file = MockFile()

        def add_value(records):
            for r in records:
                r['new'] = "value"
            return records

        to_test.process_file(in_file, out_file, add_value)
        self.assertTrue('{"foo": "bar1", "new": "value"}' in out_file.out)
        self.assertTrue('{"foo": "bar2", "new": "value"}' in out_file.out)  # add assertion here
        self.assertTrue('{"foo": "bar3", "new": "value"}' in out_file.out)  # add assertion here
        self.assertTrue('{"foo": "bar4", "new": "value"}' in out_file.out)  # add assertion here
        self.assertTrue('{"foo": "bar5", "new": "value"}' in out_file.out)  # add assertion here
        self.assertEqual(160, len(out_file.out))  # Check we do not have too much data


if __name__ == '__main__':
    unittest.main()
