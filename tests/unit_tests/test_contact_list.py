import pytest
from importers.current_ads.converter import build_contacts
from importers.current_ads.converter import is_union_representative


def test_contact_list():
    kontakt_personer = [{'fornamn': 'Anders', 'efternamn': 'Andersson', 'befattning': 'Rektor', 'telefonnummer': None,
                         'mobilnummer': '123456', 'epost': 'test@example.com',
                         'fackligRepresentant': False, 'beskrivning': None},
                        {'fornamn': 'Maria', 'efternamn': 'Johansson', 'befattning': 'Rektor', 'telefonnummer': None,
                         'mobilnummer': '987654321', 'epost': 'test@example.com',
                         'fackligRepresentant': False, 'beskrivning': None}]

    expected = [
        {'name': 'Anders Andersson', 'description': None, 'email': 'test@example.com', 'telephone': '123456',
         'contactType': 'Rektor'},
        {'name': 'Maria Johansson', 'description': None, 'email': 'test@example.com', 'telephone': '987654321',
         'contactType': 'Rektor'}]

    assert build_contacts(kontakt_personer) == expected


def test_union_representative():
    kontakt_personer = [
        {'fornamn': None, 'efternamn': None, 'befattning': None, 'telefonnummer': '031-00 000 00', 'mobilnummer': None,
         'epost': None, 'fackligRepresentant': True, 'beskrivning': 'Kommunal Mölndal'},
        {'fornamn': None, 'efternamn': None, 'befattning': 'Enhetschef', 'telefonnummer': None, 'mobilnummer': None,
         'epost': 'c.svensson@example.com', 'fackligRepresentant': False, 'beskrivning': 'C Svensson'}]

    expected = [{'name': None, 'description': 'C Svensson', 'email': 'c.svensson@example.com',
                 'telephone': None, 'contactType': 'Enhetschef'}]

    assert build_contacts(kontakt_personer) == expected


def test_union_representative_description():
    kontakt_personer = [
        {'fornamn': None, 'efternamn': None, 'befattning': None, 'telefonnummer': '031-00 000 00', 'mobilnummer': None,
         'epost': None, 'fackligRepresentant': True, 'beskrivning': 'Kommunal Mölndal'},
        {'fornamn': None, 'efternamn': None, 'befattning': 'Enhetschef', 'telefonnummer': None, 'mobilnummer': None,
         'epost': 'c.svensson@example.com', 'fackligRepresentant': False, 'beskrivning': 'facklig Representant'}]

    assert build_contacts(kontakt_personer) == []


@pytest.mark.parametrize('description, include', [
    ('facklig representant', False),
    ('facklig representant för fackförbundet', False),
    ('hen är facklig representant för ', False),
    ('Facklig Representant', False),
    ('Facklig repreSentant', False),
    ('facKlig represenTant', False),
    ('facklig representant', False),
    ('Fackligrepresentant', True),
    ('fackligrepresentant', True),
    ('fack', True),
    ('facket', True),
    ('kommunal', True)
])
def test_union_in_description(description, include):
    """
    Check if this is a union representative
    """
    kontakt_personer = [
        {'fornamn': None, 'efternamn': None, 'befattning': None, 'telefonnummer': '031-00 000 00', 'mobilnummer': None,
         'epost': None, 'fackligRepresentant': False, 'beskrivning': description}]
    result = build_contacts(kontakt_personer)
    if include:
        assert result[0]['description'] == description
    else:
        assert result == []


def test_2_union_1_other():
    kontakt_personer = [
        {'fornamn': None, 'efternamn': None, 'befattning': None, 'telefonnummer': '031-00 000 00', 'mobilnummer': None,
         'epost': None, 'fackligRepresentant': True, 'beskrivning': 'Kommunal Mölndal'},
        {'fornamn': None, 'efternamn': None, 'befattning': 'Enhetschef', 'telefonnummer': None, 'mobilnummer': None,
         'epost': 'c.svensson@example.com', 'fackligRepresentant': False, 'beskrivning': 'facklig Representant'},
        {'fornamn': "Lee Scratch", 'efternamn': "Perry", 'befattning': 'musician', 'telefonnummer': None,
         'mobilnummer': None,
         'epost': 'hello@example.com', 'fackligRepresentant': False, 'beskrivning': 'hello'}
    ]

    expected = [{'contactType': 'musician',
                 'description': 'hello',
                 'email': 'hello@example.com',
                 'name': 'Lee Scratch Perry',
                 'telephone': None}]

    assert build_contacts(kontakt_personer) == expected


def test_no_union_all_valid():
    kontakt_personer = [
        {'fornamn': None, 'efternamn': None, 'befattning': None, 'telefonnummer': '031-00 000 00', 'mobilnummer': None,
         'epost': None, 'fackligRepresentant': False, 'beskrivning': 'Kommunal Mölndal'},
        {'fornamn': None, 'efternamn': None, 'befattning': 'Enhetschef', 'telefonnummer': None, 'mobilnummer': None,
         'epost': 'c.svensson@example.com', 'fackligRepresentant': False, 'beskrivning': None},
        {'fornamn': "Lee Scratch", 'efternamn': "Perry", 'befattning': 'musician', 'telefonnummer': None,
         'mobilnummer': None,
         'epost': 'hello@example.com', 'fackligRepresentant': False, 'beskrivning': 'hello'}
    ]

    expected = [{'contactType': None,
                 'description': 'Kommunal Mölndal',
                 'email': None,
                 'name': None,
                 'telephone': '031-00 000 00'},
                {'contactType': 'Enhetschef',
                 'description': None,
                 'email': 'c.svensson@example.com',
                 'name': None,
                 'telephone': None},
                {'contactType': 'musician',
                 'description': 'hello',
                 'email': 'hello@example.com',
                 'name': 'Lee Scratch Perry',
                 'telephone': None}]

    assert build_contacts(kontakt_personer) == expected


def test_is_not_union():
    """
    Check that this is a union representative
    because 'fackligRepresentant' is False
    AND 'beskrivning' does not have any related text
    """
    contact_person = {'fornamn': None, 'efternamn': None, 'befattning': None, 'telefonnummer': '031-00 000 00',
                      'mobilnummer': None,
                      'epost': None, 'fackligRepresentant': False, 'beskrivning': None}
    assert not is_union_representative(contact_person)


def test_is_not_union_2():
    """
    Check that this is a union representative
    because 'fackligRepresentant' is False
    AND 'beskrivning' does not have any related text
    """
    contact_person = {'fornamn': 'Carl Hubertus', 'efternamn': 'Bernadotte', 'befattning': None,
                      'telefonnummer': '031-00 000 00',
                      'mobilnummer': None,
                      'epost': None, 'fackligRepresentant': False, 'beskrivning': 'kungen är kung'}
    assert not is_union_representative(contact_person)


def test_is_union_1():
    """
    Check that this is a union representative
    because 'fackligRepresentant is True
    """
    contact_person = {'fornamn': None,
                      'efternamn': None,
                      'befattning': None,
                      'telefonnummer': '031-00 000 00',
                      'mobilnummer': None,
                      'epost': None,
                      'fackligRepresentant': True,
                      'beskrivning': None}
    assert is_union_representative(contact_person)


def test_is_union_2():
    """
    Check that this is a union representative
    because of text 'facklig representant in 'beskrivning'
    even though 'fackligRepresentant is False
    """
    contact_person = {'fornamn': None, 'efternamn': None, 'befattning': None, 'telefonnummer': '031-00 000 00',
                      'mobilnummer': None,
                      'epost': None, 'fackligRepresentant': False, 'beskrivning': 'facklig representant'}
    assert is_union_representative(contact_person)
