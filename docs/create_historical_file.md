# Create historical files

_Note:_ Make sure that you have installed the repos in the instructions as per their respective readmes before using them.

The deliverables for historical files are the files described in the table below.

| Filename | Containing | Location | Comment |
|----------|------------|----------|---------|
| `YYYY.zip` | `YYYY.json` | `annonser/historiska/` | |
| `YYYY.jsonl.zip` | `YYYY.jsonl` | `annonser/historiska/` | |
| `YYYY.jsonl.zip` | `YYYY.jsonl` | `annonser/historiska/berikade/kompletta/` | Enriched ads, not the same content as YYYY.jsonl.zip in `annonser/historiska`. |
| `YYYY_beta1_metadata_jsonl.zip` | `YYYY.metadata.jsonl` | `annonser/historiska/berikade/metadata/` | |
| `YYYY_beta1_1_percent_jsonl.zip` | `YYYY.sample.jsonl` | `annonser/historiska/berikade/exempel/` | |

## Steps for each file

To create the different files perform the following steps (described further down).

### The file `YYYY.zip` in `annonser/historiska/`

This file contains all the ads non-enriched. In _json_ format.

1. download
2. merge
3. anonymize
4. convert
5. json
6. rename
7. zip
8. upload

### The file `YYYY.jsonl.zip` in `annonser/historiska/`

This file contains all the ads non-enriched.

1. download
2. merge
3. anonymize
4. convert
5. rename
6. zip
7. upload

### The file `YYYY.jsonl.zip` in `annonser/historiska/berikade/kompletta/`

This file contains all the ads in enriched form.

1. download
2. merge
3. anonymize
4. convert
5. enrich
6. rename
7. zip

### The file `YYYY_beta1_metadata_jsonl.zip` in `annonser/historiska/berikade/metadata/`

This file contains all the ads filtered to only contain some data.

1. download
2. merge
3. anonymize
4. convert
5. enrich
6. metadata
7. rename
8. zip

### The file `YYYY_beta1_1_percent_jsonl.zip` in `annonser/historiska/berikade/exempel/`

This file contains all the ads filtered to only contain some data.

1. download
2. merge
3. anonymize
4. convert
5. enrich
6. sample
7. rename
8. zip

## Steps

These are the steps used to generate the different files.

### download: Transfer files from MinIO to local system

_Use the cli tool `mc` or download with browser gui at [minio-console.arbetsformedlingen.se](https://minio-console.arbetsformedlingen.se)._

_Purpose:_ Download daily files from MinIO (bucket: employer / folder: stream_ads) to a local folder.

Take care to get the correct files in the start and end of the time period you want the file to contain.
The file named `stream_ads_date_now_2024-10-24_from_2024-10-23.json` contains data for the date _2024-10-23_ (from 2024-10-23 00:00:00 up to and including 2024-10-23 23:59:59).

### merge: Combine multiple daily files

_Uses the [create-historical-from-stream-ads](https://gitlab.com/arbetsformedlingen/job-ads/development-tools/create-historical-from-stream-ads) repo._

_Purpose:_ Combine multiple files downloaded from MinIO into a single file without duplicates.

Run `python main.py` in create-historical-from-stream-ads repo with the following env.variables set:
`STREAM_ADS_FOLDER` (folder with the daily files)
`YEAR` (the year to create file for)

The output file will be saved in format `YYYY_summarized.jsonl`.

### anonymize: Anonymize ads

_Uses the repo [anonymisering](https://gitlab.com/arbetsformedlingen/libraries/anonymisering)._

_Purpose:_ Anonymize the ads one by one.

Run `python main.py <path/to/YYYY_summarized.jsonl>` and you will get a new file with the input files name with the suffix `-ANON.jsonl` attached.

### convert: Convert ads in file

_Uses the [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers) repo._

_Purpose:_ Fix some issues with data format that have changed over the years.

Run `historical convert1 <path/to/YYYY_summarized.jsonl-ANON.jsonl> --output-file <YYYY_converted.jsonl>`.

### enrich: Enrich ads in file

_Uses the [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers) repo._

_Purpose:_ Adds structured data from the unstructured data by calling JobAd Enrichments.

Make sure the following env.variable is set:
`URL_ENRICH_TEXTDOCS_SERVICE=[url to enrich text documents REST endpoint]`
Run `historical enrich <path/to/YYYY_converted.jsonl-ANON.jsonl> --output-file <YYYY_enriched.jsonl>`.

### metadata: Generate meta data file

_Uses the [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers) repo._

_Purpose:_ Creates a file with only some of the data for each ad.

Run `historical extract_metadata <path/to/YYYY_enriched.jsonl> <YYYY_metadata.jsonl>`.

### sample: Generate sample file

_Uses the [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers) repo._

_Purpose:_ Create a file containing 1 % sample of the ads.

Run `historical sample <path/to/YYYY_enriched.jsonl> --output-file <YYYY_sample.jsonl>`.

Note that it should be generated from enriched file, not from meta data file!

### json: Convert jsonl to json

_Use the cli tool `jq`._

_Purpose:_ Create a file in json format from the jsonl file.

To create a json file from a jsonl file the tool `jq` is handy. (it is a great tool for everything json related.)

```bash
jq -s '.' <input.jsonl> > <output.json>
```

Note the `>` that sends the output to a file.

_Note:_ My machine ran out of memory when working on a full year. `jq -c -n '[inputs]' <input.jsonl> > <output.json>` is supposed to require less memory but it doesn't really look like it. /@limpan

### rename: Rename file

_Uses `mv` or a tool of your choice._

Rename the file (using `mv` or your preferred way).

### zip: Rename and compress file

_Uses the cli tool `zip` or windows explorer right click command for compressing._

_Purpose:_ Create a zip file containing a data file.

To create a zip file containing the jsonl or json file use `zip`.

```bash
zip <output.zip> <input-file>
```

### upload: Publish historical files

_Use the browser gui at [eu-north-1.console.aws.amazon.com/s3/buckets](https://eu-north-1.console.aws.amazon.com/s3/buckets)._

_Purpose:_ Upload files to AWS so that they will be made available at `data.jobtechdev.se`.

You need a IAM user for Amazon S3 (can be provided by Calamari), i.e. _account-id_, _username_ and _password_.

Upload the file to the bucket `data.jobtechdev.se-adhoc` and in about 15 minutes the new files will be available at [data.jobtechdev.se](https://data.jobtechdev.se/annonser/historiska/).

## Notes on using IntelliJ

The steps that uses `jobsearch-importers` can be run with IntelliJ by configuring it to run the path `jobsearch-importers\importers\historical\main.py` with the described command (except `historical`) as parameters.
