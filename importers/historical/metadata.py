import json
import logging

logging.basicConfig(level="INFO", datefmt='%Y-%m-%d %H:%M:%S',
                    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s')
log = logging.getLogger(__name__)


def remove_unwanted_fields(ad):
    for key in ['headline', 'description', 'employer', 'application_details', 'hash', 'id', 'external_id', 'uuid',
                'logo_url', 'webpage_url']:
        ad.pop(key, None)
    return ad


def extract_metadata(input_filename: str, output_filename: str) -> None:
    log.info(f"Loading file {input_filename} to be extract metadata")
    with open(input_filename, encoding="utf8") as input_file:
        with open(output_filename, 'w', encoding="utf8") as output_file:
            while line := input_file.readline():
                ad = json.loads(line)
                remove_unwanted_fields(ad)
                json.dump(ad, output_file)
                output_file.write('\n')