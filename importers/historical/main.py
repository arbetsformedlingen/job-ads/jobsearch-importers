import glob
import logging
from typing import Any

import fire
from importers import settings

from importers.common import enricher

from importers.historical import historical_converter
from jobtech.common.customlogging import configure_logging
from importers.historical.historical_converter import load_original_and_convert
from importers.historical.historical_importer import write_file_to_es
from importers.historical.historical_importer import write_ads_to_monthly_index
from importers.historical.historical_importer import refresh_alias
from importers.historical.historical_importer import refresh_alias_with_monthly_indices
from importers.historical.metadata import extract_metadata
from importers.historical.sample import sample

configure_logging([__name__.split('.')[0], 'jobtech', 'importers'])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')


def _glob_list_to_file_list(globs: tuple[Any]) -> list[str]:
    files = []
    for f in globs:
        files += glob.glob(f)
    return files


class HistoricalCLI(object):
    """class to provide CLI for historical import."""

    def convert1(self, *filenames, output_file) -> None:
        """
        Converts ads
        e.g common format for dates and correct format in various fields
        """
        load_original_and_convert(_glob_list_to_file_list(filenames), output_file)

    def enrich(self, input_file, output_file) -> None:
        """
        Calls the enrichment api and adds enrichment data to the ads
        """
        enricher.enrich_file(input_file, output_file, False, True)

    def elastic(self, filename: str, batch_size: int = settings.BATCH_SIZE) -> None:
        """
        Saves the ads in the file to ElasticSearch
        """
        write_file_to_es(filename, batch_size)

    def sample(self, inputfile, outputfile, percent=1):
        """
        Creates a smaller file (default 1% of the ads choosen randomly)
        """
        sample(inputfile, outputfile, percent)

    def extract_metadata(self, inputfile, outputfile):
        """
        Deletes some fields, e.g. ad text and keeps structured data for occupation, publication dates, location etc
        """
        extract_metadata(inputfile, outputfile)

    def refresh_alias(self, year) -> None:
        """
        Sets the historical alias to point to the latest index for that year
        """
        refresh_alias(year)

    def json_to_json_line(self, *filenames, output_file):
        """
        Converts jsonfile to jsonlines files to be able to process them with lower cpu & ram usage
        """
        historical_converter.json_to_jsonlines(_glob_list_to_file_list(filenames), output_file)

    def write_ads_to_monthly_index(self, filename: str, batch_size: int = settings.BATCH_SIZE) -> None:
        write_ads_to_monthly_index(filename, batch_size)

    def refresh_alias_with_monthly_indices(self, filename: str) -> None:
        refresh_alias_with_monthly_indices(filename)


def main():
    fire.Fire(HistoricalCLI)


if __name__ == '__main__':
    main()
