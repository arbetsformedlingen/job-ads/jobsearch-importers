#  -*- coding: utf-8 -*-
import json

from importers import settings
from importers.common.format_conversions import _str2int_if_needed, make_list_with_original_value, \
    add_original_values_in_list, log
from importers.common.helpers import end_date, isodate, clean_html
from importers.common.keywords_extracted import add_keywords_extracted
from importers.common.language_detection import detect_language_in_ad
from importers.historical.historical_common import read_json, write_json, hash_ad
from importers.common.phrase_checks import all_phrase_checks

#  move stuff from main and from common.format_conversions here
from importers.historical.historical_importer import create_index_name


def env_info_convert():
    log.info(f"Environment variables settings for CONVERT:")
    log.info(f"HISTORICAL_PREFIX: {settings.HISTORICAL_PREFIX}")
    log.info(f"HISTORICAL_SUFFIX: {settings.HISTORICAL_SUFFIX}")
    log.info(f"HISTORICAL_CHUNK_SIZE: {settings.HISTORICAL_CHUNK_SIZE}")
    log.info(f"URL_ENRICH_TEXTDOCS_SERVICE: {settings.URL_ENRICH_TEXTDOCS_SERVICE}")
    log.info(f"ENRICHER_PROCESSES: {settings.ENRICHER_PROCESSES}")
    log.info(f"ENRICHER_BATCH_SIZE: {settings.ENRICHER_BATCH_SIZE}")


def convert_ad(ad):
    # TODO: Taxonomy (replaced_by)

    # Simon stuff
    adjust_old_JT_formats(ad)
    # Narval format
    convert_JT_to_NV_minimal(ad)
    # The ad will get a sha1-based id
    ad['original_id'] = ad.get('id', None)
    ad['id'] = hash_ad(ad)
    ad['detected_language'] = detect_language_in_ad(ad)

    remove_microseconds_from_timestamp(ad)

    # Personal information must be removed
    ad['application_contacts'] = []
    converted_ad = remove_email(ad)

    # sorting to minimise problems when conversion to other formats
    ad = dict(sorted(ad.items()))

    return converted_ad


def convert(ad_list):
    log.info(f'Converting {len(ad_list)} historical ads format')
    for ad in ad_list:
        convert_ad(ad)


def remove_email(ad):
    if other := ad.get("application_details", {}).get('other', ' '):
        if '@' in other:
            ad["application_details"]["other"] = None
            log.debug(f"removed suspected email: {other}")
    return ad


def remove_microseconds_from_timestamp(ad):
    for key_name in ['application_deadline', 'publication_date', 'last_publication_date', 'removed_date']:
        if value := ad.get(key_name, None):
            if len(value) > 20:
                ad[key_name] = value[0:19]  # to remove microseconds that are in some dates


def load_original_and_convert(input_filenames: list[str], output_filename: str) -> None:
    """
    Load json line files and convert them to suitable format
    for enrichment.
    """
    with open(output_filename, "w", encoding='utf-8') as outfile:
        for in_filename in input_filenames:
            with open(in_filename, encoding='utf-8') as infile:
                while line := infile.readline():
                    ad = json.loads(line)
                    converted_ad = convert_ad(ad)
                    json.dump(converted_ad, outfile, ensure_ascii=False)
                    outfile.write('\n')


def adjust_old_JT_formats(ad):
    """
    JT -> JT
    In previous years dates and other attributes where stored differently
    """
    # 2006-2017 number_of_vacancies was stored as a string, e.g. "1"
    ad['number_of_vacancies'] = _str2int_if_needed(ad.get('number_of_vacancies'))
    # For these four date attributes the format has changed during the years:
    # 2006-2017 as "2008-02-12"
    # 2018-2020 as "2018-03-28 23:59:59,000000000"
    # 2021 as "2021-01-17T23:59:59"
    ad['application_deadline'] = end_date(isodate(ad.get('application_deadline')))
    ad['publication_date'] = isodate(ad.get('publication_date'))
    ad['last_publication_date'] = end_date(isodate(ad.get('last_publication_date')))
    ad['removed_date'] = isodate(ad.get('removed_date'))


def convert_JT_to_NV_minimal(ad):
    """
    JT -> NV
    This is a minimal conversion from Jobtech to Narval format
    without passing through taxonomy or enrichment
    """
    ad['occupation'] = make_list_with_original_value(ad.get('occupation'))
    employment_type = ad.get('employment_type')
    if isinstance(employment_type, dict):
        ad['employment_type'] = make_list_with_original_value(
            employment_type)  # TODO: once the 2021 bug is fixed only this line is needed
    else:
        if isinstance(employment_type, list):  # TODO: it's a bug in 2021
            add_original_values_in_list(employment_type)
        log.error(f"employment_type not dict in ad {ad['id']}: \"{ad['headline']}\"")
        log.error(f"employment_type: {employment_type}")
    ad['occupation_group'] = [ad.get('occupation_group')]
    ad['occupation_field'] = [ad.get('occupation_field')]

    formatted_description = clean_html(ad.get('description', {}).get('text_formatted', ''))

    headline = ad.get('headline')
    ad = all_phrase_checks(ad, formatted_description, headline)

    if must_skills := ad['nice_to_have'].get('skills', None):
        add_original_values_in_list(must_skills)
    if nice_skills := ad['must_have'].get('skills', None):
        add_original_values_in_list(nice_skills)
    add_keywords_extracted(ad)


def json_to_jsonlines(input_filenames: list[str], output_filename: str):
    """
    Converts a file with a json array on top level to a file
    of json line records.
    """
    total = 0
    with open(output_filename, "w", encoding='utf-8') as outfile:
        log.info(f"opening output file {output_filename}")
        for in_filename in input_filenames:
            log.info(f"loading file {in_filename}")
            with open(in_filename) as infile:
                row_list = json.loads(infile.read())
                log.info(f"loading {len(row_list)} ads from {in_filename    }")
                for row in row_list:
                    json.dump(row, outfile, ensure_ascii=False)
                    outfile.write('\n')
                    total += 1

    log.info(f"saved {total} ads from to {output_filename}")
