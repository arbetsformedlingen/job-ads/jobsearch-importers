import hashlib
import json
import logging
import gc
import glob

from jobtech.common.customlogging import configure_logging

configure_logging([__name__.split('.')[0], 'jobtech', 'importers'])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')


def read_json(file_name):
    log.info(f"Opening '{file_name}'")
    with open(file_name, 'r') as file:
        data = json.load(file)
    log.info(f"Read {len(data)} ads from '{file_name}'")
    return data


def write_json(data, filename):
    log.info(f"Write {len(data)} ads to {filename}")
    with open(filename, 'w') as file:
        json.dump(data, file)
    log.info(f"Write to {filename} completed")


def file_names_from_pattern(pattern):
    return glob.glob(pattern)


class FileHandlerWithGC():
    """
    Load file,
    write new file and do garbage collection when leaving scope
    """

    def __init__(self, file_name, output_name):
        self.file_name = file_name
        self.ads = read_json(self.file_name)
        self.output_file_name = f"{self.file_name.remove_suffix('.json')}{output_name}.json"

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        # write to json file
        write_json(self.ads, self.output_file_name)
        del self.ads
        gc.collect()
        log.info(f"Finished converting {self.file_name} to {self.output_file_name}")


def hash_ad(ad):
    # Create SHA1 hash from parts of the ad
    headline = ad.get('headline', ' ')
    description = ad.get('description', {}).get('text', ' ')
    publication_date = ad.get('publication_date', ' ')
    last_publication_date = ad.get('last_publication_date', ' ')
    deadline = ad.get('application_deadline', ' ')
    url = ad.get('webpage_url', ' ')
    occupation = ad.get('occupation', ' ')
    ad_id = ad.get('id', ' ')

    text_to_hash = f"{headline}{description}{publication_date}{last_publication_date}{deadline}{url}{occupation}{ad_id}"
    hash_object = hashlib.sha1(text_to_hash.encode("utf-8"))
    hash_string = hash_object.hexdigest()
    return hash_string
