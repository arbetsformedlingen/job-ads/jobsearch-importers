import json
import os
import time
import logging
from jobtech.common.customlogging import configure_logging

configure_logging([__name__.split('.')[0], 'jobtech', 'importers'])
log = logging.getLogger(__name__)
log.info(logging.getLevelName(log.getEffectiveLevel()) + ' log level activated')

# handles the import to Elastic
from importers import settings
from importers.common import elastic
from importers.common.error_handling import ImportFailedException
from importers.mappings import platsannons_mappings


def write_ads_to_monthly_index(filename: str, batch_size: int = settings.BATCH_SIZE) -> None:
    """
    Use by import pipeline. This gets a file and reads into an index matching the month
    that the ads in the file were archived by historical-importer.
    """
    env_info_import()
    filename_parts = filename.split("-")
    year_and_month = f"{filename_parts[0]}-{filename_parts[1]}"
    
    prefix = f"{settings.ES_HISTORICAL_INDEX_PREFIX}.{settings.ENVIRONMENT}.{year_and_month}"
    indices_for_year_and_month = elastic.get_indices_with_prefix(prefix)
    if len(indices_for_year_and_month) == 0: # No index for this month
        index_name = f"{prefix}.{settings.INDEX_TIMESTAMP}"
    else:
        indices_for_year_and_month.sort() # Last in list will be latest created index, due to date+time suffix.
        index_name = indices_for_year_and_month[-1]

    if not elastic.index_exists(index_name):
        log.info(f"Creating index {index_name}")
        elastic.create_index(index_name, platsannons_mappings)
    else:
        log.info(f"Index exists.")

    log.info(f"Import JSON file: {filename}. Index name: {index_name}")

    with open(filename, encoding="utf8") as f:
        c = 0
        total = 0
        records = []
        while line := f.readline():
            c += 1
            total += 1
            records.append(json.loads(line))
            if c >= batch_size:
                load_into_elastic(records, index_name)
                log.info(f"Imported {total} ads to {index_name}")
                c = 0
                records = []
        if c != 0:
            load_into_elastic(records, index_name)
            log.info(f"Imported {total} ads to {index_name}")

def refresh_alias_with_monthly_indices(filename: str):
    env_info_import()
    filename_parts = filename.split("-")
    year_and_month = f"{filename_parts[0]}-{filename_parts[1]}"
    if elastic.alias_exists(settings.HISTORICAL_ALIAS):
        indices_in_current_alias = elastic.get_indices_in_alias(settings.HISTORICAL_ALIAS)
    else:
        indices_in_current_alias = []

    prefix = f"{settings.ES_HISTORICAL_INDEX_PREFIX}.{settings.ENVIRONMENT}.{year_and_month}"
    indices_for_year = elastic.get_indices_with_prefix(prefix)
    indices_for_year.sort() # Last in list will be latest created index, due to date+time suffix.
    latest_index_for_year = indices_for_year[-1]
    current_indices_for_year = list(filter(lambda i: i.startswith(prefix),
                                           indices_in_current_alias))
    if not (len(current_indices_for_year) == 1 and latest_index_for_year == current_indices_for_year[0]):
        elastic.update_alias_historical_importer(latest_index_for_year, settings.HISTORICAL_ALIAS)
        log.info(f"Latest index added: {latest_index_for_year}")



def write_file_to_es(filename: str, batch_size: int = settings.BATCH_SIZE) -> None:
    env_info_import()
    index_name = f"{settings.ES_HISTORICAL_INDEX_PREFIX}-{filename.split(os.sep)[-1].split('.')[0].lower()}-{settings.INDEX_TIMESTAMP}"
    log.info(f"Import JSON file: {filename}. Index name: {index_name}")
    elastic.create_index(index_name, platsannons_mappings)

    with open(filename, encoding="utf8") as f:
        c = 0
        total = 0
        records = []
        while line := f.readline():
            c += 1
            total += 1
            records.append(json.loads(line))
            if c >= batch_size:
                load_into_elastic(records, index_name)
                log.info(f"Imported {total} ads to {index_name}")
                c = 0
                records = []
        if c != 0:
            load_into_elastic(records, index_name)
            log.info(f"Imported {total} ads to {index_name}")


def load_into_elastic(ad_list, es_index):
    max_retries = 5
    fail_count = 0

    for x in range(max_retries):
        try:
            elastic.bulk_index(ad_list, es_index, timeout=settings.HISTORICAL_TIMEOUT)
            break
        except Exception as e:
            fail_count += 1
            exception_text = str(e)
            if len(exception_text) > 400:
                error_text = f"{exception_text[:400]}"
            else:
                error_text = exception_text
            log.warning(f"Failed to import to Elastic, failed attempt: {fail_count}. Exception: {error_text}")

            if fail_count >= max_retries:
                log.error(f"Failed to import after {fail_count} attempts. Exit!  {error_text}")
                raise ImportFailedException(error_text)
            time.sleep(fail_count)  # longer sleep for each retry


def create_index_name(year):
    suffix = '-' + settings.HISTORICAL_SUFFIX if settings.HISTORICAL_SUFFIX else ''
    index_name = f"{settings.HISTORICAL_PREFIX}-{year}{suffix}"
    return index_name


def env_info_import():
    log.info(f"Environment for IMPORT: ES_HOST: {settings.ES_HOST} ES_PORT: {settings.ES_PORT} HISTORICAL_ALIAS: {settings.HISTORICAL_ALIAS}")


def refresh_alias(year):
    env_info_import()
    if elastic.alias_exists(settings.HISTORICAL_ALIAS):
        indices_in_current_alias = elastic.get_indices_in_alias(settings.HISTORICAL_ALIAS)
    else:
        indices_in_current_alias = []
    indices_for_year = elastic.get_indices_with_prefix(f"{settings.HISTORICAL_PREFIX}-{year}-")
    indices_for_year.sort() # Last in list will be latest created index, due to date+time suffix.
    latest_index_for_year = indices_for_year[-1]
    current_indices_for_year = list(filter(lambda i: i.startswith(f"{settings.HISTORICAL_PREFIX}-{year}-"),
                                           indices_in_current_alias))
    if not (len(current_indices_for_year) == 1 and latest_index_for_year == current_indices_for_year[0]):
        elastic.update_alias([latest_index_for_year], current_indices_for_year, settings.HISTORICAL_ALIAS)
