import random
import logging

logging.basicConfig(level="INFO", datefmt='%Y-%m-%d %H:%M:%S',
                    format='%(asctime)s.%(msecs)03d %(levelname)s %(module)s - %(funcName)s: %(message)s')
log = logging.getLogger(__name__)


def _selected(percent: int):
    """
    Returns true in percent of cases called.
    """
    return random.randrange(100) < percent


def sample(input_filename: str,  output_filename: str, percent: int = 1) -> None:
    log.info(f"Loading file {input_filename} to be reduced to {percent} % of original")
    random.seed()
    with open(input_filename, encoding="utf8") as input_file:
        with open(output_filename, 'w', encoding="utf8") as output_file:
            while line := input_file.readline():
                if _selected(percent):
                    output_file.write(line)

