import sys
import requests

import importers.settings
from importers.taxonomy import taxonomy_settings, log
from importers import settings
from importers.taxonomy.converter import convert_occupation_value, convert_general_value, convert_value_with_replaced, \
    convert_region_value, convert_municipality_value
from importers.taxonomy.queries import OCCUPATIONS_QUERY, GENERAL_QUERY, QUERY_WITH_REPLACED, REGION_QUERY, \
    MUNICIPALITY_QUERY
from importers.common.error_handling import ImportFailedException


def fetch_taxonomy_version():
    try:
        taxonomy_response = requests.get(url=settings.TAXONOMY_VERSION_URL, headers={})
        taxonomy_response.raise_for_status()
        versions = taxonomy_response.json()
        latest_version = 0
        timestamp = None
        for version in versions:
            if version.get("taxonomy/version") > latest_version:
                latest_version = version.get("taxonomy/version")
                timestamp = version.get("taxonomy/timestamp")
                log.info(f"Taxonomy version: {latest_version}, timestamp: {timestamp}")
    except requests.exceptions.ConnectionError as e:
        log.error(f"Failed to connect to taxonomy host: {settings.TAXONOMY_VERSION_URL}. Check your TAXONOMY_VERSION_URL environment variable and that the taxonomy service is reachable and up-and-running.")
        log.error(e)
        log.error("Quiting due to previous error(s)...")
        sys.exit(1)
    except Exception as e:
        log.error('Failed to fetch taxonomy version', e)
        return

    return latest_version, timestamp

def _fetch_taxonomy_values(params):
    taxonomy_response = requests.get(url=settings.TAXONOMY_GRAPHQL_URL, headers={}, params=params)
    taxonomy_response.raise_for_status()
    return taxonomy_response.json()


def fetch_value(query):
    params = {'query': query}
    response = _fetch_taxonomy_values(params)
    values = response.get('data', {}).get('concepts', [])
    return values


def fetch_and_convert_values(version):
    version_string = '"' + str(version) + '"'
    converted_values = []
    occupations = fetch_value(OCCUPATIONS_QUERY % version_string)
    if occupations:
        converted_values += [item for value in occupations for item in convert_occupation_value(value)]
    else:
        log.error(f"Could not fetch occupations. Query: {OCCUPATIONS_QUERY % version_string}")

    regions = fetch_value(REGION_QUERY % version_string)
    log.debug(f"Regions: {regions}")
    if regions:
        converted_values += [convert_region_value(region) for region in regions[0].get('narrower', [])]
    else:
        log.error(f"Could not fetch regions. Query: {REGION_QUERY % version_string}")

    municipalities = fetch_value(MUNICIPALITY_QUERY % version_string)
    log.debug(f"Municipalities: {municipalities}")
    if municipalities:
        converted_values += [convert_municipality_value(municipality) for municipality in municipalities]
    else:
        log.error(f"Could not fetch municipalities. Query: {MUNICIPALITY_QUERY % version_string}")

    if not occupations or not regions or not municipalities:
        error_msg = f"At least one of Occupations, Regions, Municipalities could not load. Exit!"
        log.error(error_msg)
        raise ImportFailedException(error_msg)

    driving_licenses = fetch_value(GENERAL_QUERY % ('"driving-licence"', version_string))
    log.debug(f"Driving_licences: {driving_licenses}")
    converted_values += [convert_general_value(driving_license, 'driving-license') for driving_license in
                         driving_licenses]

    for type_general in taxonomy_settings.GENERAL_VALUES:
        field = '"' + type_general + '"'
        values = fetch_value(GENERAL_QUERY % (field, version_string))
        log.info(f"General type: {type_general}. Values in DEBUG")
        log.debug(f"Values: {values}")
        converted_values += [convert_general_value(value, type_general) for value in values]

    for type_replaced in taxonomy_settings.REPLACED_VALUES:
        field = '"' + type_replaced + '"'
        values = fetch_value(QUERY_WITH_REPLACED % (field, version_string))
        log.info(f"Replaced type: {type_replaced}. Values in DEBUG")
        log.debug(f"Values: {values}")
        converted_values += [convert_value_with_replaced(value, type_replaced) for value in values]

    return converted_values
