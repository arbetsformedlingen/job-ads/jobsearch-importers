import concurrent.futures
import logging
import time
from multiprocessing import Value
import requests

from importers import settings
from importers.common import elastic

log = logging.getLogger(__name__)

counter = None
logo_cache = {}


def bulk_fetch_ad_details(ad_batch):
    global counter
    
    len_ad_batch = len(ad_batch)
    parallelism = settings.LA_DETAILS_PARALLELISM if len_ad_batch > 99 else 1
    counter = Value('i', 0)
    result_output = {}
    
    log.info(f'Fetch ad details. Processes: {parallelism}, batch len: {len_ad_batch}')
    log.info(f'ad details base url: {settings.LA_DETAILS_URL}')

    # with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=parallelism) as executor:
        # Start the load operations
        future_to_fetch_result = {
            executor.submit(_load_ad_details, ad_data): ad_data
            for ad_data in ad_batch
        }

        for future in concurrent.futures.as_completed(future_to_fetch_result):
            try:
                detailed_result = future.result()
                result_output[detailed_result['annonsId']] = detailed_result
                # += operation is not atoqmic, so we need to get a lock for counter:
                if not hasattr(counter, "value"):
                    log.warning("Counter has no valid field.")
                    continue
                with counter.get_lock():
                    counter.value += 1
                    if counter.value % 500 == 0:
                        log.info(f"Threaded fetch ad details. Processed docs: {str(counter.value)}")
            except requests.exceptions.HTTPError as exc:
                # status_code = exc.response.status_code
                log.error(f'Fetch ad details call generated an exception: {exc}')
            except Exception as exc:
                log.error(f'Fetch ad details call generated an exception: {exc}')

    return result_output


def _load_ad_to_remove(unpublished_ad_meta):
    # Populate an unpublished ad with details
    ad_id = unpublished_ad_meta['annonsId']
    log.info(f"Ad is avpublicerad, preparing to remove it: {ad_id}")
    removed_date = unpublished_ad_meta.get('avpubliceringsdatum') or \
                    time.strftime("%Y-%m-%dT%H:%M:%S", time.localtime(unpublished_ad_meta.get('uppdateradTid') / 1000))
    occupation_concept_id = None
    occupation_group_concept_id = None
    occupation_field_concept_id = None
    municipality_concept_id = None
    region_concept_id = None
    country_concept_id = None
    # Fetch filtering details for unpublished ad from Elastic Search...
    ad_from_elastic = elastic.get_ad_by_id(ad_id)
    if ad_from_elastic:
        occupations = ad_from_elastic.get('occupation')
        if occupations:
            for occupation in occupations:
                if occupation.get('original_value', False):
                    occupation_concept_id = occupation.get('concept_id')

        # TODO: this will pick the last value in the list.
        # Normally there's only one value but if multiple there might be mismatch with occupation
        if occupation_fields := ad_from_elastic.get('occupation_field', None):
            for occupation_field in occupation_fields:
                occupation_field_concept_id = occupation_field.get('concept_id', None)
        if occupation_groups := ad_from_elastic.get('occupation_group', None):
            for occupation_group in occupation_groups:
                occupation_group_concept_id = occupation_group.get('concept_id', None)

        workplace_address = ad_from_elastic.get('workplace_address')
        if workplace_address:
            municipality_concept_id = workplace_address.get('municipality_concept_id')
            region_concept_id = workplace_address.get('region_concept_id')
            country_concept_id = workplace_address.get('country_concept_id')
    else:
        log.info(f'Could not fetch details from Elastic for removed ad. Id: {ad_id}')

    return {'annonsId': ad_id,
            'id': ad_id,
            'removed': True,
            'avpublicerad': True,
            'avpubliceringsdatum': removed_date,
            'removed_date': removed_date,
            'uppdateradTid': unpublished_ad_meta.get('uppdateradTid'),
            'updatedAt': unpublished_ad_meta.get('uppdateradTid'),
            'timestamp': unpublished_ad_meta.get('uppdateradTid'),
            'removed_ad_filter': {
                'occupation_concept_id': occupation_concept_id,
                'occupation_field_concept_id': occupation_field_concept_id,
                'occupation_group_concept_id': occupation_group_concept_id,
                'municipality_concept_id': municipality_concept_id,
                'region_concept_id': region_concept_id,
                'country_concept_id': country_concept_id}
            }


def _load_ad_details(ad_meta):
    ad_id = ad_meta['annonsId']
    # If ad is unpublished; handle it as removed...
    
    if ad_meta.get('avpublicerad', False):
        return _load_ad_to_remove(ad_meta)

    detail_url_la = settings.LA_DETAILS_URL + str(ad_id)
    if ad := _get_with_retries(detail_url_la, timeout=settings.LA_ANNONS_TIMEOUT):
        ad['id'] = str(ad['annonsId'])
        ad['updatedAt'] = ad_meta['uppdateradTid']
        ad['expiresAt'] = ad['sistaPubliceringsdatum']
        ad['logo_url'] = _find_correct_logo_url(ad.get('arbetsplatsId'), ad.get('organisationsnummer'), ad_id)
        desensitized_ad = _clean_sensitive_data(ad, detail_url_la)
        clean_ad = _cleanup_string_values(desensitized_ad)
        return clean_ad


def _clean_sensitive_data(ad, detail_url):
    if 'organisationsnummer' in ad and len(ad.get('organisationsnummer', '').strip()) > 9:
        orgnr = ad['organisationsnummer']
        significate_number_position = 4 if len(orgnr) == 12 else 2
        try:
            if int(orgnr[significate_number_position]) < 2:
                ad['organisationsnummer'] = None
        except ValueError:
            ad['organisationsnummer'] = None
            log.error(f"Value error in loader for orgnummer: {orgnr}, ad: {detail_url}. Replacing with None.")
    return ad


def _get_with_retries(url, timeout):
    fail = 0
    fail_max = settings.LA_ANNONS_MAX_TRY
    while True:
        try:
            if settings.LA_CERT_PATH is None:
                # No environment variable - use certificate from certifi package
                ver = True
            else:
                # A certificate name is provided
                ver = settings.LA_CERT_PATH
            r = requests.get(url, timeout=timeout, verify=ver)
            r.raise_for_status()
            return r.json()
        except requests.exceptions.RequestException as e:
            fail += 1
            time.sleep(0.3)
            log.warning(f"Unable to load data from: {url} Exception: {type(e).__name__}, try: {fail}")
            if fail >= fail_max:
                log.error(f"Failed to read from {url} after: {fail_max}.")
                raise e


def load_list_of_updated_ads(timestamp=0):
    feed_url = settings.LA_BOOTSTRAP_FEED_URL if timestamp == 0 else settings.LA_FEED_URL + str(timestamp)
    log.info(f"Loading updates from endpoint: {feed_url}")
    json_result = _get_with_retries(feed_url, timeout=60)
    items = json_result.get('idLista', [])
    return items


def _find_correct_logo_url(workplace_id, org_number, ad_id):
    global logo_cache
    logo_cache_flag = False
    logo_url = None
    cache_key = "%s-%s" % (str(workplace_id), str(org_number))
    if cache_key in logo_cache:
        logo_url = logo_cache.get(cache_key)
        log.debug(f"Ad: {ad_id} Returning cached logo for workplace-orgnr {cache_key}: {logo_url}")
        return logo_url

    try:
        if workplace_id and int(workplace_id) > 0:
            possible_logo_url = "%sarbetsplatser/%s/logotyper/logo.png" % (settings.COMPANY_LOGO_BASE_URL, workplace_id)
            r = requests.head(possible_logo_url, timeout=settings.COMPANY_LOGO_TIMEOUT)
            if r.status_code == 200:
                logo_url = possible_logo_url
                logo_cache_flag = True

        if not logo_url and org_number:
            possible_logo_url = '%sorganisation/%s/logotyper/logo.png' % (settings.COMPANY_LOGO_BASE_URL, org_number)
            r = requests.head(possible_logo_url, timeout=settings.COMPANY_LOGO_TIMEOUT)
            if r.status_code == 200:
                logo_url = possible_logo_url
                logo_cache_flag = True
    except requests.exceptions.RequestException as e:
        log.warning(f"Ad: {ad_id} Unable to load logo: {logo_url} Error message: {type(e).__name__}")

    if logo_cache_flag:
        logo_cache[cache_key] = logo_url

    log.debug(f"Ad: {ad_id} Found logo url for workplace-orgnr {workplace_id}-{org_number}:{logo_url}")
    return logo_url


def _cleanup_string_values(value):
    if isinstance(value, dict):
        value = {_cleanup_string_values(k): _cleanup_string_values(v)
                for k, v in value.items()}
    elif isinstance(value, list):
        value = [_cleanup_string_values(v) for v in value]
    elif isinstance(value, str):
        value = ''.join([i if ord(i) > 0 else '' for i in value])
    return value
