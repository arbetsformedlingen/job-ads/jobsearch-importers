import time
import logging
import sys
import math
from datetime import datetime

from jobtech.common.customlogging import configure_logging
import importers.mappings
from importers import settings
from importers.current_ads import loader, converter
from importers.common import elastic, enricher
from importers.indexmaint.main import set_platsannons_read_alias, set_platsannons_write_alias, \
    check_index_size_before_switching_alias
from importers.common.helpers import grouper
from importers.common.error_handling import ImportFailedException

configure_logging([__name__.split('.')[0], 'importers'])
log = logging.getLogger(__name__)
enriched_ads_to_save = []


def _setup_index(es_index):
    if len(sys.argv) > 1:
        es_index = sys.argv[1]

    try:
        es_index, delete_index = elastic.setup_indices(es_index,
                                                       settings.ES_ANNONS_PREFIX,
                                                       importers.mappings.platsannons_mappings,
                                                       importers.mappings.platsannons_deleted_mappings)
        log.info(f'Starting importer with batch: {settings.BATCH_SIZE} for index: {es_index}')
        log.info(f'Index for removed items: {delete_index}')
    except Exception as e:
        error_msg = f"Elastic operations failed. Exit! {e}"
        log.error(error_msg)
        raise ImportFailedException(error_msg)
    return es_index, delete_index


def _check_last_timestamp(es_index):
    if not settings.LA_LAST_TIMESTAMP_MANUAL:
        last_timestamp = elastic.get_last_timestamp(es_index)
        from_timestamp = datetime.fromtimestamp(last_timestamp / 1000)
        log.info(f"Index: {es_index} Last timestamp: {last_timestamp} ({from_timestamp})")
    else:
        last_timestamp = settings.LA_LAST_TIMESTAMP
        from_timestamp = datetime.fromtimestamp(last_timestamp / 1000)
        log.warning(f"Index: {es_index} Last timestamp set MANUALLY: {last_timestamp} ({from_timestamp})")
    return last_timestamp


def start(es_index=None):
    import_failed_message = "Import failed"
    log.info(f"Start() index name: {es_index}")
    try:
        _start(es_index)
    except ImportFailedException as importFailed:
        # controlled shutdown after some failed check
        log.error(import_failed_message)
        log.error(importFailed)
        sys.exit(1)
    except Exception as e:
        # any other exception will be re-raised
        log.error(import_failed_message)
        log.error(e)
        raise


def _start(es_index=None):
    start_time = time.time()
    if not elastic.get_alias(importers.settings.ES_TAX_INDEX_ALIAS):
        error_msg = f"no index for: {importers.settings.ES_TAX_INDEX_ALIAS}. Exit!"
        log.error(error_msg)
        raise ImportFailedException(error_msg)
    if not settings.LA_FEED_URL:
        error_msg = "LA_FEED_URL is not set. Exit!"
        log.error(error_msg)
        raise ImportFailedException(error_msg)

    # Get, set and create elastic index
    es_index, es_index_deleted = _setup_index(es_index)
    log.info(f"Starting ad import into index: {es_index}")
    log.info(f"Using taxonomy index: {elastic.get_index_name_for_alias(importers.settings.ES_TAX_INDEX_ALIAS)}")
    last_timestamp = _check_last_timestamp(es_index)
    log.info(f"Timestamp to load from: {last_timestamp}")

    # Load list of updated ad ids
    ad_ids = loader.load_list_of_updated_ads(last_timestamp)
    number_total = len(ad_ids)
    number_of_ids_to_load = number_total
    # variable to skip endless loop of fetching missing ads:
    number_of_ids_missing_fix = -1
    while number_of_ids_to_load > 0:
        log.info(f'Fetching details for ads: {number_of_ids_to_load}')
        _load_and_process_ads(ad_ids, es_index, es_index_deleted)
        log.info(f"Verifying that all ads were indexed in: {es_index}")
        ad_ids = _find_missing_ids_and_create_loadinglist(ad_ids, es_index)
        number_of_ids_to_load = len(ad_ids)

        if number_of_ids_missing_fix == number_of_ids_to_load:
            log.error(f"Missing ads amount is same as before: {number_of_ids_to_load}")
            log.error(f"No more trying to fetch. Check these ads: {ad_ids}")
            break
        if number_of_ids_to_load > 0:
            number_of_ids_missing_fix = number_of_ids_to_load
            log.info(f"Missing ads: {ad_ids}")
            log.warning(f"Still missing ads: {number_of_ids_to_load}. Trying again...")
        else:
            log.info("No missing ads to load.")
            break

    elapsed_time = time.time() - start_time
    m, s = divmod(elapsed_time, 60)
    log.info(f"Processed %d docs in: %d minutes %5.2f seconds." % (number_total, m, s))

    num_doc_elastic = elastic.document_count(es_index)
    if num_doc_elastic:
        log.info(f"Index: {es_index} has: {num_doc_elastic} indexed documents.")
    if not check_index_size_before_switching_alias(es_index):
        error_msg = f"Index: {es_index} has: {num_doc_elastic} indexed documents. Exit!"
        log.error(error_msg)
        raise ImportFailedException(error_msg)


def _load_and_process_ads(ad_ids, es_index, es_index_deleted):
    doc_counter = 0
    len_ads = len(ad_ids)
    nr_of_items_per_batch = settings.BATCH_SIZE
    nr_of_items_per_batch = min(nr_of_items_per_batch, len_ads)
    if nr_of_items_per_batch < 1:
        error_msg = "Failed to retrieve any ads. Exit!"
        log.error(error_msg)
        raise ImportFailedException(error_msg)
    nr_of_batches = math.ceil(len_ads / nr_of_items_per_batch)
    # Partition list into manageable chunks
    ad_batches = grouper(nr_of_items_per_batch, ad_ids)
    processed_ads_total = 0

    for i, ad_batch in enumerate(ad_batches):
        log.info(f'Processing batch {i + 1}/{nr_of_batches}')

        # Fetch ads from LA to raw-list
        ad_details = loader.bulk_fetch_ad_details(ad_batch)

        raw_ads = [raw_ad for raw_ad in list(ad_details.values()) if not raw_ad.get('removed', False)]
        doc_counter += len(raw_ads)
        log.info(f'doc_counter=len(raw_ads): {doc_counter}')
        log.debug(f'Fetched batch of ads (id, updatedAt): '
                  f'{", ".join(("(" + str(ad["annonsId"]) + ", " + str(ad["updatedAt"])) + ")" for ad in raw_ads)}')

        _convert_and_save_to_elastic(ad_details.values(), es_index, es_index_deleted)
        processed_ads_total = processed_ads_total + len(ad_batch)

        log.info(f'Processed ads: {processed_ads_total}/{len_ads}')

    return doc_counter


def _convert_and_save_to_elastic(raw_ads, es_index, deleted_index):
    # Loop over raw-list, convert and enrich into cooked-list
    log.info(f"Converting: {len(raw_ads)} ads to proper format ...")
    converted_ads = [converter.convert_ad(raw_ad) for raw_ad in raw_ads]
    log.info("Enriching ads with ML ...")
    enriched_ads = enricher.enrich(converted_ads)
    log.info(f"Indexing: {len(enriched_ads)} enriched documents into: {es_index}")
    # Bulk save cooked-list to elastic
    num_indexed = elastic.bulk_index(enriched_ads, es_index, deleted_index)
    return num_indexed


def _find_missing_ids_and_create_loadinglist(ad_ids, es_index):
    id_lookup = {str(a['annonsId']): a for a in ad_ids if not a['avpublicerad']}
    loaded_ids = [str(a['annonsId']) for a in ad_ids if not a['avpublicerad']]
    missing_ids = elastic.find_missing_ad_ids(loaded_ids, es_index)
    return [id_lookup[missing_id] for missing_id in missing_ids]


def start_daily_index():
    new_index_name = f"{settings.ES_ANNONS_PREFIX}-{settings.INDEX_TIMESTAMP}"
    log.info(f"Start creating new daily index: {new_index_name}")
    log.info(f"Using taxonomy index: {elastic.get_index_name_for_alias(importers.settings.ES_TAX_INDEX_ALIAS)}")
    start(new_index_name)
    set_platsannons_read_alias(new_index_name)
    set_platsannons_write_alias(new_index_name)
    log.info(f"Switching alias to new index completed successfully: {new_index_name}")


if __name__ == '__main__':
    configure_logging([__name__, 'importers'])
    log = logging.getLogger(__name__)
    start_daily_index()
