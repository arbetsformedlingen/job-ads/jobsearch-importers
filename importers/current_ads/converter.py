import logging
import time
from tokenize import String

import jmespath
from elasticsearch.exceptions import RequestError

from importers.common import taxonomy
from importers.common.helpers import clean_html, isodate, get_null_safe_value
from importers.common.keywords_extracted import add_keywords_extracted
from importers.common.language_detection import detect_language_in_ad
from importers.common.phrase_checks import all_phrase_checks

logging.basicConfig()
logging.getLogger(__name__).setLevel(logging.INFO)
log = logging.getLogger(__name__)

MUST_HAVE_WEIGHT = 10
NICE_TO_HAVE_WEIGHT = 5

"""
Naming conventions:
annons : from Ledigt Arbete, dictionary keys in Swedish
ad : after conversion to JobSearch format
"""


def convert_ad(annons):
    ad = dict()
    start_time = int(time.time() * 1000)
    if 'removed_ad_filter' in annons:
        ad['removed_ad_filter'] = annons.get('removed_ad_filter')

    ad['id'] = annons.get('annonsId')
    ad['external_id'] = annons.get('externtAnnonsId')
    ad['headline'] = annons.get('annonsrubrik')
    ad['application_deadline'] = isodate(annons.get('sistaAnsokningsdatum'))
    ad['number_of_vacancies'] = annons.get('antalPlatser')
    labels = []
    # Map labels and make them lowercase...
    if annons.get("nyckelord"):
        if isinstance(annons.get("nyckelord"), list):
            for nyckelord in annons['nyckelord']:
                labels.append(nyckelord.lower())
        if isinstance(annons.get("nyckelord"), str):
            labels.append(annons['nyckelord'].lower())
    ad['label'] = labels

    cleaned_description_text = clean_html(annons.get('annonstextFormaterad'))
    if cleaned_description_text == '' and not annons.get('avpublicerad'):
        log.debug(f"description.text is empty for ad id: {ad['id']}")

    ad = all_phrase_checks(ad, cleaned_description_text, ad['headline'])

    ad['description'] = {
        'text': cleaned_description_text,
        'text_formatted': annons.get('annonstextFormaterad'),
        'company_information': annons.get('ftgInfo'),
        'needs': annons.get('beskrivningBehov'),
        'requirements': annons.get('beskrivningKrav'),
        'conditions': annons.get('villkorsbeskrivning'),
    }
    ad['detected_language'] = detect_language_in_ad(ad)

    ad['employment_type'] = _expand_taxonomy_value_and_replaced('anstallningstyp', 'anstallningTyp', annons)
    ad['salary_type'] = _expand_taxonomy_value('lonetyp', 'lonTyp', annons)
    ad['salary_description'] = annons.get('lonebeskrivning')
    ad['duration'] = _expand_taxonomy_value('varaktighet', 'varaktighetTyp', annons)
    ad['working_hours_type'] = _expand_taxonomy_value('arbetstidstyp', 'arbetstidTyp', annons)
    default_min_omf, default_max_omf = _get_default_scope_of_work(annons.get('arbetstidTyp', {}).get('varde'))
    ad['scope_of_work'] = {
        'min': annons.get('arbetstidOmfattningFran', default_min_omf),
        'max': annons.get('arbetstidOmfattningTill', default_max_omf)
    }
    ad['access'] = annons.get('tilltrade')
    ad['employer'] = {
        'phone_number': annons.get('telefonnummer'),
        'email': annons.get('epost'),
        'url': annons.get('webbadress'),
        'organization_number': annons.get('organisationsnummer'),
        'name': annons.get('arbetsgivareNamn'),
        'workplace': annons.get('arbetsplatsNamn'),
        'workplace_id': annons.get('arbetsplatsId')
    }
    ad['application_details'] = {
        'information': annons.get('informationAnsokningssatt'),
        'reference': annons.get('referens'),
        'email': annons.get('ansokningssattEpost'),
        'via_af': annons.get('ansokningssattViaAF'),
        'url': annons.get('ansokningssattWebbadress'),
        'other': annons.get('ansokningssattAnnatSatt')
    }
    ad['experience_required'] = not annons.get('ingenErfarenhetKravs', False)
    ad['access_to_own_car'] = annons.get('tillgangTillEgenBil', False)
    if annons.get('korkort', []):
        ad['driving_license_required'] = True
        ad['driving_license'] = parse_driving_licence(annons)
    else:
        ad['driving_license_required'] = False

    _set_occupations(ad, annons)

    ad['workplace_address'] = _build_workplace_address(annons.get('arbetsplatsadress', {}))

    ad['must_have'] = {
        'skills': [skill for kompetens in annons.get('kompetenser', [])
                   for skill in _get_concept_and_replaced_with_weight(
                'kompetens', kompetens.get('varde'), kompetens.get('vikt'))
                   if get_null_safe_value(kompetens, 'vikt', 0) >= MUST_HAVE_WEIGHT
                   ],
        'languages': [
            _get_concept_as_annons_value_with_weight('sprak', sprak.get('varde'),
                                                     sprak.get('vikt'))
            for sprak in annons.get('sprak', [])
            if get_null_safe_value(sprak, 'vikt', 0) >= MUST_HAVE_WEIGHT
        ],
        'work_experiences': [
            _get_concept_as_annons_value_with_weight('yrkesroll', yrkerf.get('varde'), yrkerf.get('vikt'))
            for yrkerf in annons.get('yrkeserfarenheter', [])
            if get_null_safe_value(yrkerf, 'vikt', 0) >= MUST_HAVE_WEIGHT
        ],
        'education': [],
        'education_level': [],
    }

    ad['nice_to_have'] = {
        'skills': [skill for kompetens in annons.get('kompetenser', [])
                   for skill in _get_concept_and_replaced_with_weight(
                'kompetens', kompetens.get('varde'), kompetens.get('vikt'))
                   if get_null_safe_value(kompetens, 'vikt', NICE_TO_HAVE_WEIGHT) < MUST_HAVE_WEIGHT
                   ],
        'languages': [
            _get_concept_as_annons_value_with_weight('sprak', sprak.get('varde'), sprak.get('vikt'))
            for sprak in annons.get('sprak', [])
            if get_null_safe_value(sprak, 'vikt', NICE_TO_HAVE_WEIGHT) < MUST_HAVE_WEIGHT
        ],
        'work_experiences': [
            _get_concept_as_annons_value_with_weight('yrkesroll', yrkerf.get('varde'), yrkerf.get('vikt'))
            for yrkerf in annons.get('yrkeserfarenheter', [])
            if get_null_safe_value(yrkerf, 'vikt', NICE_TO_HAVE_WEIGHT) < MUST_HAVE_WEIGHT
        ],
        'education': [],
        'education_level': [],
    }

    ad['application_contacts'] = build_contacts(annons.get('kontaktpersoner', []), ad['id'])

    if annons.get('utbildningsinriktning'):
        try:
            _set_education(ad, annons)
        except TypeError as e:
            log.debug(f"Skipping education on ad: {ad['id']} due to TypeError: {e}")

    ad['publication_date'] = isodate(annons.get('publiceringsdatum'))
    ad['last_publication_date'] = isodate(annons.get('sistaPubliceringsdatum'))
    ad['removed'] = annons.get('avpublicerad')
    ad['removed_date'] = isodate(annons.get('avpubliceringsdatum'))
    ad['source_type'] = annons.get('kallaTyp')
    ad['timestamp'] = annons.get('updatedAt')
    ad['logo_url'] = annons.get('logo_url')
    # Extract labels as keywords for easier searching
    log.debug(f"Convert ad: {ad['id']} took: {int(time.time() * 1000) - start_time} ms")
    return add_keywords_extracted(ad)


def _set_education(ad, annons):
    if get_null_safe_value(annons.get('utbildningsinriktning'), 'vikt', 0) >= MUST_HAVE_WEIGHT:
        ad['must_have']['education'] = _get_education(annons)
        ad['must_have']['education_level'] = _get_education_level(annons)
    else:
        ad['nice_to_have']['education'] = _get_education(annons)
        ad['nice_to_have']['education_level'] = _get_education_level(annons)


def _get_education_level(annons):
    return [_get_concept_as_annons_value_with_weight(
        ['sun-education-level-1', 'sun-education-level-2', 'sun-education-level-3'],
        annons.get('utbildningsniva', {}).get('varde'),
        annons.get('utbildningsniva', {}).get('vikt'))]


def _get_education(annons):
    return [
        _get_concept_as_annons_value_with_weight(
            ['sun-education-field-1', 'sun-education-field-2', 'sun-education-field-3'],
            annons.get('utbildningsinriktning', {}).get('varde'),
            annons.get('utbildningsinriktning', {}).get('vikt'))]


# TODO: unit tests, does it handle new concept ids?
def _get_default_scope_of_work(work_time_type):
    default_max_omf = None
    default_min_omf = None

    full_time = "6YE1_gAC_R2G"
    part_time = "947z_JGS_Uk2"
    part_or_full_time = "hGCt_6Ni_XPz"

    if work_time_type == full_time:
        default_min_omf = 100
        default_max_omf = 100
    elif work_time_type == part_time or work_time_type == part_or_full_time:
        default_min_omf = 0
        default_max_omf = 100
    elif work_time_type:
        log.debug(f"Unknown working_hours_type (arbetstidTyp): {work_time_type}")

    return default_min_omf, default_max_omf


def is_union_representative(contact_person):
    """
    if a contact person is a union representative can be determined in two ways:
    1. boolean value for field 'fackligRepresentant'
    2. text 'facklig representant in field 'beskrivning' or any other field
    """
    if contact_person.get('fackligRepresentant'):
        return True

    # check all fields for matching string just to be sure, even if 'fackligRepresentant' is not True
    check_for = 'facklig representant'
    for key, value in contact_person.items():
        if value and not isinstance(value, bool):
            if check_for in value.lower():
                return True
    return False


def build_contacts(kontaktpersoner, ad_id='1'):
    appl_contact_list = []
    for kontaktperson in kontaktpersoner:
        if not is_union_representative(kontaktperson):
            appl_contact = {}
            name = f"{get_null_safe_value(kontaktperson, 'fornamn', '')} {get_null_safe_value(kontaktperson, 'efternamn', '')}"
            appl_contact['name'] = name.strip() or None
            appl_contact['description'] = kontaktperson.get('beskrivning')
            appl_contact['email'] = kontaktperson.get('epost')
            phone_numbers = f"{get_null_safe_value(kontaktperson, 'telefonnummer', '')}, {get_null_safe_value(kontaktperson, 'mobilnummer', '')}"
            appl_contact['telephone'] = phone_numbers.strip(', ') or None
            appl_contact['contactType'] = kontaktperson.get('befattning')
            appl_contact_list.append(appl_contact)
        else:
            log.debug(f'Found union person in id: {ad_id}')

    return appl_contact_list


def _set_occupations(ad, annons):
    if 'yrkesroll' in annons:
        yrkesroll = taxonomy.get_legacy_by_concept_id('yrkesroll', annons.get('yrkesroll', {}).get('varde'))

        if yrkesroll and 'parent' in yrkesroll:
            yrkesgrupp = yrkesroll.get('parent', {})
            yrkesomrade = yrkesgrupp.get('parent', {})
            yrkesroller_replaced = yrkesroll.get('replaced_by', [])
            yrkesroller_replaces = yrkesroll.get('replaces', [])
            collections = yrkesroll.get('collections', {})
            ad['occupation'] = [{'concept_id': yrkesroll.get('concept_id', None),
                                 'label': annons.get('yrkesroll', {}).get('namn'),
                                 'legacy_ams_taxonomy_id': yrkesroll.get('legacy_ams_taxonomy_id', None),
                                 'original_value': True}]
            # changed occupation name while same concept_id
            if yrkesroll.get('label', None) != annons.get('yrkesroll', {}).get('namn'):
                ad['occupation'] += [{'concept_id': yrkesroll.get('concept_id', None),
                                      'label': yrkesroll.get('label', None),
                                      'legacy_ams_taxonomy_id': yrkesroll.get('legacy_ams_taxonomy_id', None)}]
            if yrkesroller_replaced:
                ad['occupation'] += [{'concept_id': yrkesroll_replaced.get('concept_id', None),
                                      'label': yrkesroll_replaced.get('label', None),
                                      'legacy_ams_taxonomy_id': yrkesroll_replaced.get('legacy_ams_taxonomy_id',
                                                                                       None)}
                                     for yrkesroll_replaced in yrkesroller_replaced]
            if yrkesroller_replaces:
                ad['occupation'] += [{'concept_id': yrkesroll_replaces.get('concept_id', None),
                                      'label': yrkesroll_replaces.get('label', None),
                                      'legacy_ams_taxonomy_id': yrkesroll_replaces.get('legacy_ams_taxonomy_id',
                                                                                       None)}
                                     for yrkesroll_replaces in yrkesroller_replaces]

            ad['occupation_group'] = [{'concept_id': yrkesgrupp.get('concept_id', None),
                                       'label': yrkesgrupp.get('label', None),
                                       'legacy_ams_taxonomy_id':
                                           yrkesgrupp.get('legacy_ams_taxonomy_id', None)}]
            ad['occupation_field'] = [{'concept_id': yrkesomrade.get('concept_id', None),
                                       'label': yrkesomrade.get('label', None),
                                       'legacy_ams_taxonomy_id':
                                           yrkesomrade.get('legacy_ams_taxonomy_id', None)}]
            ad['collections'] = [{'concept_id': collection.get('concept_id', None),
                                  'label': collection.get('label', None),
                                  'legacy_ams_taxonomy_id': collection.get('legacy_ams_taxonomy_id', None)}
                                 for collection in collections]
        elif not yrkesroll:
            log.debug(f"Taxonomy value not found for: {annons['yrkesroll']}, ad: {ad['id']}")
        else:  # yrkesroll is not None and 'parent' not in yrkesroll
            log.debug(f"Parent not found for yrkesroll: {annons['yrkesroll']}, ({yrkesroll}), ad: {ad['id']}")
    else:
        log.debug(f"Word 'yrkesroll' is not found in: {annons}, ad: {ad['id']}")


def _build_workplace_address(arbplats_message):
    kommun = None
    lansnamn = None
    kommun_concept_id = None
    kommun_legacy_id = None
    land = None
    lan_legacy_id = None
    lan_concept_id = None
    land_legacy_id = None
    land_concept_id = None
    longitud = None
    latitud = None
    if jmespath.search('kommun', arbplats_message):
        kommun_concept_id = arbplats_message.get('kommun', {}).get('varde', {})
        kommun_temp = taxonomy.get_legacy_by_concept_id('kommun', kommun_concept_id)
        kommun_legacy_id = kommun_temp.get('legacy_ams_taxonomy_id', None)
        kommun = arbplats_message.get('kommun', {}).get('namn', {})
    if jmespath.search('lan.varde', arbplats_message):
        lan_concept_id = arbplats_message.get('lan', {}).get('varde', {})
        lan_temp = taxonomy.get_legacy_by_concept_id('lan', lan_concept_id)
        lan_legacy_id = lan_temp.get('legacy_ams_taxonomy_id', None)
        lansnamn = arbplats_message.get('lan', {}).get('namn', {})
    if jmespath.search('land', arbplats_message):
        land_concept_id = arbplats_message.get('land', {}).get('varde', {})
        land_temp = taxonomy.get_legacy_by_concept_id('land', land_concept_id)
        land_legacy_id = land_temp.get('legacy_ams_taxonomy_id', None)
        land = arbplats_message.get('land', {}).get('namn', {})
    if jmespath.search('longitud', arbplats_message):
        longitud = float(arbplats_message.get('longitud'))
    if jmespath.search('latitud', arbplats_message):
        latitud = float(arbplats_message.get('latitud'))

    return {
        'municipality_code': kommun_legacy_id,
        'municipality_concept_id': kommun_concept_id,
        'municipality': kommun,
        'region_code': lan_legacy_id,
        'region_concept_id': lan_concept_id,
        'region': lansnamn,
        'country_code': land_legacy_id or "199",
        'country_concept_id': land_concept_id or "i46j_HmG_v64",
        'country': land or "Sverige",
        'street_address': arbplats_message.get('gatuadress', ''),
        'postcode': arbplats_message.get('postnr'),
        'city': arbplats_message.get('postort'),
        'coordinates': [longitud, latitud]
    }


def _expand_taxonomy_value(ad_key, annons_key, message_dict):
    message_value = message_dict.get(annons_key, {}).get('varde') if message_dict else None
    if message_value:
        if concept := taxonomy.get_legacy_by_concept_id(ad_key, message_value):
            return {
                "concept_id": concept.get('concept_id', None),
                "label": concept.get('label', None),
                "legacy_ams_taxonomy_id": concept.get('legacy_ams_taxonomy_id', None)
            }
        else:
            log.debug(f"(expand_taxonomy_value)Empty message_key: {message_value} Type: {ad_key} Key:{annons_key},"
                      f" Id: {message_dict.get('annonsId') if message_dict else None}")
            return None
    else:
        ads_id = message_dict.get('annonsId') if message_dict else None
        log.debug(
            f"(expand_taxonomy_value)Empty message_key: {message_value} Type: {ad_key} Key:{annons_key} Id: {ads_id} ")
    return None


def _expand_taxonomy_value_and_replaced(ad_key, annons_key, message_dict):
    message_value = message_dict.get(annons_key, {}).get('varde') if message_dict else None
    values = []
    if message_value:
        concept = taxonomy.get_legacy_by_concept_id(ad_key, message_value)
        if concept:
            values.append({
                "concept_id": concept.get('concept_id', None),
                "label": concept.get('label', None),
                "legacy_ams_taxonomy_id": concept.get('legacy_ams_taxonomy_id', None),
                "original_value": True
            })
            if replaced_values := concept.get('replaced_by', []):
                values += [{"concept_id": replaced_value.get('concept_id', None),
                            "label": replaced_value.get('label', None),
                            "legacy_ams_taxonomy_id": replaced_value.get('legacy_ams_taxonomy_id', None)}
                           for replaced_value in replaced_values]
    return values


def _get_concept_as_annons_value_with_weight(taxonomy_type, ad_id, weight=None):
    weighted_concept = {
        'concept_id': None,
        'label': None,
        'weight': None,
        'legacy_ams_taxonomy_id': None
    }
    try:
        concept = taxonomy.get_legacy_by_concept_id(taxonomy_type, ad_id)
        weighted_concept['concept_id'] = concept.get('concept_id', None)
        weighted_concept['label'] = concept.get('label', None)
        weighted_concept['weight'] = weight
        weighted_concept['legacy_ams_taxonomy_id'] = concept.get('legacy_ams_taxonomy_id', None)
    except AttributeError:
        log.debug(f'Taxonomy (3) value not found for {taxonomy_type} {ad_id}')
    except RequestError:
        log.warning(f'Request failed with argtype: {taxonomy_type} and legacy_id or concept_id: {ad_id}')
    return weighted_concept


def _get_concept_and_replaced_with_weight(taxonomy_type, ad_id, weight=None):
    concepts = []
    weighted_concept = {
        'concept_id': None,
        'label': None,
        'weight': None,
        'legacy_ams_taxonomy_id': None,
    }
    try:
        concept = taxonomy.get_legacy_by_concept_id(taxonomy_type, ad_id)
        weighted_concept['concept_id'] = concept.get('concept_id', None)
        weighted_concept['label'] = concept.get('label', None)
        weighted_concept['weight'] = weight
        weighted_concept['legacy_ams_taxonomy_id'] = concept.get('legacy_ams_taxonomy_id', None)
        weighted_concept['original_value'] = True
        concepts.append(weighted_concept)
        replaced_concepts = concept.get('replaced_by', [])
        if replaced_concepts:
            concepts += [{'concept_id': replaced_concept.get('concept_id', None),
                          'label': replaced_concept.get('label', None),
                          'weight': weight,
                          'legacy_ams_taxonomy_id': replaced_concept.get('label', None)}
                         for replaced_concept in replaced_concepts]
    except AttributeError:
        log.debug(f'Taxonomy value not found for type: {taxonomy_type}, id: {ad_id}')
    except RequestError:
        log.warning(f'Request failed with argtype: {taxonomy_type} and legacy_id or concept_id: {ad_id}')
    return concepts


def parse_driving_licence(annons):
    taxonomy_for_driving_license = []
    for korkort in annons.get('korkort'):
        if taxonomy_id_for_dl := taxonomy.get_legacy_by_concept_id('korkort', korkort['varde']):
            taxonomy_for_driving_license.append({
                "concept_id": taxonomy_id_for_dl.get('concept_id', None),
                "legacy_ams_taxonomy_id": taxonomy_id_for_dl.get('legacy_ams_taxonomy_id', None),
                "label": taxonomy_id_for_dl.get('label', None)
            })
    return taxonomy_for_driving_license
