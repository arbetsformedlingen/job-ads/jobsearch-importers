import os
from datetime import datetime

VERSION = '1.17.0'

ES_HOST = os.getenv("ES_HOST", "127.0.0.1")
ES_PORT = os.getenv("ES_PORT", 9200)
ES_USER = os.getenv("ES_USER")
ES_PWD = os.getenv("ES_PWD")
ES_USE_SSL = os.getenv("ES_USE_SSL", "TRUE").upper()
ES_VERIFY_SSL = os.getenv("ES_VERIFY_SSL", "TRUE").upper()

LA_CERT_PATH = os.getenv("LA_CERT_PATH", None)

# For platsannonser
WRITE_INDEX_SUFFIX = '-write'
READ_INDEX_SUFFIX = '-read'
DELETED_INDEX_SUFFIX = '-deleted'
STREAM_INDEX_SUFFIX = '-stream'
ES_ANNONS_PREFIX = os.getenv('ES_ANNONS_INDEX', os.getenv('ES_ANNONS', 'platsannons'))
ES_ANNONS_INDEX = "%s%s" % (ES_ANNONS_PREFIX, WRITE_INDEX_SUFFIX)
ES_HISTORICAL_INDEX_PREFIX = os.getenv('ES_HISTORICAL_INDEX_PREFIX', 'historical')

INDEX_TIMESTAMP = datetime.now().strftime('%Y%m%d-%H%M')

# don't switch alias to new index if number of ads in new index < current index * coefficient
NEW_ADS_COEF = float(os.getenv('NEW_ADS_COEF', 0.8))

# legacy name starts with PG
BATCH_SIZE = int(os.getenv('PG_BATCH_SIZE', 2000))

LA_FEED_URL = os.getenv('LA_FEED_URL')
LA_BOOTSTRAP_FEED_URL = os.getenv('LA_BOOTSTRAP_FEED_URL')
LA_DETAILS_URL = os.getenv('LA_DETAILS_URL')
LA_DETAILS_PARALLELISM = int(os.getenv('LA_DETAILS_PARALLELISM', 8))
LA_ANNONS_MAX_TRY = int(os.getenv('LA_ANNONS_MAX_TRY', 10))
LA_ANNONS_TIMEOUT = int(os.getenv('LA_ANNONS_TIMEOUT', 10))
LA_LAST_TIMESTAMP_MANUAL = os.getenv('LA_LAST_TIMESTAMP_MANUAL', 'false').lower() == 'true'
LA_LAST_TIMESTAMP = int(os.getenv('LA_LAST_TIMESTAMP', 0))

URL_ENRICH_TEXTDOCS_SERVICE = os.getenv('URL_ENRICH_TEXTDOCS_SERVICE', '')
ENRICH_THRESHOLD_OCCUPATION = os.getenv('ENRICH_THRESHOLD_OCCUPATION', 0.8)
ENRICH_THRESHOLD_SKILL = os.getenv('ENRICH_THRESHOLD_SKILL', 0.5)
ENRICH_THRESHOLD_GEO = os.getenv('ENRICH_THRESHOLD_GEO', 0.7)
ENRICH_THRESHOLD_TRAIT = os.getenv('ENRICH_THRESHOLD_TRAIT', 0.5)
ENRICHER_PARAM_DOC_ID = 'doc_id'
ENRICHER_PARAM_DOC_HEADLINE = 'doc_headline'
ENRICHER_PARAM_DOC_TEXT = 'doc_text'
ENRICHER_PROCESSES = int(os.getenv("ENRICHER_PROCESSES", 8))
ENRICHER_BATCH_SIZE = int(os.getenv("ENRICHER_BATCH_SIZE", 100))

COMPANY_LOGO_BASE_URL = os.getenv('COMPANY_LOGO_BASE_URL',
                                  'https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/')
COMPANY_LOGO_TIMEOUT = int(os.getenv('COMPANY_LOGO_TIMEOUT', 10))

TAXONOMY_URL = os.getenv('TAXONOMY_URL', "https://taxonomy.api.jobtechdev.se/v1/taxonomy")
TAXONOMY_GRAPHQL_URL = f"{TAXONOMY_URL}/graphql?"
TAXONOMY_VERSION_URL = f"{TAXONOMY_URL}/main/versions"
ES_TAX_INDEX_ALIAS = os.getenv("ES_TAX_INDEX_ALIAS", "taxonomy")

# Use TAXONOMY_VERSION = 0 for latest version
TAXONOMY_VERSION = int(os.getenv('TAXONOMY_VERSION', 21))

HISTORICAL_PREFIX = os.getenv('HISTORICAL_PREFIX', 'historical')
HISTORICAL_SUFFIX = os.getenv('HISTORICAL_SUFFIX', '')
HISTORICAL_ALIAS = os.getenv('HISTORICAL_ALIAS', 'historical-ads')
HISTORICAL_CHUNK_SIZE = int(os.getenv('HISTORICAL_CHUNK_SIZE', 50000))
HISTORICAL_TIMEOUT = int(os.getenv('HISTORICAL_TIMEOUT', 60))

ENVIRONMENT=os.getenv("ENVIRONMENT", "dev")
