import logging
import json
import os
import queue
import threading
from typing import TextIO

from importers import settings

log = logging.getLogger(__name__)


class ProcessJsonLineFile:
    """
    Process each row in a jsonline file and write the result to an output file.
    The processing is done in parallel.
    The jobs send to the processor is grouped into batches.
    """

    in_queue = queue.Queue()
    out_queue = queue.Queue()

    def __init__(self,
                 processes=settings.ENRICHER_PROCESSES,
                 batch_size=settings.ENRICHER_BATCH_SIZE):
        self.processes = processes
        self.batch_size = batch_size

    def process_file(self, input_file, output_file, process_fn) -> None:
        log.info(f'processing data in {self.processes} processes')
        # Create one thread for each processor.
        for p in range(self.processes):
            threading.Thread(target=self._get_processor(process_fn), daemon=True).start()
        # Create one thread to write result to file
        threading.Thread(target=self._get_output_writer(output_file), daemon=True).start()

        self._read_file_to_queue(input_file)

        # Wait for all job to be done.
        self.in_queue.join()
        self.out_queue.join()

    def _read_file_to_queue(self, input_file: TextIO) -> None:
        c = 0
        records = []
        while line := input_file.readline():
            c += 1
            records.append(json.loads(line))
            if c >= self.batch_size:
                self.in_queue.put(records)
                c = 0
                records = []
        if c != 0:
            self.in_queue.put(records)

    def _get_processor(self, process_fn):
        in_q = self.in_queue
        out_q = self.out_queue

        def process():
            try:
                while True:
                    workload = in_q.get()
                    result = process_fn(workload)
                    out_q.put(result)
                    in_q.task_done()
            except BaseException as e:
                log.error("Processing failed permanently, aborting:", e)
                os._exit(1)  # This is a bit ugly, but cleanest way to quit program in daemon thread.
        return process

    def _get_output_writer(self, output_file: TextIO):
        q = self.out_queue

        def write_output():
            c = 0
            try:
                while True:
                    records = q.get()
                    for r in records:
                        c += 1
                        sorted_r = dict(sorted(r.items()))
                        json.dump(sorted_r, output_file, ensure_ascii=False)
                        output_file.write('\n')
                        if c % 1000 == 0:
                            log.info(f'Documents processed: {c}')
                    q.task_done()
            except BaseException as e:
                log.error("Writing result failed permanently, aborting:", e)
                os._exit(2)  # This is a bit ugly, but cleanest way to quit program in daemon thread.
        return write_output
