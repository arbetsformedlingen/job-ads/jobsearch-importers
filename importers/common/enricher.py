import concurrent.futures
import logging
import math
import os
import time
from multiprocessing import Value

import jmespath
import requests

from importers import settings
from importers.common.error_handling import ImportFailedException
from importers.common.helpers import grouper, get_null_safe_value
from importers.common.thread_processor import ProcessJsonLineFile

log = logging.getLogger(__name__)

currentdir = os.path.dirname(os.path.realpath(__file__)) + '/'
filepath_english_stopwords = f'{currentdir}../resources/english_stopwords.txt'
filepath_english_stopwords_nltk = f'{currentdir}../resources/english_stopwords_nltk.txt'

TIMEOUT_ENRICH_API = 90
counter = None
RETRIES = 10

DETECTED_LANGUAGE_ENG = 'en'


def _read_stopwords_file(filepath):
    with open(filepath) as f:
        lines = [line.strip() for line in f]

    return lines


def _load_english_stopwords():
    return set(_read_stopwords_file(filepath_english_stopwords) + _read_stopwords_file(filepath_english_stopwords_nltk))


english_stopwords = _load_english_stopwords()


def enrich(ads, typeahead=True, historical=False):
    ads_size = len(ads)
    parallelism = settings.ENRICHER_PROCESSES if ads_size > 100 else 1
    log.info(
        f'Enriching docs: {ads_size}. Number of processes: {parallelism}. Calling: {settings.URL_ENRICH_TEXTDOCS_SERVICE} ')

    global counter
    counter = Value('i', 0)

    # Prepare input
    enrich_input = _prepare_enrich_input(ads, historical)

    nr_of_items_per_batch = ads_size // parallelism
    nr_of_items_per_batch = int(math.ceil(nr_of_items_per_batch / 1000.0)) * 1000
    nr_of_items_per_batch = min(nr_of_items_per_batch, ads_size, settings.ENRICHER_BATCH_SIZE)
    if nr_of_items_per_batch == 0:
        nr_of_items_per_batch = ads_size
    log.info(f'Items per batch: {nr_of_items_per_batch}')

    ads_batches = grouper(nr_of_items_per_batch, enrich_input)

    batch_indatas = []
    for i, ads_batch in enumerate(ads_batches):
        ad_batch_input = [annons_indata for annons_indata in ads_batch]
        batch_indata = {
            "include_terms_info": True,
            "include_sentences": True,
            "documents_input": ad_batch_input
        }
        batch_indatas.append(batch_indata)

    enriched_results = _execute_calls(batch_indatas, parallelism)
    log.info(f"Enriched docs: {len(enriched_results)} / {ads_size}")
    log.info('Typeahead terms will be not enriched!') if not typeahead else None
    for ad in ads:
        doc_id = str(ad.get('id', ''))
        if doc_id in enriched_results:
            enriched_output = enriched_results[doc_id]
            _save_enriched_values_in_ad(ad, enriched_output, typeahead)

    return ads


def enrich_file(input_filename: str, output_filename: str, typeahead=True, historical=False):
    """
    Enrich a file with jsonl jobads and write it to another file.
    """
    process_controller = ProcessJsonLineFile()
    with open(output_filename, "w", encoding='utf-8') as outfile:
        with open(input_filename, encoding='utf-8') as infile:
            process_controller.process_file(infile, outfile, _get_enrich_processor(typeahead, historical))


def _get_enrich_processor(typeahead=True, historical=False, timeout=TIMEOUT_ENRICH_API):
    def enrich_processor(ads: list[dict]) -> list[dict]:
        # Prepare input to enrichment
        enrich_input = _prepare_enrich_input(ads, historical)
        batch_indata = {
            "include_terms_info": True,
            "include_sentences": True,
            "documents_input": enrich_input
        }
        # Do enrichment
        enriched_result = _get_enrich_result(batch_indata, timeout)

        # Merge enrichment result with ads.
        enriched_output = {}
        for result_row in enriched_result:
            enriched_output[result_row['doc_id']] = result_row

        # Match result with indata
        for ad in ads:
            doc_id = str(ad.get('id', ''))
            if doc_id in enriched_output:
                _save_enriched_values_in_ad(ad, enriched_output[doc_id], typeahead)
        return ads

    return enrich_processor


def _prepare_enrich_input(ads, historical=False):
    ad_input = []
    for ad in ads:
        if historical or not ad.get('removed_ad_filter', False):
            doc_id = str(ad.get('id', ''))
            doc_headline = _get_doc_headline_input(ad)
            doc_text = jmespath.search('description.text_formatted', ad)
            if not doc_text:
                doc_text = jmespath.search('description.text', ad)

            if not doc_text:
                log.debug(f"No enrich - empty description for id: {doc_id}, moving on to the next one.")
                continue
            if doc_id == '':
                log.error(f"Value error - no id, enrichment is not possible. Headline: {doc_headline}")
                raise ValueError

            input_doc_params = {
                settings.ENRICHER_PARAM_DOC_ID: doc_id,
                settings.ENRICHER_PARAM_DOC_HEADLINE: doc_headline,
                settings.ENRICHER_PARAM_DOC_TEXT: doc_text
            }

            ad_input.append(input_doc_params)
    return ad_input


def _get_doc_headline_input(annons):
    sep = ' | '
    # Add occupation from structured data in headline.
    doc_headline_occupation = sep.join(
        [get_null_safe_value(occupation, 'label', '') for occupation in annons.get('occupation', [])])
    occupation_group = annons.get('occupation_group', {})
    if not occupation_group:
        log.error(f"No occupation group: {occupation_group} for ad: {annons.get('id')}")

    if not doc_headline_occupation:
        doc_headline_occupation = ''
    log.debug(f"enriched headline occupation: {doc_headline_occupation} for ad: {annons.get('id')}")

    wp_address_node = annons.get('workplace_address', {})
    wp_address_input = ''
    if wp_address_node:
        wp_city = get_null_safe_value(wp_address_node, 'city', '')
        wp_municipality = get_null_safe_value(wp_address_node, 'municipality', '')
        wp_region = get_null_safe_value(wp_address_node, 'region', '')
        wp_country = get_null_safe_value(wp_address_node, 'country', '')
        wp_address_input = wp_city + sep + wp_municipality + sep + wp_region + sep + wp_country

    doc_headline = get_null_safe_value(annons, 'headline', '')

    doc_headline_input = wp_address_input + sep + doc_headline_occupation + sep + doc_headline

    return doc_headline_input


def _get_enrich_result(batch_indata, timeout):
    headers = {'Content-Type': 'application/json'}
    for retry in range(RETRIES):
        try:
            r = requests.post(url=settings.URL_ENRICH_TEXTDOCS_SERVICE, headers=headers, json=batch_indata,
                              timeout=timeout)
            r.raise_for_status()
        except Exception as e:
            log.warning(f"get_enrich_result() retrying #{retry + 1} after error: {e}")
            time.sleep(retry)  # longer and longer sleep
        else:  # no error
            return r.json()
    error_msg = f"_get_enrich_result failed after: {RETRIES} retries with error. Exit!"
    log.error(error_msg)
    raise ImportFailedException(error_msg)


def _execute_calls(batch_indatas, parallelism):
    global counter
    enriched_output = {}
    # We can use a with statement to ensure threads are cleaned up promptly
    with concurrent.futures.ThreadPoolExecutor(max_workers=parallelism) as executor:
        # Start the load operations and mark each future with its URL
        future_to_enrich_result = {executor.submit(_get_enrich_result, batch_indata,
                                                   TIMEOUT_ENRICH_API): batch_indata
                                   for batch_indata in batch_indatas}
        for future in concurrent.futures.as_completed(future_to_enrich_result):
            try:
                enriched_result = future.result()
                for result_row in enriched_result:
                    enriched_output[result_row[settings.ENRICHER_PARAM_DOC_ID]] = result_row
                    # += operation is not atomic, so we need to get a lock:
                    with counter.get_lock():
                        counter.value += 1
                        if counter.value % 1000 == 0:
                            log.info(f'enrichtextdocuments - Processed docs: {counter.value}')
            except Exception as e:
                error_msg = f'Enrichment call generated an exception: {e}'
                log.error(error_msg)
                raise ImportFailedException(error_msg)

    return enriched_output


def _save_enriched_values_in_ad(annons, enriched_output, typeahead):
    if 'keywords' not in annons:
        annons['keywords'] = {}

    _process_enriched_candidates(annons, enriched_output)
    _process_enriched_candidates_typeahead_terms(annons, enriched_output) if typeahead else None

    return annons


SOURCE_TYPE_OCCUPATION = "occupations"
SOURCE_TYPE_SKILL = "competencies"
SOURCE_TYPE_TRAIT = "traits"
SOURCE_TYPE_LOCATION = "geos"

TARGET_TYPE_OCCUPATION = "occupation"
TARGET_TYPE_SKILL = "skill"
TARGET_TYPE_TRAIT = "trait"
TARGET_TYPE_LOCATION = "location"
TARGET_TYPE_COMPOUND = "compound"


def _process_enriched_candidates(annons, enriched_output):
    enriched_candidates = enriched_output['enriched_candidates']
    fieldname = 'enriched'

    candidate_prop_name = 'concept_label'
    if fieldname not in annons['keywords']:
        annons['keywords'][fieldname] = {}

    enriched_node = annons['keywords'][fieldname]

    enriched_node[TARGET_TYPE_OCCUPATION] = list(set([candidate[candidate_prop_name].lower()
                                                      for candidate in
                                                      _filter_candidates(enriched_candidates, SOURCE_TYPE_OCCUPATION,
                                                                         settings.ENRICH_THRESHOLD_OCCUPATION,
                                                                         annons)]))

    enriched_node[TARGET_TYPE_SKILL] = list(set([candidate[candidate_prop_name].lower()
                                                 for candidate in
                                                 _filter_candidates(enriched_candidates, SOURCE_TYPE_SKILL,
                                                                    settings.ENRICH_THRESHOLD_SKILL, annons)]))

    enriched_node[TARGET_TYPE_TRAIT] = list(set([candidate[candidate_prop_name].lower()
                                                 for candidate in
                                                 _filter_candidates(enriched_candidates, SOURCE_TYPE_TRAIT,
                                                                    settings.ENRICH_THRESHOLD_TRAIT, annons)]))

    enriched_node[TARGET_TYPE_LOCATION] = list(set([candidate[candidate_prop_name].lower()
                                                    for candidate in
                                                    _filter_candidates(enriched_candidates, SOURCE_TYPE_LOCATION,
                                                                       settings.ENRICH_THRESHOLD_GEO, annons)]))

    enriched_node[TARGET_TYPE_COMPOUND] = enriched_node[TARGET_TYPE_OCCUPATION] + \
                                          enriched_node[TARGET_TYPE_SKILL] + \
                                          enriched_node[TARGET_TYPE_LOCATION]


def _is_detected_language_english(annons):
    return DETECTED_LANGUAGE_ENG == jmespath.search('detected_language', annons)


def _process_enriched_candidates_typeahead_terms(annons, enriched_output):
    enriched_candidates = enriched_output['enriched_candidates']
    field_name = 'enriched_typeahead_terms'

    if field_name not in annons['keywords']:
        annons['keywords'][field_name] = {}

    enriched_typeahead_node = annons['keywords'][field_name]

    enriched_typeahead_node[TARGET_TYPE_OCCUPATION] = _filter_valid_typeahead_terms(
        _filter_candidates(enriched_candidates, SOURCE_TYPE_OCCUPATION, settings.ENRICH_THRESHOLD_OCCUPATION, annons))

    enriched_typeahead_node[TARGET_TYPE_SKILL] = _filter_valid_typeahead_terms(
        _filter_candidates(enriched_candidates, SOURCE_TYPE_SKILL, settings.ENRICH_THRESHOLD_SKILL, annons))

    enriched_typeahead_node[TARGET_TYPE_TRAIT] = _filter_valid_typeahead_terms(
        _filter_candidates(enriched_candidates, SOURCE_TYPE_TRAIT, settings.ENRICH_THRESHOLD_TRAIT, annons))

    enriched_typeahead_node[TARGET_TYPE_LOCATION] = _filter_valid_typeahead_terms(
        _filter_candidates(enriched_candidates, SOURCE_TYPE_LOCATION, settings.ENRICH_THRESHOLD_GEO, annons))

    enriched_typeahead_node[TARGET_TYPE_COMPOUND] = enriched_typeahead_node[TARGET_TYPE_OCCUPATION] \
                                                    + enriched_typeahead_node[TARGET_TYPE_SKILL] \
                                                    + enriched_typeahead_node[TARGET_TYPE_LOCATION]


def _filter_valid_typeahead_terms(candidates):
    typeahead_for_type = set()
    for candidate in candidates:
        typeahead_for_type.add(candidate['concept_label'].lower())
        if not candidate['term_misspelled']:
            typeahead_for_type.add(candidate['term'].lower())
    return list(typeahead_for_type)


def _filter_candidates(enriched_candidates, type_name, prediction_threshold, annons):
    if _is_detected_language_english(annons):
        # The enriching only works on ads written in swedish and totally random on ads written in for example english.
        # If we exclude the enriched terms for ads written in english, there is a risk that the english ad won't
        # show up in the search, even though it should.
        candidates = [candidate
                      for candidate in enriched_candidates[type_name]]
        # Remove unwanted english stopwords, like 'do', 'general' etc.
        candidates = [candidate for candidate in candidates if candidate['term'] not in english_stopwords]

        if type_name == SOURCE_TYPE_OCCUPATION or type_name == SOURCE_TYPE_LOCATION:
            # Only keep terms for occupation and location if they were found in the ad headline
            candidates = [candidate for candidate in candidates if candidate['sentence_index'] == 0]
    else:
        candidates = [candidate
                      for candidate in enriched_candidates[type_name]
                      if candidate['prediction'] >= prediction_threshold]

        if type_name == SOURCE_TYPE_OCCUPATION or type_name == SOURCE_TYPE_LOCATION:
            # Check if candidates are in ad headline and add them as enriched
            # even though the prediction value is below threshold.
            candidates = candidates + [candidate
                                       for candidate in enriched_candidates[type_name]
                                       if candidate['sentence_index'] == 0 and candidate not in candidates]

    return candidates
