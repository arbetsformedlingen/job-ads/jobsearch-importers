import logging

"""
The importers are dealing with three ad formats:

LA = The Ledigt Arbete format, with key names in Swedish.
JT = The general JobTech format, which is the format we present our ads with (the contract), and also the format that historical ads are saved in.
NV = The internal Narval format, which is an expansion of the JT format, to make it more searchable, with enrichment, taxonomy expansions etc.
"""

log = logging.getLogger(__name__)


def _str2int_if_needed(item):
    if isinstance(item, str):
        return int(item)
    else:
        return item


def make_list_with_original_value(dic):
    if not isinstance(dic, dict):
        log.error(f"make_list_with_original_value, not dict: {dic}")
        return dic
    dic['original_value'] = True
    return [dic]


def add_original_values_in_list(lis):
    if not isinstance(lis, list):
        log.error(f"add_original_values_in_list, not list: {lis}")
        return lis
    for dic in lis:
        dic['original_value'] = True





