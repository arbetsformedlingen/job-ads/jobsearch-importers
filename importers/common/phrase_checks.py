from importers.phrases import REMOTE_PHRASES, OPEN_FOR_ALL_PHRASES, TRAINEE_PHRASES, LARLING_PHRASES, FRANCHISE_PHRASES, \
    HIRE_WORKPLACE_PHRASES


def phrase_check(desc, title, list_of_phrases):
    """
    returns True if any of the phrases is found in the ad's title, description.text or description.formatted,
    False otherwise
    """
    text_to_check = f"{desc} {title}".lower()
    return any(x in text_to_check for x in make_phrases_lowercase(list_of_phrases))


def all_phrase_checks(ad, desc, headline):
    ad['remote_work'] = phrase_check(desc, headline, REMOTE_PHRASES)
    ad['open_for_all'] = phrase_check(desc, headline, OPEN_FOR_ALL_PHRASES)
    ad['trainee'] = phrase_check(desc, headline, TRAINEE_PHRASES)
    ad['larling'] = phrase_check(desc, headline, LARLING_PHRASES)
    ad['franchise'] = phrase_check(desc, headline, FRANCHISE_PHRASES)
    ad['hire_work_place'] = phrase_check(desc, headline, HIRE_WORKPLACE_PHRASES)
    return ad


def make_phrases_lowercase(list_of_phrases):
    """
    returns a list with all strings converted to lowercase
    """
    return [y.lower() for y in list_of_phrases]
