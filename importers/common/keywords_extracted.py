import logging
import re

log = logging.getLogger(__name__)


def add_keywords_extracted(annons):
    if 'keywords' not in annons:
        annons['keywords'] = {'extracted': {}}
    for key_dict in [
        {
            'occupation': [
                'occupation.label',
                'occupation_group.label',
                'occupation_field.label',
            ]
        },
        {
            'skill': [
                'must_have.skills.label',
                'must_have.languages.label',
                'nice_to_have.skills.label',
                'nice_to_have.languages.label',
            ]
        },
        {
            'location': [
                'workplace_address.city',
                'workplace_address.municipality',
                'workplace_address.region',
                'workplace_address.country',
            ]
        },
        {
            'employer': [
                'employer.name',
                'employer.workplace'
            ]
        }
    ]:
        field = list(key_dict.keys())[0]
        keywords = set()
        values = []
        for key in list(key_dict.values())[0]:
            values += _get_nested_value(key, annons)
        if field == 'employer':
            for value in _create_employer_name_keywords(values):
                keywords.add(value)
        elif field == 'location':
            for value in values:
                trimmed = _trim_location(value)
                if trimmed:
                    keywords.add(trimmed)
        else:
            for value in values:
                for kw in _extract_taxonomy_label(value):
                    keywords.add(kw)
        annons['keywords']['extracted'][field] = list(keywords)
    return annons


def _create_employer_name_keywords(companynames):
    names = []
    for companyname in companynames or []:
        converted_name = companyname.lower().strip()
        converted_name = __right_replace(converted_name, ' ab', '')
        converted_name = __left_replace(converted_name, 'ab ', '')
        names.append(converted_name)

    if names:
        names.sort(key=lambda s: len(s))
        shortest = len(names[0])
        uniques = [names[0]]
        for i in range(1, len(names)):
            if names[i][0:shortest] != names[0] and names[i]:
                uniques.append(names[i])

        return uniques

    return []


def __right_replace(astring, pattern, sub):
    return sub.join(astring.rsplit(pattern, 1))


def __left_replace(astring, pattern, sub):
    return sub.join(astring.split(pattern, 1))


def _trim_location(locationstring):
    # Look for unwanted words (see tests/unit/test_converter.py)
    pattern = '[0-9\\-]+|.+,|([\\d\\w]+\\-[\\d]+)|\\(.*|.*\\)|\\(\\)|\\w*\\d+\\w*'
    regex = re.compile(pattern)
    stopwords = ['box']
    if locationstring:
        # Magic regex
        valid_words = []
        for word in locationstring.lower().split():
            if word and not re.match(regex, word) and word not in stopwords:
                valid_words.append(word)
        return ' '.join(valid_words)
    return locationstring


def _get_nested_value(path, data):
    key_path = path.split('.')
    values = []
    for i in range(len(key_path)):
        element = data.get(key_path[i])
        if isinstance(element, str):
            values.append(element)
            break
        if isinstance(element, list):
            for item in element:
                if item:
                    values.append(item.get(key_path[i + 1]))
            break
        if isinstance(element, dict):
            data = element
    return values


def _extract_taxonomy_label(label):
    if not label:
        return []
    try:
        label = label.replace('m.fl.', '').strip()
        if '-' in label:
            return [word.lower() for word in re.split(r'/', label)]
        else:
            return [word.lower().strip() for word in re.split(r'/|, | och ', label)]
    except AttributeError:
        log.warning(f'(extract_taxonomy_label) extract fail for: {label}')
    return []
