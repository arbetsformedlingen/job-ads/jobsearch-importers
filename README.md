# Jobsearch-importers
Imports job ads and taxonomy values into Elasticsearch.
Creates console script entry points to run with different intervals, usually via cron to load data into elastic.

## Python version
    Python 3.10.5

## Installation

Ensure to setup so you can download packages from https://pypi.jobtechdev.se/repository/pypi-jobtech/simple. See
[JobTech common documentation](https://gitlab.com/arbetsformedlingen/collaboration/documentation/-/blob/main/engineering/plattform/python-packages.md#ladda-ner-paket)
for details.

Install the required dependencies by

    $ python -m pip install -r requirements.txt

and install the import scripts by typing 

    $ python setup.py 
    $ python setup.py develop (for development) 

This creates symlinked versions of the scripts to enable development without having to run setup for every change.


## Configuration
The application is entirely configured using environment variables. 

|Environment variable   | Default value  | Comment |Used by system|
|---|---|---|---|
| ES_HOST  | localhost  | Elasticsearch host | all |
| ES_PORT  | 9200  | Elasticsearch port | all |
| ES_USER  |   | Elasticsearch username | all |
| ES_PWD   |   | Elasticsearch password | all |
| ES_TAX_INDEX_ALIAS  |  taxonomy | Alias for index that is the current version of the taxonomy |import-taxonomy|
| TAXONOMY_VERSION  |  taxonomy | The version of taxonomy to be imported (0 for latest) |import-taxonomy|
| ES_ANNONS_INDEX | platsannons | Base index name for job ads |import-platsannonser, import-platsannonser-daily|
| LA_FEED_URL | http://localhost:5000/sokningar/andradeannonser/ | REST feed API for changes in job ads | import-platsannonser, import-platsannonser-daily |
| LA_BOOTSTRAP_FEED_URL | http://localhost:5000/sokningar/publiceradeannonser| REST feed API for all currently available job ads | import-platsannonser-daily |
| LA_DETAILS_URL | http://localhost:5000/annonser/ | REST feed API job ad details (i.e. the entire job ad) | import-platsannonser, import-platsannonser-daily |
| LA_DETAILS_PARALLELISM | 8 | Limits how many simultaneous threads are run for loading ad details | import-platsannonser, import-platsannonser-daily |
| COMPANY_LOGO_BASE_URL | https://www.arbetsformedlingen.se/rest/arbetsgivare/rest/af/v3/ | Endpoint to check for available company logo associated with job ad | import-platsannonser, import-platsannonser-daily|
| URL_ENRICH_TEXTDOCS_SERVICE | ask from jobtech development | Endpoint for ML enrichment of job ads |import-platsannonser, import-platsannonser-daily, convert-historical |
| API_KEY_ENRICH_TEXTDOCS | | API key to use for enrichment | import-platsannonser, import-platsannonser-daily, convert-historical |
| HISTORICAL_PREFIX | historical | Prefix for the historical (yearly) Elastic indices | convert-historical | 
| HISTORICAL_SUFFIX | | Suffix for the historical (yearly) Elastic indices | convert-historical |  
| HISTORICAL_CHUNK_SIZE | 200000 | Defines how many ads are kept in memory simultaneously during conversion | convert-historical | 
| HISTORICAL_ALIAS | platsannons-historical | Alias grouping together all the historical (yearly) Elastic indices | import-historical |


## Console scripts
All scripts uses the following environments variables which must be set for the scripts to work (se table above for more information):

    - ES_HOST
    - ES_PORT
    - ES_USER
    - ES_PWD


### import-taxonomy
Dumps taxonomy data, from source, into elasticsearch. Make sure that no previous alias with the name 'taxonomy' exists which can cause conflicts.

#### Usage
set environment variable 'TAXONOMY_VERSION' to desired version, or 0 for latest version
    
    $ import-taxonomy <version>
    
### import-platsannonser
Imports updated, new and removed job ads from a REST endpoint at Arbetsförmedlingen.

If the configured index does not exists it is created and two aliases are created, with the suffixes "-read", and "-write" (e.g. platsannons-write) once import is completed.
The "-read" alias is used by default by the JobSearch API.

#### Usage
    
    $ import-platsannonser
    
Starts an import into the current "-write" alias if it exists. Otherwise, a new is created as stated above.
This script is typically run every few minutes by cron.

### import-platsannoser-daily

    $ import-platsannonser-daily
    
Creates a new index for platsannonser according to the configured ```ES_ANNONS_INDEX``` environment variable, with todays date and hour as a suffix, and starts a full load into that index.
After successful import, new "-read" and "-write" aliases are pointed to the new index.
This is run every night to create a fresh index and make sure no ads are missed by ```import-platsannonser```.

### historical
An CLI program that provides tools for operate e.g. import, convert current ads to historical or enrich ads from given source. The command historical has the following subcommands:

    $ convert1
    $ elastic
    $ enrich
    $ extract_metadata
    $ json_to_jsonline
    $ refrsh_alias
    $ refresh_alias_with_monthly_indices
    $ sample
    $ write_ads_to_monthly_index

Each subcommand have a help text which is invoked by calling

    $ historical <subcommand> --help

#### convert1
Reads JSON files with archived historical ads (typically named 2006.json etc.). Converts to internal Elastic index format including JAE.  Writes result as JSON files (with names such as historical-2006.json). These files are ready to be loaded into Elastic.

##### usage
    historical command <flags> [FILENAMES] ... 
##### flags
    -o, --output_file=OUTPUT_FILE (required)


#### elastic
Reads ads from JSON files created by convert-historical and load it into Elastic. 
##### usage
    historical elastic FILENAME <flags>
##### flags
    -b, --batch_size=BATCH_SIZE, Type: int, Default: 2000

#### enrich
Reads a json file, enrich it's contents and outputs the result to a file in json format
##### usage
    historical enrich INFILE OUTFILE

#### extract_metadata
Extracts meta data from historical json file and outputs it to a file 
##### usage
    historical extract_metadata INFILE OUTFILE

#### json_to_jsonline
Converts a file in json format to jsonline format. Writes result to file. Note: No helptext is provided by --help
##### usage
    historical json_to_jsonline INFILE OUTFILE

#### refresh_alias
Loopes through all indeces with a predefined pattern and add them to the historical alias
##### usage
    historical refresh_alias [YEAR]
##### flags

#### refresh_alias_with_monthly_indices
##### usage
    historical refresh_alias_with_monthly_indices [MONTH]
##### flags

#### Sample
Reads a file with ads and outputs a part of them. Default is 1% of the input file
##### usage
    historical sample [INPUTFILE] [OUTOUTFILE] <flags>
##### flags
      -p, --percent=PERCENT, default: 1

#### write_ads_to_monthly_index
##### usage
##### flags

<br />
<br />
*Read more about historical importing in docs/historical_importer.md*

## Test

## Run unit tests
    $ python3 -m pytest -svv -ra -m unit tests/
    
### Run integrations tests    
When running integration tests, the system needs access to other services so you need to specify environment variables in order for it to run properly.

    $ python3 -m pytest -svv -ra -m integration tests/
