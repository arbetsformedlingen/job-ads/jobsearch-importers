FROM docker.io/library/python:3.10.15-slim-bookworm

ENV TZ=Europe/Stockholm

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY . /app
COPY importers/cert/* /CERT/

WORKDIR /app

# Install git
RUN apt-get -y update && \
    apt-get install -y git

# runs unit tests with @pytest.mark.unit annotation only
RUN python -m pip install --upgrade setuptools wheel pip && \
    python -m pip install -r requirements.txt && \
    find tests -type d -name __pycache__ -prune -exec rm -rf -vf {} \; && \
    python setup.py install && \
    python -m pytest tests/unit_tests

RUN rm -fr /app/*
